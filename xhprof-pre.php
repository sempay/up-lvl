<?php
global $_beget_microtime_0;
global $_beget_microtime_1;

function _beget_xhprof() {
	global $_beget_microtime_0;
	global $_beget_microtime_1;
  $SITE_PATH=dirname(__FILE__);
  $LOG_FILE=$SITE_PATH . "/xhprof.log";

	$request_time_0 = microtime(true);

	$time_total_0 = round(($request_time_0 - $_beget_microtime_1) * 1000, 0);
	$time_total_1 = round(($request_time_0 - $_beget_microtime_0) * 1000, 0);
	$time_xhprof_0 = round(($_beget_microtime_1 - $_beget_microtime_0) * 1000, 0);

	$xhprof_data = xhprof_disable();

	$XHPROF_ROOT = $SITE_PATH . "/xhprof-0.9.4";
	include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_lib.php";
	include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_runs.php";

	$xhprof_runs = new XHProfRuns_Default();
	$run_id = $xhprof_runs->save_run($xhprof_data, "xhprof_beget");

	$request_time_1 = microtime(true);

	$time_xhprof_1 = round(($request_time_1 - $request_time_0) * 1000, 0);
	$time_total_2  = round(($request_time_1 - $_beget_microtime_0) * 1000, 0);

	file_put_contents($LOG_FILE, "{$_SERVER['SERVER_NAME']} {$_SERVER['REMOTE_ADDR']} \"{$_SERVER['REQUEST_METHOD']} {$_SERVER['REQUEST_URI']}\" \"{$_SERVER['SERVER_NAME']}/xhprof-0.9.4/xhprof_html/index.php?run={$run_id}&source=xhprof_beget\" t0:{$time_total_0}ms t1:{$time_total_1}ms t2:{$time_total_2}ms x0:{$time_xhprof_0}ms x1:{$time_xhprof_1}ms\n", FILE_APPEND);
}

if (!preg_match("/xhprof/", $_SERVER['REQUEST_URI'])) {
	$_beget_microtime_0 = microtime(true);
        xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
	register_shutdown_function('_beget_xhprof');
	$_beget_microtime_1 = microtime(true);
}

