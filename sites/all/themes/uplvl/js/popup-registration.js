(function ($) {
  Drupal.behaviors.dropWindowPopup = {
    attach: function() {
      $('.link-open').once('dropWindowPopup', function () {
        $(this).bind('click', function (e) {
          e.preventDefault();
          $('#popup1').show();
        });
      });
      $('.link-close').once('dropWindowPopup', function () {
        $(this).bind('click', function (e) {
          e.preventDefault();
          $('#popup1').hide();
        });
      });
    }
  };
})(jQuery);