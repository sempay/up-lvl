(function ($, Drupal, w, d, undefined) {

  "use strict";

  Drupal.behaviors.uplvlFormLabels = {
    attach: function() {
      $('.process-uplvl-labels').once('uplvlFormLabels', function () {
        var id, $field;
        $(this).find('.form-item').each(function () {
          id = $(this).find('label').attr('for');
          if (id) {
            $field = $('#' + id);
            if (!$field.length) {
              return;
            }
            if ($field.prop('tagName') === 'SELECT' || $field.attr('type') == 'checkbox') {
              return;
            }
            if ($field.val().length) {
              $(this).find('label').hide();
            }
            $field.bind('focus', function () {
              $(this).parents('.form-item').find('label').hide();
            }).bind('blur', function () {
              if (!$(this).val().length) {
                $(this).parents('.form-item').find('label').show();
              }
            });
          }
          $field = null;
        });
      });
    }
  };

  Drupal.behaviors.uplvlFileField = {
    attach: function() {
      $('.uplvl-file-field').find('input[type="file"]').once('uplvlFileField', function () {
        var $field = $(this),
            $button = $('<span />', {
              class: 'btn btn-default btn-file uplvl-file-field-btn'
            }).html(Drupal.t('Browse...')),
            $textField = $('<input />', {
              type: 'text',
              class: 'form-control',
              readonly: 'readonly'
            }),
            $group = $('<div />', {
              class: 'input-group-btn'
            }).append($button);
        $button.bind('click', function () {
          $field.trigger('click');
        });
        $field.after($group).hide();
        $group.after($textField);
        $field.bind('change', function () {
            var $input = $(this),
                numFiles = $input.get(0).files ? $input.get(0).files.length : 1,
                label = $input.val().replace(/\\/g, '/').replace(/.*\//, '');
            if (numFiles == 1) {
              $input.next('.input-group-btn').next('input').val(label);
            }
            else if (numFiles > 1) {
              $input.next('.input-group-btn').next('input').val(Drupal.t('@count files selected', {
                '@count': numFiles
              }));
            }
            else {
              $input.next('.input-group-btn').next('input').val('');
            }
        });
      });
    }
  };

  Drupal.behaviors.uplvlResponsiveMenu = {
    attach: function() {
      $('#header-menu').once('uplvlResponsiveMenu', function () {
        var rebuild = function() {
          var width = {
            logo: $('#logo-private-pages').outerWidth(true),
            right: $('.mini-profile').outerWidth(true),
            doc: $('header').outerWidth(true),
            menu: $('#header-menu').outerWidth(true),
          };
          width.cont = width.doc - (width.logo + width.right);


          var normal = width.menu <= width.cont,
              i = 0,
              attached = $('#header-menu').find('.menu-dropdown-arrow').length;

          var $arrow = $('#header-menu').find('.menu-dropdown-arrow');

          while (normal && $arrow.find('li').length) {
            $arrow.find('li').last().detach().appendTo($('#header-menu ul.menu'));
            width.menu = $('#header-menu').outerWidth(true);
            normal     = width.menu <= width.cont;

            i++;
            if (i > 15) {
              break;
            }
          }

          i = 0;

          while (!normal) {
            if (!attached) {
              $arrow = $('<div />', {
                class: 'menu-dropdown-arrow'
              });
              $('#header-menu').append($arrow);
              attached = true;
            }
            else {
              $arrow = $('#header-menu').find('.menu-dropdown-arrow');
              attached = $arrow.length;
            }

            $('#header-menu ul.menu').find('li').last().detach().appendTo($arrow);
            width.menu = $('#header-menu').outerWidth(true);
            normal     = width.menu <= width.cont;
            i++;
            if (i > 15) {
              break;
            }
          }

          if (!$arrow.find('li').length) {
            $arrow.hide();
          }
          else {
            $arrow.show();
          }
        };
        rebuild();
        $(window).bind('resize', function () {
          rebuild();
        });
        $(window).bind('load', function () {
          rebuild();
        });
      });
    }
  };

  Drupal.behaviors.uplvlReadMore = {
    attach: function() {
      setTimeout(function() {
      $('#block-system-main .course-content .course-description').readmore({
        moreLink: '<a href="#">Читать полностью</a>',
        lessLink: '<a href="#">свернуть</a>',
        maxHeight: 220,
        afterToggle: function(trigger, element, more) {
          if(! more) { // The "Close" link was clicked
            $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
          }
        }
      });
    }, 100);
    }
  };

  Drupal.behaviors.uplvlShowBlock = {
    attach: function() {
      $('#lesson-node-form').once('uplvlShowBlock', function(){
        var moreLink = $('#lesson_descr'),
            lesson_descr_content = $('#body-add-more-wrapper');
        lesson_descr_content.hide();
        moreLink.bind('click', function(event) {
          event.preventDefault();
          $(this).toggleClass('icon-show icon-hide');
          lesson_descr_content.slideToggle('slow');
        });
    });
    }
  };

  Drupal.behaviors.uplvlAddEditLessons = {
    attach: function() {
      $('.course-lessons #block-with-lessons .lesson-number').live('click', function(){
        $('.course-lessons #block-with-lessons .lesson-number').removeClass('highlight');
        $('.course-lessons #block-with-lessons .lesson-title-tasks .lesson-edit a').removeClass('edit-show');
        $( this ).addClass("highlight");
        $( this ).parent('.less').children('.lesson-title-tasks').children('.lesson-edit').children('a').addClass("edit-show");
      });

      $('.course-lessons #block-with-lessons .lesson-title').live('click', function(){
        $('.course-lessons #block-with-lessons .lesson-number').removeClass('highlight');
        $('.course-lessons #block-with-lessons .lesson-title-tasks .lesson-edit a').removeClass('edit-show');
        $( this ).parent('.lesson-title-tasks').parent('.less').children('.lesson-number').addClass("highlight");
        $( this ).parent('.lesson-title-tasks').children('.lesson-edit').children('a').addClass("edit-show");
      });

      $('.page-library #block-system-main .s_number').live('click', function(){
        $('.page-library #block-system-main .s_number').removeClass('highlight');
        $('.page-library #block-system-main .section-short-inform .section-edit a').removeClass('edit-show');
        $( this ).addClass("highlight");
        $( this ).parent('.uplvl-section').children('.section-short-inform').children('.section-edit').children('a').addClass("edit-show");
      });

      $('.page-library #block-system-main .s_name').live('click', function(){
        $('.page-library #block-system-main .s_number').removeClass('highlight');
        $( this ).parent('.section-short-inform').parent('.uplvl-section').children('.s_number').addClass("highlight");
        $( this ).parent('.section-short-inform').find('.section-edit a').addClass("edit-show");
      });
    }
  };

  Drupal.behaviors.uplvlCheckCourseCheckLesson = {
    attach: function() {
      $('.course-lessons-check #block-with-lessons .check-lessons a').bind('click', function(){
        $('.course-lessons-check #block-with-lessons .lesson-number').removeClass('highlight');
        $( this ).parents('.less').find('.lesson-number').addClass("highlight");
      });
    }
  };

  Drupal.behaviors.uplvlCheckCourseCheckTask = {
    attach: function() {
      $('.course-lessons-check #block-with-lessons #courator-check-task-pupils .task-number a').bind('click', function(){
        $('.course-lessons-check #block-with-lessons #courator-check-task-pupils .task-number').removeClass('highlight');
        $( this ).parent('.task-number').addClass("highlight");
      });
    }
  };

  Drupal.behaviors.uplvlCheckCourseChangeChekForm = {
    attach: function() {
      $('.change-check-form').click('click', function(){
        $('.check-form').addClass('active-check-form');
        $( this ).parents('.check-form').removeClass("active-check-form");
      });
    }
  };

  Drupal.behaviors.uplvlRemovePopup = {
      attach: function () {
        $('.tooltip.fade.in').remove()
      }
  };

  Drupal.behaviors.uplvlscrollTop = {
    attach: function() {
      $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
          $('#toTop').fadeIn();
        } else {
          $('#toTop').fadeOut();
        }
      });
      $('#toTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
      });
    }
  };

  Drupal.behaviors.uplvlSelectTaskType = {
    attach: function() {
      $('#lesson-node-form label.select-type').once('uplvlSelectTaskType', function () {
        $(this)
          .bind('click', function () {
            var $form = $(this).parents('form'),
                taskid = $(this).data('taskid'),
                val = $(this).data('val');
            $form
              .find('.form-radio[id^="edit-lessons-tasks-lessons-tasks-form-' + taskid + '-lessons-tasks-type-switch-select-task-type-radio-' + val + '"]')
              .prop('checked', true);
          });
      });
    }
  };

  Drupal.behaviors.sidebarToggle = {
    attach: function() {
      $('aside#sidebar').once('sidebarToggle', function () {
        var $btn = $('<div />', {
          class: 'sidebar-toggle navbar-toggle'
        }).html('<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>'),
            $sidebar = $(this);
        if ($.cookie('sidebarToggleClosed') === 'true') {
          $sidebar.addClass('closed').addClass('no-animate');
          $btn.addClass('closed');
          setTimeout(function () {
            $sidebar.removeClass('no-animate');
          }, 400);
        }
        $btn.bind('click', function () {
          if (!$sidebar.hasClass('closed')) {
            $.cookie('sidebarToggleClosed', true, {
              path: Drupal.settings.basePath
            });
            $btn.addClass('closed');
          }
          else {
            $.cookie('sidebarToggleClosed', false, {
              path: Drupal.settings.basePath
            });
            $btn.removeClass('closed');
          }
          $sidebar.toggleClass('closed');
        });
        $('#logo-private-pages').after($btn);
      });
    }
  };

  /*Drupal.behaviors.uplvlMultiselect = {
    attach: function() {

    $('.field-name-field-bonuse-reference select').addClass("chosen").attr("multiple", 'true');
    $('#edit-field-bonuse-reference-und').chosen();

    $('#edit-field-library-course-und').addClass("chosen").attr("multiple", 'true');
    $('#edit-field-library-course-und').chosen();

    $("#user-profile-form").each(function() {
      $('#edit-field-cat-ob-progr-und').chosen();
    });
  }
};*/

  Drupal.behaviors.tableResultsCuratorFormLabel = {
    attach: function() {
      $('#table-results-courator-form, #table-course-rules-form, form[id*="results-pupil-form"]')
        .find('.select-all .form-checkbox')
        .once('tableResultsCuratorFormLabel', function () {
          var id = Math.random().toString().split('.')[1];

          $(this).attr('id', id);

          $('<label />', {
            for: id
          }).insertAfter(this);
        });
    }
  };

  Drupal.behaviors.lessonsForCheckSlider = {
    attach: function() {
      $('.bxslider-custom').once('lessonsForCheckSlider', function () {
        var $ul = $(this);
        $(this).bxSlider({
          adaptiveHeight: true,
          minSlides: 1,
          maxSlides: 4,
          moveSlides: 1,
          pager: false,
          slideWidth: 50,
          onSliderLoad: function() {
            $ul
              .find('a')
              .addClass('use-ajax');
            Drupal.attachBehaviors();
          }
        });

      });
    }
  };

  Drupal.behaviors.sliderWithoutAjax = {
    attach: function() {
      $('.bxslider-custom-wa').once('sliderWithoutAjax', function () {
        $(this).bxSlider({
          adaptiveHeight: true,
          minSlides: 1,
          maxSlides: 4,
          moveSlides: 1,
          pager: false,
          slideWidth: 50
        });
      });
    }
  };

  Drupal.behaviors.switchCheckTask = {
    attach: function() {
      $('.bxslider-custom').once('switchCheckTask', function(){
        $(this).find('a').bind('click', function() {
          var $self = $(this),
              $sliderContent = $('.bxslider-custom li');
          $sliderContent.each(function(i, el) {
            $(el).find('a').removeClass('active');
            console.log(el);
          });
          $self.addClass('active');
        });
      });
    }
  };

  Drupal.behaviors.uplvlLessonTabs = {
    attach: function() {
      $('#lesson-attach-content').once('uplvlLessonTabs', function () {
        $('#to-presentation, #to-task').hide();
        $('#lesson-attach-content').find('#lesson-attach-tabs').find('a[href="#to-video"]').addClass('selected');

        $(this).find('#lesson-attach-tabs').find('a').bind('click', function (e) {
          e.preventDefault();
          var id = $(this).attr('href').substr(1);

          $('#to-video, #to-presentation, #to-task').hide();
          $('#' + id).show();
          $('#lesson-attach-content').find('#lesson-attach-tabs').find('a').removeClass('selected');
          $(this).addClass('selected');

          return false;
        });
      });

      $('#video-switcher-tabs').once('uplvlLessonTabs', function () {
        var id;
        $(this).find('a').bind('click', function (e) {
          e.preventDefault();

          $('#video-switcher-tabs').find('a').each(function () {
            id = $(this).attr('href').substr(1);
            $('#' + id).hide();
            $(this).removeClass('selected');
          });

          id = $(this).attr('href').substr(1);
          $(this).addClass('selected');
          $('#' + id).show();

          return false;
        }).each(function () {
          id = $(this).attr('href').substr(1);

          $('#' + id).hide();
        }).first().trigger('click');
      });



      $('#presentation-switcher-tabs').once('uplvlLessonTabs', function () {
        var id;
        $(this).find('a').bind('click', function (e) {
          e.preventDefault();

          $('#presentation-switcher-tabs').find('a').each(function () {
            id = $(this).attr('href').substr(1);
            $('#' + id).hide();
            $(this).removeClass('selected');
          });

          id = $(this).attr('href').substr(1);
          $(this).addClass('selected');
          $('#' + id).show();

          return false;
        }).each(function () {
          id = $(this).attr('href').substr(1);

          $('#' + id).hide();
        }).first().trigger('click');
      });



      $('#task-switcher-tabs').once('uplvlLessonTabs', function () {
        var id;
        $(this).find('a').bind('click', function (e) {
          e.preventDefault();

          $('#task-switcher-tabs').find('a').each(function () {
            id = $(this).attr('href').substr(1);
            $('#' + id).hide();
            $(this).removeClass('selected');
          });

          id = $(this).attr('href').substr(1);
          $(this).addClass('selected');
          $('#' + id).show();

          return false;
        }).each(function () {
          id = $(this).attr('href').substr(1);

          $('#' + id).hide();
        }).first().trigger('click');
      });
    }
  };

  Drupal.behaviors.taskTimer = {
    attach: function() {
      $('.task-timer').once('taskTimer', function () {
        TaskTimer('.task-timer');
      });

      function TaskTimer (selector) {
        var time = parseInt($(selector).attr('time'));
        $(selector).html(TimerList(time));

        var t = parseInt($(selector).attr('time'));

        window.attctffs = setInterval(function() {
          t--;
          $(selector).html(TimerList(t));
          if (t <= 0) {
            clearInterval(window.attctffs);
            var taskID = $(selector).attr('taskid');
            var redirectPath = window.location.href + '?task='+taskID+'&false='+ false;
            window.location.replace(redirectPath);
          }
        }, 1000);
      }

      function TimerList (time) {
        var timeSec = time%60;
        var timeMin = (time-timeSec)/60;
        if (timeMin >= 60) {
          var timeMin = timeMin%60;
          var timeHour = 1;
          var timeHTML = '<div id="task-timer"><div class="timer-hour">' + timeHour + '</div> : <div class="timer-minutes">' + timeMin + '</div> : <div class="timer-seconds">' + timeSec + '</div></div>';

        } else {
          var timeHTML = '<div id="task-timer"><div class="timer-minutes">' + timeMin + '</div> : <div class="timer-seconds">' + timeSec + '</div></div>';
        }
        return timeHTML;
      }
    }
  };

  Drupal.behaviors.ditoolsiAdapriveContent = {
    attach: function() {
      var timeout = setTimeout(function () {

      }, 500);
      $('#main').once('ditoolsiAdapriveContent', function () {
        function resize() {
          var windowHeight = $(window).height(),
              otherHeight  = $('#admin-menu').outerHeight(true) +
                $('#page-header').outerHeight(true) +
                $('#footer').outerHeight(true) +
                $('#main').children('.page-title').outerHeight(true);

          // clearTimeout(timeout);
          // timeout = setTimeout(function () {
            $('#main')
              .children('.page-title')
              .next('.content')
              .css({
                minHeight: windowHeight - otherHeight - 53
              });
          // }, 200);
        }

        $(window).bind('load resize', resize);
      });
    }
  };

  Drupal.behaviors.uplvlSwitchPupil = {
    attach: function() {
      $('.select-pupil-lesson-switch').on('change', function (e) {

          var optionSelected = $("option:selected", this);
          var valueSelected = this.value;
          var uI = $(this).attr('id');

          var pathname = window.location.pathname.split("/");
          var linkCourse = pathname[pathname.length-1];

          var json_param = {};
          json_param['cID'] = linkCourse;
          json_param['uID'] = uI;
          json_param['lesson_switch'] = valueSelected;

          $.ajax({
              type: 'POST',
              url: 'http://' + window.location.hostname + '/courator/pupil-switch',
              data: json_param,
              dataType: 'json',
              async: false,
              success: function(data){
                $('#table-pupil-search-results #table-content').empty();
                $('#table-pupil-search-results #table-content').append(data.status);
              }
          });
        });
    }
  };

  Drupal.behaviors.uplvlYoutubeVideo = {
    attach: function() {
      $('a.training-video-link').once('uplvlYoutubeVideo', function () {
        $(this).bind('click', function (e) {
          e.preventDefault();
          var mrgLeft = $(document).outerWidth(true) / 4;
          var $overlay = $('<div />', {
                id: 'ditoolsi-overlay'
              }),
              $videoContainer = $('<div />', {
                id: 'training-video',
                style: 'margin-left: -' + mrgLeft + 'px; height: ' + mrgLeft + 'px',
              }),
              $close = $('<a />', {
                class: 'close-popup'
              }).html('<img class="close" src="/sites/all/themes/uplvl/css/images/gtk-close.png">').bind('click', function () {
                $('#ditoolsi-overlay, #training-video').remove();
              }).appendTo($videoContainer),
              $video = $('<iframe />', {
                src: 'http://www.youtube.com/embed/' + $(this).data('v'),
                allowFullScreen: 'allowFullScreen'
              });
          $('body').append($videoContainer.append($video)).append($overlay);
        });
      });
    }
  };

})(jQuery, Drupal, window, document, undefined);
