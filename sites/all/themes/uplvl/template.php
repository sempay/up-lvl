<?php

/**
 * @file
 * template.php
 */

/**
 * Implements template_preprocess_page().
 */
function uplvl_preprocess_page(&$vars) {
  global $user;

  // if (isset($vars['node']->type) && $vars['node']->type == 'course') {
  //   $vars['theme_hook_suggestions'] = array('page__private');
  //   drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/private.css');
  //   //Add css for node with type 'course'
  // }

  $private_pages = array(
    // 'user/*',
    // 'add-lessons/*',
    // 'edit-task/*',
    // 'edit-course/*',
    // 'curator-list/*',
    // 'curator-add/*',
    // 'add-students/*',
    // 'pupil-course/*',
    // 'courator/check-course/*',
    // 'my-courses',
    // 'my-courses/*',
    // 'node/add/course',
    // 'library/*',
    // 'node/add/library',
    // 'node/add/section',
    // 'node/add/bonus',
    // 'library/*',
    // 'library',
    // 'bonus',
    // 'bonus/*',
    // 'node/add/library',
    // 'library/',
    // 'section/*/edit',
    // 'pupil-list/*',
    'user/*' => array(
      '/css/private.css',
    ),
    'user/*/edit' => array(
      '/css/user-profile.css',
    ),
     'user/registration' => array(
     ),
    'add-lessons/*' => array(
      '/css/lessons.css',
    ),
    'curator/delete-lesson/*' => array(
      '/css/lessons.css',
    ),
    'edit-task/*' => array(
      '/css/lessons.css',
    ),
    'edit-course/*' => array(
      '/css/page_add_courses.css',
    ),
    'curator-list/*' => array(
    ),
    'curator-add/*' => array(
      '/css/add-user.css',
    ),
    'courator/view-history/*' => array(
      '/css/course.css',
    ),
    'courator/view-lesson-history/*' => array(
      '/css/course.css',
    ),
    'add-students/*' => array(
      '/css/add-user.css',
    ),
    'curator/check-course/*' => array(
      '/css/home-work.css',
    ),
    'courator/check-lesson/*' => array(
      '/css/home-work.css',
    ),
    'courator/check-task/*' => array(
      '/css/home-work.css',
    ),
    'pupil-list/*' => array(
      '/css/add-user.css',
    ),
    'my-courses' => array(
      '/css/course-list.css',
    ),
    'my-courses/*' => array(
      '/css/course.css',
    ),
    'node/add/course' => array(
      '/css/page_add_courses.css',
    ),
    'library' => array(
      '/css/library-course.css',
    ),
    'library/*' => array(
      '/css/library-course.css',
    ),
    'node/add/library' => array(
      '/css/library-course.css',
    ),
    'node/add/section' => array(
      '/css/library-course.css',
    ),
    'section/*/edit' => array(
      '/css/library-course.css',
    ),
    'section/*/delete' => array(
      '/css/library-course.css',
    ),
    'add-section/*' => array(
      '/css/library-course.css',
    ),
    'bonus' => array(
      '/css/bonus-course.css',
    ),
    'bonus/*' => array(
      '/css/bonus-course.css',
    ),
    'node/add/bonus' => array(
      '/css/bonus-course.css',
    ),
    'pupil-course/*' => array(
      '/css/course.css',
    ),
    'project-team/*' => array(
      '/css/course.css',
    ),
    'pupil-lesson/*' => array(
      '/css/answer-task.css',
    ),
    'curator/send-messages/*' => array(
      '/css/send-message.css',
    ),
    'user' => array(
      '/css/send-message.css',
    ),
    'statistic/*' => array(
      '/css/statistic.css',
    ),
    'curator/tendency/*' => array(
      '/css/curator-tendency.css',
    ),
    'im' => array(
      '/css/my-message.css',
    ),
    'im/*' => array(
      '/css/my-message.css',
    ),
    'messages/*' => array(
      '/css/my-message.css',
    ),
   'messages/new/*' => array(
      '/css/my-message.css',
    ),
   'curator/delete-lesson/*' => array(
      '/css/lessons.css',
    ),
  );

  $exclude = array(
    'user/login',
    // 'user/register',
    'user',
  );

  foreach ($private_pages as $pageKey => $pageCSS) {
    if (drupal_match_path(current_path(), $pageKey)
      && !drupal_match_path(current_path(), implode(PHP_EOL, $exclude))) {
      $vars['header_link'] = '';
      $vars['theme_hook_suggestions'] = array('page__private');
      drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style.css');
      foreach ($pageCSS as $style) {
        drupal_add_css(drupal_get_path('theme', 'uplvl') . $style);
      }
    }
  }
  if (arg(0) == 'user' && is_numeric(arg(1))) {
    $account = user_load(arg(1));
  }
  else {
    // Load user object for get all fields in some cases.
    $account = user_load($user->uid);
  }

  $variables = array(
    'account'      => $account,
    'picture'      => new stdClass(),
    'user_fio'     => '',
  );

  if (($field_user_picture = field_get_items('user', $account, 'field_user_image'))
    && !empty($field_user_picture[0]['fid'])) {
    $variables['picture'] = file_load($field_user_picture[0]['fid']);
  }
  $info = field_info_instance('user', 'field_user_image', 'user');
  if (empty($variables['picture']->uri) && !empty($info) && $info['settings']['default_image'] > 0) {
      $default_img_fid  = $info['settings']['default_image'];
      $default_img_file = file_load($default_img_fid);
      if ($default_img_file) {
        $variables['picture'] = $default_img_file;
      }
  }
  // Get FIO.
  $field_first_name = field_get_items('user', $account, 'field_fio');
  if (!empty($field_first_name[0]['value'])) {
    $variables['user_fio'] = $field_first_name[0]['value'];
  }

  $variables['dropdown'] = uplvl_miniprofile_dropdown();
  $vars['account'] = $variables;
  if (arg(0) == 'user' && is_numeric(arg(1))) {
    if (arg(1) == $user->uid || array_key_exists(3, $user->roles)) {
      $vars['title'] = t('Профиль');
      if (!arg(2)) {
        $vars['header_link'] = uplvl_header_link('user-profile', array(
          'uid' => arg(1),
        ));
      }
    }
  }
  elseif ((arg(0) == 'messages' && arg(1) == 'view') || arg(0) == 'im' || (arg(0) == 'messages' && arg(1) == 'new')) {
    drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style.css');
    $vars['theme_hook_suggestions'] = array('page__private__privatemsg');
    drupal_set_title('Мои сообщения');
  }
  elseif (arg(0) == 'users' && arg(1) == 'registration') {
    drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style.css');
    $vars['theme_hook_suggestions'] = array('page__private__user__registration');
  }
  elseif (arg(0) == 'node' && arg(1) == '39') {
    drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style.css');
    $vars['theme_hook_suggestions'] = array('page__private__service__conditions');
  }
  elseif (arg(0) == 'node' && arg(1) == '44') {
    drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style.css');
    $vars['theme_hook_suggestions'] = array('page__private__confident');
  }
  elseif (arg(0) == 'node' && arg(1) == '42') {
    drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style.css');
    $vars['theme_hook_suggestions'] = array('page__private__contacts');
  }
  elseif (arg(0) == 'user') {
    $vars['theme_hook_suggestions'] = array('page__private');
    drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/style-login.css');
     $vars['header_link'] = '';
  }
}

function uplvl_header_link($type, $data = array()) {
  $link = '';

  $link = l(t('Edit', array(), array('context' => 'profile')), 'user/' . $data['uid'] . '/edit', array(
    'attributes' => array(
      'class' => array(
        'edit-profile-link',
      ),
    ),
  ));

  return $link;
}

/**
 * Returns HTML for miniprofile on dropdown
 */
function uplvl_miniprofile_dropdown() {
  global $user;

  $html = '';
  if($user->uid <> 0){
    if(array_key_exists(4, $user->roles)){ $user_status = 'преподаватель'; }
    if(array_key_exists(5, $user->roles)){ $user_status = 'куратор курса'; }
    if(array_key_exists(8, $user->roles)){ $user_status = 'преподаватель(не актив.)'; }
    if(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)) {
      $user_status = 'ученик';
    }
    $html .= '<div class="user-dropdown-profile">';
    $html .= '<div class="user-dropdown-mail">';
    $html .= l($user->mail, "user/$user->uid", array('html' => 'true'));
    $html .= '</div>';
    $html .= '<div class="user-dropdown-status">';
    $html .= l(@$user_status, "user/$user->uid", array('html' => 'true'));
    $html .= '</div>';
    $html .= uplvl_my_courses_statistic();
    $html .= '<div class="user-dropdown-logout">';
    $html .= l(t('Выход'), "user/logout", array('html' => 'true'));
    $html .= '</div>';
    $html .= '</div>';
  }
  return $html;
}

/**
 * Returns HTML for courses statisitc in miniprofile on dropdown
 */
function uplvl_my_courses_statistic() {
  global $user;
  $html = '';

  if(array_key_exists(4, $user->roles)
    || array_key_exists(8, $user->roles)
    || array_key_exists(5, $user->roles)) {

    $html .= '<div class="dropdown-courses-statistic">';

    if(array_key_exists(5, $user->roles)){
      $query = db_select('field_data_field_course_curator', 'n');
      $query->fields('n', array('entity_id'));
      $query->condition('n.field_course_curator_target_id', $user->uid);
      $res = $query->execute()->fetchAll();

      if(!empty($res)){
        foreach ($res as $nnid) {
          $result = new stdClass();
          $result->nid = $nnid->entity_id;
          $courses[] = $result;
        }
      }
    }else{
      $select = db_select('node', 'n');
      $select->fields('n', array('nid'));
      $select->condition('n.type', 'course');
      $select->condition('n.uid', $user->uid);
      $courses = $select->execute()->fetchAll();
    }

    $p_count = 0;
    $curators = 0;
    $bids = 0;
    $PerWeekCount = 0;
    $unchecked = 0;
    if(!empty($courses)){
      foreach ($courses as $value) {
        $query_users = db_select('non_approved_students', 'uc');
        $query_users->addExpression('COUNT(*)');
        // $query_users->condition('uc.status', 1);
        $query_users->condition('uc.cid', $value->nid);
        $res_users = $query_users->execute()->fetchAll();

        $p_count = $p_count + $res_users[0]->expression;

        $course = node_load($value->nid);
        if (!empty($course->field_course_curator)) {
          $curators = $curators + count($course->field_course_curator['und']);
        }

        $nodes = db_select('non_approved_students', 'n');
        $nodes->addExpression('COUNT(*)');
        $nodes->condition('n.cid', $value->nid);
        $nodes->condition('n.status', 0);
        $res = $nodes->execute()->fetchAll();

        $bids = $bids + $res[0]->expression;

        $query = db_select('ul_accepted_lesson', 'c');
        $query->condition('c.course_id', $value->nid);
        $query->condition(
          db_and()
          ->condition('c.updated', time() - 604800, '>')
          ->condition('c.updated', time(), '<')
        );
        $query->addExpression('COUNT(*)');
        $per_week = $query->execute()->fetchObject();
        $PerWeekCount = $PerWeekCount + $per_week->expression;

        $select = db_select('send_lesson_check', 'lc');
        $select->fields('lc', array('uid', 'lid'));
        $select->condition('lc.coid', $value->nid);
        $select->condition('lc.accepted', 0);
        // $select->addExpression('COUNT(*)');
        $results = $select->execute()->fetchAll();

        foreach ($results as $value) {
          $u = user_load($value->uid);
          if($u){
            $select = db_select('ul_accepted_lesson', 'ul');
            $select-> fields('ul', array('alid'));
            $select-> condition('ul.pupil_uid', $value->uid);
            $select-> condition('ul.lesson_id', $value->lid);
            $results = $select->execute()->fetchAll();

            if(empty($results)){
              $unchecked++;
            }
          }
        }
      }

      $html .= '<div class="count-courses">'.t('Кол-во курсов: ');
      $html .= '<span>' . count($courses) . '</span>';
      $html .= '</div>';
      $html .= '<div class="count-pupils">'.t('Кол-во учеников: ');
      $html .= '<span>' . $p_count . '</span>';
      $html .= '</div>';
      $html .= '<div class="count-courators">'.t('Кол-во кураторов: ');
      $html .= '<span>' . $curators . '</span>';
      $html .= '</div>';
    }

    $html .= '</div>';

    $query = db_select('node', 'n');
    $query ->fields('n', array('nid'));
    $query->condition('n.type', 'course');
    $query->condition('n.uid', $user->uid);
    $res = $query->execute()->fetchAll();
    $count = count($res);
    $course_efe = 0;
    foreach ($res as $value) {
      $effectivity = efectivity_course($value->nid);
      $course_efe = $course_efe + $effectivity;
    }

    $html .= '<div class="dropdown-courses-effectivity">';
    if($course_efe > 0){
      $efe = $course_efe/$count;
      $html .= '<div class="effectivity">'.t('Эффективность').': ';
      $html .= '<span>' . round($efe) . '</span>';
      $html .= '</div>';
    }
    $html .= '<div class="new-pupil">'.t('Кол-во заявок(New): ');
    $html .= '<span>' . $bids . '</span>';
    $html .= '</div>';
    $html .= '<div class="checked-per-week">'.t('Кол-во проверок (сутки): ');
    $html .= '<span>' . $PerWeekCount . '</span>';
    $html .= '</div>';
    $html .= '<div class="not-checked">'.t('Непроверенные: ');
    $html .= '<span>' . $unchecked . '</span>';
    $html .= '</div>';
    $html .= '</div>';
  }
  return $html;
}

function uplvl_preprocess_privatemsg_view(&$vars) {
  global $user;
  $account = $vars['message']->author;
  if (!empty($account->field_user_image[LANGUAGE_NONE][0]['fid'])) {
    $vars['author_picture'] = theme('image', array(
      'path'       => $account->field_user_image[LANGUAGE_NONE][0]['uri'],
      'attributes' => array(
        'class' => array(
          'img-responsive',
        ),
      ),
    ));
  }
  else {
    $instance        = field_info_instance('user', 'field_user_image', 'user');
    $default_fid     = isset($instance['settings']['default_image']) ? $instance['settings']['default_image'] : NULL;
    $default_picture = file_load($default_fid);
    $vars['author_picture'] = theme('image', array(
      'path'       => $default_picture->uri,
      'attributes' => array(
        'class' => array(
          'img-responsive',
        ),
      ),
    ));
  }
  if (!empty($account->field_fio[LANGUAGE_NONE][0]['value'])){
    $vars['author_name_link'] = l($account->field_fio[LANGUAGE_NONE][0]['value'], 'user/' . $account->uid, array(
      'attributes' => array(
        'class'    => array('username'),
        'xml:lang' => '',
        'about'    => url('user/' . $account->uid),
        'typeof'   => 'sioc:UserAccount',
        'property' => 'foaf:name',
        'datatype' => '',
        'target'   => '_blank',
      ),
    ));
  }
  else {
    $vars['author_name_link'] = l($account->name, 'user/' . $account->uid, array(
      'attributes' => array(
        'class'    => array('username'),
        'xml:lang' => '',
        'about'    => url('user/' . $account->uid),
        'typeof'   => 'sioc:UserAccount',
        'property' => 'foaf:name',
        'datatype' => '',
        'target'   => '_blank',
      ),
    ));
  }
  $vars['author_picture'] = l($vars['author_picture'], 'user/' . $account->uid, array(
      'html'       => TRUE,
      'attributes' => array(
        'target' => '_blank',
      ),
    ));
}

// function uplvl_theme() {
//   $items = array();

//     // $items['user_login'] = array(
//     //     'render element' => 'form',
//     //     'path' => drupal_get_path('theme', 'base') . '/tpl',
//     //     'template' => 'user-login',
//     //     'preprocess functions' => array(
//     //       'base_preprocess_user_login'
//     //   ),
//     // );
//     $items['user_register_form'] = array(
//       'render element' => 'form',
//       'path' => drupal_get_path('theme', 'base') . '/tpl',
//       'template' => 'user-register-form',
//       'preprocess functions' => array(
//         'uplvl_preprocess_user_register_form'
//       ),
//     );

//   return $items;
// }

function uplvl_preprocess_user_register_form(&$vars) {
  $vars['intro_text'] = t('');
}
