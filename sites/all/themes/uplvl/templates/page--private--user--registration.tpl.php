<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu" id="header-menu">
      <?php
        $menu = teacher_main_menu_block_content();
        print $menu;
      ?>
    </div>
  </header>
  <div id="main">
    <div class="content">
      <div id="drupal-messages"><?php print $messages; ?></div>
      <div class="text-you-can">Вы можете зарегистрироватся на платформе дистанционного обучения в качестве:</div>
      <div class="registr-boxes">
        <div class="teacher-box">
          <div class="teacher-icon"></div>
          <div class="teacher-choose-box">
            <div class="teacher-button">
              <?php
                print l('Предподавателя', 'users/registration/teacher', array('html' =>TRUE,
                                                                              'attributes' => array(
                                                                                'class' => array(
                                                                                  ''
                                                                                  )
                                                                                )
                                                                              )
                );
              ?>
            </div>
            <div class="teacher-text">
              для создания программы обучения
            </div>
          </div>
        </div>
        <div class="pupil-box">
          <div class="pupil-icon"></div>
          <div class="pupil-choose-box">
            <div class="pupil-button">
              <?php
                print l('Ученика', 'users/registration/pupil', array('html' =>TRUE,
                                                                              'attributes' => array(
                                                                                'class' => array(
                                                                                  ''
                                                                                  )
                                                                                )
                                                                              )
                );
              ?>
            </div>
            <div class="pupil-text">
              для прохождения обучения
            </div>
          </div>
        </div>
      </div>
      <div id="registration-content">

        <?php print render($page['content']); ?>
      </div>
    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks">
          <span class="label"><?php print t('Социальные сети:'); ?></span>
          <span class="vkontakte">&nbsp;</span>
          <span class="youtube">&nbsp;</span>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Способы оплаты:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
        </div>
        <div class="copyright">
          <span class="label"><?php print date('Y'); ?> &copy; Профология. <?php print t('Все права защищены.'); ?></span>
        </div>
      </div>
    </footer>
  </div>
</div>