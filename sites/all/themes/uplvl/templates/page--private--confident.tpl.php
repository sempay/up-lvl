<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu" id="header-menu">
      <?php
        $menu = teacher_main_menu_block_content();
        print $menu;
      ?>
    </div>
  </header>
  <div id="main">
    <div class="page-title clearfix">
      <h1><?php print $title; ?></h1>
    </div>
    <div class="content">
      <div id="drupal-messages"><?php print $messages; ?></div>
      <p>
        Система By-step.ru разработана специально для проведения авторских образовательных программ, для чего была применена уникальная
        трёхступенчатая система защиты информации.
      </p>
      <br>
      <p>
        При получении доступа к платформе у Вас появляется индивидуальный аккаунт, к которому не имеют доступа сторонние лица и прочие
        пользователи By-step.ru.
      </p>
      <br>
      <p>
      Данные Ваших учеников остаются только у Вас, не используются для каких - либо рассылок, акций и прочих маркетинговых мероприятий.
      </p>
      <br>
      <p>
        Ваши видеоматериалы надёжно защищены от копирования. При необходимости Вы в любой момент можете полностью удалить свой курс,
        не оставляя резервных файлов. Система защиты от "пиратов" By-step.ru неоднократно проверялась и показала самые высокие результаты
        по устойчивости к попыткам взлома.
      </p>

      <p>
      Используя нашу платформу, Вы получаете не только инновационную систему обучения, но и передовой уровень конфиденциальности, уникальный
      для российского рынка.
      </p>
    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks">
          <span class="label"><?php print t('Социальные сети:'); ?></span>
          <span class="vkontakte">&nbsp;</span>
          <span class="youtube">&nbsp;</span>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Способы оплаты:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
        </div>
        <div class="copyright">
          <span class="label"><?php print date('Y'); ?> &copy; Профология. <?php print t('Все права защищены.'); ?></span>
        </div>
      </div>
    </footer>
  </div>
</div>