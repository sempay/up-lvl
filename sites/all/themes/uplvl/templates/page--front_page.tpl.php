<?php

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/sites/all/modules/custom_front_page/plugin/bxslider/jquery.bxslider.css"/>
<script type="text/javascript" src="/sites/all/modules/custom_front_page/plugin/bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/misc/jquery.once.js"></script>

<div class="front_page">
  <?//= $breadcrumb; ?>
  <?= $page['highlighted'] ? "<div id=highlighted>" . render($page['highlighted']) . "</div>" : NULL ?>
  <a id="main-content"></a>

  <?= render($title_prefix); ?>
  <h1><?php $title ?> </h1>
  <?php if (!empty($site_name_and_slogan)): ?>
  <h1 class=element-invisible>
  <?php strip_tags($site_name_and_slogan); ?>
  <?php endif; ?>
  </h1>
  <?= render($title_suffix); ?>

  <?= $tabs ? "<div id=tabs-wrapper class=clearfix>" . render($tabs) . "</div>" : NULL ?>

  <?= render($tabs2) ?>
  <?= $messages ?>
  <?= render($page['help']) ?>

  <?= $action_links ? "<ul class=action-links>" . render($action_links) . "</ul>" : NULL ?>
  <?= $messages ?>

  <div class="front_menu">
    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><img src="/sites/all/modules/custom_front_page/img/logo.png"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="hidden">
                <a href="#page-top"></a>
              </li>
              <li class="page-scroll">
                <a href="#Jvideos">О платформе</a>
              </li>
              <li class="page-scroll">
                <a href="#Jproblem">Ваши задачи</a>
              </li>
              <li class="page-scroll">
                <a href="#Jreshen">Наше решение</a>
              </li>
              <li class="page-scroll">
                <a href="#Jlist">Преимущества СДО</a>
              </li>
              <li class="page-scroll">
                <a href="#Jfeadback">Отзывы</a>
              </li>
              <li class="page-scroll">
                <?php
                  global $user;
                  if (user_is_anonymous()){
                    print '<a href="/user" role="button" class="btn btn-default go">Войти</a>';
                  } else {
                    print l($user->mail, "user/$user->uid", array('html' => 'true'));
                  }
                ?>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
  </div>
  <div id="Jvideos"></div>
  <?php render($page['header']); ?>
  <header id="Jvideo">
    <div class="bg-picture">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="yb-video">
              <?php
              $cfp_variables = variable_get('cfp_fb_video', '');
              if (!empty($cfp_variables)){
                print '<iframe frameborder="0" height="350" src="' . $cfp_variables . '" width="48%"></iframe>';
              }
              ?>
            </div>
            <div class="intro-text">
              <h2 class="text">
              <?php
              $cfp_variables = variable_get('cfp_fb_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
              </h2>
              <div class="button">
                <div class="tryn-block"><a href="#" role="button" class="btn btn-default tryn link-open">
              <?php
              $cfp_variables = variable_get('cfp_fb_button', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
              </a></div>
              </div>
              <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_fb_text_second', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section id="Jproblem">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="sub-title">
              <?php
              $cfp_variables = variable_get('cfp_sb_first_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
              </h2>
            <hr class="red">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_1.png" class="img-responsive centred" alt="">
              <?php
              $cfp_variables = variable_get('cfp_sb_fr_image', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          <h2 class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_fr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          </h2>
          <hr class="red">
          <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_fr_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          </div>
        </div>
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_2.png" class="img-responsive centred" alt="">
              <?php
              $cfp_variables = variable_get('cfp_sb_sr_image', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          <h2 class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_sr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          </h2>
          <hr class="red">
          <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_sr_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          </div>
        </div>
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_3.png" class="img-responsive centred" alt="">
              <?php
              $cfp_variables= variable_get('cfp_sb_tr_image', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          <h2 class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_tr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>          </h2>
          <hr class="red">
          <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_tr_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables
                ;
              }
              ?>
          </div>
        </div>
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_4.png" class="img-responsive centred" alt="">
              <?php
              $cfp_variables = variable_get('cfp_sb_fourth_image', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          <h2 class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_forth_r_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          </h2>
          <hr class="red">
          <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_sb_forth_r_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
          </div>
        </div>
      </div>
      <div class="row col-centered">
        <div class="tryn-block button"><a href="#" role="button" class="btn btn-default tryn link-open">
              <?php
              $cfp_variables = variable_get('cfp_sb_button', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
        </a></div>
      </div>
    </div>
  </section>
  <section id="Jreshen">
    <div class="bg-picture2">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="sub-title banner margin-tb-50">
              <?php
              $cfp_variables = variable_get('cfp_tb_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jcool">
    <div class="bg-picture">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="intro-text">
              <h2 class="text">
              <?php
              $cfp_variables = variable_get('cfp_fourth_b_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
              </h2>
              <hr class="red banner">
              <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_fourth_b_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
              </div>
              <script type="text/javascript">
                function attachSlider() {
                  if (jQuery(document).outerWidth(true) < 750) {
                    jQuery('.slider .bxslider').once('bx-mini-slider-sm600', function () {
                      window.bxsliderSlider = jQuery('.slider .bxslider').bxSlider({
                        slideWidth: 300,
                        minSlides: 1,
                        maxSlides: 1,
                        moveSlides: 1,
                        slideMargin: 1,
                        pager: false,
                        control: true
                      });
                    });
                  }
                  else {
                    jQuery('.slider .bxslider').once('bx-mini-slider-lg600', function () {
                      window.bxsliderSlider = jQuery('.slider .bxslider').bxSlider({
                        slideWidth: 300,
                        minSlides: 3,
                        maxSlides: 3,
                        moveSlides: 1,
                        slideMargin: 1,
                        pager: false,
                        control: true
                      });
                    });
                  }
                }


                jQuery(window).resize(function () {
                  if (jQuery(document).outerWidth(true) < 750 && jQuery('.slider .bxslider').hasClass('bx-mini-slider-lg600-processed')) {
                    window.bxsliderSlider.destroySlider();
                    var slider = jQuery('.slider .bxslider').removeClass('bx-mini-slider-lg600-processed');
                    setTimeout(function () {
                      attachSlider();
                    }, 100);
                  }
                  else if (jQuery(document).outerWidth(true) >= 750 && jQuery('.slider .bxslider').hasClass('bx-mini-slider-sm600-processed')) {
                    window.bxsliderSlider.destroySlider();
                    var slider = jQuery('.slider .bxslider').removeClass('bx-mini-slider-sm600-processed');
                    setTimeout(function () {
                      attachSlider();
                    }, 100);
                  }
                });

                jQuery(document).ready(attachSlider);

              </script>
              <!-- <div class="slider" style="width:820px;"> -->
              <div class="slider">
                <ul class="bxslider">
                  <?php
                    $type_node = 'slideshow_front_page';
                    $nodes = node_load_multiple(array(), array('type' => $type_node));
                    foreach ($nodes as $key => $value) {
                    $logo_partner_path = $value->field_slideshow_image[LANGUAGE_NONE][0]['uri'];
                    $name_partner = $value->field_field_name_slideshow[LANGUAGE_NONE][0]['safe_value'];
                    $job_partner = $value->field_field_descr_slideshow[LANGUAGE_NONE][0]['value'];
                    print('<li>');
                    print('<div class="octagon">');
                      print theme('image', array(
                        'path' => $logo_partner_path,
                        'width' => 200,
                        'height' => 200,
                      ));
                    print '</div>';
                    print('<div class="text name">');
                      print $name_partner;
                    print '</div>';
                    print('<div class="text job">');
                      print $job_partner;
                    print '</div>';
                    print('</li>');
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php //render($page['header']); ?>
    </div>
  </section>
  <section id="Jfact">
    <div class="bg-picture2">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="sub-title banner">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
            <hr class="red">
            <div class="box">
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_4.png">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_fr_image', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                <div class="text number">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_fr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
                </div>
                <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_fr_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
                </div>
              </div>
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_3.png">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_sr_image', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                <div class="text number">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_sr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
                </div>
                <div class="text">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_sr_text', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
                </div>
              </div>
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_2.png">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_tr_image', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                <div class="text number">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_tr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
                </div>
                <div class="text">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_tr_text', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                </div>
              </div>
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_1.png">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_fourth_r_image', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                <div class="text number">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_fourth_r_title', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                </div>
                <div class="text">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_fourth_r_text', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row margin-tb-60">
          <div class="col-lg-12 text-center">
            <div class="button">
              <div class="arrow l"></div>
              <div class="tryn-block"><a href="#" role="button" class="btn btn-default tryn link-open">
                  <?php
                  $cfp_variables = variable_get('cfp_fifth_b_button', '');
                  if (!empty($cfp_variables)){
                    print $cfp_variables;
                  }
                  ?>
              </a></div>
              <div class="arrow r"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jlist">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="sub-title">
            <?php
            $cfp_variables = variable_get('cfp_sixth_b_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red bold">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/8.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_b_first_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_first_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_first_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/7.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_second_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_second_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_second_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/6.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_third_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_third_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_third_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/5.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_fourth_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_fourth_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_fourth_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_fifth_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_fifth_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_fifth_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/3.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_six_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_six_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_six_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/2.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_seven_r_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_seven_r_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_seven_r_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/1.png" class="img-responsive centred" alt="">
            <?php
            $cfp_variables = variable_get('cfp_six_b_er_image', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          <h2 class="text">
            <?php
            $cfp_variables = variable_get('cfp_six_b_er_title', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </h2>
          <hr class="red">
          <div class="text grey">
            <?php
            $cfp_variables = variable_get('cfp_six_b_er_text', '');
            if (!empty($cfp_variables)){
              print $cfp_variables;
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jprice">
    <div class="bg-picture">
      <div class="container">
    <div class="row margin-tb-30">
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-blue">
            <h2 class="heading-md">
              <?php
              $cfp_variables = variable_get('cfp_seven_b_fr_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
            <h3><span class="pr">
              <?php
              $cfp_variables = variable_get('cfp_seven_b_fr_price', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              elseif ($cfp_variables === '0') {
                $cfp_variables = '0';
                print $cfp_variables;
              }
              ?>
            </span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_fr_fc_first_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_fr_fc_second_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_fr_fc_third_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_fr_fc_fourth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_fr_fc_fifth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
            </ul>
            <a href="#" class="btn btn-default btn-price">
              <?php
              $cfp_variables = variable_get('cfp_seven_b_fr_button', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-sea">
            <h2 class="heading-md">
              <?php
              $cfp_variables = variable_get('cfp_seventh_b_second_r_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
            <h3><span class="pr">
              <?php
              $cfp_variables = variable_get('cfp_seventh_b_second_r_price', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_sr_fc_first_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_sr_fc_second_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_sr_fc_third_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_sr_fc_fourth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_sr_fc_fifth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
            </ul>
            <a href="#" class="btn btn-default btn-price">
              <?php
              $cfp_variables = variable_get('cfp_fifth_b_second_r_button', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-sea">
            <h2 class="heading-md">
              <?php
              $cfp_variables = variable_get('cfp_seven_b_third_r_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
            <h3><span class="pr">
              <?php
              $cfp_variables = variable_get('cfp_seven_b_third_r_price', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_tr_fc_first_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_tr_fc_second_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_tr_fc_third_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_tr_fc_fourth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_tr_fc_fifth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
            </ul>
            <a href="#" class="btn btn-default btn-price">
              <?php
                $cfp_variables = variable_get('cfp_seven_b_third_r_button', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-sea">
            <h2 class="heading-md">
              <?php
                $cfp_variables = variable_get('cfp_seven_b_fourth_r_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
            <h3><span class="pr">
              <?php
                $cfp_variables = variable_get('cfp_seven_b_fourth_r_price', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_four_r_fc_first_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_four_r_fc_second_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_four_r_fc_third_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_four_r_fc_fourth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
              <li>
                <?php
                $cfp_variables = variable_get('cfp_sb_four_r_fc_fifth_text', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
                ?>
              </li>
            </ul>
            <a href="#" class="btn btn-default btn-price">
              <?php
                $cfp_variables = variable_get('cfp_seven_b_fourth_r_button', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </a>
          </div>
        </div>
      </div>
    </div>
      </div>
      <?php //render($page['header']); ?>
    </div>
  </section>
  <section id="Jfeadback">
    <div class="bg-picture2">
      <div class="container">
        <div class="row">
          <div class="feadbacks-img margin-tb-40">
            <img src="/sites/all/modules/custom_front_page/img/22.png">
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
              <div class="intro-text">
                <script type="text/javascript">
                  jQuery(document).ready(function(){
                    jQuery('.slider2 .bxslider').bxSlider({
                      moveSlides: 1,
                      autoReload: true,
                      slideMargin: 1,
                      pager: true,
                      control: false
                    });
                  });
                </script>
                <div class="slider2 margin-tb-50">
                  <ul class="bxslider">
                  <?php
                    $type_node = 'slideshow_to_front_page_block4';
                    $nodes = node_load_multiple(array(), array('type' => $type_node));
                    foreach ($nodes as $k => $v) {
                      //exit('<pre>' . print_r($v->body['ru'],1) . '</pre>');
                    $body_message = $v->body['ru'][0]['value'];
                    $user_name = $v->field_fio_users[LANGUAGE_NONE][0]['value'];
                    $users_job = $v->field_user_role[LANGUAGE_NONE][0]['value'];
                    print('<li>');
                    print('<div class="feadback-text">');
                      print $body_message;
                    print '</div>';
                    print('<div class="text name">');
                      print $user_name;
                    print '</div>';
                    print('<div class="text job">');
                      print $users_job;
                    print '</div>';
                    print('</li>');
                  }
                  ?>
                  </ul>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </section>
  <?php
     $cfp_variables = variable_get('cfp_eb_nid_form', '');
     if (!empty($cfp_variables_nid)){
  ?>
  <section id="Jcontact">
    <div class="bg-picture">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="text sub-title">
              <?php
              $cfp_variables = variable_get('cfp_eb_title', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
              }
              ?>
            </h2>
            <hr class="blue">
          </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
          <?php
            $node = node_load($cfp_variables_nid, '');
            webform_node_view($node, 'full');
            $node_rendered = theme_webform_view($node->content);
            print render($node_rendered);
          //drupal_set_message('<pre>'.print_r(node_load(10593),1).'</pre>');
          ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
    }
  ?>
  <div class="container">

  </div>
  <footer>
    <div class="bg-picture-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-centred">
            <div class="col-sm-3 footer-logo">
              <img src="/sites/all/modules/custom_front_page/img/footer-logo.png">
            </div>
            <div class="col-sm-9 footer-menu">
              <ul class="nav navbar-nav footer">
                <li class="hidden">
                  <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                  <a href="#Jvideos">О платформе</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jproblem">Ваши задачи</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jreshen">Наше решение</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jlist">Преимущества СДО</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jfeadback">Отзывы</a>
                </li>
                <li class="page-scroll">
                  <?php
                    global $user;
                    if (user_is_anonymous()){
                      print '<a href="/user" role="button" class="btn btn-default go">Войти</a>';
                    } else {
                      print l($user->mail, "user/$user->uid", array('html' => 'true'));
                    }
                  ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
          <div class="row margin-tb-40">
            <div class="col-sm-12">
            <div class="col-sm-4 footer-inform footer-social-button">
              <p>СОЦИАЛЬНЫЕ СЕТИ:</p>
              <a href=
               <?php
                $cfp_variables = variable_get('cfp_nb_link_vk', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
              ?>
               target="_blank">
              <img src="/sites/all/modules/custom_front_page/img/w.PNG">
              </a>
              <a href=
               <?php
                $cfp_variables = variable_get('cfp_nb_link_youtube', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
              }
              ?>
               target="_blank">
              <img src="/sites/all/modules/custom_front_page/img/wqe.png">
              </a>
            </div>
            <div class="col-sm-4 footer-inform footer-social-button">
              <p>СПОСОБЫ ОПЛАТЫ:</p>
              <img src="/sites/all/modules/custom_front_page/img/wq.PNG">
              <img src="/sites/all/modules/custom_front_page/img/mastercard.PNG">
            </div>
            <div class="col-sm-4 footer-inform footer-provologi">
              <p id="confident"><?php print date('Y'); ?> &copy; ПРОФОЛОГИЯ. ВСЕ ПРАВА ЗАЩИЩЕНЫ.</p>
            </div>
              <?php //render($page['footer']); ?>
            </div>
          </div>
      </div>
    </div>
  <div class="b-popup" id="popup1">
    <div class="popup-registration-content">
    <div class="link-close">
      <a href="#"><img src="/sites/all/modules/custom_front_page/img/pop-up-close.png"></a>
    </div>
    <div class="mounth-testing">Приступить к месяцу <a href="">бесплатного тестирования</a> (только для преподавателей):</div>
    <?php
      $test = node_load(3758);
      webform_node_view($test, 'full');
      $test_rendered = theme_webform_view($test->content);
      print render($test_rendered);
    ?>
    <div class="framework-testing">На прошлой неделе к тестированию платформы приступило <span class="count-visited">143 компании</span></div>
    </div>
</div>

  </footer>
</div>
<div id="toTop">^ Наверх</div>