<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu" id="header-menu">
      <?php
        $menu = teacher_main_menu_block_content();
        print $menu;
      ?>
    </div>
    <?php
      print theme('uplvl_user_miniprofile', $variables['account']);
    ?>
  </header>
  <aside class="sidebar" id="sidebar">
    <div id="sidebar-menu" class="menu-private-sidebar-menu clearfix">
      <?php
        $leftMenu = left_side_user_menu_content();
        print $leftMenu;
      ?>
    <div>
  </aside>
  <div id="main">
    <div class="page-title clearfix">
      <h1><?php print $title; ?></h1>
          <div class="link-faq">
          <?php
            if (arg(0) == 'im') {
              $video_on_message = variable_get('video_on_message', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_message)) {
                print $video_on_message;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            ?>
        </div>
    </div>

    <div class="content">

        <div class="search-form">
      <?php
        $form = drupal_get_form('ul_messanger_search_form');
        print render($form);
      ?>
    </div>
    <div id="tab-messages">

      <a href="/im">Диалоги</a>
      <?php
        drupal_add_js(drupal_get_path('module', 'ul_messanger') . '/js/msg.js');
        if (arg(0) == 'messages' && arg(1) == 'view'){
          print '<a class="active">Просмотр диалога</a>';
        }
      ?>
      <a href="/im/list-contacts">Список контактов</a>

    </div>
      <div id="drupal-messages"><?php print $messages; ?></div>
      <?php print render($page['content']); ?>
    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks">
          <span class="label"><?php print t('Социальные сети:'); ?></span>
              <a href=
               <?php
                $cfp_variables = variable_get('cfp_nb_link_vk', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
              ?>
              target="_blank">
          <span class="vkontakte">&nbsp;</span>
          </a>
          <a href=
             <?php
              $cfp_variables = variable_get('cfp_nb_link_youtube', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
            }
            ?>
            target="_blank">
          <span class="youtube">&nbsp;</span>
          </a>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Способы оплаты:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
        </div>
        <div class="copyright">
          <span class="label"><?php print date('Y'); ?> &copy; Профология. <?php print t('Все права защищены.'); ?></span>
        </div>
      </div>
    </footer>
  </div>
</div>