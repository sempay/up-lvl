<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu" id="header-menu">
      <?php
        $menu = teacher_main_menu_block_content();
        print $menu;
      ?>
    </div>
    <?php
      print theme('uplvl_user_miniprofile', $variables['account']);
    ?>
  </header>
  <aside class="sidebar" id="sidebar">
    <div id="sidebar-menu" class="menu-private-sidebar-menu clearfix">
      <?php
        $leftMenu = left_side_user_menu_content();
        print $leftMenu;
      ?>
    <div>
  </aside>
  <div id="main">
    <div class="page-title clearfix">
      <h1><?php print $title; ?></h1>
      <div class="link-faq">
          <?php
            if (arg(0) == 'user' || arg(0) == 'users') {
              $video_on_profile = variable_get('video_on_profile', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_profile)) {
                print $video_on_profile;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'curator-add') {
              $video_on_add_curator = variable_get('video_on_add_curator', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_add_curator)) {
                print $video_on_add_curator;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'add-students') {
              $video_on_add_pupils = variable_get('video_on_add_pupils', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_add_pupils)) {
                print $video_on_add_pupils;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'pupil-list') {
              $video_on_list_pupils = variable_get('video_on_list_pupils', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_list_pupils)) {
                print $video_on_list_pupils;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'curator' && arg(1) == 'tendency' || arg(0) == 'statistic') {
              $video_on_statistic_pupils = variable_get('video_on_statistic_pupils', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_statistic_pupils)) {
                print $video_on_statistic_pupils;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'curator' && arg(1) == 'send-messages') {
              $video_on_alert_system = variable_get('video_on_alert_system', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_alert_system)) {
                print $video_on_alert_system;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'curator' && arg(1) == 'check-course') {
              $video_on_check_course = variable_get('video_on_check_course', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_check_course)) {
                print $video_on_check_course;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'edit-course') {
              $video_on_edit_course = variable_get('video_on_edit_course', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_edit_course)) {
                print $video_on_edit_course;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'my-courses' || arg(0) == 'pupil-course') {
              $video_on_edit_lesson = variable_get('video_on_edit_lesson', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_edit_lesson)) {
                print $video_on_edit_lesson;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'edit-task' || arg(0) == 'pupil-lesson') {
              $video_on_edit_task = variable_get('video_on_edit_task', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_edit_task)) {
                print $video_on_edit_task;
              }
              print '> <span class="link-video">link</span> </a>';
            }
            elseif (arg(0) == 'library') {
              $video_on_library = variable_get('video_on_library', '');
              print '<a class="training-video-link" href="#" data-v=';
              if (!empty($video_on_library)) {
                print $video_on_library;
              }
              print '> <span class="link-video">link</span> </a>';
            }
          ?>
      </div>
      <?php
        if ($header_link) {
          print '<div class="header-link">' . $header_link . '</div>';
        }
      ?>
    </div>
    <div class="content">
      <div id="drupal-messages"><?php print $messages; ?></div>
      <?php print render($page['content']); ?>
    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks">
          <span class="label"><?php print t('Социальные сети:'); ?></span>
            <a href=
               <?php
                $cfp_variables = variable_get('cfp_nb_link_vk', '');
                if (!empty($cfp_variables)){
                  print $cfp_variables;
                }
              ?>
               target="_blank">
          <span class="vkontakte">&nbsp;</span>
          </a>
          <a href=
             <?php
              $cfp_variables = variable_get('cfp_nb_link_youtube', '');
              if (!empty($cfp_variables)){
                print $cfp_variables;
            }
            ?>
             target="_blank">
          <span class="youtube">&nbsp;</span>
          </a>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Способы оплаты:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
        </div>
        <div class="copyright">
          <span class="label"><?php print date('Y'); ?> &copy; Профология. <?php print t('Все права защищены.'); ?></span>
        </div>
      </div>
    </footer>
  </div>
</div>