<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu" id="header-menu">
      <?php
        $menu = teacher_main_menu_block_content();
        print $menu;
      ?>
    </div>
  </header>
  <div id="main">
    <div class="page-title clearfix">
      <h1><?php print $title; ?></h1>
    </div>
    <div class="content">
      <div id="drupal-messages"><?php print $messages; ?></div>
      <p>Уважаемые преподаватели!
        <p>
          Мы действительно понимаем все то, что Вам необходимо для полноценной работы с обучающейся аудиторией. Более того, мы самостоятельно разрабатываем свои курсы, которые, естественно, находятся также на этой платформе.
        </p>
        <br>
        <p>
          Мы готовы предоставлять нашу платформу компаниям, тренинговым центрам, лицам, которые ведут образовательную деятельность (инфобизнесменам), чтобы и Вы и Ваши ученики почувствовали все преимущества обучения своей аудитории через автоматизированный метод.
        </p>
        <br>
        <p>
          Мы предлагаем Вам:
        </p>
        <p>
          1. Два месяца бесплатного использования платформы By-step.ru
        </p>
        <p>
          2. Определение стоимости после тестового периода - индивидуально с каждым клиентом
        </p>
        <p>
          3. Учёт всех Ваших замечаний и пожеланий, постоянную оптимизацию системы
        </p>
        <br>
        <p>
        Наша техническая поддержка доступна 24 часа в сутки 7 дней в неделю. Мы тщательно охраняем Ваше спокойствие и комфорт учеников.
        </p>
    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks">
          <span class="label"><?php print t('Социальные сети:'); ?></span>
          <span class="vkontakte">&nbsp;</span>
          <span class="youtube">&nbsp;</span>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Способы оплаты:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
        </div>
        <div class="copyright">
          <span class="label"><?php print date('Y'); ?> &copy; Профология. <?php print t('Все права защищены.'); ?></span>
        </div>
      </div>
    </footer>
  </div>
</div>