<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu" id="header-menu">
      <?php
        $menu = teacher_main_menu_block_content();
        print $menu;
      ?>
    </div>
  </header>
  <div id="main">
    <div class="page-title clearfix">
      <h1><?php print $title; ?></h1>
    </div>
    <div class="content">
      <div class="distance-comunication">
        <div id="drupal-messages"><?php print $messages; ?></div>
        <div class="pers-inform">
          <div class="tel"><span class="title-inform">Тел.:</span> +7(812)-649-93-20</div>
          <div class="mob-phone"><span class="title-inform">Моб.:</span> +7(904)-617-36-37</div>
        </div>
        <br>
        <div class="net-inform">
          <div class="e-mail"><span class="title-inform">e-mail:</span> topsmo.ia@gmail.com</div>
          <div class="skype"><span class="title-inform">skype:</span> top-smo.ru</div>
          <div class="icq"><span class="title-inform">icq:</span> 603-010-702</div>
        </div>
      </div>
      <br>
      <div class="ur-adress">Юридический адрес:</div>
      <div class="adress">190121, Россия,Санкт Петербург, ул Декабристов 62-64 лит А</div>
      <br>
      <div class="top-smo">ООО «ТОП-СМО»</div>
      <div class="inn">ИНН 7839424638 КПП 783901001</div>
      <div class="ohrn">ОГРН 1107847153149</div>
      <br>
      <div class="ur-adress">Фактический адрес:</div>
      <div class="adress">198095, Россия, г.Санкт-Петербург, ул.Швецова, д.41 лит.А пом.7H, офис 23.</div>

    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks">
          <span class="label"><?php print t('Социальные сети:'); ?></span>
          <span class="vkontakte">&nbsp;</span>
          <span class="youtube">&nbsp;</span>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Способы оплаты:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
        </div>
        <div class="copyright">
          <span class="label"><?php print date('Y'); ?> &copy; Профология. <?php print t('Все права защищены.'); ?></span>
        </div>
      </div>
    </footer>
  </div>
</div>