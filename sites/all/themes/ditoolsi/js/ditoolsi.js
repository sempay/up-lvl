(function ($, Drupal, w, d, undefined) {

  "use strict";

  Drupal.behaviors.sidebarToggle = {
    attach: function() {
      $('aside#sidebar').once('sidebarToggle', function () {
        var $btn = $('<div />', {
          class: 'sidebar-toggle navbar-toggle'
        }).html('<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>'),
            $sidebar = $(this),
            $miniProfileFields = $('.mini-profile .name,\
              aside#sidebar .language-switcher,\
              aside#sidebar .mini-profile .button-logout');
        if ($.cookie('sidebarToggleClosed')) {
          $sidebar.addClass('closed').addClass('no-animate');
          $btn.addClass('closed');
          $miniProfileFields.hide();
          setTimeout(function () {
            $sidebar.removeClass('no-animate');
          }, 400);
        }
        $btn.bind('click', function () {
          if (!$sidebar.hasClass('closed')) {
            $miniProfileFields.hide();
            $.cookie('sidebarToggleClosed', true);
            $btn.addClass('closed');
          }
          else {
            $.cookie('sidebarToggleClosed', null);
            setTimeout(function () {
              $miniProfileFields.css('opacity', 0).show().animate({opacity:1}, 400);
            }, 400);
            $btn.removeClass('closed');
          }
          $sidebar.toggleClass('closed');
        });
        $('#logo-private-pages').after($btn);
      });
    }
  };

  Drupal.behaviors.frontSlideshow = {
    attach: function() {
      if (jQuery.fn.bxSlider) {
        $('.bxslider').bxSlider({
          slideWidth: 200,
          minSlides: 5,
          maxSlides: 5,
          moveSlides: 1,
          slideMargin: 10,
          pager: false,
          responsive: true,
        });
      }
    }
  };

  Drupal.behaviors.ditoolsiFormLabels = {
    attach: function() {
      $('.process-ditools-labels').once('ditoolsiFormLabels', function () {
        var id, $field;
        $(this).find('.form-item').each(function () {
          id = $(this).find('label').attr('for');
          if (id) {
            $field = $('#' + id);
            if ($field.val().length) {
              $(this).find('label').hide();
            }
            $field.bind('focus', function () {
              $(this).parents('.form-item').find('label').hide();
            }).bind('blur', function () {
              if (!$(this).val().length) {
                $(this).parents('.form-item').find('label').show();
              }
            });
          }
          $field = null;
        });
      });
    }
  };

  Drupal.behaviors.ditoolsiFileField = {
    attach: function() {
      $('.ditoolsi-file-field').find('input[type="file"]').once('ditoolsiFileField', function () {
        var $field = $(this),
            $button = $('<span />', {
              class: 'btn btn-default btn-file ditoolsi-file-field-btn'
            }).html(Drupal.t('Browse...')),
            $textField = $('<input />', {
              type: 'text',
              class: 'form-control',
              readonly: 'readonly'
            }),
            $group = $('<div />', {
              class: 'input-group-btn'
            }).append($button);
        $button.bind('click', function () {
          $field.trigger('click');
        });
        $field.after($group).hide();
        $group.after($textField);
        $field.bind('change', function () {
            var $input = $(this),
                numFiles = $input.get(0).files ? $input.get(0).files.length : 1,
                label = $input.val().replace(/\\/g, '/').replace(/.*\//, '');
            if (numFiles == 1) {
              $input.next('.input-group-btn').next('input').val(label);
            }
            else if (numFiles > 1) {
              $input.next('.input-group-btn').next('input').val(Drupal.t('@count files selected', {
                '@count': numFiles
              }));
            }
            else {
              $input.next('.input-group-btn').next('input').val('');
            }
        });
      });
    }
  };

  Drupal.setMessage = function(message, type) {
    if (!type || $.inArray(type, ['status', 'warning', 'error']) < 0) {
      type = 'status';
    }

    var typeClasses = {status: 'success', warning: 'warning', error: 'danger'},
        $container = $('<div />', {
          class: 'alert alert-dismissible alert-' + typeClasses[ type ],
          role: 'alert'
        }),
        $close = $('<button />', {
          type: 'button',
          class: 'close',
          'data-dismiss': 'alert',
          'aria-label': 'Close'
        }).html('<span aria-hidden="true">&times;</span>');

    var n = $('#drupal-messages').find('.alert').length;
    $('#drupal-messages').find('.alert').filter(function (i, e) {
      return $(this).index() <= n - 5;
    }).remove();

    $('#drupal-messages').append($container.append($close).append(message));

    Drupal.attachBehaviors();
  };

})(jQuery, Drupal, window, document, undefined);