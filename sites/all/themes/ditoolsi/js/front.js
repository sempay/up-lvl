(function ($, Drupal, w, d, undefined) {

  Drupal.behaviors.frontSlideshow = {
    attach: function() {
      $('.bxslider').bxSlider({
        slideWidth: 200,
        minSlides: 5,
        maxSlides: 5,
        moveSlides: 1,
        slideMargin: 10,
        pager: false,
        responsive: true,
      });
    }
  };

})(jQuery, Drupal, window, document, undefined);