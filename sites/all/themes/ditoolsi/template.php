<?php

/**
 * @file
 * template.php
 */


/**
 * Implements template_preprocess_html().
 */
function bootstrap_subtheme_preprocess_html(&$vars) {
  if (arg(0) == 'user' && (is_numeric(arg(1)) || !arg(1))) {
    drupal_add_css(drupal_get_path('theme', 'bootstrap_subtheme') . '/css/profile.css');
  }
  elseif (drupal_is_front_page()) {
    drupal_add_css(drupal_get_path('theme', 'bootstrap_subtheme') . '/css/front.css');
    drupal_add_css(drupal_get_path('theme', 'ditoolsi') .'/css/jquery.bxslider.css', 'file');
    drupal_add_js(drupal_get_path('theme', 'ditoolsi') .'/js/jquery.bxslider.min.js', 'file');
    drupal_add_js(drupal_get_path('theme', 'ditoolsi') .'/js/front.js', 'file');
  }
  drupal_add_library('system', 'jquery.cookie');
}

/**
 * Implements template_preprocess_page().
 */
function bootstrap_subtheme_preprocess_page(&$vars) {
  global $user, $language;

  $private_pages = array(
    'user/*',
    'contacts',
    'contacts/*',
  );

  $exclude = array(
    'user/login',
    'user/password',
    'user/register',
  );

  if (drupal_match_path(current_path(), implode(PHP_EOL, $private_pages)) && !drupal_match_path(current_path(), implode(PHP_EOL, $exclude))) {
    $vars['header_link'] = '';

    $vars['theme_hook_suggestions'] = array('page__private');
    drupal_add_css(drupal_get_path('theme', 'bootstrap_subtheme') . '/css/private.css');
    drupal_add_css(drupal_get_path('theme', 'bootstrap_subtheme') . '/fontawesome/css/font-awesome.min.css');
    drupal_add_js(drupal_get_path('module', 'ditoolsi_profile') . '/js/ditoolsi-profile-lang-switcher.js');

    if (arg(0) == 'user' && is_numeric(arg(1))) {
      $account = user_load(arg(1));
    }
    else {
      // Load user object for get all fields in some cases.
      $account = user_load($user->uid);
    }

    $variables = array(
      'account'      => $account,
      'picture'      => new stdClass(),
      'current_lang' => $language->language,
      'first_name'   => '',
      'last_name'    => '',
    );

    if (($field_user_picture = field_get_items('user', $account, 'field_user_picture')) && !empty($field_user_picture[0]['fid'])) {
      $variables['picture'] = file_load($field_user_picture[0]['fid']);
    }
    $info = field_info_instance('user', 'field_user_picture', 'user');
    if (!empty($info) && $info['settings']['default_image'] > 0) {
        $default_img_fid  = $info['settings']['default_image'];
        $default_img_file = file_load($default_img_fid);
        if ($default_img_file) {
          $variables['picture'] = $default_img_file;
        }
    }

    // Get first name.
    $field_first_name = field_get_items('user', $account, 'field_first_name');
    if (!empty($field_first_name[0]['value'])) {
      $variables['first_name'] = $field_first_name[0]['value'];
    }

    // Get last name.
    $field_last_name = field_get_items('user', $account, 'field_last_name');
    if (!empty($field_last_name[0]['value'])) {
      $variables['last_name'] = $field_last_name[0]['value'];
    }

    $vars['account'] = $variables;

    if (arg(0) == 'user' && is_numeric(arg(1))) {
      if (arg(1) == $user->uid) {
        $vars['title'] = t('My profile');
        if (!arg(2)) {
          $vars['header_link'] = bootstrap_subtheme_header_link('user-profile', array(
            'uid' => $user->uid,
          ));
        }
      }
      else {
        $variables = array();
        $field_first_name = field_get_items('user', $account, 'field_first_name');
        if (!empty($field_first_name[0]['value'])) {
          $variables['first_name'] = $field_first_name[0]['value'];
        }

        // Get last name.
        $field_last_name = field_get_items('user', $account, 'field_last_name');
        if (!empty($field_last_name[0]['value'])) {
          $variables['last_name'] = $field_last_name[0]['value'];
        }

        // Get middle name.
        $field_middle_name = field_get_items('user', $account, 'field_middle_name');
        if (!empty($field_middle_name[0]['value'])) {
          $variables['middle_name'] = $field_middle_name[0]['value'];
        }
        $vars['title'] = implode(' ', $variables);
      }

      if (arg(2) == 'edit') {
        $vars['header_link'] = bootstrap_subtheme_header_link('edit-user-profile');
      }
    }
  }
}

function bootstrap_subtheme_sidebar_menu() {
  global $user;
  $tree      = menu_tree('menu-private-sidebar-menu');
  $is_logged = user_is_logged_in();

  foreach ($tree as $mlid => $item) {
    if (!empty($item['#href']) && $item['#href'] == 'user' && $is_logged) {
      $tree[ $mlid ]['#href'] = 'user/' . $user->uid;
      if (arg(0) == 'user' && arg(1) == $user->uid && arg(2) == 'edit') {
        $tree[ $mlid ]['#localized_options']['attributes']['class'][] = 'active';
      }
    }
    if (!empty($item['#localized_options']['font_class'])) {
      $tree[ $mlid ]['#title'] = '<i class="fa ' . $item['#localized_options']['font_class'] . '"></i> ' . $tree[ $mlid ]['#title'];
      $tree[ $mlid ]['#localized_options']['html'] = TRUE;
    }
  }

  return $tree;
}

function bootstrap_subtheme_header_link($type, $data = array()) {
  $link = '';

  switch ($type) {
    case 'user-profile':
      $link = l(t('Edit', array(), array('context' => 'profile')), 'user/' . $data['uid'] . '/edit', array(
        'attributes' => array(
          'class' => array(
            'edit-profile-link',
          ),
        ),
      ));
      break;
    case 'edit-user-profile':
      $link = l('&nbsp;', 'http://youtube.com', array(
        'html'       => TRUE,
        'absolute'   => TRUE,
        'attributes' => array(
          'class' => array(
            'edit-profile-video-link',
          ),
          'target' => '_blank',
        ),
      ));
      break;
  }

  return $link;
}


function bootstrap_subtheme_textarea($variables) {
  $element = $variables['element'];
  $element['#attributes']['name'] = $element['#name'];
  $element['#attributes']['id']   = $element['#id'];
  $element['#attributes']['cols'] = $element['#cols'];
  $element['#attributes']['rows'] = $element['#rows'];
  _form_set_class($element, array('form-textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!(isset($element['#resizable']) && $element['#resizable'] === FALSE)) {
    $wrapper_attributes['class'][] = 'resizable';
    _form_set_class($element, array('resizable'));
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}