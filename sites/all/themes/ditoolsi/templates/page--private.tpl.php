<div class="main-container container-fluid">

  <header role="banner" id="page-header">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . t('DiToolsi') . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>

    <div class="right">
      <div class="phone">
        <span class="phone-icon">&nbsp;</span>
        <span class="number"><?php print variable_get('site_phone'); ?></span>
      </div>
      <div class="support">
        <span class="support-icon">&nbsp;</span>
        <span class="label"><?php print t('Tech. Support') ?></span>
      </div>
    </div>
  </header>

  <aside class="sidebar" id="sidebar">
    <?php
      print theme('ditoolsi_profile_miniprofile', $variables['account']);
    ?>
    <div id="sidebar-menu" class="menu-private-sidebar-menu">
      <?php
        $tree = bootstrap_subtheme_sidebar_menu();
        print drupal_render($tree);
      ?>
    </div>
  </aside>
  <div id="main">
    <div class="page-title">
      <h1><?php print $title; ?></h1>
      <?php
        if ($header_link) {
          print '<div class="header-link">' . $header_link . '</div>';
        }
      ?>
    </div>
    <div class="content">
      <div id="drupal-messages"><?php print $messages; ?></div>
      <?php print render($page['content']); ?>
    </div>
    <footer id="footer">
      <div class="container-fluid clearfix">
        <div class="social-networks col-md-4">
          <span class="label"><?php print t('Social networks:'); ?></span>
          <span class="vkontakte">&nbsp;</span>
          <span class="youtube">&nbsp;</span>
          <span class="facebook">&nbsp;</span>
        </div>
        <div class="payment-methods col-md-4">
          <span class="label"><?php print t('Payment methods:'); ?></span>
          <span class="visa">&nbsp;</span>
          <span class="mastercard">&nbsp;</span>
          <span class="paypal">&nbsp;</span>
          <span class="yamoney">&nbsp;</span>
        </div>
        <div class="copywright col-md-4">
          <span class="label"><?php print date('Y'); ?> &copy; DiToolsi. <?php print t('All rights reserved.'); ?></span>
          <div class="links">
            <span class="project-team"><?php print l(t('Project team'), '<front>'); ?></span>
            <span class="vacancies"><?php print l(t('Vacancies'), '<front>'); ?></span>
          </div>
        </div>
      </div>
    </footer>
  </div>
</div>
