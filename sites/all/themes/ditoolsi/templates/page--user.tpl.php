<div class="main-container container">

  <header role="banner" id="page-header">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . t('DiToolsi') . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-admin-pages',
        ),
      ));
    ?>

    <div class="right">
      <div class="phone">
        <span class="phone-icon">&nbsp;</span>
        <span class="number"><?php print variable_get('site_phone'); ?></span>
      </div>
      <div class="support">
        <span class="support-icon">&nbsp;</span>
        <span class="label"><?php print t('Tech. Support') ?></span>
      </div>
    </div>
  </header> <!-- /#page-header -->

  <div class="row">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php /* print render($tabs); */ ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
