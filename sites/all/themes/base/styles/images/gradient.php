<?php
header('Content-Type:image/svg+xml; charset=utf-8');
header('Expires: 01 Jan ' . (date('Y') + 10) . ' 00:00:00 GMT');
header('Cache-control: max-age=2903040000');
$args = trim($_SERVER['PATH_INFO'], '/');
$args = preg_replace("/\(\s*([^\)]+)\s*,\s*([^\)]+)\s*,\s*([^\)]+)\s*\)/", "($1;$2;$3)", $args);
$args = preg_replace("/\(\s*([^\)]+)\s*,\s*([^\)]+)\s*\)/", "($1;$2)", $args);
$args = explode(',', $args);
$args = preg_replace("/;/", ",", $args);
$dir = trim(array_shift($args));
ob_start();
switch ($dir) {
  case '90deg':
    $dir = 'x1="0%" y1="0%" x2="100%" y2="0%"';
    break;
  case '180deg':
    $dir = 'x1="0%" y1="0%" x2="0%" y2="100%"';
    break;
  case '270deg':
    $dir = 'x1="100%" y1="0%" x2="0%" y2="0%"';
    break;
  case '360deg':
    $dir = 'x1="0%" y1="100%" x2="0%" y2="0%"';
    break;
}
?>
<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1 1" preserveAspectRatio="none">
    <linearGradient id="g" gradientUnits="userSpaceOnUse" <?=$dir?>>
      <?foreach ($args as $stop_color) {
      $stop_color = preg_split('/\s+/', trim($stop_color));
      ?>
        <stop stop-color="<?=$stop_color[0]?>" offset="<?=$stop_color[1]?>"/>
      <? }?>
    </linearGradient>
    <rect x="0" y="0" width="1" height="1" fill="url(#g)"/>
</svg>
<?
header('Content-Encoding:gzip');
$s = ob_get_contents();
ob_clean();
echo gzencode($s, 9);
