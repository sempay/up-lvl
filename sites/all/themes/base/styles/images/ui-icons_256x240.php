<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Punk_UnDeaD
 * Date: 25.09.12
 * Time: 22:24
 * To change this template use File | Settings | File Templates.
 */
header('Content-Type:image/png');
header('Content-Encoding:gzip');
$color = isset($_GET['c']) ? $_GET['c'] : "#000";
$color = preg_replace("/^#(\w+)/", "$1", $color);
$color = preg_replace("/^(\w)(\w)(\w)$/", "$1$1$2$2$3$3", $color);
preg_match_all("/^(\w\w)(\w\w)(\w\w)$/", $color, $color);
//readfile('ui-icons_ffd27a_256x240.png');
$r = hexdec($color[1][0]);
$g = hexdec($color[2][0]);
$b = hexdec($color[3][0]);
$img = imagecreatefrompng('ui-icons_ffd27a_256x240.png');
$imgn = imagecreatetruecolor(imagesx($img), imagesy($img));

imagealphablending($imgn, FALSE);
imagesavealpha($imgn, TRUE);
for ($y = 0; $y < imagesy($img); $y++) {
  for ($x = 0; $x < imagesx($img); $x++) {
    $old = imagecolorat($img, $x, $y);
    $colors = imagecolorsforindex($img, $old);
    $color = imagecolorallocatealpha($imgn, $r, $g, $b, $colors['alpha']);
    imagesetpixel($imgn, $x, $y, $color);
//    imagecolordeallocate($img, $color);
  }
}
//imagetruecolortopalette($imgn, false, 255);
ob_start();
imagepng($imgn);
$s = ob_get_contents();
ob_clean();
echo gzencode($s, 9);
imagedestroy($img);