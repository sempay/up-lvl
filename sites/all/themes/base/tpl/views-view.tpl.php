<?php

?>
<section class="<?= $classes; ?>">
  <?=render($title_prefix); ?>
  <?=$title ? $title : NULL?>
  <?=render($title_suffix); ?>
  <?=$header ? "<header class=view-header>$header</header>" : NULL?>
  <?=$exposed ? "<nav class=view-filters>$exposed</nav>" : NULL;?>
  <?=$attachment_before ? "<div class='attachment attachment-before'>$attachment_before</div>" : NULL?>
  <?=$rows ? "<div class=view-content>$rows</div>" : ($empty ? "<div class=view-empty>$empty</div>" : NULL)?>
  <?=$pager ? $pager : NULL?>
  <?=$attachment_after ? "<div class='attachment attachment-after'>$attachment_after</div>" : NULL?>
  <?=$more ? $more : NULL?>
  <?=$footer ? "<footer class=view-footer>$footer</footer>" : NULL?>
  <?=$feed_icon ? "<div class=feed-icon>$feed_icon</div>" : NULL?>
</section>
