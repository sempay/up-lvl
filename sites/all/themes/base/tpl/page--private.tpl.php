<div class="main-container container-fluid">
  <header class="clearfix">
    <?php
      print l('<div><span class="logo">&nbsp;</span>
      <span class="label">' . '</span></div>', '<front>', array(
        'html' => TRUE,
        'attributes' => array(
          'id' => 'logo-private-pages',
        ),
      ));
    ?>
    <div class="menu">
      <?php
        $menu = menu_tree('menu-header-menu');
        print render($menu);
      ?>  
    </div> 
    <div class="user-block">
      <span class="user-logo"></span>  
    </div> 
  </header>
  <aside class="sidebar" id="sidebar">
    <div id="sidebar-menu" class="menu-private-sidebar-menu clearfix">
      <?php
        $menu = menu_tree('menu-header-menu');
        print render($menu);
      ?>
    <div>
  </aside>
  <div id="main">
    <div class="page-title">
      
    </div>
    <div class="content">
      <div id="drupal-messages"><?php print $messages; ?></div>
      <?php print render($page['content']); ?>  
    </div>
  </div>
  <footer>
    
  </footer>
</div>