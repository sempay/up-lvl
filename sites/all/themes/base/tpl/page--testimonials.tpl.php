<?php
// if(arg(2) == 'edit'){

// }else{
?>
<div id=wrapper>

  <header>
    <?= render($page['header']); ?>
    <div id=logo-floater>
      <?php if ($logo || $site_html): ?>
        <div id=branding><strong><a href="<?= $front_page ?>">
              <?= $logo ? "<img src='$logo' alt='" . strip_tags($site_name_and_slogan) . "' title='" . strip_tags($site_name_and_slogan) . "' class=logo />" : NULL?>
              <?= $site_html ?>
            </a></strong></div>
      <?php endif; ?>
    </div>

    <?= $primary_nav ? "<nav id=primary-nav>$primary_nav</nav>" : NULL ?>
    <?= $secondary_nav ? "<nav id=secondary-nav>$secondary_nav</nav>" : NULL ?>
  </header>
  
  
  <!-- /#header -->
  <div id=container class=clearfix>
	<?php if($node->type == 'testimonials'):?>
		  <div class="reviews_box">
			  <div class="reviews_top">
				  <div class="rev_1">
					  <div class="rev_title">Отзывы о платформе</div>
					  		   <?php $next = base_next_page_link($node);
								$previous = base_previous_page_link($node); ?>
								<?php if($previous !== false): ?>
								<div class="rev_pag_l"><a href="<?=$previous; ?>" class="previouslink">&lt; НАЗАД</a></div>
								<?php endif; ?>
								<div class="rev_video"><?php 
	
								$symbol = 'http://www.youtube.com/embed/';

								@$haystack = $node->field_youtube_link_review['und'][0]['value'];
								$needle   = 'http://youtu.be/';
								$needle2   = 'http://www.youtube.com/watch?v=';

								$pos  = strripos($haystack, $needle);
								$pos2 = strripos($haystack, $needle2);

								if($pos == 0){
									$newString = str_replace('http://youtu.be/', "$symbol", $haystack);
									$video_out = '<iframe src="'.$newString.'" frameborder="0" width="485" height="305"></iframe>';
									print $video_out;
								}

								if($pos2 == 0){
									$newString = str_replace('http://www.youtube.com/watch?v=', "$symbol", $haystack);
									$video_out = '<iframe src="'.$newString.'" frameborder="0" width="485" height="305"></iframe>';
									print $video_out;
								}
								  	
								
								?></div>
								<?php if($next !== false): ?>
									<div class="rev_pag_r"><a href="<?=$next; ?>" class="nextlink">ВПЕРЕД &gt;</a></div>
								<?php endif; ?>
								<?php endif; ?>
				  </div>
				  <div class="rev_2">
					  <div class="rev_block_title">By-step.ru - Лучший выбор!</div>
					  <div class="rev_block_body"><?php print @$node->body['ru'][0]['value'];?></div>
				  </div>
			  </div>
			  <div class="reviews_bottom">
				  <div class="rev_3">
				  	 <?php 
						$user = user_load($node->uid);
						?>
					<div class="rev_user_title_top">Отзыв любезно предоставил:</div>
					<div class="rev_user_img">
					<?php
						$image_settings = array(
							'style_name' => 'user_otziv',
							'path' => @$user->picture->uri,
							'attributes' => array('class' => 'image'),
							'getsize' => FALSE,);
						print theme('image_style', $image_settings);
					?>
					</div>
					<div class="rev_user_tit_box_a">Преподаватель</div>
					<div class="rev_user_fio">ФИО:<span><?php echo @$user->field_fio['und']['0']['safe_value'];?></span></div>
					<div class="rev_user_city">Город:<span><?php echo @$user->field_city['und']['0']['safe_value'];?></span></div>
					<div class="rev_user_company">Компания:<span><?php echo @$user->field_company['und']['0']['safe_value'];?></span></div>
					<div class="rev_user_position">Должность:<span><?php echo @$user->field_position['und']['0']['safe_value'];?></span></div>
					<div class="rev_user_tit_box_b">Курсы преподавателя</div>
					<div class="rev_user_title_bottom">
<!-- 					<a href="#">Бизнес:  «Ошибки начинающих предпренимателей»</a><br/>
					<a href="#">Маркетинг: «От малого к большому. 5 практических советов»</a>
 -->					
					</div>
					<div class="rev_user_body"></div>
				  </div>
				  <div class="rev_4">
					<?php print @render(field_view_field('node', $node, 'field_evaluation_bal')); ?>
				  </div>
			  </div>
		  </div>
		  

		  
		  
    <section id=center>
     <div id=squeeze>	 
	 <?php ?>
        <?//= $breadcrumb; ?>
        <?= $page['highlighted'] ? "<div id=highlighted>" . render($page['highlighted']) . "</div>" : NULL?>
        <a id=main-content></a>
		
        <?//= render($title_prefix); ?>
        <?//= $title ? "<h1>$title</h1>" : '<h1 class=element-invisible>' . strip_tags($site_name_and_slogan) . '</h1>'?>
        <?//= render($title_suffix); ?>

        <?//= $tabs ? "<div id=tabs-wrapper class=clearfix>" . render($tabs) . "</div>" : NULL ?>

        <?= render($tabs2) ?>
        <?= $messages ?>
        <?= render($page['help']) ?>
		<?php //krumo($page['content']);?>
        <?//= $action_links ? "<ul class=action-links>" . render($action_links) . "</ul>" : NULL ?>
 <?//= render($page['content']); ?>

        <div class=clearfix>
         
		  <!-- /#reviews-->
        </div>
        <?= $feed_icons ?>
     </div>
    </section>
    <?= $page['sidebar_first'] ? "<aside id=sidebar-first>" . render($page['sidebar_first']) . "</aside>" : NULL?>
    <?= $page['sidebar_second'] ? "<aside id=sidebar-second>" . render($page['sidebar_second']) . "</aside>" : NULL?>
  <?php //$inode = custom_pagin_pager();
 // print $inode;
	//krumo($inode);
  ?>
</div>
  <!-- /#container -->
  <footer>
    <?= render($page['footer']); ?>
  </footer>
</div> <!-- /#wrapper -->

<?php //} ?>