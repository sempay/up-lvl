<?php
// $Id: comment.tpl.php,v 1.21 2010/12/01 00:18:15 webchick Exp $
hide($content['links']);
?>
<article class="<?= "$classes $zebra"; ?>"<?= $attributes; ?>>
  <div class=clearfix>
    <span class=submitted><?= $submitted ?></span>

    <?= $new ? "<span class=new>" . drupal_ucfirst($new) . "</span>" : NULL?>

    <?= $picture ?>

    <?= render($title_prefix); ?>
    <h3<?= $title_attributes; ?>><?= $title ?></h3>
    <?= render($title_suffix); ?>

    <div class="content"<?= $content_attributes; ?>>
      <?=render($content); ?>
      <?= $signature ? "<div class=clearfix><div>—</div>$signature</div>" : NULL?>
    </div>
  </div>

  <?= render($content['links']) ?>
</article>