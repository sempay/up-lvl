<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>

<?php

?>

<div class="profile"<?php print $attributes; ?>>

  <div id="user-picture">
    <?php
    global $base_url;
    global $user;


    $select = db_select('ul_user_messages', 'ul');
    $select->condition('ul.recive_uid', $user->uid);
    $select->condition('ul.status', 0);
    $select->addExpression('COUNT(ul.mid)');
    $msgs = $select->execute()->fetchObject();

    $msg = '';
    if($msgs->expression > 0){
      $msg = '<div class="u_new_msgs"><span>'.$msgs->expression.'</span></div>';
    }

    $user_pic = user_load(arg(1));

    $image_params = array('style_name' => '195x300_sc', 'path' => @$user_pic->field_user_image[LANGUAGE_NONE][0]['uri'], 'attributes' => array('class' => 'image-user'));
    print $user_image =  theme('image_style', $image_params);
    if(arg(0) == 'user' && arg(1) == $user->uid){
      print '<div class="u_ms"><a href='.$base_url.'/im class="edit-user">Мои сообщения</a></div>'.$msg;
    }
    // print render($user_profile['user_picture']);
    ?>
  </div>

  <div id="second-content">
    <div class="user-info">
      <label class="user-info-title">Информация о пользователе</label>
      <?php
        print render($user_profile['field_fio']);
        print render($user_profile['field_old']);
        print render($user_profile['field_city']);
        // print render($user_profile['field_fio']);
      ?>
    </div>
    <div class="user-contact-info">
      <label class="user-contact-title">Контактная информация</label>
      <?php
        print render($user_profile['field_skype']);
        print render($user_profile['field_phone_number']);
      ?>
    </div>
  </div>

  <div id="user-more-info">
    <label class="user-contact-title">Дополнительная информация</label>
    <div class="job-exp">
      <?php
        print render($user_profile['field_company']);
        print render($user_profile['field_position']);
        print render($user_profile['field_education']);
        print render($user_profile['field_foreign_languages']);
      ?>
    </div>
    <div class="more-fields"><?php print render($user_profile['field_exp']); ?></div>
  </div>
  <div id="user-courses">
    <?php if(array_key_exists(4, $user->roles)): ?>

      <?php $user_courses = module_invoke('by_step', 'block_view', 'user_page_courses'); //print $user_courses['content']; ?>
      <?php print theme('ctools_collapsible', array('handle' => '<label class="user-courses-title">Курсы преподавателя</label>', 'content' => $user_courses['content'], 'collapsed' => TRUE )); ?>

    <?php endif; ?>
    <?php if(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)): ?>
      <?php $user_courses = module_invoke('by_step', 'block_view', 'user_page_courses'); //print $user_courses['content']; ?>
      <?php print theme('ctools_collapsible', array('handle' => '</span><label class="user-courses-title">Курсы ученика</label>', 'content' => $user_courses['content'], 'collapsed' => TRUE )); ?>
    <?php endif; ?>

  </div>

  <div id="user-soc-about">
    <div class="user-soc">
      <label class="user-courses-title">Мои страницы в соц.сетях</label>
      <div class="soc-vk">
      <?php
        if(!empty($user_profile['field_vkontakte_soc_user'])){
          $image_vk = drupal_get_path('theme', 'base') . '/styles/images/vk.png';
          $arbitrary_array_vk = array(
            'path' => $image_vk,
            'width' => '40px',
            'height' => '40px',
            'attributes' => array('class' => 'img-vk', 'id' => 'img-vk'),
          );
          $img_vk  = theme_image($arbitrary_array_vk);
          print '<span>'.l($img_vk, $user_profile['field_vkontakte_soc_user']['#items'][0]['value'], array('attributes' => array('target' => '_blank'),'html'=> 'true')).'</span>';
        }
      ?>
      </div>
      <div class="soc-fb">
      <?php
        if(!empty($user_profile['field_facebook_soc_user'])){
          $image_fb = drupal_get_path('theme', 'base') . '/styles/images/fb.png';
          $arbitrary_array_fb = array(
            'path' => $image_fb,
            'width' => '40px',
            'height' => '40px',
            'attributes' => array('class' => 'img-fb', 'id' => 'img-fb'),
          );
          $img_fb  = theme_image($arbitrary_array_fb);
          print '<span>'.l($img_fb, $user_profile['field_facebook_soc_user']['#items'][0]['value'], array('attributes' => array('target' => '_blank'),'html'=> 'true')).'</span>';
        }
      ?>
      </div>
      <div class="soc-tw">
      <?php
        if(!empty($user_profile['field_twitter_soc_user'])){
          $image_tw = drupal_get_path('theme', 'base') . '/styles/images/tr.png';
          $arbitrary_array_tw = array(
            'path' => $image_tw,
            'width' => '40px',
            'height' => '40px',
            'attributes' => array('class' => 'img-tr', 'id' => 'img-tr'),
          );
          $img_tw  = theme_image($arbitrary_array_tw);
          print '<span>'.l($img_tw, $user_profile['field_twitter_soc_user']['#items'][0]['value'], array('attributes' => array('target' => '_blank'),'html'=> 'true')).'</span>';
        }
      ?>
      </div>
      <div class="soc-od">
      <?php
        if(!empty($user_profile['field_soc_class'])){
          $image_cm = drupal_get_path('theme', 'base') . '/styles/images/od.png';
          $arbitrary_array = array(
            'path' => $image_cm,
            'width' => '40px',
            'height' => '40px',
            'attributes' => array('class' => 'img-od', 'id' => 'img-od'),
          );
          $img  = theme_image($arbitrary_array);
          print '<span>'.l($img, $user_profile['field_soc_class']['#items'][0]['value'], array('attributes' => array('target' => '_blank'),'html'=> 'true')).'</span>';
        }
      ?>
      </div>
      <div class="soc-description">(Нажми кнопку для перехода)</div>
    </div>

    <?php if(!empty($user_profile['field_pay_information']['#items'][0]['value'])): ?>
      <div class="user-about">
        <label class="user-courses-title">О себе:</label>
        <div class="content"><?php print $user_profile['field_pay_information']['#items'][0]['value'];  ?></div>
      </div>
    <?php endif; ?>

  </div>
<?php
  // print render($user_profile);
  // echo '<h2>Ratamahatta wa/s here:</h2><pre>'; print_r($user_pic->picture->uri); echo '</pre>';
?>
</div>
