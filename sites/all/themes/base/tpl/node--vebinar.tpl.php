<?php
hide($content['comments']);
hide($content['links']);
?>

<article id=node-<?= $node->nid; ?> class="<?= $classes; ?>" <?= $attributes; ?>>

  <?= $user_picture; ?>

  <?= render($title_prefix); ?>
  <?= $page ? NULL : "<h2 $title_attributes><a href='$node_url'>$title</a></h2>"?>
  <?= render($title_suffix); ?>

  <?=$display_submitted ? "<span class=submitted>$submitted</span>" : NULL ?>

  <div class="content clearfix" <?= $content_attributes; ?>>
    <?=render($content);?>
  </div>

  <div class=clearfix>
    <?= $content['links'] ? "<div class=links>" . render($content['links']) . "</div>" : NULL ?>
    <?= render($content['comments']); ?>
  </div>

</article>
