<?php

/**
 * @file
 * Default theme implementation to present a picture configured for the
 * user's account.
 *
 * Available variables:
 * - $user_picture: Image set by the user or the site's default. Will be linked
 *   depending on the viewer's permission to view the user's profile page.
 * - $account: Array of account information. Potentially unsafe. Be sure to
 *   check_plain() before use.
 *
 * @see template_preprocess_user_picture()
 *
 * @ingroup themeable
 */
?>
<div class="user-picture">
<?php if ($user_picture): ?>
    <?php    print $user_picture;    ?>
<?php endif; ?>
<?php
  // global $user;
  // global $base_url;
  //   if(arg(0) == 'user' || arg(1) == $user->uid){
  //     print '<div><a href='.$base_url.'/user/' . $user->uid . '/edit class="edit-user">Редактировать</a></div>';
  //   }
?>
</div>
