<?php
  $block = module_invoke('by_step', 'block_view', 'left_user_menu');
  $pupil_class = '';
  if(arg(0) == 'user' && is_numeric(arg(1))){
    $user = user_load(arg(1));
    if(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
      $pupil_class = 'pupil';
    }
  }

?>

<div id=wrapper class="<?php print $pupil_class; ?>">

  <?php if (arg(0) == 'write' || arg(1) == 'tendency' || arg(0) == 'im' || arg(0) == 'dialog' || arg(0) == 'statistic' || arg(0) == 'project-team' || arg(1) == 'send-messages' || arg(0) == 'welcome' ||arg(0) == 'pupil-welcome'|| arg(0) == 'pupil-list' || arg(0) == 'add-lessons' || arg(0) == 'edit-task' || arg(0) == 'edit-course' || arg(0) == 'curator-list' || arg(0) == 'curator-add' || arg(0) == 'add-students' || arg(0) == 'pupil-course' || arg(1) == 'check-course' || arg(0) == 'courator' || arg(1) == 'view-history'): ?>
    <div id="block-by-step-left-user-menu">
      <div class="content">
        <?= render($block['content']); ?>
      </div>
    </div>
  <?php endif; ?>

  <header>
    <?= render($page['header']); ?>
    <div id=logo-floater>
      <?php if ($logo || $site_html): ?>
        <div id=branding><strong><a href="<?= $front_page ?>">
              <?= $logo ? "<img src='$logo' alt='" . strip_tags($site_name_and_slogan) . "' title='" . strip_tags($site_name_and_slogan) . "' class=logo />" : NULL?>
              <?= $site_html ?>
            </a></strong></div>
      <?php endif; ?>
    </div>

    <?= $primary_nav ? "<nav id=primary-nav>$primary_nav</nav>" : NULL ?>
    <?= $secondary_nav ? "<nav id=secondary-nav>$secondary_nav</nav>" : NULL ?>
  </header>
  <!-- /#header -->
  <div id="container" class="clearfix">

    <section id="center">
      <div id="squeeze">
        <?//= $breadcrumb; ?>
        <?= $page['highlighted'] ? "<div id=highlighted>" . render($page['highlighted']) . "</div>" : NULL?>
        <a id="main-content"></a>

        <?= render($title_prefix); ?>
        <?= $title ? "<h1>$title</h1>" : '<h1 class=element-invisible>' . strip_tags($site_name_and_slogan) . '</h1>'?>
        <?= render($title_suffix); ?>

        <?= $tabs ? "<div id=tabs-wrapper class=clearfix>" . render($tabs) . "</div>" : NULL ?>

        <?= render($tabs2) ?>
        <?= $messages ?>
        <?= render($page['help']) ?>

        <?= $action_links ? "<ul class=action-links>" . render($action_links) . "</ul>" : NULL ?>

        <div class="clearfix">
          <?= render($page['content']); ?>
        </div>
        <?= $feed_icons ?>
      </div>
    </section>
    <?= $page['sidebar_first'] ? "<aside id=sidebar-first>" . render($page['sidebar_first']) . "</aside>" : NULL?>
    <?= $page['sidebar_second'] ? "<aside id=sidebar-second>" . render($page['sidebar_second']) . "</aside>" : NULL?>
  </div>
  <!-- /#container -->
  <footer>
    <?= render($page['footer']); ?>
  </footer>
</div> <!-- /#wrapper -->