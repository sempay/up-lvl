<?php
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/sites/all/modules/custom_front_page/plugin/bxslider/jquery.bxslider.css"/>
<script type="text/javascript" src="/sites/all/modules/custom_front_page/plugin/bxslider/jquery.bxslider.min.js"></script>

<div class="front_page">
  <?//= $breadcrumb; ?>
  <?= $page['highlighted'] ? "<div id=highlighted>" . render($page['highlighted']) . "</div>" : NULL ?>
  <a id="main-content"></a>

  <?= render($title_prefix); ?>
  <?= $title ? "<h1>$title</h1>" : '<h1 class=element-invisible>' . strip_tags($site_name_and_slogan) . '</h1>'?>
  <?= render($title_suffix); ?>

  <?= $tabs ? "<div id=tabs-wrapper class=clearfix>" . render($tabs) . "</div>" : NULL ?>

  <?= render($tabs2) ?>
  <?= $messages ?>
  <?= render($page['help']) ?>

  <?= $action_links ? "<ul class=action-links>" . render($action_links) . "</ul>" : NULL ?>
  <?= $messages ?>

  <div class="front_menu">
    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><img src="/sites/all/modules/custom_front_page/img/logo.png"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="hidden">
                <a href="#page-top"></a>
              </li>
              <li class="page-scroll">
                <a href="#Jvideos">О платформе</a>
              </li>
              <li class="page-scroll">
                <a href="#Jproblem">Ваши задачи</a>
              </li>
              <li class="page-scroll">
                <a href="#Jreshen">Наше решение</a>
              </li>
              <li class="page-scroll">
                <a href="#Jlist">Преимущества СДО</a>
              </li>
              <li class="page-scroll">
                <a href="#Jfeadback">Отзывы</a>
              </li>
              <li class="page-scroll">
                <?php
                  global $user; 
                  if (user_is_anonymous()){
                    print '<a href="/user" role="button" class="btn btn-default go">Войти</a>';
                  } else {
                    print l($user->mail, "user/$user->uid", array('html' => 'true'));
                  }
                ?>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
  </div>
  <div id="Jvideos"></div>
  <?php render($page['header']); ?>
  <header id="Jvideo">
    <div class="bg-picture">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="yb-video">
              <iframe frameborder="0" height="350" src="http://www.youtube.com/embed/uvElFs1qzKc?&amp;disablekb=1&amp;rel=0&amp;showinfo=0" width="48%"></iframe>
            </div>
            <div class="intro-text">
              <h2 class="text">Так ефективно вы еще </br> не обучались никогда!</h2>
              <div class="button">
                <div class="tryn-block"><a href="/user" role="button" class="btn btn-default tryn">Попробовать</br>сейчас</a></div>
              </div>
              <div class="text">Создание образовательных программ </br> на основе пошаговых алгоритмов </br> с высокой степенью автоматизации</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section id="Jproblem">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
            <h2 class="sub-title"><?php print variable_get('who_must_use', 'All'); ?> Кому стоит </br> воспользоватся?</h2>
            <hr class="red">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_1.png" class="img-responsive centred" alt="">
          <h2 class="text">Малому и</br> среднему бизнесу</h2>
          <hr class="red">
          <div class="text">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_2.png" class="img-responsive centred" alt="">
          <h2 class="text">Учебным</br>заведениям</h2>
          <hr class="red">
          <div class="text">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_3.png" class="img-responsive centred" alt="">
          <h2 class="text">Тренинговым</br>центрам и тренерам</h2>
          <hr class="red">
          <div class="text">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-6 problem-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4_4.png" class="img-responsive centred" alt="">
          <h2 class="text">Частным</br>репетиторам</h2>
          <hr class="red">
          <div class="text">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
      </div>
      <div class="row col-centered">
        <div class="tryn-block button"><a href="/user" role="button" class="btn btn-default tryn">Попробовать</br>сейчас</a></div>
      </div>
    </div>
  </section>
  <section id="Jreshen">
    <div class="bg-picture2">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="sub-title banner margin-tb-50">99% нашых клиентов рекомендуют </br>нас в первый месяц тестирования</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jcool">
    <div class="bg-picture">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="intro-text">
              <h2 class="text">С нами зарабатывают</br> нас рекомендуют</h2>
              <hr class="red banner">
              <div class="text">эти люди по достоинству оценили все преимущества нашего сервиса</br>Обучая сотни людей ежедневно и получая прибыль на полном автомате</div>
              <script type="text/javascript">
                jQuery(document).ready(function(){
                  jQuery('.slider .bxslider').bxSlider({
                    slideWidth: 300,
                    minSlides: 3,
                    maxSlides: 3,
                    moveSlides: 1,
                    slideMargin: 1,
                    pager: false,
                    control: true
                  });
                });
              </script>
              <!-- <div class="slider" style="width:820px;"> -->
              <div class="slider">
                <ul class="bxslider">
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">FFАлексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                  <li>
                    <div class="octagon">
                      <img src="/sites/all/modules/custom_front_page/img/pic02.jpg" width="200" height="200">
                    </div>
                    <div class="text name">Алексей Дементьев</div>
                    <div class="text job">Инвобизнесмен</div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php //render($page['header']); ?>
    </div>
  </section>
  <section id="Jfact">
    <div class="bg-picture2">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="sub-title banner">Интересные факты</h2>
            <hr class="red">
            <div class="box">
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_4.png">
                <div class="text number">7085</div>
                <div class="text">Довольных </br>учеников</div>
              </div>
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_3.png">
                <div class="text number">78</div>
                <div class="text">Успашных </br>авторов</div>
              </div>
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_2.png">
                <div class="text number">236</div>
                <div class="text">Работающих </br>курсов</div>
              </div>
              <div class="col-sm-3">
                <img src="/sites/all/modules/custom_front_page/img/3_1.png">
                <div class="text number">1250</div>
                <div class="text">Положытельных </br>отзывов</div>
              </div>
            </div>
          </div>
        </div>
        <div class="row margin-tb-60">
          <div class="col-lg-12 text-center">
            <div class="button">
              <div class="arrow l"></div>
              <div class="tryn-block"><a href="/user" role="button" class="btn btn-default tryn">Попробовать</br>сейчас</a></div>
              <div class="arrow r"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jlist">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="sub-title">Основные преимущества</h2>
          <hr class="red bold">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/8.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/7.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/6.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/5.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/4.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/3.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/2.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
        <div class="col-sm-3 list-item col-centered">
          <img src="/sites/all/modules/custom_front_page/img/1.png" class="img-responsive centred" alt="">
          <h2 class="text">экономия</br>времени</h2>
          <hr class="red">
          <div class="text grey">Проведение обучающих курсов. Вашых</br>сотрудников, проведение тренингов и семинаров</div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jprice">
    <div class="bg-picture">
      <div class="container">
    <div class="row margin-tb-30">
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-blue">
            <h2 class="heading-md">FREE</h2>
            <h3><span class="pr">0</span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>Только Promo курсы</li>
              <li>Курсов на акаунте - 1</li>
              <li>До 30 учеников</li>    
              <li>Кураторов - 1</li>
              <li>Обьем данных - 500 МБ</li>
            </ul>
            <a href="#" class="btn btn-default btn-price">Заказать</a>                  
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-sea">
            <h2 class="heading-md">STANDART</h2>
            <h3><span class="pr">790</span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>Только Promo курсы</li>
              <li>Курсов на акаунте - 1</li>
              <li>До 30 учеников</li>    
              <li>Кураторов - 1</li>
              <li>Обьем данных - 500 МБ</li>
            </ul>
            <a href="#" class="btn btn-default btn-price">Заказать</a>                     
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-sea">
            <h2 class="heading-md">STANDART</h2>
            <h3><span class="pr">790</span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>Только Promo курсы</li>
              <li>Курсов на акаунте - 1</li>
              <li>До 30 учеников</li>    
              <li>Кураторов - 1</li>
              <li>Обьем данных - 500 МБ</li>
            </ul>
            <a href="#" class="btn btn-default btn-price">Заказать</a>                   
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 col-width-full margin-tb-30">
        <div class="pricing-table-v6 v6-plus">
          <div class="service-block service-block-sea">
            <h2 class="heading-md">STANDART</h2>
            <h3><span class="pr">790</span><span class="cr">руб.</span></h3>
            <ul class="list-unstyled pricing-v4-content">
              <li>Только Promo курсы</li>
              <li>Курсов на акаунте - 1</li>
              <li>До 30 учеников</li>    
              <li>Кураторов - 1</li>
              <li>Обьем данных - 500 МБ</li>
            </ul>
            <a href="#" class="btn btn-default btn-price">Заказать</a>                  
          </div>
        </div>
      </div>
    </div>
      </div>
      <?php //render($page['header']); ?>
    </div>
  </section>
  <section id="Jfeadback">
    <div class="bg-picture2">
      <div class="container">
        <div class="row">
          <div class="feadbacks-img margin-tb-40">
            <img src="/sites/all/modules/custom_front_page/img/22.png">
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
              <div class="intro-text">
                <script type="text/javascript">
                  jQuery(document).ready(function(){
                    jQuery('.slider2 .bxslider').bxSlider({
                      moveSlides: 1,
                      autoReload: true,
                      slideMargin: 1,
                      pager: true,
                      control: false
                    });
                  });
                </script>
                <div class="slider2 margin-tb-50">
                  <ul class="bxslider">
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                    <li>
                      <div class="feadback-text">
                        <p>
                          Praesent adipiscing. Donec mollis hendrerit risus. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Curabitur ullamcorper ultricies nisi. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci.
                        Sed cursus turpis vitae tortor. Fusce commodo aliquam arcu. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Curabitur nisi. Morbi mollis tellus ac sapien.
                        Nulla facilisi. Praesent nonummy mi in odio. Nullam tincidunt adipiscing enim. Vivamus quis mi. Etiam ut purus mattis mauris sodales aliquam.
                        Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Etiam rhoncus. Etiam ut purus mattis mauris sodales aliquam. Sed a libero. Maecenas malesuada.
                        Suspendisse potenti. Aliquam erat volutpat. Phasellus a est. Vivamus aliquet elit ac nisl. Sed hendrerit.
                        </p>
                      </div>
                      <div class="text name">Алексей Дементьев</div>
                      <div class="text job">Инвобизнесмен</div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </section>
  <section id="Jcontact">
    <div class="bg-picture">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="text sub-title">Напишыте нам</h2>
            <hr class="blue">
          </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
          <?php 
            $node = node_load(10593);
            webform_node_view($node, 'full');
            $node_rendered = theme_webform_view($node->content);
            print render($node_rendered);
          //drupal_set_message('<pre>'.print_r(node_load(10593),1).'</pre>');
          ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="container">

  </div>
  <footer>
    <div class="bg-picture-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-centred">
            <div class="col-sm-3">
              <img src="/sites/all/modules/custom_front_page/img/logo.png">
            </div>
            <div class="col-sm-9 footer-menu">
              <ul class="nav navbar-nav footer">
                <li class="hidden">
                  <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                  <a href="#Jvideos">О платформе</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jproblem">Ваши задачи</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jreshen">Наше решение</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jlist">Преимущества СДО</a>
                </li>
                <li class="page-scroll">
                  <a href="#Jfeadback">Отзывы</a>
                </li>
                <li class="page-scroll">
                  <?php
                    global $user; 
                    if (user_is_anonymous()){
                      print '<a href="/user" role="button" class="btn btn-default go">Войти</a>';
                    } else {
                      print l($user->mail, "user/$user->uid", array('html' => 'true'));
                    }
                  ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
          <div class="row margin-tb-40">
            <div class="col-sm-12">
            <div class="col-sm-4">
              <p style="float: left;">СОЦИАЛЬНЫЕ СЕТИ:</p>
              <img src="/sites/all/modules/custom_front_page/img/w.PNG">
              <img src="/sites/all/modules/custom_front_page/img/wqe.png">
            </div>
            <div class="col-sm-4">
              <p style="float: left;">СПОСОБЫ ОПЛАТЫ:</p>
              <img src="/sites/all/modules/custom_front_page/img/wq.PNG">
              <img src="/sites/all/modules/custom_front_page/img/mastercard.PNG">
            </div>
            <div class="col-sm-4">
              <p style="float: left;">2014 © ПРОВОЛОГИЯ. ВСЕ ПРАВА ЗАЩИЩЕНЫ.</p>
            </div>
              <?php //render($page['footer']); ?>
            </div>
          </div>
      </div>
    </div>
  </footer>
</div>