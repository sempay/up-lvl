<div class="reg_form_teach_pupil"><p>Вы можете зарегистрироватся на платформе дистанционного обучения в качестве:</p>
  <div class="teachr_box">
    <div class='teacher_icon'></div>
    <div class='teacher_botton'><a href='/teacher/register'>Преподавателя</a></div>
    <p>для создания программы обучения</p>
  </div>
  <div class="pupil_box">
    <div class='pupil_icon'></div>
    <div class='pupil_botton'><a href='/pupil/register'>Ученика</a></div>
    <p>для прохождения обучения</p>
  </div>
</div>
<div class="first-step-text">
<?php 
    @$main_register_body = variable_get('main_register_body','');
    print $main_register_body['value'];
  ?>
</div>


<div class='clear'></div>

<?php 
  
  $text_for = <<<html
<p>Наша техническая поддержка доступна 24 часа в сутки 7 дней в неделю. Мы тщательно охраняем Ваше спокойствие и комфорт учеников.</p>
html;


  @$pupil_register_body_1 = variable_get('pupil_register_body_1', '');
  @$pupil_register_body_2 = variable_get('pupil_register_body_2', '');

  @$teacher_register_body = variable_get('teacher_register_body', '');

?>
<?php if (arg(0) == 'pupil' && arg(1) =='register'){
  print "<div id='reg-form' class='pupil-form'><div class='strell_pupil'></div>";
  print '<div class="base-user-login-form-wrapper">' . $pupil_register_body_1['value'] . '<p class="p_reg">Отправить заявку на обучение:</p>';
  print drupal_render_children($form);
  print '<div class="clear"></div>' . $pupil_register_body_2['value'] . '</div>';
  print '</div>';
}
elseif(arg(0) == 'teacher' && arg(1) =='register'){
  print "<div id='reg-form' claSS='teach-form'><div class='strell_teacher'></div>";
  print "<div class='teacher_reg_form' style='margin-top: 5px;'>";
  print $teacher_register_body['value'];
  print drupal_render_children($form);
  print $text_for;
  print "</div></div>";
};
?>