<?php
?>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '156712';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();
</script>
<!-- {/literal} END JIVOSITE CODE -->

<div class="front_page_wrapper">
  <?= $messages ?>
  <div class="fixed_menu">
    <ul class="menu clearfix"><li class="first leaf"><a href="#Jvideo">О платформе</a></li>
      <li class="leaf"><a href="#Jproblem">Ваши задачи</a></li>
      <li class="leaf"><a href="#Jreshen">Наше решение</a></li>
      <li class="leaf"><a href="#Jcool">Преимущества СДО</a></li>
      <li class="leaf"><a href="#Jotzivi">Отзывы</a></li>
      <?php global $user; ?>
      <?php if ($user->uid == 0){
        print '<li class="last leaf"><a href="/user">Вход/Регистрация</a></li>';
      }else{
        print '<li class="last leaf">'.l($user->mail, "user/$user->uid", array('html' => 'true')).'</li>';
      }?>
    </ul>
  </div>
  <div class="top_info_block">
    <div id="name_site"><a href="/"></a>
    </div>
    <div id="vvod_slovo" class="center">
    <p class="first">Система дистанционного <br>
    обучения нового поколения:</p>
    <p class="second">Создание образовательных программ на основе пошаговых алгоритмов с высокой степенью автоматизации! <br>
    <strong>Так эффективно Вы еще не обучали никогда!</strong></p>
    </div>
    <div id="phone_zakaz">
    <p class="phone">8 (812) <span class="gray">649-93-10</span></p>
    <p class="phone">8 (495) <span class="gray">649-93-10</span></p>
    <p class="zakaz"><a href="" onclick="return false;">Заказать звонок</a></p>
    </div>
    <div class="zakaz_form">
      <div class="title">Заказать звонок </div>
      <div class="close"></div>
      <?php
        $call = node_load(3757);
        webform_node_view($call, 'full');
        $call_rendered = theme_webform_view($call->content);
        print render($call_rendered);
      ?>
    </div>
  </div>
  <div class="video_and_reg_block">
    <a class="Jjj" name="Jvideo" id="Jvideo"></a>
    <div class="video_reg_wrapper">
      <div id="video_main">
        <?php
          $block = module_invoke('block', 'block_view', 14);
          print $block['content'];
        ?>
      </div>
      <div id="form_for_test">
        <p class="center">Приступить к <strong>месяцу бесплатного</strong> тестирования платформы</p>
        <?php
          $test = node_load(3758);
          webform_node_view($test, 'full');
          $test_rendered = theme_webform_view($test->content);
          print render($test_rendered);
        ?>
      </div>
    </div>
  </div>
  <div class="recommendations">
    <?php
      $block = module_invoke('block', 'block_view', 15);
      print $block['content'];
    ?>
  </div>
  <div class="problems">
    <?php
      $block = module_invoke('block', 'block_view', 16);
      print $block['content'];
    ?>
  </div>
  <div class="steps">
    <?php
      $block = module_invoke('block', 'block_view', 17);
      print $block['content'];
    ?>
  </div>
  <div class="benefits">
    <?php
      $block = module_invoke('block', 'block_view', 18);
      print $block['content'];
    ?>
  </div>
  <div class="statistic_image">
    <?php
      $block = module_invoke('block', 'block_view', 19);
      print $block['content'];
    ?>
  </div>
  <div class="partners">
    <p class="center zag">Компании, оценившие по достоинству все наши плюсы:</p>
    <div id="comp"></div>
  </div>
  <div class="reviews">
    <?php
      $block = module_invoke('block', 'block_view', 20);
      print $block['content'];
    ?>
  </div>
  <div class="footer">
    <div class="content">
      <p><a class="but_test center" href="#Jvideo">Приступить к бесплатному тестированию</a></p>
      <hr>
      <p class="rtecenter"><span style="font-size:11px;"><a href="https://drive.google.com/file/d/0B68QhTrcXS1sSlJ0U1ZRM0VVX28/edit?usp=sharing" target="_blank">Политика Конфеденциальности</a></span></p>
      <p class="rtecenter"><span style="font-size:11px;"><a href="https://drive.google.com/file/d/0B68QhTrcXS1sY2dEWXVrampQZEk/edit?usp=sharing" target="_blank">Пользовательское положение</a></span></p>
    </div>
  </div>
</div>
<div id="toTop">^ Наверх</div>

