<!DOCTYPE html>
<html lang="<?= $language->language; ?>" dir="<?= $language->dir;?>">
<head>
  <?= $head; ?>
<title><?= strip_tags($head_title); ?></title>
  <?= $styles; ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <!--  <script type="text/javascript" src="/sites/all/libraries/ckeditor/ckeditor.js"></script> -->
  <!--  <script type="text/javascript" src="/sites/all/libraries/ckeditor/adapters/jquery.js"></script> -->

  <?= $scripts; ?>


<?php if(arg(0) == 'pupil-video'): ?>
  <script  src="http://dev.up-lvl.ru/sites/all/libraries/mediaelement/build/mediaelement-and-player.min.js"></script>
  <script  src="http://dev.up-lvl.ru/sites/all/libraries/mediaelement/build/flashmediaelement.swf"></script>
  <link  rel="stylesheet" href="http://dev.up-lvl.ru/sites/all/libraries/mediaelement/build/mediaelementplayer.css"  media="screen">
<?php endif; ?>

<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery(".up-lvl-carousel").fadeIn();
  });
</script>

<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>

</head>
<body class="<?= $classes; ?>" <?= $attributes;?>>
  <div id=skip-link>
    <a href="#main-content" class="element-invisible element-focusable"><?= t('Skip to main content'); ?></a>
  </div>
  <?php if(!empty($sys_msg)) print $sys_msg; ?>
  <?= $page_top; ?>
  <?= $page; ?>
  <?= $page_bottom; ?>
</body>
</html>
