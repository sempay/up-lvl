(function ($) {
  $(function (){

  });


  // register choose AJAX form change
  $(document).ready(function(){
    $('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .form-type-textfield #edit-field-youtube-link-review-und-0-value').attr('placeholder', 'ссылка на видео');
  });

  $('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .add-youtube').live('click', function(){
    var youparsID = $('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .form-type-textfield #edit-field-youtube-link-review-und-0-value').val();
    var url = youparsID;
    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    if(videoid != null) {
      //console.log("video id = ",videoid[1]);
    } else {
      alert("The youtube url is not valid.");
    }
    //$('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .second-step .plus-img').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe>');
    $('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .second-step .plus-img').html('<img class="alignnone" title="0" alt="Preview" width="120" height="120" src="http://img.youtube.com/vi/'+ videoid[1] +'/1.jpg">');
  });

  $('#by-step-curator-form #edit-curator-list a.upload.button').live('click', function(){
    var youparsID = $('#by-step-curator-form #edit-curator-list #edit-curator-list-video').val();
    var url = youparsID;
    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    if(videoid != null) {
      //console.log("video id = ",videoid[1]);
    } else {
      alert("The youtube url is not valid.");
    }
    //$('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .second-step .plus-img').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe>');
    $('#by-step-curator-form #edit-curator-list .curator-list-video-preview').html('<img class="alignnone" title="0" alt="Preview" width="120" height="120" src="http://img.youtube.com/vi/'+ videoid[1] +'/1.jpg">');
  });
  //  End


  // jquery choosen stylin select pupil edit
  $(document).ready(function(){


    $("#user-profile-form").each(function() {
      $('#edit-field-select-user-lessons-und').chosen();
    });

    //$("#course-node-form").each(function() {
      $('#course-node-form #edit-field-course-type-und').chosen();
    //});

    //$("#lesson-node-form").each(function() {
      $(' #lesson-node-form #edit-field-course-type-und').chosen();
    //});

    $("#curator-search-result .curator-list").each(function() {
      $('.form-select').chosen();
    });

    //$(".page-add-task #block-slide-task .select-task .form-type-select").each(function() {
      $('.page-add-task #block-slide-task .select-task .form-type-select .form-select').chosen();
    //});

    $('#edit-field-type-selling-course-und').chosen();
    $('#edit-field-field-cours-study-und').chosen();
    $('#edit-field-course-study-und').chosen();


    $('.form-item-lesson-select #edit-lesson-select').chosen({
      disable_search: true,
      width: "100%"
    });
    //$('#block-by-step-block-courses .course-description').dotdotdot();
  });
  //  End


  //  Lesson add or edit lesson
  //----------------------------------------
  $(document).ready(function(){
    setTimeout(function() {
      $('#block-system-main .course-content .course-description').readmore({
        moreLink: '<a href="#">Читать полностью</a>',
         lessLink: '<a href="#">свернуть</a>',
        maxHeight: 150,
        afterToggle: function(trigger, element, more) {
          if(! more) { // The "Close" link was clicked
            $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
          }
        }
      });
    }, 100);

    $('.video-instruction .instruction-title .plus.closed').live('click', function(){
      $('.instruction-second-col').slideDown("slow");
      $(this).removeClass('closed');
      $(this).addClass('open');
    });

    $('.video-instruction .instruction-title .plus.open').live('click', function(){
      $('.instruction-second-col').slideUp("slow");
      $(this).removeClass('open');
      $(this).addClass('closed');
    });

    $('.add-student-title .plus.closed, .list-settings-title .plus.closed, .curator-add-settings .list-settings-title .plus.closed').live('click', function(){
      $('.curator-list-settings, .curator-add-form, .student-add-settings').slideDown("slow");
      $(this).removeClass('closed');
      $(this).addClass('open');
    });

    $('.add-student-title .plus.open, .list-settings-title .plus.open, .curator-add-settings .list-settings-title .plus.open').live('click', function(){
      $(this).removeClass('open');
      $(this).addClass('closed');
      $('.curator-list-settings, .curator-add-form , .student-add-settings').slideUp("slow");
    });

    $('.approve-student-title .plus.closed').live('click', function(){
      $('.student-approve-settings').slideDown("slow");
      $(this).removeClass('closed');
      $(this).addClass('open');
    });

    $('.approve-student-title .plus.open').live('click', function(){
      $(this).removeClass('open');
      $(this).addClass('closed');
      $('.student-approve-settings').slideUp("slow");
    });


    $(' #block-with-lessons .add-more-lessons').live('click', function(){
      $('.page-add-lessons #lesson-node-form ').fadeIn('slow');

      $('html,body').animate({
        scrollTop: $("#lesson-node-form").offset().top
      });
    });

    $(' #block-with-lessons .add-more-lessons-text').live('click', function(){
      $('.page-add-lessons #lesson-node-form ').fadeIn('slow');

      $('html,body').animate({
        scrollTop: $("#lesson-node-form").offset().top
      });
    });




    //  Lesson add or edit lesson
    //-----------------------------------

      $('.course-lessons #block-with-lessons .lesson-number').live('click', function(){
        $('.course-lessons #block-with-lessons .lesson-number').removeClass('highlight');
        $('.course-lessons #block-with-lessons  .span-line').removeClass('highlightspan');
        $( this ).addClass("highlight");
        $( this ).parent('.less').children('.span-line').addClass("highlightspan");
      });

      $('.course-lessons #block-with-lessons .lesson-title').live('click', function(){
        $('.course-lessons #block-with-lessons .lesson-number').removeClass('highlight');
        $('.course-lessons #block-with-lessons .span-line').removeClass('highlightspan');
        $( this ).parent('.less').children('.lesson-number').addClass("highlight");
        $( this ).parent('.less').children('.span-line').addClass("highlightspan");
      });


      $('#block-with-history .lesson-number').live('click', function(){
        $('#block-with-history .lesson-number').removeClass('highlight');
        $('#block-with-history  .span-line').removeClass('highlightspan');
        $( this ).addClass("highlight");
        $( this ).parent('.less').children('.span-line').addClass("highlightspan");
      });

      $('#block-with-history .lesson-title').live('click', function(){
        $('#block-with-history .lesson-number').removeClass('highlight');
        $('#block-with-history .span-line').removeClass('highlightspan');
        $( this ).parent('.less').children('.lesson-number').addClass("highlight");
        $( this ).parent('.less').children('.span-line').addClass("highlightspan");
      });

      // $('#block-by-step-block-lessons-info div .lesson-number').live('click', function(){
      //   $('#block-by-step-block-lessons-info div .lesson-number').removeClass('highlight');
      //   $('#block-by-step-block-lessons-info div .span-line').removeClass('highlightspan');
      //   $( this ).addClass("highlight");
      //   $( this ).parent('.less').children('.span-line').addClass("highlightspan");
      // });



    //    custom scroll for choose lesson block
    //-----------------------------------------
    $(".less-scrol").mCustomScrollbar({
      callbacks:{
        onScroll:function(){
          onScrollCallback();
        },
        onTotalScroll:function(){
          onTotalScrollCallback();
        },
        onTotalScrollOffset:40,
        onTotalScrollBack:function(){
          onTotalScrollBackCallback();
        },
        onTotalScrollBackOffset:20
      }
    });


    // Add Task
    //-----------------------------------------
    $('.page-add-task  #block-slide-task .lessons.add-lesson').live('click', function(){
        $('.page-add-task  #block-slide-task  .select-task').slideDown("slow");
    });


    $('.page-add-task #block-slide-task .chzn-drop .chzn-results li.active-result:first').live('click', function(){
      $('.page-add-task  #block-system-main  .content #block-by-step-block-test-task-add-form, #block-by-step-courator-add-auto-test').slideUp("fast");
      $('.page-add-task  #block-system-main .content #block-by-step-block-task-add-form').slideDown("slow");
    });

    $('.page-add-task #block-slide-task .chzn-drop .chzn-results li.active-result:last').live('click', function(){
      //alert('uuuuu');
        $('.page-add-task  #block-system-main .content #block-by-step-block-task-add-form').slideUp("fast");
	$('.page-add-task  #block-system-main .content #block-by-step-block-test-task-add-form').slideUp("fast");
        $('.page-add-task  #block-system-main  .content #block-by-step-courator-add-auto-test').slideDown("slow");
    });

    $('.page-add-task #block-slide-task .chzn-drop .chzn-results li.active-result:nth-child(2)').live('click', function(){

      //alert('uuuuu');
        $('.page-add-task  #block-system-main .content #block-by-step-block-task-add-form').slideUp("fast");
      $('.page-add-task  #block-system-main .content #block-by-step-courator-add-auto-test').slideUp("fast");
	$('.page-add-task  #block-system-main  .content #block-by-step-block-test-task-add-form').slideDown("slow");
    });



    $('form.node-task_test-form #edit-field-task-time input[type="text"]').keypress(function(e) {
      if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
        return true;
      } else {
        return false;
      }
    });

    $('form.node-task_test-form #edit-field-task-time input[type="text"]').keyup(function(e) {
      var newtime = $(this).val();
      if (newtime < 101) {
        $('form.node-task_test-form #edit-field-task-time input[type="text"]').val(newtime);
      } else {
        $('form.node-task_test-form #edit-field-task-time input[type="text"]').val(100);
      }
    });


    $('form.node-task_test-form #edit-field-task-limit input[type="text"]').keypress(function(e) {
      if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
        return true;
      } else {
        return false;
      }
    });

    $('form.node-task_test-form #edit-field-task-limit input[type="text"]').keyup(function(e) {
      var newtime = $(this).val();
      if (newtime < 101) {
        $('form.node-task_test-form #edit-field-task-limit input[type="text"]').val(newtime);
      } else {
        $('form.node-task_test-form #edit-field-task-limit input[type="text"]').val(100);
      }
    });

//    Autotest task form
    $('form.node-auto_test-form #edit-field-auto-test-time input[type="text"]').keypress(function(e) {
      if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
        return true;
      } else {
        return false;
      }
    });

    $('form.node-auto_test-form #edit-field-auto-test-time input[type="text"]').keyup(function(e) {
      var newtime = $(this).val();
      if (newtime < 101) {
        $('form.node-auto_test-form #edit-field-auto-test-time input[type="text"]').val(newtime);
      } else {
        $('form.node-auto_test-form #edit-field-auto-test-time input[type="text"]').val(100);
      }
    });


    $('form.node-auto_test-form #edit-field-auto-test-percent input[type="text"]').keypress(function(e) {
      if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
        return true;
      } else {
        return false;
      }
    });

    $('form.node-auto_test-form #edit-field-auto-test-percent input[type="text"]').keyup(function(e) {
      var newtime = $(this).val();
      if (newtime < 101) {
        $('form.node-auto_test-form #edit-field-auto-test-percent input[type="text"]').val(newtime);
      } else {
        $('form.node-auto_test-form #edit-field-auto-test-percent input[type="text"]').val(100);
      }
    })

    //$('form.node-task_test-form .field-name-field-answer-weight input[type="text"]').keypress(function(e) {
    //  if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
    //    return true;
    //  } else {
    //    return false;
    //  }
    //});
    //
    //$('form.node-task_test-form .field-name-field-answer-weight input[type="text"]').keyup(function(e) {
    //  var newtime = $(this).val();
    //  if (newtime < 101) {
    //    $(this).val(newtime);
    //  } else {
    //    $(this).val(100);
    //  }
    //});


    // $('.add-task-to').live('click', function(){
    //   var w = $('body').width();
    //   var dw = (w-300)/2;
    //   $("#dialog").css('left', dw);
    //   $("#dialog").show();
    // });

    // $('#dialog .dialog-close').live('click', function() {
    //   $("#dialog").hide();
    // });

    // $('#dialog .body .button.yes').live('click', function() {
    //   $('form.node-lesson-form .form-actions input[type="submit"]').click();
    //   $("#dialog").hide();
    // });

    // $('#dialog .body .button.no').live('click', function() {
    //   $("#dialog").hide();
    //   var href = $('.add-task-to').attr('href');
    //   location = location.origin + '/' + href;
    // });

    $('.curator-list-settings .curator-search-form a.button').live('click', function() {
      var name = $('.curator-list-settings .curator-search-form input#curator-name').val();
      jQuery.ajax({
        url: location.pathname,
        type: "POST",
        cache: false,
        dataType: "html",
        async:false,
        data:{name: name},
        success: function(data){
          var page = $(data).find("#curator-search-result").html();
          $('.curator-list-settings #curator-search-result').html(page);
        }
      });
    });


    $('.curator-add-form a.button.add-more').live('click', function() {
      $('<input type="text" size="60" maxlength="128" class="form-text">').appendTo($('.curator-add-form .form-inner .form-item'));
    });

    $('.curator-add-form a.button.upload').live('click', function() {
       $('<div class="load-sucsess" style="display: none; margin:0px;"><div class="textin">Таких кураторов не найдено</div></div>').appendTo($('body'));
      var mails = [];
      $('.curator-add-form .form-inner .form-item input[type="text"]').each(function() {
        var mail = $(this).val();
        mails.push(mail);
      });
      jQuery.ajax({
        url: location.pathname,
        type: "POST",
        cache: false,
        dataType: "html",
        async:false,
        data:{mails: mails},
        success: function(data){

          if (data.length >= 1){
            $(".load-sucsess .textin").empty();

            $(".load-sucsess .textin").html(data);
            $('.curator-add-form .form-type-textfield input.form-text').val("");
          }

          $(".load-sucsess").show();
          setTimeout(function() { $(".load-sucsess").hide().remove(); }, 4000);
        }
      });
    });


    //   add-students/388
    $('.student-add-form a.button.add-more').live('click', function() {
      $('<input type="text" size="60" maxlength="128" class="form-text">').appendTo($('.student-add-form .form-inner .form-item'));
    });

    $('.student-add-settings .student-add-form a.button.upload').live('click', function() {
       $('<div class="load-sucsess" style="display: none; margin:0px;"><div class="textin">Таких учеников не найдено</div></div>').appendTo($('body'));
      var mails = [];
      //$('.student-add-form .form-inner .form-item input[type="text"]').each(function() {
      //  var mail = $(this).val();
      //  mails.push(mail);
      //});
      //jQuery.ajax({
      //  url: location.pathname,
      //  type: "POST",
      //  cache: false,
      //  dataType: "html",
      //  async:false,
      //  data:{mails: mails},
      //  success: function(data){
      //    //var page = $(data).find("#curator-search-result").html();
      //    //$('.curator-list-settings #curator-search-result').html(page);
      //
      //    if (data.length >= 1){
      //      $(".load-sucsess .textin").empty();
      //
      //      $(".load-sucsess .textin").html(data);
      //      $('.curator-add-form .form-type-textfield input.form-text').val("");
      //    }
      //
      //    $(".load-sucsess").show();
      //    setTimeout(function() { $(".load-sucsess").hide().remove(); }, 4000);
      //  }
      //});
    });

  });

  function myValidation() {
    $(document).ajaxSuccess(function() {
      $('form.node-task_test-form .field-name-field-answer-weight input[type="text"]').keypress(function(e) {
        if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
          return true;
        } else {
          return false;
        }
      });

      $('form.node-task_test-form .field-name-field-answer-weight input[type="text"]').keyup(function(e) {
        var newtime = $(this).val();
        if (newtime < 101) {
          $(this).val(newtime);
        } else {
          $(this).val(100);
        }
      });
    });

    $(document).ready(function() {
      $('form.node-task_test-form .field-name-field-answer-weight input[type="text"]').keypress(function(e) {
        if ((e.keyCode > 47 && e.keyCode < 58) || e.keyCode  == 8 || e.keyCode == 46 ) {
          return true;
        } else {
          return false;
        }
      });

      $('form.node-task_test-form .field-name-field-answer-weight input[type="text"]').keyup(function(e) {
        var newtime = $(this).val();
        if (newtime < 101) {
          $(this).val(newtime);
        } else {
          $(this).val(100);
        }
      });

      // $('form.node-task_test-form .form-actions input[type="submit"]').live('click', function() {
      //   $('form.node-task_test-form .field-name-field-task-test').each(function() {
      //     var t = $(this).find('table.tabledrag-processed tbody').children().length;
      //     console.log(t);
      //     // if (t < 2 && t > 0) {
      //       // alert("Должно быть не менее двух вариантов ответа для теста");
      //       // return false;
      //     // } else {
      //     //   return true;
      //     // }
      //   });
      //               return false;

      // });
    });
  }
  myValidation();


  //  moving lines
  //-----------------------------
  $(document).ready(function() {

    $('.page-pupil-task #block-by-step-block-add-answer-text .node-text_type_task_answer-form .form-textarea-wrapper textarea, .page-pupil-course #block-by-step-block-add-answer-text .node-text_type_task_answer-form .form-textarea-wrapper textarea').attr('placeholder','Ответ');
    $('.page-add-students #block-by-step-block-courses #block-system-main .course .block-sdw').insertAfter('.page-add-students #block-by-step-block-courses #block-system-main .course > .course-content');
    $('.page-add-students #block-by-step-block-courses #block-system-main .course .block-sdw2').insertAfter('.page-add-students #block-by-step-block-courses #block-system-main .course > .student-add-settings');
    $('.page-add-students #block-by-step-block-courses #block-system-main .course .block-sdw2').clone().insertAfter('.page-add-students #block-by-step-block-courses #block-system-main .course > .video-instruction');
  });

  //  puppil add cuorse
  //------------------------------
  $(document).ready(function() {
    $('#block-by-step-block-add-course-pupil').hide();
    $('.add-course-img-by-id .image, .add-course-title-by-id').live('click', function() {
      $('#block-by-step-block-add-course-pupil').slideDown("slow");
    });

    $('#block-video .block-video .video-buttons div').live('click', function(){
    if (!$(this).hasClass('active')) {
      var tab = $(this).attr('class');
      $('#block-video .block-video .video').removeClass('active');
      $('#block-video .block-video .video-buttons div').removeClass('active');

      $('#block-video .block-video .video.' + tab).addClass('active');
      $('#block-video .block-video .video-buttons div.' + tab).addClass('active');
      }
    });

    $('.page-pupil-course .lesson-number').live('click', function(){
      var numless = $(this).attr('id');
      var lessvidId = $('.page-pupil-course #block-system-main .content #block-video .block-video').attr('id');
      $('.page-pupil-course #block-system-main .content #block-video .block-video').hide();
      $('.page-pupil-course #block-system-main .content #block-video').find('#'+numless).fadeIn();
    });

  });

  $(document).ready(function(){
    var BodyWidth =  $(document).width();

    var aniRight = ((BodyWidth - 1000)/2) + 166;

    //console.log(BodyWidth +'tr'+ aniRight);
    $('#block-by-step-block-courses .content .content-courses .view-content').css('width','1000');
    $('#block-by-step-block-courses .content .content-courses .view-content .content-carousel').css('width','960');
    $('#block-by-step-menu-for-teacher').css('right','-'+aniRight+'px');
  $('#block-by-step-menu-for-teacher .hide-line').toggle(
  function(){
      $('#block-by-step-menu-for-teacher .hide-line').addClass('ll');
      $('#block-by-step-menu-for-teacher').animate({'right':'0'},500);
      $('#block-by-step-block-courses .content .content-courses .view-content').animate({'width':'720'},500);
      $('#block-by-step-block-courses .content .content-courses .view-content .content-carousel').animate({'width':'680'},500);
  },
  function(){
     $('#block-by-step-menu-for-teacher .hide-line').removeClass('ll');
    $('#block-by-step-menu-for-teacher').animate({'right':'-'+aniRight},500);
    $('#block-by-step-block-courses .content .content-courses .view-content').animate({'width':'1000'},500);
    $('#block-by-step-block-courses .content .content-courses .view-content .content-carousel').animate({'width':'960'},500);
    }
  )
  });

  //	prezentation tabs
  $(document).ready(function(){
    $('.main-prezentation-content .present-buttons div').live('click', function(){
    if (!$(this).hasClass('active')) {
      var tab = $(this).attr('class');
      $('.main-prezentation-content .block-prezentation .prezentation').removeClass('active');
      $('.main-prezentation-content .present-buttons div').removeClass('active');

      $('.main-prezentation-content .block-prezentation .prezentation.' + tab).addClass('active');
      $('.main-prezentation-content .present-buttons div.' + tab).addClass('active');
      }
    });
  });

  //    custom scroll for check page-courator-check-lesson
  //-------------------------------------------------------
  $(document).ready(function(){
      $(".request-content").mCustomScrollbar({
	callbacks:{
	  onScroll:function(){
	    onScrollCallback();
	  },
	  onTotalScroll:function(){
	    onTotalScrollCallback();
	  },
	  onTotalScrollOffset:40,
	  onTotalScrollBack:function(){
	    onTotalScrollBackCallback();
	  },
	  onTotalScrollBackOffset:20
	}
      });
  });

  $(document).ajaxSuccess(function() {
      $(".request-content").mCustomScrollbar({
	callbacks:{
	  onScroll:function(){
	    onScrollCallback();
	  },
	  onTotalScroll:function(){
	    onTotalScrollCallback();
	  },
	  onTotalScrollOffset:40,
	  onTotalScrollBack:function(){
	    onTotalScrollBackCallback();
	  },
	  onTotalScrollBackOffset:20
	}
      });
  });

  //$(document).ready(function(){
  //  var  aaa= $('.tab-1 .field-name-field-lesson-videos #edit-field-lesson-videos-und-0-remove-button').val();
  //  var ccc= $('.tab-1 .field-name-field-lesson-video-link #edit-field-lesson-video-link-und-0-value').attr('name');
  //  var trueIn = 'Удалить';
  //  var falseIn= 'Закачать';
  //  console.log(aaa+'dhkjh    '+trueIn);
  //  $('body .tab-triggers span').live('click', function(){
  //  if (aaa===trueIn) {
  //    alert(ccc);
  //    ccc.attr('disabled','true');
  //  }
  //  else{
  //    alert(ccc);
  //   ccc.prop('disabled',false);
  //  }
  //  });
  //
  //});

})(jQuery);
