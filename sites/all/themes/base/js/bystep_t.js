(function($){

    //+div
    $(document).ready(function() {
        $('.page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal .form-type-radios').append('<div id="zero" ><div class="mark" ></div></div>');
        $('.page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal .form-type-radios #zero').insertBefore('#testimonials-node-form #edit-field-evaluation-bal #edit-field-evaluation-bal-und');

        //placeholder for form
        $('#edit-keys-wrapper #edit-keys').attr('placeholder', 'здесь текст плайс холдера');
    });

    //+div fon slider
    //$(document).ready(function() {
    //    $('body').append('<div id="bg-fon" ></div>');
    //});

    $(document).ready(function() {
        $('.page-node-add-course #course-node-form > div').append('<div id="edit-shadow" ></div>');
        $('.page-node-add-course #course-node-form div #edit-shadow').insertBefore('.page-node-add-course #course-node-form div #edit-actions');
    });

    //+div form for page add-lessons
    $(document).ready(function() {
        $('#lesson-node-form .form-item-title').append('<div class="description">не более 120 символов</div>');

        $('#block-by-step-block-courses #block-system-main .course').append('<div class="block-sdw"></div>');
        $('#block-by-step-block-courses #block-system-main .course .block-sdw').insertBefore('#block-by-step-block-courses #block-system-main .course .course-lessons');

        $('#block-by-step-block-courses #block-system-main .course').append('<div class="block-sdw2"></div>');
        $('#block-by-step-block-courses #block-system-main .course .block-sdw2').insertAfter('#block-by-step-block-courses #block-system-main .course .course-lessons');
    });

    //+div for add-video
    $(document).ready(function() {
        $('#block-system-main .video-lesson').append('<div id="bl-shadow" ></div>');
        $('#block-system-main .video-lesson #bl-shadow').insertAfter('#block-system-main .video-lesson .header-course');

        //$('#block-by-step-block-lessons-add-video #lesson-node-form > div').append('<div id="block-img" ></div>');
        //$('#block-by-step-block-lessons-add-video #lesson-node-form div #block-img').insertBefore('#block-by-step-block-lessons-add-video #lesson-node-form #edit-field-video');
    });

    //+div zadanie k yroku
    // $(document).ready(function() {
    //     $('.page-add-task #block-system-main > .content').append('<div id="bl-shadow" ></div>');
    //     $('.page-add-task #block-system-main .content #bl-shadow').insertAfter('.page-add-task #block-system-main .content .header-content-course');
    // });


    //+div description
    $(document).ready(function() {
        $('#block-by-step-block-test-task-add-form #task-test-node-form > div .form-item-title').append('<div class="description">не более 120 символов</div>');
    });


    //+div add-task
    $(document).ready(function() {
        $('.page-add-task #block-system-main > .content').append('<div id="bl-shadow2" ></div>');
        $('.page-add-task #block-system-main .content #bl-shadow2').insertAfter('.page-add-task #block-system-main .content #block-slide-task');
    });


    //peremistit div
    $(document).ready(function() {
        $('.page-curator-list #block-by-step-block-courses #block-system-main .course .block-sdw').insertAfter('.page-curator-list #block-by-step-block-courses #block-system-main .course > .course-content');

        $('.page-curator-list #block-by-step-block-courses #block-system-main .course .block-sdw2').insertAfter('.page-curator-list #block-by-step-block-courses #block-system-main .course > .video-instruction');
    });


    //peremistit div
    $(document).ready(function() {
        $('.page-curator-add #block-by-step-block-courses #block-system-main .course .block-sdw').insertAfter('.page-curator-add #block-by-step-block-courses #block-system-main .course > .course-content');

        $('.page-curator-add #block-by-step-block-courses #block-system-main .course .block-sdw2').insertAfter('.page-curator-add #block-by-step-block-courses #block-system-main .course > .video-instruction');
    });

    //peremist div v form na str edit-course
    $(document).ready(function() {
      $('.page-edit-course #lesson-node-form .field-name-field-short-description').insertAfter('.page-edit-course #lesson-node-form .field-name-field-link-soc-kurs');
    });


    // Mark teacher rating
    $(document).ready(function(){
        var anes = $('.page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal .form-type-radios .form-type-radio label').text();
        $('input:radio').change(
            function(){
                $('.page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal .form-type-radios .form-type-radio').removeClass("actione");
                    $(this).parents('.form-type-radio').addClass("actione");
            }
        );
        $(".page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal .form-type-radios .form-type-radio label").click(function(){
            var labelnum = $(this).text();
            labelnuma = labelnum.replace(/\s/g, '')
            $('.page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal .form-type-radios #zero .mark').attr('id', 'icon-mark'+labelnuma);
        });

        $('.page-node-add-testimonials #wrapper #container .region-content #block-by-step-user-params-reviews').insertBefore('.page-node-add-testimonials #testimonials-node-form #edit-field-evaluation-bal');
    });

  //+video video the youtube
  $('.page-add-video .field-type-text .add-video-button').live('click', function(){
    var youparsID = $(this).parents('.tabs').find('.field-type-text .form-type-textfield .text-full').val();
    var url = youparsID;
    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    if(videoid != null) {
      //console.log("video id = ",videoid[1]);
    } else {
      alert("The youtube url is not valid.");
    }
    //$('.page-node-add-testimonials #field-youtube-link-review-add-more-wrapper .second-step .plus-img').html('<iframe width="560" height="315" src="//www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe>');
    $(this).parents('.tabs').find(' #block-img').html('<img class="alignnone" title="0" alt="Preview" width="120" height="120" src="http://img.youtube.com/vi/'+ videoid[1] +'/1.jpg">');
  });


})(jQuery);
