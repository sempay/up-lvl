(function ($,jwplayer) {
  function get_player_data(index){
      var result = new Array();
      var parent = $('#block-views-video-slide-block .views-row').eq(index);
      result[0]=$('.views-field-field-video .field-content',parent).text();
      result[1]=$('.views-field-field-image img',parent).attr('src');
      return result;
  }
  function player_change_video(index){
    var init_player = get_player_data(index);
    jwplayer("videoplayer").setup({
      file: init_player[0],
      image: init_player[1],
      width: "630",
      height: "350"
    });
  }
  $(function (){
    var padding=0;
    if( $('body.front').length > 0){
      $('#block-views-video-slide-block .views-field-title a').each(function(){
        padding = (30 - parseFloat($(this).height()))/2;
        $(this).css('padding-top',padding).css('padding-bottom',padding);
      });
      $('#block-views-video-slide-block .views-field-title:first').addClass('activeTitle');
      jwplayer.key="aGi+iSKpe5dJUMatwTx3D/FSns8+CKgr012FRw==";
      var init_player = get_player_data(0);
      jwplayer("videoplayer").setup({
        file: init_player[0],
        image: init_player[1],
        width: "630",
        height: "350"
      });
      $('#block-views-video-slide-block .views-field-title a').bind('click', function(){
        $('.activeTitle').removeClass('activeTitle');
        $(this).closest('.views-field-title').addClass('activeTitle');
        player_change_video($(this).closest('.views-row').index());
        return false;
      });
    }
  });
})(jQuery,jwplayer);