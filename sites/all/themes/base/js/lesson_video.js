(function ($,jwplayer) {
  $(function (){
    var i, file_data;
    if($("#block-custom-user-custom-user-lesson-video .video").length>1)
    {
      var video_navigation='<ul class="video_navigation" style="text-align: left">';
      $("#block-custom-user-custom-user-lesson-video .video").each(function(i){
        video_navigation+='<li> Видео '+(i+1)+'</li>';
      });
      video_navigation+='</ul>';
      $(video_navigation).insertAfter('.wrap_video');
    }
    $('.video_navigation li').bind('click', function(){
      $('.video_navigation_active').removeClass('video_navigation_active');
      $(this).addClass('video_navigation_active');
      file_data = $("#block-custom-user-custom-user-lesson-video .video").eq($(this).index()).text();
      jwplayer.key="aGi+iSKpe5dJUMatwTx3D/FSns8+CKgr012FRw==";
      jwplayer("videoplayer").setup({
        file: file_data,
        image: '',
        width: "630",
        height: "350"
      });
      jwplayer().play();
    });
    $("#block-custom-user-custom-user-lesson-video .video").each(function(i){
      if(i==0) file_data = $(this).text();
        $(this).addClass('it-'+i);
    });
    jwplayer.key="aGi+iSKpe5dJUMatwTx3D/FSns8+CKgr012FRw==";
    jwplayer("videoplayer").setup({
      file: file_data,
      image: '',
      width: "630",
      height: "350"
    });

  });
})(jQuery,jwplayer);