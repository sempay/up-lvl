(function ($,jwplayer) {
  $(function (){
    var i, file_data;
    if($(".field-name-field-video .field-item").length>0)
    {
      $('<div id="videoplayer"></div>').insertBefore('.field-name-field-video .field-items');
      var video_navigation='<ul class="video_navigation" style="text-align: left">';
      $(".field-name-field-video .field-item").each(function(i){
        video_navigation+='<li> Видео '+(i+1)+'</li>';
      });
      video_navigation+='</ul>';
      $(video_navigation).insertAfter('.field-name-field-video');
    }
    $('.video_navigation li').bind('click', function(){
      $('.video_navigation_active').removeClass('video_navigation_active');
      $(this).addClass('video_navigation_active');
      file_data = $(".field-name-field-video .field-item").eq($(this).index()).text();
      jwplayer.key="aGi+iSKpe5dJUMatwTx3D/FSns8+CKgr012FRw==";
      jwplayer("videoplayer").setup({
        file: file_data,
        image: '',
        width: "430",
        height: "350"
      });
      jwplayer().play();
    });
    $(".field-name-field-video .field-item").each(function(i){
      if(i==0) file_data = $(this).text();
        $(this).addClass('it-'+i);
    });
    jwplayer.key="aGi+iSKpe5dJUMatwTx3D/FSns8+CKgr012FRw==";
    jwplayer("videoplayer").setup({
      file: file_data,
      image: '',
      width: "430",
      height: "350"
    });

  });
})(jQuery,jwplayer);