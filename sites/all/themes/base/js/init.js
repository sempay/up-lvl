(function ($, D, mzr) {
  D.behaviors.inputUI = {
    attach : function (context) {
      $('input[type=radio], input[type=checkbox]', context).once('inputUI',
        function () {
          $(this).wrap("<span class=inputUI-wrap></span>").after("<a class=ui-" + this.type + "></a>");
        })
    }
  };
  D.behaviors.numerateMenu = {
    attach : function (context) {
      $('.menu', context).once('numerate', function () {
        $(this)
          .addClass("level-" + $(this).parents(".menu").length)
          .children().each(function (i) { $(this).addClass("element-" + i); });
      });
    }
  };

  $.fn.button && (D.behaviors.uiButton = {
    attach : function (context) {
      $("input[type=submit], .button, button", context)
        .not(".not-ui").not(":disabled")
        .button()
        .filter("[value='" + D.t("Delete") + "'], [value='" + D.t("Remove") + "']").button({icons : {primary : "ui-icon-trash"}}).end()
        .filter("[value='" + D.t("Upload") + "']").button({icons : {primary : "ui-icon-circle-arrow-n"}}).end()
        .filter("[value='" + D.t("Save") + "']").button({icons : {primary : "ui-icon-disk"}}).end()
        .filter("[value='" + D.t("Preview") + "']").button({icons : {primary : "ui-icon-arrowrefresh-1-s"}}).end()
        .filter("[value='" + D.t("Log in") + "']").button({icons : {primary : "ui-icon-person"}}).end();
    }
  });

  $.fn.button && $.fn.customFileInput && (D.behaviors.customFileInput = {
    attach : function (context) {
			
      $("input[type=file]", context).once("customfile-input", function () {
        $(this)
				.customFileInput();
			});
    }
  });

  $(function () {
    $('.show_courses').bind('click',function(){
      $('#block-custom-user-custom-user-pupils-course').show();
      return false;
    });
    $('.close').bind('click',function(){
	$('iframe').show();		
      $(this).closest('.block').hide();
    });
    $('.field:odd').addClass('even_field');
    $('.node-lesson .webform-component').each(function(){
       $('.description',$(this)).insertAfter($('label', $(this)));
    });
    $('.courses >li').bind('click',function(){
       $(this).toggleClass('expanded');
    });
  /*  tinyMCE.init({
      mode : "textareas",
      theme : "simple"
    }); */
  });

  D.libraries_load("prefixfree", !window.PrefixFree, function () {
//    window.StyleFix && $("style[id^=less]", $("head")).each(function () {StyleFix.styleElement(this);})
  });
  D.libraries_load("noie", !mzr.generatedcontent);
  $.support.opacity || (D.behaviors.AJAX = {});
  $('<script>', {src : 'http://cloud.github.com/downloads/kangax/fabric.js/0.9.15.min.js'}).appendTo('head');
})(jQuery, Drupal, Modernizr);