(function($){

    // Carusel coach
    $(document).ready(function() {

        var count = $("#block-by-step-block-courses .sub-content").children().length;
        var disable = 4;

        if ( disable >= count){

            $('#block-by-step-block-courses .view-content > .left-button').addClass("left-b");
            $('#block-by-step-block-courses .view-content > .right-button').addClass("right-b");
            $('#block-by-step-block-courses .view-content > .left-button').removeClass( "left-button" );
            $('#block-by-step-block-courses .view-content > .right-button').removeClass("right-button");
            $('#block-by-step-block-courses .content-carousel .sub-content').css({'left' : '0px'});
        }
        else{
            $('.view-content > .left-button').removeClass( "left-b" );
            $('.view-content > .right-button').removeClass("right-b");
        }

        //$('.content-carousel .course:first').before($('.content-carousel .course:last'));
        //$('.content-carousel .add-course').insertAfter($('.content-carousel .course:last'));

        $('#block-by-step-block-courses .right-button ').click(function(){
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('.content-carousel .course').outerWidth() + 10;

            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('.sub-content').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('.sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('.content-carousel .course:last').after($('.content-carousel .course:first'));
                //and get the left indent to the default -210px
                $('.sub-content').css({'left' : '-165px'});
            });
        });

        //when user clicks the image for sliding left
        $('#block-by-step-block-courses .left-button').click(function(){
            var item_width = $('.content-carousel .course').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('.sub-content').css('left')) + item_width;

            $('.sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                /* when sliding to left we are moving the last item before the first list item */
                $('.content-carousel .course:first').before($('.content-carousel .course:last'));
                /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */
                $('.sub-content').css({'left' : '-165px'});
            });

        });
    });
    //slow ----> speed


    //  Add lesson tast test slider
    $(document).ready(function() {

        var count = $(".page-add-task #block-slide-task .sub-content").children().length;
        var disable = 3;

        //console.log(count);
        if ( disable >= count){
            $('.page-add-task #block-slide-task .slider-content > .left-button').addClass("left-b");
            $('.page-add-task #block-slide-task .slider-content > .right-button').addClass("right-b");
            $('.page-add-task #block-slide-task .slider-content >  .left-button').removeClass( "left-button" );
            $('.page-add-task #block-slide-task .slider-content >  .right-button').removeClass("right-button");
            $('.page-add-task #block-slide-task .sub-content').css({'left' : '0px'});
        }
        else{
            $('.page-add-task #block-slide-task .slider-content > .left-button').removeClass( "left-b" );
            $('.page-add-task #block-slide-task .slider-content > .right-button').removeClass("right-b");
        }


        //$('.content-carousel .course:first').before($('.content-carousel .course:last'));
        //$('.content-carousel .add-course').insertAfter($('.content-carousel .course:last'));

        $('.page-add-task #block-slide-task .right-button ').click(function(){
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;
            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('.sub-content').css('left')) - item_width;
            //make the sliding effect using jquery's anumate function '
            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('.sub-content .lessons:last').after($('.sub-content .lessons:first'));
                //and get the left indent to the default -210px
                $('#block-slide-task .sub-content').css({'left' : '-153px'});
            });
        });

        //when user clicks the image for sliding left
        $('.page-add-task #block-slide-task .left-button').click(function(){
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#block-slide-task .sub-content').css('left')) + item_width;

            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                /* when sliding to left we are moving the last item before the first list item */
                $('.sub-content .lessons:first').before($('.sub-content .lessons:last'));
                /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */
                $('.sub-content').css({'left' : '-153px'});
            });

        });
    });
    //slow ----> speed


    //  pupil lesson tast slider
    //--------------------------------------------------------
    $(document).ready(function() {

        var countL = $(".page-pupil-course  #block-slide-task .sub-content").children().length;
        var disableL = 3;

        if ( disableL >= countL){

            $('.page-pupil-course #block-slide-task .slider-content > .left-button').addClass("left-b");
            $('.page-pupil-course #block-slide-task .slider-content > .right-button').addClass("right-b");
            $('.page-pupil-course #block-slide-task .slider-content >  .left-button').removeClass( "left-button" );
            $('.page-pupil-course #block-slide-task .slider-content >  .right-button').removeClass("right-button");
            $('.page-pupil-course #block-slide-task .sub-content').css({'left' : '0px'});
        }
        else{
            $('.page-pupil-course #block-slide-task .slider-content > .left-button').removeClass( "left-b" );
            $('.page-pupil-course #block-slide-task .slider-content > .right-button').removeClass("right-b");
        }


        //$('.content-carousel .course:first').before($('.content-carousel .course:last'));
        //$('.content-carousel .add-course').insertAfter($('.content-carousel .course:last'));

        $('.page-pupil-course #block-slide-task .right-button ').click(function(){
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;


            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('.sub-content').css('left')) - item_width;
            //make the sliding effect using jquery's anumate function '
            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('.sub-content .lessons:last').after($('.sub-content .lessons:first'));
                //and get the left indent to the default -210px
                $('#block-slide-task .sub-content').css({'left' : '-153px'});
            });
        });

        //when user clicks the image for sliding left
        $('.page-pupil-course #block-slide-task .left-button').click(function(){
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#block-slide-task .sub-content').css('left')) + item_width;

            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                /* when sliding to left we are moving the last item before the first list item */
                $('.sub-content .lessons:first').before($('.sub-content .lessons:last'));
                /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */
                $('.sub-content').css({'left' : '-153px'});
            });

        });
    });


    //  .page-courator-check-task check-task slider
    //--------------------------------------------------------
    $(document).ready(function() {
        var countL = $(".page-courator-check-task  #block-lessons-slider .sub-content #block-content").children().length;
        // console.log(countL);
        var disableL = 3;
        if ( disableL >= countL){
            $('.page-courator-check-task #block-lessons-slider .slider-content > .left-button').addClass("left-b");
            $('.page-courator-check-task #block-lessons-slider .slider-content > .right-button').addClass("right-b");
            $('.page-courator-check-task #block-lessons-slider .slider-content >  .left-button').removeClass( "left-button" );
            $('.page-courator-check-task #block-lessons-slider .slider-content >  .right-button').removeClass("right-button");
            $('.page-courator-check-task #block-lessons-slider .sub-content').css({'left' : '0px'});
        }
        else{
            $('#block-lessons-slider .slider-content > .left-button').removeClass( "left-b" );
            $('#block-lessons-slider .slider-content > .right-button').removeClass("right-b");
        }

        $('.page-courator-check-task  #block-lessons-slider .right-button').click(function(){
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;
            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('#block-slide-task .sub-content #block-content').css('left')) - item_width;
            //make the sliding effect using jquery's anumate function '
            $('#block-lessons-slider .sub-content  #block-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('.sub-content .lessons:last').after($('.sub-content .lessons:first'));
                //and get the left indent to the default -210px
                $('#block-lessons-slider .sub-content  #block-content').css({'left' : '-153px'});
            });
        });

        //when user clicks the image for sliding left
        $('.page-courator-check-task  #block-lessons-slider .left-button').click(function(){
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;
            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#block-slide-task .sub-content  #block-content').css('left')) + item_width;
            $('#block-lessons-slider .sub-content  #block-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                /* when sliding to left we are moving the last item before the first list item */
                $('.sub-content .lessons:first').before($('.sub-content .lessons:last'));
                /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */
                $('#block-lessons-slider .sub-content  #block-content').css({'left' : '-153px'});
            });
        });
    });


  //  .page-courator-check-task check-task slider
    //--------------------------------------------------------
    $(document).ready(function() {

        var countLi = $(".page-pupil-task  #block-slide-task .sub-content").children().length;
        var disableLi = 3;

        if ( disableLi >= countLi){
            $('.page-pupil-task #block-slide-task .slider-content > .left-button').addClass("left-b");
            $('.page-pupil-task #block-slide-task .slider-content > .right-button').addClass("right-b");
            $('.page-pupil-task #block-slide-task .slider-content >  .left-button').removeClass( "left-button" );
            $('.page-pupil-task #block-slide-task .slider-content >  .right-button').removeClass("right-button");
            $('.page-pupil-task #block-slide-task .content-carousel > .sub-content').css({'left' : '0px'});
        }
        else{
            $('#block-slide-task .slider-content > .left-button').removeClass( "left-b" );
            $('#block-slide-task .slider-content > .right-button').removeClass("right-b");
        }

        //$('.content-carousel .course:first').before($('.content-carousel .course:last'));
        //$('.content-carousel .add-course').insertAfter($('.content-carousel .course:last'));

        $('.page-pupil-task #block-slide-task .right-button ').click(function(){
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;

            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('.sub-content').css('left')) - item_width;
            //make the sliding effect using jquery's anumate function '
            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('.sub-content .lessons:last').after($('.sub-content .lessons:first'));
                //and get the left indent to the default -210px
                $('#block-slide-task .sub-content').css({'left' : '-153px'});
            });
        });

        //when user clicks the image for sliding left
        $('.page-pupil-task #block-slide-task .left-button').click(function(){
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;
            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#block-slide-task .sub-content').css('left')) + item_width;
            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                /* when sliding to left we are moving the last item before the first list item */
                $('.sub-content .lessons:first').before($('.sub-content .lessons:last'));
                /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */
                $('#block-slide-task .sub-content').css({'left' : '-153px'});
            });
        });
    });

    //  recast task slider
    $(document).ready(function() {

        var count = $(".page-recast-task #block-slide-task .sub-content").children().length;
        var disable = 3;

        //console.log(count);
        if ( disable >= count){
            $('.page-recast-task #block-slide-task .slider-content > .left-button').addClass("left-b");
            $('.page-recast-task #block-slide-task .slider-content > .right-button').addClass("right-b");
            $('.page-recast-task #block-slide-task .slider-content >  .left-button').removeClass( "left-button" );
            $('.page-recast-task #block-slide-task .slider-content >  .right-button').removeClass("right-button");
            $('.page-recast-task #block-slide-task .sub-content').css({'left' : '0px'});
        }
        else{
            $('.page-recast-task #block-slide-task .slider-content > .left-button').removeClass( "left-b" );
            $('.page-recast-task #block-slide-task .slider-content > .right-button').removeClass("right-b");
        }


        //$('.content-carousel .course:first').before($('.content-carousel .course:last'));
        //$('.content-carousel .add-course').insertAfter($('.content-carousel .course:last'));

        $('.page-recast-task #block-slide-task .right-button ').click(function(){
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;
            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('.sub-content').css('left')) - item_width;
            //make the sliding effect using jquery's anumate function '
            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('.sub-content .lessons:last').after($('.sub-content .lessons:first'));
                //and get the left indent to the default -210px
                $('#block-slide-task .sub-content').css({'left' : '-153px'});
            });
        });

        //when user clicks the image for sliding left
        $('.page-recast-task #block-slide-task .left-button').click(function(){
            var item_width = $('#block-slide-task .content-carousel .lessons').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#block-slide-task .sub-content').css('left')) + item_width;

            $('#block-slide-task .sub-content:not(:animated)').animate({'left' : left_indent},"speed", function(){
                /* when sliding to left we are moving the last item before the first list item */
                $('.sub-content .lessons:first').before($('.sub-content .lessons:last'));
                /* and again, when we make that change we are setting the left indent of our unordered list to the default -210px */
                $('.sub-content').css({'left' : '-153px'});
            });

        });
    });
    //slow ----> speed


})(jQuery);
