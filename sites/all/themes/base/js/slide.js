(function ($) {
  $(function (){
    if($('#block-views-video-block-5').length>0) var obj = $('#block-views-video-block-5');
    if($('#block-block-7').length>0) var obj = $('#block-block-7');
    if($('#block-block-9').length>0) var obj = $('#block-block-9');
    if($('#block-block-13').length>0) var obj = $('#block-block-13');
    var flag= $('#block-block-7').length + $('#block-block-9').length + $('#block-block-13').length + $('#block-views-video-block-5').length;
    if(flag>0){
      var w_height=$('#wrapper').height() - 1300;
      var offset = obj.offset();
      var topOffset = offset.top;
      var leftOffset = offset.left;
      var marginTop = obj.css("marginTop");
      var marginLeft = obj.css("marginLeft");
      $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        if ((scrollTop >= topOffset)&&(scrollTop <= w_height)){
          obj.css({
            marginTop: marginTop,
            position: 'fixed',
          });
        }

        if ((scrollTop < topOffset)||(scrollTop >w_height)){
          obj.css({
            marginTop: marginTop,
            marginLeft: marginLeft,
            position: 'relative',
          });
        }
      });
    }

  });
})(jQuery);