
(function ($) {
  $(function (){
    $( ".page-pupil-register .base-user-login-form-wrapper .form-type-textfield" ).append( "<div class='form_bec'></div><div class='clear'></div>" );
    $( ".page-teacher-register .teacher_reg_form .form-type-textfield" ).append( "<div class='form_bec'></div><div class='clear'></div>" );
    $( ".page-teacher-register .teacher_reg_form  #edit-actions" ).append( "<div class='bot_reg_ten'></div><div class='clear'></div>" );
    $( ".page-pupil-register .base-user-login-form-wrapper  #edit-actions" ).append( "<div class='bot_reg_ten'></div><div class='clear'></div>" );
  });

  $(document).ready(function(){
    $('div.top_info_block div.zakaz_form').hide();
    $('p.zakaz a').live('click', function(){
      $('div.top_info_block div.zakaz_form').fadeIn(500);
    });
    $('div.top_info_block div.zakaz_form div.close').live('click', function(){
      $('div.top_info_block div.zakaz_form').fadeOut(500);
    });
  });

  $(document).ajaxSuccess(function() {
    var uImg = $( "#user-profile-form .field-name-field-user-image" ).find('.image-preview');

    if(uImg.length == 0){
      // console.log(uImg);
      $( "#user-profile-form .field-name-field-user-image .form-managed-file" ).prepend('<div class="image-preview"></div>');
    }
  });

  $(document).ready(function () {
    var uImg = $( "#user-profile-form .field-name-field-user-image" ).find('.image-preview');

    if(uImg.length == 0){
      $( "#user-profile-form .field-name-field-user-image .form-managed-file" ).prepend('<div class="image-preview"></div>');
    }

    $( "#block-by-step-left-user-menu .menu-item" ).hover(
      function() {
        $( this ).find('.link-tips').css( "display", "block" );
      }, function() {
        $( '#block-by-step-left-user-menu .menu-item' ).find('.link-tips').css( "display", "none" );
      }
    );
  });

  $(document).ready(function () {
    $(function() {
       $('form#webform-client-form-3757').bind('submit');
       $('form#webform-client-form-3758').bind('submit');
    });

    $("form#webform-client-form-3757").submit(function () {
      var inputs = $(this).find('.form-item input');
      var error = false;

      $(this).find('.er-mess').remove();
      inputs.removeClass('error');

      for (var i = 0; i < inputs.length; i++) {

        if(!inputs[i].value && inputs[i].id != 'edit-submitted-vremya-zvonka-msk'){
          $(this).find('#' + inputs[i].id).parent().prepend("<div class='er-mess'>Поле " + inputs[i].placeholder +" обязательно для заполнения!</div>");
          $(this).find('#' + inputs[i].id).addClass('error');
          error = true;
        }else{
          if(inputs[i].id == 'edit-submitted-vash-telefon' && !$.isNumeric(inputs[i].value)){
            $(this).find('#' + inputs[i].id).parent().prepend("<div class='er-mess'>Поле " + inputs[i].placeholder + " может состоять только из цифр!</div>");
            $(this).find('#' + inputs[i].id).addClass('error');
            error = true;
          }
        }
      };

      if(error){
        return false;
      }
    });

    $("form#webform-client-form-3758").submit(function (event) {
      var inputs = $(this).find('.form-item input');
      var error = false;
      var form = this;

      $(this).find('.er-mess').remove();
      inputs.removeClass('error');

      for (var i = 0; i < inputs.length; i++) {

        if(!inputs[i].value){
          $(this).find('#' + inputs[i].id).parent().prepend("<div class='er-mess'>Поле " + inputs[i].placeholder +" обязательно для заполнения!</div>");
          $(this).find('#' + inputs[i].id).addClass('error');
          error = true;
        }else{
          if(inputs[i].id == 'edit-submitted-vash-telefon--2' && !$.isNumeric(inputs[i].value)){
            $(this).find('#' + inputs[i].id).parent().prepend("<div class='er-mess'>Поле " + inputs[i].placeholder + " может состоять только из цифр!</div>");
            $(this).find('#' + inputs[i].id).addClass('error');
            error = true;
          }
        }
      };

      if(error){
        return false;
      }else{

        var json_param = {};
        json_param['c_n'] = $(this).find('#webform-component-vashe-imya input').val();
        json_param['c_m'] = $(this).find('#webform-component-vash-e-mail input').val();
        json_param['c_p'] = $(this).find('#webform-component-vash-telefon input').val();

        $.ajax({
          type: 'POST',
          url: 'http://' + window.location.hostname + '/registration/user',
          data: json_param,
          dataType: 'json',
          async: false,
          success: function(data){
            event.preventDefault();

            if(data['er_m'] == 1){
              $(form).find('#webform-component-vash-e-mail input').addClass('error');
              $(form).find('#webform-component-vash-e-mail').append('<div class="er-mess">Неверный формат электронной почты</div>');
            }
            else if(data['ex_m'] == 1){
              $(form).find('#webform-component-vash-e-mail input').addClass('error');
              $(form).find('#webform-component-vash-e-mail').append('<div class="er-mess">Такой e-mail уже используется</div>');
            }else{
              var html = '';
              var messageContent = '';

              messageContent += '<div style="width: 400px;" class="content">';
              messageContent += '<h2 class="messages-label status">Статус</h2>';
              messageContent += '<div class="messages status">';
              messageContent += '<ul><li class="message-item first">Спасибо за регистрацию , логин и пароль от Вашего доступа у Вас на почте!</li><li class="message-item second">Сейчас Вы будете перенаправлены на страницу ознакомления с платформой.</li></ul>';
              messageContent += '</div>';
              messageContent += '</div>';

              html += '<div id="better-messages-wrapper" style="overflow: visible; position: fixed; z-index: 9999; width: 400px; top: 0px; right: 10px;"><div id="better-messages-default"><div id="messages-inner">';
              html += '<table><tbody>'; // panels-modal-content
              html += '<tr><td class="tl"></td><td class="b"></td><td class="tr"></td></tr><tr><td class="b"></td><td class="body">';
              html += messageContent;
              html += '<div class="footer"><span class="message-timer" style="display: none;"></span><a class="message-close" href="#"></a></div></td><td class="b"></td></tr><tr><td class="bl"></td><td class="b"></td><td class="br"></td></tr>';
              html += '</tbody></table>';
              html += '</div>';
              html += '</div>';
              html += '</div>';

              $('.front_page_wrapper').prepend(html);
              setTimeout(function() {
                window.location.href = 'http://' + window.location.hostname + '/welcome';
              }, 2000);
            }
          }
        });
      }
    });
  });

  $(document).ready(function(){

    $('.field-name-field-bonuse-reference select').addClass("chosen").attr("multiple", 'true');
    $('#edit-field-bonuse-reference-und').chosen();

    $('#edit-field-library-course-und').addClass("chosen").attr("multiple", 'true');
    $('#edit-field-library-course-und').chosen();

    $("#user-profile-form").each(function() {
      $('#edit-field-cat-ob-progr-und').chosen();
    });
  });

  // register choose AJAX form change
  $(document).ready(function(){

    $('#block-by-step-left-user-menu .menu-item .link-text').hyphenate();


    $( ".page-pupil-register #block-system-main .first-step-text" ).remove();
    $( ".page-teacher-register #block-system-main .first-step-text" ).remove();

    function AddDiv(){
      //$( ".page-user-register .base-user-login-form-wrapper .form-type-textfield" ).append( "<div class='clear'></div>" );
      $( ".page-user-register .teacher_reg_form .form-type-textfield" ).append( "<div class='clear'></div>" );
      $( ".page-user-register .teacher_reg_form  #edit-actions" ).append( "<div class='bot_reg_ten'></div><div class='clear'></div>" );
      $( ".page-user-register .base-user-login-form-wrapper  #edit-actions" ).append( "<div class='bot_reg_ten'></div><div class='clear'></div>" );
    };

    $('.page-user-register #container .reg_form_teach_pupil a').addClass('click');
    $('.page-user-register #container .reg_form_teach_pupil a').live('click', function(){ return false; })

    // Teacher
    $('.page-user-register #container .reg_form_teach_pupil .teacher_botton a.click').live('click', function(){

      $('.page-user-register #container .reg_form_teach_pupil .teacher_icon').addClass('light');
      $('.page-user-register #container .reg_form_teach_pupil .pupil_icon').removeClass('light');
      $('.page-user-register #container .reg_form_teach_pupil a').addClass('click');
      $(this).removeClass('click');
      $('.page-user-register #container .region-content > #block-system-main > .content > form #reg-form').slideUp("slow", function() { $(this).remove(); });
      $.ajax({
      url: "/teacher/register",
      type: "POST",
      cache: false,
      success: function(data){
        var page = $(data).find("#container .region-content > #block-system-main > .content > form #reg-form");
        var blockContent = $('#container .region-content > #block-system-main> .content > form');
        $(blockContent).append(page);
        AddDiv();

        $('.teach-form #edit-profile-teacher-prf-field-cel-reg').insertAfter('.teach-form #user_user_form_group_acc_info');
        $('.teach-form #edit-profile-teacher-prf-field-info-about-site').insertAfter('.teach-form fieldset#user_user_form_group_acc_info');
        $('.teach-form #edit-profile-teacher-prf-field-phone-number').insertAfter('.teach-form #user_user_form_group_acc_info .form-item-mail');
        $("#container .region-content > #block-system-main > .content > form #reg-form").slideDown("slow");
      }
      });
      return false;
    });

    $('.page-user-register #container .reg_form_teach_pupil .teacher_botton a.click').live('click', function(){
      $('.first-step-text').fadeOut("slow", function() { $(this).remove(); });
    });

    $('.page-user-register #container .reg_form_teach_pupil .pupil_botton a.click').live('click', function(){
      $('.first-step-text').fadeOut("slow", function() { $(this).remove(); });
    });

    // Puppil
    $('.page-user-register #container .reg_form_teach_pupil .pupil_botton a.click').live('click', function(){

      $('.page-user-register #container .reg_form_teach_pupil .teacher_icon').removeClass('light');
      $('.page-user-register #container .reg_form_teach_pupil .pupil_icon').addClass('light');

      $('.page-user-register #container .reg_form_teach_pupil a').addClass('click');
      $(this).removeClass('click');
      $('.page-user-register #container .region-content > #block-system-main > .content > form #reg-form').slideUp("slow", function() { $(this).remove(); });
      $.ajax({
      url: "/pupil/register",
      type: "POST",
      cache: false,
      success: function(data){
        var page = $(data).find("#container .region-content > #block-system-main > .content > form #reg-form");
        var blockContent = $('#container .region-content > #block-system-main > .content > form');
        $(blockContent).append(page);
        AddDiv();
            $("#reg-form select").each(function() {
              $(this).chosen();
            });
            $('.pupil-form #edit-profile-pupil-field-phone-number').insertAfter('.pupil-form #user_user_form_group_acc_info .form-item-mail');
            $('.pupil-form  .form-item-name').insertAfter('.page-user-register .pupil-form #user_user_form_group_acc_info')
          $("#container .region-content > #block-system-main > .content > form #reg-form").slideDown("slow");
      }
      });
      return false;
    });
  });

  // ajax courator register form validation
  $('.page-user-register #container .teacher_reg_form input.form-submit').live('click', function(){
    $('.er-mess').remove();
    $('input').removeClass('error');

    var json_param = {};
    json_param['c_m'] = $('.teacher_reg_form').find('#edit-mail').val();
    json_param['c_n'] = $('.teacher_reg_form').find('#edit-name').val();
    json_param['c_p'] = $('.teacher_reg_form').find('#edit-profile-teacher-prf-field-phone-number-und-0-value').val();
    json_param['c_rm'] = $('.teacher_reg_form').find('#edit-profile-teacher-prf-field-cel-reg-und-0-value').val();

    if(json_param['c_m'] === "" || json_param['c_n'] === "" || json_param['c_p'] === "" || json_param['c_rm'] === ""){
      $('.teacher_reg_form').find('#user_user_form_group_acc_info').prepend('<div class="er-mess">Заполните обязательные поля</div>');
    }else{

      if(!$.isNumeric(json_param['c_p'])){
        $('.teacher_reg_form').find('#edit-profile-teacher-prf-field-phone-number-und-0-value').addClass('error');
        $('.teacher_reg_form').find('.field-name-field-phone-number .form-item-profile-teacher-prf-field-phone-number-und-0-value').append('<div class="prev-err"><div class="er-mess">Неверный формат номера телефона</div></div>');
      }else{
        $.ajax({
          type: 'POST',
          url: 'http://' + window.location.hostname + '/user-reg-teacher',
          data: json_param,
          dataType: 'json',
          async: false,
          success: function(data){

            if(data['er_m'] == 1){
              $('.teacher_reg_form').find('#edit-mail').addClass('error');
              $('.teacher_reg_form').find('.form-item-mail').append('<div class="er-mess">Неверный формат электронной почты</div>');
            }

            else if(data['ex_n_m'] == 1){
              $('.teacher_reg_form').find('#edit-name').addClass('error');
              $('.teacher_reg_form').find('.form-item-name').append('<div class="er-mess">Имя пользователя уже существует</div>');
              $('.teacher_reg_form').find('#edit-mail').addClass('error');
              $('.teacher_reg_form').find('.form-item-mail').append('<div class="er-mess">Такой e-mail уже используется</div>');
            }

            else if(data['ex_n'] == 1){
              $('.teacher_reg_form').find('#edit-name').addClass('error');
              $('.teacher_reg_form').find('.form-item-name').append('<div class="er-mess">Имя пользователя уже существует</div>');
            }

            else if(data['ex_m'] == 1){
              $('.teacher_reg_form').find('#edit-mail').addClass('error');
              $('.teacher_reg_form').find('.form-item-mail').append('<div class="er-mess">Такой e-mail уже используется</div>');
            }

            else if(data['good'] == 1){
              window.location.href = 'http://' + window.location.hostname + '/welcome';
            }

          }
        });
      }

    }

    return false;
  });


  // ajax student register form validation
  $('.page-user-register #container .pupil-form input.form-submit').live('click', function(){
    $('div.er-mess-pupil').remove();
    $('input').removeClass('error');

    var json_param = {};
    json_param['s_m'] = $('.pupil-form').find('#edit-mail').val();
    json_param['s_n'] = $('.pupil-form').find('#edit-name').val();
    json_param['s_p'] = $('.pupil-form').find('#edit-profile-pupil-field-phone-number-und-0-value').val();

    if(json_param['s_m'] === "" || json_param['s_n'] === "" || json_param['s_p'] === ""){
      $('.pupil-form').find('input').addClass('error');
      $('.pupil-form').find('#user_user_form_group_acc_info').prepend('<div class="er-mess-pupil">Все поля обязательны для заполнения</div>');
    }else{

      if(!$.isNumeric(json_param['s_p'])){
        $('.pupil-form').find('#edit-profile-pupil-field-phone-number-und-0-value').addClass('error');
        $('.pupil-form').find('.field-name-field-phone-number .form-item-profile-pupil-field-phone-number-und-0-value').append('<div class="clear"></div><div class="prev-err"><div class="er-mess-pupil">Неверный формат номера телефона</div></div>');
      }else{
        $.ajax({
          type: 'POST',
          url: 'http://' + window.location.hostname + '/user-reg-pupil',
          data: json_param,
          dataType: 'json',
          async: false,
          success: function(data){

            if(data['er_m'] == 1){
              $('.pupil-form').find('#edit-mail').addClass('error');
              $('.pupil-form').find('.form-item-mail').append('<div class="clear"></div><div class="er-mess-pupil">Неверный формат электронной почты</div>');
            }

            else if(data['ex_n_m'] == 1){
              $('.pupil-form').find('#edit-name').addClass('error');
              $('.pupil-form').find('.form-item-name').append('<div class="clear"></div><div class="er-mess-pupil">Имя пользователя уже существует</div>');
              $('.pupil-form').find('#edit-mail').addClass('error');
              $('.pupil-form').find('.form-item-mail').append('<div class="er-mess-pupil">Такой e-mail уже используется</div>');
            }

            else if(data['ex_n'] == 1){
              $('.pupil-form').find('#edit-name').addClass('error');
              $('.pupil-form').find('.form-item-name').append('<div class="clear"></div><div class="er-mess-pupil">Имя пользователя уже существует</div>');
            }

            else if(data['ex_m'] == 1){
              $('.pupil-form').find('#edit-mail').addClass('error');
              $('.pupil-form').find('.form-item-mail').append('<div class="er-mess-pupil">Такой e-mail уже используется</div>');
            }

            else if(data['good'] == 1){
              window.location.href = 'http://' + window.location.hostname + '/pupil-welcome';
            }

          }
        });
      }
    }

    return false;
  });

  $(document).ready(function(){

    $('.node-type-testimonials .field-name-field-evaluation-bal .field-item a').live('click', function(){
      return false;
    });

    $("#course-node-form #edit-title").attr('placeholder', 'Введите названия курса, не более 120 символов');
    $("#course-node-form #edit-field-link-soc-kurs-und-0-value").attr('placeholder', 'Введите ссылку на группу курса в соц. сетях: vk.com, fb.com и др.');
    $("#course-node-form #edit-body-und-0-value").attr('placeholder', 'Введите краткое описание курса, не более 500 символов');
    $(".page-add-task- #task-test-node-form .field-type-text-with-summary textarea").attr('placeholder', 'Учтите у вас сейчас один вариант ответа, для создания вопроса вам нужно минимум 2');
    $('.page-add-task- #auto-test-node-form .form-type-textarea textarea').attr('placeholder', 'Учтите у вас сейчас один вариант ответа, для создания вопроса вам нужно минимум 2');

    $('.content-courses .course-image > img').live('click', function(){
      var courseId = $(this).attr('id');

      jQuery.ajax({
        url: 'http://' + window.location.hostname + window.location.pathname,
        type: "POST",
        dataType: "html",
        async:false,
        data: {
          courseId: $(this).attr('id'),
        },
        success: function( jqXHR, status, responseText ) {
          var content = $(jqXHR).find('#block-by-step-block-lessons-info');
          $( "#block-by-step-block-lessons-info" ).replaceWith(content);
        }
      });
    });


    // --- curator controls
    $('.page-add-lessons- div .lesson-number').live('click', function(){
      var lessonId = $(this).attr('id');
      $('.page-add-lessons- div .add-prezentation').live('click', function(){
        window.location.replace('http://' + window.location.hostname + '/add-prezentation/' + lessonId);
      });
    });

    $('.page-add-lessons- div .lesson-number').live('click', function(){

      var lessonId = $(this).attr('id');
      var pathname = window.location.pathname.split("/");
      var linkCourse = pathname[pathname.length-1];

      $('.page-add-lessons- div .add-video').live('click', function(){
        window.location.replace('http://' + window.location.hostname + '/add-video/' + linkCourse + '/' + lessonId);
      });
    });

    $('.page-add-lessons- div .lesson-number').live('click', function(){

      var lessonId = $(this).attr('id');

      $('.page-add-lessons- div .add-task').live('click', function(){
        window.location.replace('http://' + window.location.hostname + '/add-task/' + lessonId);
      });
    });

    $('.page-add-lessons- div .lesson-number').live('click', function(){

      var lessonId = $(this).attr('id');

      $('.page-add-lessons- div .delete-lesson').live('click', function(){
        window.location.replace('http://' + window.location.hostname + '/node/' + lessonId + '/delete');
      });
    });

    $('#block-with-history div.lesson-number').live('click', function(){

      var lessonId = $(this).attr('id')
      var lessCh = $(this).attr('status');

      $('#block-with-history div.to-task').live('click', function(){
        var typeC = $(this).parent().attr('type');

        if(lessCh == 1){
          if(typeC == 1){
            window.location.replace('http://' + window.location.hostname + '/lesson/log/' + lessonId);
          }
        } else {
          if(lessCh == 0) {
            window.location.replace('http://' + window.location.hostname + '/pupil-task/' + lessonId);
          }
        }
      });
    });

// --- students control
    $('.page-pupil-course- div.lesson-number').live('click', function(){

      var lessonId = $(this).attr('id')
      var lessCh = $(this).attr('status');

      $('.page-pupil-course- div.to-task').live('click', function(){
        var typeC = $(this).parent().attr('type');

        if(lessCh == 1){
          if(typeC == 1){
            window.location.replace('http://' + window.location.hostname + '/lesson/log/' + lessonId);
          }else{
            $('#block-with-lessons #student-message-popup').live('click');
          }
        } else {
          if(lessCh == 0) {
            window.location.replace('http://' + window.location.hostname + '/pupil-task/' + lessonId);
          }
        }
      });
    });

    $('.page-pupil-course- div.lesson-number').live('click', function(){
      var lessonId = $(this).attr('id');
      var lessCh = $(this).attr('status');
      var lessV = $(this).attr('v');

      $('.page-pupil-course- div.to-video').live('click', function(){
        var typeC = $(this).parent().attr('type');

        if(lessV > 0){
          if(typeC == 1 || lessCh != 2){
            window.location.replace('http://' + window.location.hostname + '/pupil-video/' + lessonId);
          }else{
             $('#block-with-lessons #student-message-popup').live('click');
          }
        }
      });
    });

    $('.page-pupil-course- div.lesson-number').live('click', function(){
      var lessonId = $(this).attr('id');
      var lessCh = $(this).attr('status');
      var lessP = $(this).attr('p');

      $('.page-pupil-course- div.to-prezentation').live('click', function(){
        var typeC = $(this).parent().attr('type');

        if(lessP > 0){
          if(typeC == 1 || lessCh != 2){
            window.location.replace('http://' + window.location.hostname + '/lesson-presentation/' + lessonId);
          }else{
             $('#block-with-lessons #student-message-popup').live('click');
          }
        }
      });
    });
/////////

    $('.page-add-lessons- div .lesson-number').live('click', function(){
      var lessonId = $(this).attr('id');
      $('.page-add-lessons- div .edit-lesson').live('click', function(){
        window.location.replace('http://' + window.location.hostname + '/edit-task/' + lessonId);
      });
    });

    $('.page-add-lessons- div .lesson-title').live('click', function(){
      var lessonId = $(this).attr('id');
      $('.page-add-lessons- div .edit-lesson').live('click', function(){
        window.location.replace('http://' + window.location.hostname + '/edit-task/' + lessonId);
      });
    });

    $('#block-by-step-block-lessons-add-video').appendTo('.video-upload-form');


    $('.page-add-task- .select-lesson .select-less select.form-select').change(function() {
      var lessonId = $(this).val();
      window.location.replace('http://' + window.location.hostname + '/add-task/' + lessonId);
    });

    $('.page-add-video- .select-lesson .select-less select.form-select').change(function() {

      var lessonId = $(this).val();
      var pathname = window.location.pathname.split("/");
      var linkCourse = pathname[pathname.length-2];
      window.location.replace('http://' + window.location.hostname + '/add-video/' + linkCourse + '/' + lessonId);
    });


    $('.page-pupil-task- .select-lesson .select-less select.form-select').change(function() {
      var lessonId = $(this).val();
      window.location.replace('http://' + window.location.hostname + '/pupil-task/' + lessonId);
    });

    $('.page-pupil-video- .select-lesson .select-less select.form-select').change(function() {
      var lessonId = $(this).val();
      window.location.replace('http://' + window.location.hostname + '/pupil-video/' + lessonId);
    });

    $('#edit-field-lesson-video-link-und-0-value').attr('placeholder', 'ссылка на видео с youtube.com');
    $('#edit-field-lesson-video-link-1-und-0-value').attr('placeholder', 'ссылка на видео с youtube.com');
    $('#edit-field-lesson-video-link-2-und-0-value').attr('placeholder', 'ссылка на видео с youtube.com');
    $('#edit-field-lesson-video-link-3-und-0-value').attr('placeholder', 'ссылка на видео с youtube.com');

    $('form#lesson-node-form input[name="title"]').keyup(function(e) {
      var string = $('form#lesson-node-form input[name="title"]').val();
      var res = string.substring(0, 120);
      $('form#lesson-node-form input[name="title"]').val(res);
    });

    $('form#lesson-node-form textarea[name="field_message[und][0][value]"]').keyup(function(e) {
      var string = $('form#lesson-node-form textarea[name="field_message[und][0][value]"]').val();
      var res = string.substring(0, 120);
      $('form#lesson-node-form textarea[name="field_message[und][0][value]"]').val(res);
    });

    $('form#course-node-form textarea[name="field_specializ_course[und][0][value]"]').keyup(function(e) {
      var string = $('form#course-node-form textarea[name="field_specializ_course[und][0][value]"]').val();
      var res = string.substring(0, 20);
      $('form#course-node-form textarea[name="field_specializ_course[und][0][value]"]').val(res);
    });

    $('form#course-node-form input[name="title"]').keyup(function(e) {
      var string = $('form#course-node-form input[name="title"]').val();
      var res = string.substring(0, 120);
      $('form#course-node-form input[name="title"]').val(res);
    });

  });

  $('#lesson-node-form #tabs .tab-triggers span').live('click', function(){
    if (!$(this).hasClass('active')) {
      var tab = $(this).attr('class');
      $('#lesson-node-form #tabs .tabs').removeClass('active');
      $('#lesson-node-form #tabs .tab-triggers span').removeClass('active');

      $('#lesson-node-form #tabs .tabs.' + tab).addClass('active');
      $('#lesson-node-form #tabs .tab-triggers span.' + tab).addClass('active');
    }
  });

  $(document).ajaxSuccess(function() {
      $(".page-add-task- #task-test-node-form .field-type-text-with-summary textarea").attr('placeholder', 'Учтите у вас сейчас один вариант ответа, для создания вопроса вам нужно минимум 2')
      $('.page-add-task- #auto-test-node-form  .form-type-textarea textarea').attr('placeholder', 'Учтите у вас сейчас один вариант ответа, для создания вопроса вам нужно минимум 2');
  });

  $(document).ajaxSuccess(function() {

    $('#block-by-step-block-test-task-add-form form.node-task_test-form .form-actions input[type="submit"]').live('click', function() {
      var answersCounter = [];

      $('#block-by-step-block-test-task-add-form form.node-task_test-form .field-name-field-task-test').each(function() {
        var t = $(this).find('table.field-multiple-table tbody').children().size();
        if(t > 0){
          answersCounter.push(t);
        }

      });

      if(jQuery.inArray(1, answersCounter) !== -1){
        return false;
        alert("Должно быть не менее двух вариантов ответа для теста");
      }
    });


    $('#block-by-step-courator-add-auto-test form.node-auto_test-form .form-actions input[type="submit"]').live('click', function() {
      var answersCounter = [];

      $('#block-by-step-courator-add-auto-test form.node-auto_test-form .field-name-field-auto-test-task').each(function() {
        var t = $(this).find('table.field-multiple-table tbody').children().size();
        if(t > 0){
          answersCounter.push(t);
        }
      });

      if(jQuery.inArray(1, answersCounter) !== -1){
        return false;
        alert("Должно быть не менее двух вариантов ответа для авто-теста");
      }
    });

    $('#block-by-step-courator-edit-task #task-edit-results form.node-task_test-form .form-actions input[type="submit"]').live('click', function() {
      var answersCounter = [];

      $('#block-by-step-courator-edit-task #task-edit-results form.node-task_test-form .field-name-field-task-test').each(function() {
        var t = $(this).find('table.field-multiple-table tbody').children().size();
        if(t > 0){
          answersCounter.push(t);
        }
      });

      if(jQuery.inArray(1, answersCounter) !== -1){
        return false;
        alert("Должно быть не менее двух вариантов ответа для теста");
      }
    });

    $('.select-pupil-lesson-switch').on('change', function (e) {

      var optionSelected = $("option:selected", this);
      var valueSelected = this.value;
      var uI = $(this).attr('id');

      var pathname = window.location.pathname.split("/");
      var linkCourse = pathname[pathname.length-1];

      var json_param = {};
      json_param['cID'] = linkCourse;
      json_param['uID'] = uI;
      json_param['lesson_switch'] = valueSelected;

      $.ajax({
          type: 'POST',
          url: 'http://' + window.location.hostname + '/courator/pupil-switch',
          data: json_param,
          dataType: 'json',
          async: false,
          success: function(data){
            $('#table-pupil-search-results #table-content').empty();
            $('#table-pupil-search-results #table-content').append(data.status);
          }
      });
    });

    if($("div").is(".task-timer")){
      TaskTimer('.task-timer');
    }

    $('form.node-task_test-form > div > .field-name-field-task-question > div > div > .field-multiple-table > tbody > tr').each(function(i){
      $('.tt_msg').remove();
      var index = i + 1;
      $(this).prepend('<td class="question-number">Вопрос №' + index + '</td>');
      if(index == 10) {
        $('#block-by-step-block-test-task-add-form form.node-task_test-form .field-name-field-task-question > div > div > div.clearfix input').remove();
        $('#block-by-step-block-test-task-add-form form.node-task_test-form .field-name-field-task-question > div > div > div.clearfix').append('<div style="display:none;" class="tt_msg"><span>При создании задания Вы можете создать только 10 вопросов. Если Вы хотите добавить еще вопросы, Вы сможете это сделать после создания задания, тоесть при редактировании.</span></div>');
        $('.tt_msg').fadeIn('400');
      }
    });

    $('form.node-auto_test-form > div > .field-name-field-auto-test-quest > div > div > .field-multiple-table > tbody > tr').each(function(i){
      $('.t_msg').remove();
      var index = i + 1;
      $(this).prepend('<td class="question-number">Вопрос №' + index + '</td>');
      if(index == 10) {
        $('#block-by-step-courator-add-auto-test form.node-auto_test-form .field-name-field-auto-test-quest > div > div > div.clearfix input').remove();
        $('#block-by-step-courator-add-auto-test form.node-auto_test-form .field-name-field-auto-test-quest > div > div > div.clearfix').append('<div style="display:none;" class="t_msg"><span>При создании задания Вы можете создать только 10 вопросов. Если Вы хотите добавить еще вопросы, Вы сможете это сделать после создания задания, тоесть при редактировании.</span></div>');
        $('.t_msg').fadeIn('400');
      }
    });
  });

  function TaskTimer (selector) {
    var time = parseInt($(selector).attr('time'));
    $(selector).html(TimerList(time));

    var t = parseInt($(selector).attr('time'));

    setInterval(function() {
      t--;
      $(selector).html(TimerList(t));
      if (t == 0) {
        var taskID = $('input[name="field_task_id"]').val();
        var redirectPath = $(location).attr('href') + '?task='+taskID+'&false='+ false;
        window.location.replace(redirectPath);
      }
    }, 1000);
  };

  function TimerList (time) {
    var timeSec = time%60;
    var timeMin = (time-timeSec)/60;
    if (timeMin >= 60) {
      var timeMin = timeMin%60;
      var timeHour = 1;
      var timeHTML = '<div id="task-timer"><div class="timer-hour">' + timeHour + '</div> : <div class="timer-minutes">' + timeMin + '</div> : <div class="timer-seconds">' + timeSec + '</div></div>';

    } else {
      var timeHTML = '<div id="task-timer"><div class="timer-minutes">' + timeMin + '</div> : <div class="timer-seconds">' + timeSec + '</div></div>';
    }
    return timeHTML;
  };


  $(document).ready(function() {
    $('.page-add-video .field-type-text .form-type-textfield .text-full').keyup(function() {
      if ($(this).parents('.tabs').find('.form-type-managed-file .file-widget > span').hasClass('file-size')) {
        $(this).val('');
        alert('Для добавления ссылки на видео удалите загруженый файл!');
      }
    });

    $('.page-add-video .form-managed-file > .customfile > .ui-button > .ui-button-text').mousedown(function() {
      var t = $(this).parents('.tabs').find('.field-type-text .form-type-textfield .text-full').val();
      // console.log(t);
      if (t != '') {
        $(this).val('');
        alert('Для добавления видео файла удалите ссылку на видео!');
        return false;
      } else {
        return true;
      }
    });
  });

  $(document).ajaxSuccess(function() {
    $('.page-pupil-task .node-text_type_task_answer-form .form-submit').mousedown(function() {
      var t = $('.page-pupil-task #text-type-task-answer-node-form iframe').html();
    });
  });

  $(function() {
    $(window).scroll(function() {
      if($(this).scrollTop() != 0) {
        $('#toTop').fadeIn();
      } else {
        $('#toTop').fadeOut();
      }
    });
    $('#toTop').click(function() {
      $('body,html').animate({scrollTop:0},800);
    });
  });


})(jQuery);
