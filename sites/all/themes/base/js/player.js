(function ($) {
    $(function (){
    var padding=0;
    if( $('body.front').length > 0){
      $('#block-views-video-slide-block .view-footer object').each(function(){
          $(this).width('485px');
          $(this).height('305px');
      });
      $('#block-views-video-slide-block .view-footer embed').each(function(){
          $(this).width('485px');
          $(this).height('305px');
      });
      $('#block-views-video-slide-block .view-video-slide >.view-content .views-row:first').addClass('activeTitle');
      $('#block-views-video-slide-block .view-footer .views-row:first').show();
      $('#block-views-video-slide-block .view-video-slide >.view-content .views-row').bind('click', function(){
        $('.activeTitle').removeClass('activeTitle');
        $(this).addClass('activeTitle');
        $('#block-views-video-slide-block .view-footer .views-row:visible').hide();
        $('#block-views-video-slide-block .view-footer .views-row').eq($(this).index()).show();
        // return false;
      });
    }
  });
})(jQuery);
