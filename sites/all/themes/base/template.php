<?php
// $Id: template.php,v 1.45 2010/12/01 00:18:15 webchick Exp $


/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function base_next_page_link($node){
  $next_nid = db_query("
          SELECT nid FROM {node}
          WHERE created > :created AND type = :type AND status = 1
          LIMIT 1",
          array(':created' => $node->created,':type' => $node->type))
          ->fetchObject();
  if($next_nid) {
    return '/'.drupal_get_path_alias('node/'.$next_nid->nid);
  }
  else {
    return false;
  }
}

function base_previous_page_link($node){
  $next_nid = db_query("
          SELECT nid FROM {node}
          WHERE created < :created AND type = :type AND status = 1
          ORDER BY created DESC
          LIMIT 1",
          array(':created' => $node->created,':type' => $node->type))
          ->fetchObject();

  if($next_nid) {
    return '/'.drupal_get_path_alias('node/'.$next_nid->nid);
  }
  else {
    return false;
  }
}



function base_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}


function base_css_alter(&$css) {
  foreach ($css as $k => $style) {
    $n = $k;
//    $n = preg_replace('/(.*)(jquery.ui..*?)\..*css/', drupal_get_path("theme", "base") . '/replace/$2.css', $n);
//    $n = preg_replace('/.*colorbox.*/', drupal_get_path("theme", "base") . '/replace/colorbox.css', $n);
    $n = preg_replace('/.*system.messages.*/', drupal_get_path("theme", "base") . '/replace/system.messages.css', $n);
    if ($n != $k) {
      $css[$k]['data'] = $n;
      $css[$n] = $css[$k];
      unset($css[$k]);
    }
  }
}


function base_preprocess_user_login(&$vars) {
  $vars['intro_text'] = t('');
}


function base_preprocess_user_register_form(&$vars) {
  $vars['intro_text'] = t('');
}


function base_preprocess_testimonials_node_form(&$vars) {
  $vars['intro_text'] = t('');
}


function base_theme() {
  $items = array();

    $items['user_login'] = array(
        'render element' => 'form',
        'path' => drupal_get_path('theme', 'base') . '/tpl',
        'template' => 'user-login',
        'preprocess functions' => array(
          'base_preprocess_user_login'
      ),
    );
    $items['user_register_form'] = array(
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'base') . '/tpl',
      'template' => 'user-register-form',
      'preprocess functions' => array(
        'base_preprocess_user_register_form'
      ),
    );

  return $items;
}


/**
 * Override or insert variables into the maintenance page template.
 */
function base_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // base_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  base_preprocess_html($vars);
}


/**
 * Override or insert variables into the html template.
 */
function base_preprocess_html(&$vars) {
  global $user;

//  libraries_load("prefixfree");
  libraries_load("modernizr");
  drupal_add_library("system", "ui.button");
  drupal_add_js(drupal_get_path('theme', 'base') . "/third-party/jQuery-UI-FileInput/js/fileinput.jquery.js");
  drupal_add_js(drupal_get_path('theme', 'base') . "/js/init.js");
  if((arg(0)=='pupils_lessons')||(arg(0)=='pupils_task')){
    drupal_add_js(drupal_get_path('theme', 'base') . "/js/jquery.cookie.js");
    drupal_add_js($GLOBALS['base_root'] . "/jwplayer/jwplayer.js");
    drupal_add_js(drupal_get_path('theme', 'base') . "/js/lesson_video.js");
  }
  if((arg(0)=='node')&&(arg(1))){
    // $node=node_load(arg(1));
    // if($node->type=='materials'){
    //   drupal_add_js(drupal_get_path('theme', 'base') . "/js/jquery.cookie.js");
    //   drupal_add_js($GLOBALS['base_root'] . "/jwplayer/jwplayer.js");
    //   drupal_add_js(drupal_get_path('theme', 'base') . "/js/materials_video.js");
    // }
  }

  $user_msg = variable_get('sys_msg', '');
  $user_msg_role = variable_get('sys_msg_role', 1);
  if(array_key_exists($user_msg_role, $user->roles) && !empty($user_msg)){
    $vars['sys_msg'] = '<div class="sys_msg_head">' . $user_msg . '</div>';
  }


  // $path = 'your-drupal-page';
  // if (drupal_match_path($_GET['q'], $path) || drupal_match_path(drupal_get_path_alias($_GET['q']), $path)) {
  //   $vars['classes_array'][] = 'page-my-css-class';
  // }
}


/**
 * Override or insert variables into the html template.
 */
function base_process_html(&$vars) {
  // Hook into color.module
}


/**
 * Override or insert variables into the page template.
 */
function base_preprocess_page(&$vars) {
  global $user;

  //  $private_pages = array(
  //   'user/*',
  // );
 
  // if (drupal_match_path(current_path(), implode(PHP_EOL, $private_pages))) {
  //   $vars['header_link'] = '';
 
  //   $vars['theme_hook_suggestions'] = array('page__private');
  //   drupal_add_css(drupal_get_path('module', 'uplvl_user') . '/css/reset.css');
  //   drupal_add_css(drupal_get_path('module', 'uplvl_user') . '/css/private.css');
  // }

  if(arg(0) == 'user' && $user->uid != arg(1)){
    $vars['tabs']['#primary'][1]['#link']['title'] = t('Редактировать кабинет');
  }

  if (isset($vars['node'])) {
     $vars['theme_hook_suggestions'][] = 'page__'. str_replace('_', '--', $vars['node']->type);
  }

  // Move secondary tabs into a separate variable.
  $vars['tabs2'] = array(
    '#theme'     => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );
  unset($vars['tabs']['#secondary']);

  if (isset($vars['main_menu'])) {
    $vars['primary_nav'] = theme('links__system_main_menu',
      array(
        'links'      => $vars['main_menu'],
        'attributes' => array(
          'class' => array('links', 'inline', 'main-menu'),
        ),
        'heading'    => array(
          'text'  => t('Main menu'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        )
      ));
  }
  else {
    $vars['primary_nav'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_nav'] = theme('links__system_secondary_menu',
      array(
        'links'      => $vars['secondary_menu'],
        'attributes' => array('class' => array('links', 'inline', 'secondary-menu'),),
        'heading'    => array('text' => t('Secondary menu'), 'level' => 'h2', 'class' => array('element-invisible'),)
      ));
  }
  else {
    $vars['secondary_nav'] = FALSE;
  }

  // Prepare header.
  $site_fields = array();
  if (!empty($vars['site_name'])) {
    $site_fields[] = "<span class=name>{$vars['site_name']}</span>";
  }
  if (!empty($vars['site_slogan'])) {
    $site_fields[] = "<span class=slogan>{$vars['site_slogan']}</span>";
  }

  $vars['site_html'] = implode(' ', $site_fields);

  // Set a variable for the site name title and logo alt attributes text.
  $slogan_text = variable_get('site_slogan');
  $site_name_text = variable_get('site_name');
  $vars['site_name_and_slogan'] = trim($site_name_text . ' - ' . $slogan_text, ' -');
}


/**
 * Override or insert variables into the node template.
 */
function base_preprocess_node(&$vars) {
  $vars['submitted'] = $vars['date'] . ' — ' . $vars['name'];

}


/**
 * Override or insert variables into the comment template.
 */
function base_preprocess_comment(&$vars) {
  $vars['submitted'] = $vars['created'] . ' — ' . $vars['author'];
}


/**
 * Override or insert variables into the block template.
 */
function base_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'title';
  $vars['classes_array'][] = 'clearfix';
  if (in_array("block-menu", $vars['classes_array'])) {
    $vars['theme_hook_suggestions'][] = 'block__menu';
    drupal_add_js("Drupal.behaviors.numerateMenu.attach(jQuery('#" . $vars["block_html_id"] . "'));", array(
      'type'  => 'inline',
      'scope' => 'footer'
    ));
  }
}


/**
 * Override or insert variables into the page template.
 */
function base_process_page(&$vars) {
  // Hook into color.module
  if($vars['title']=='Создание материала Урок'){
     $vars['title']= 'Создать урок';
  }
  if(arg(0)=='user'){
    $vars['title']= 'Профиль '.$vars['title'];
  }
}


/**
 * Override or insert variables into the region template.
 */
function base_preprocess_region(&$vars) {
  if ($vars['region'] == 'header') {
    $vars['classes_array'][] = 'clearfix';
  }
}


function base_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}


function base_process_html_tag(&$vars) {
  $el = &$vars['element'];

  // Remove type="..." and CDATA prefix/suffix.
  unset($el['#value_prefix'], $el['#value_suffix']);

  // Remove media="all" but leave others unaffected.
  if (isset($el['#attributes']['media']) && $el['#attributes']['media'] === 'all') {
    unset($el['#attributes']['media']);
  }
}


function base_form_alter(&$form, &$form_state, $form_id) {

  if($form_id=='user_profile_form'){
   $form['account']['mail']['#type'] = 'hidden';
   $form['locale']['#type'] = 'hidden';
   $form['picture']['#weight'] = 100;
   $form['picture']['#title'] = '';
   unset($form['field_cel_reg']);
   unset($form['field_user_take_tech']);
  }

  if($form_id=='lesson_node_form'){
    $form['title']['#title'] = 'Название урока';
    $form['body']['und']['0']['#title'] = 'Описание урока';
    // if(isset($form['#node']->nid)){
    //   $course = get_course_by_lesson($form['#node']->nid);
    //   $pupil = lesson_if_is_pupil($course['cid'], $course['weight']);
    //   if($pupil){
    //     unset($form['actions']['delete']);
    //   }
    // }
  }

  if(isset($form['#node']->webform['components'])&&$form['#node']->type=='lesson'){
    global $user;
    if(arg(2)=='webform') $user1=$user;
    else
      if(isset($user->roles[5])||isset($user->roles[4]))
        $user1=user_load(arg(1));
    else $user1=$user;
    // if(isset($form['#node']->nid)&&(arg(2)!='edit')&&(arg(1)!='node')&&(!is_numeric(arg(1))) && arg(0) != 'add-prezentation'){
    //   $if_is_value = get_sid_by_user($user1->uid,$form['#node']->nid);
    //   $submission_data=webform_menu_submission_load($if_is_value, $form['#node']->nid);
    //   $course = get_course_by_lesson($form['#node']->nid);
    //   $enable_lesson = access_to_lesson($course['cid'], $user->uid);
    //   if($enable_lesson)
    //     if($enable_lesson['lid']<>$course['weight'])
    //       unset($form['actions']);
    //   $i=0;
    //   if($submission_data)
    //     if(isset($form["submitted"]))
    //     foreach ($form["submitted"] as $key => $value) {
    //       if(in_array($value["#type"], array("textfield", "webform_email", "textarea"))) {
    //         $i++;
    //         $coment=get_user_comment_by_index($submission_data->sid,$i);
    //         if(isset($submission_data->data[$i]['value'][0]))
    //           $form["submitted"][$key]['#default_value'] = $submission_data->data[$i]['value'][0];
    //         if($coment!=false)
    //           $form["submitted"][$key]['#comment_attr'] = $coment['message'];
    //       }
    //     }
    // }
    if(arg(2)=='webform'){
      $form['add']['type']['#options']= array('textarea'=>'Текстовая область');
      $form['add']['mandatory']['#value']= 1;
    }
  }
}


function base_button($variables) {

  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  return '<input type=submit' . drupal_attributes($element['#attributes']) . ' />';
//  return '<button' . drupal_attributes($element['#attributes']) . ' >' . $element["#value"] . '</button>';
}


/**
 * Implements preprocess_thema_block()
 */
function base_preprocess_thema_block(&$variables) {
  $variables['classes_array'][] = $variables['element']['class'];
  $variables['title'] = $variables['element']['title'];
  $variables['content'] = $variables['element']['#children'];
  $variables['more'] = $variables['element']['more'];
}
