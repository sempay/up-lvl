<?php
/**
 * @file
 * july11_n_types.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function july11_n_types_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-course-field_if_lesson_open'
  $field_instances['node-course-field_if_lesson_open'] = array(
    'bundle' => 'course',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_if_lesson_open',
    'label' => 'Уроки открытые',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-lesson-field_finish_page'
  $field_instances['node-lesson-field_finish_page'] = array(
    'bundle' => 'lesson',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_finish_page',
    'label' => 'Этот урок является заключительным',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Уроки открытые');
  t('Этот урок является заключительным');

  return $field_instances;
}
