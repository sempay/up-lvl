<?php
/**
 * @file
 * july11_n_types.features.inc
 */

/**
 * Implements hook_node_info().
 */
function july11_n_types_node_info() {
  $items = array(
    'course' => array(
      'name' => t('Курс'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'lesson' => array(
      'name' => t('Урок'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
