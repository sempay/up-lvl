<div class="fields-container">
  <div class="user-name"><?php print implode(' ', array($last_name, $first_name, $middle_name)); ?></div>
  <div class="years-city">
    <?php
      if ($years):
    ?>
      <div class="user-age">
        <?php
          print $years;
        ?>
      </div>
    <?php
      endif;
      if ($city):
    ?>
      <div class="user-city">
        <?php
          print $city;
        ?>
      </div>
    <?php
      endif;
    ?>
  </div>
  <div class="user-info">
    <div class="left">
      <?php
        if ($company):
      ?>
        <div class="user-company">
          <?php
            print t('Company: !company', array(
              '!company' => '<span>' . $company . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($job_title):
      ?>
        <div class="user-job-title">
          <?php
            print t('Job Title: !job_title', array(
              '!job_title' => '<span>' . $job_title . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($education):
      ?>
        <div class="user-education">
          <?php
            print t('Education: !education', array(
              '!education' => '<span>' . $education . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($languages):
      ?>
        <div class="user-languages">
          <?php
            print '<div>' . t('Foreign languages: !languages', array(
              '!languages' => '</div><span>' . $languages . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
    <div class="right">

      <div class="user-e-mail">
        <?php
          print t('E-mail: !email', array(
            '!email' => '<span>' . $account->mail . '</span>',
          ));
        ?>
      </div>

      <?php
        if ($phone):
      ?>
        <div class="user-phone">
          <?php
            print t('Phone: !phone', array(
              '!phone' => '<span>' . $phone . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
        if ($skype):
      ?>
        <div class="user-skype">
          <?php
            print t('Skype: !skype', array(
              '!skype' => '<span>' . $skype . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
        if ($vk):
      ?>
        <div class="user-vk">
          <?php
            print t('Vk: !vk', array(
              '!vk' => '<span>' . $vk . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
        if ($ok):
      ?>
        <div class="user-ok">
          <?php
            print t('Ok: !ok', array(
              '!ok' => '<span>' . $ok . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
        if ($fb):
      ?>
        <div class="user-fb">
          <?php
            print t('Fb: !fb', array(
              '!fb' => '<span>' . $fb . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
    <div class="bottom">
      <?php
        if ($experience):
      ?>
        <div class="user-experience">
          <?php
            print t('Experience: !experience', array(
              '!experience' => '<span>' . $experience . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($about):
      ?>
        <div class="user-about">
          <?php
            print t('About: !about', array(
              '!about' => '<span>' . $about . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
  </div>
</div>