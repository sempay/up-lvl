<?php

global $language;

$languages  = language_list('enabled');
$languages  = current($languages);
?>
<div class="mini-profile clearfix">
  <?php
    if (!empty($picture->uri)) {
      print '<div class="picture col-xs-12">';
      print theme('image', array(
        'path'       => $picture->uri,
        'style_name' => '118x118_sc',
        'attributes' => array(
          'class' => 'img-responsive',
          'getsize' => FALSE,
        ),
      ));
      print '</div>';
    }

  ?>
  <div class="name"><?php print implode(' ', array($first_name, $last_name)); ?></div>
  <div class="language-switcher">
    <a href="" class="current"><?php print strtoupper($language->language); ?></a>
    <div class="list">
      <?php
        foreach ($languages as $lang):
          print l(strtoupper($lang->language), current_path(), array('language' => $lang));
        endforeach;
      ?>
    </div>
  </div>
  <?php
    // foreach ($languages as $language) {

    // }
  ?>
  <?php
    print l(t('Logout'), 'user/logout', array(
      'attributes' => array(
        'class' => array(
          'button-logout',
        ),
      ),
    ));
  ?>
</div>