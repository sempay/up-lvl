<?php
 
/**
 * Form with AJAX example
 */
function custom_dev_form($form, &$form_state) {
  // Загружаем все существующие списком преподователей
  $options = custom_dev_get_teacher();
  // Создаём в форме выпадающий список, элементами которого являются списком преподователей.
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Выбери преподавателя'),
    '#options' => $options,
	'#required' => true,
    // Здесь подключаем ajax. При выборе элемента из выпадающего списка данные формы
    // будут переданы в функцию form_ajax_form_load_nodes(). Отработав, функция вернёт
    // данные, которые будут помещены в <div id = "form-ajax-nodes"></div>, заменив при этом
    // все данные, которые до этого там были (чтобы не плодить большое количество данных на странице).
    // В нашем случае функция вернёт эту же форму, но с выбранными элементами.
    '#ajax' => array(
      'callback' => 'custom_dev_form_load_nodes',
      'wrapper' => 'form-ajax-nodes',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
 //debug($form);
  // Если сработал AJAX - в $form_state будут переданы все значения формы.
  // Поэтому здесь проверка - если сработал AJAX - то выбранный тип материала берётся из
  // отправленной формы. А если не сработал - то тип будет первым элементом массива всех типов материалов.
  if (isset($form_state['values']['name'])) {
    $name = $form_state['values']['name'];
  }
  else {
    $name = array_shift($options);
  }
 
  // Устанавливаем значение по умолчанию для выпадающего списка с типами материалов.
  $form['name']['#default_value'] = $name;
 
  // Загружаем 10 последних материалов выбранного типа материала.
  // Здесь ключом является nid ноды, а значением - её заголовок.
  // Так же оборачиваем этот элемент в div, в который будет помещаться
  // обновлённая часть формы
  $options = custom_dev_get_teacher();
  $form['node'] = array(
    '#type' => 'select',
    '#title' => t('Nodes from !name name',  array('!name' => $name)),
    '#options' => $options,
    '#prefix' => '<div id = "form-ajax-nodes">',
    '#suffix' => '</div>',
	'#required' => true,
  );
 
  // Кнопка, при нажатии на которую будет отправлен AJAX запрос, обрабатываемый
  // функцией form_ajax_form_load_node_content(). Она вернёт полное содержимое выбранной ноды
  // и поместит его в <div id = "form-ajax-node-content"></div>, заменив предыдущее содержимое,
  // если оно было.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Load content'),
	'#required' => true,
    '#ajax' => array(
      'callback' => 'custom_dev_form_teacher',
      'wrapper' => 'form-ajax-node-content',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
 
  // Создаём в форме элемент страницы, куда будет помещено полное отображение ноды
  $form['markup'] = array(
    '#prefix' => '<div id = "form-ajax-node-content">',
    '#suffix' => '</div>',
    '#markup' => '',
	'#required' => true,
  );
 
  return $form;
}
 
// /**
 // * AJAX callback for loading node list
 // */
// function custom_dev_form_load_nodes($form, $form_state) {
  // // Возвращаем элемент формы, который должен быть перезагружен.
  // // В данном случае надо перезагрузить выпадающий список с материалами.
  // return $form['node'];
// }
 
// /**
 // * Return full view of selected node
 // */
// function custom_dev_form_load_node_content($form, $form_state) {
  // // Возвращаем элемент формы, который должен быть перезагружен.
  // // В данном случае надо перезагрузить элемент, который содержит представление ноды.
  // if (isset($form_state['values']['node'])) {
    // // Если есть в форме выбранный материал - загружаем его,
    // // загружаем полное представление ноды, и отдаём отрендереное представление в качестве элемента формы.
    // $nid = $form_state['values']['node'];
    // $node = node_load($nid);
    // $view = node_view($node);
    // $form['markup']['#markup'] = render($view);
  // }
  // return $form['markup'];
// }