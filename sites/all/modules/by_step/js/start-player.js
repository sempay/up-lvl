(function ($) {


  $(document).ready(function() {
    $('video').mediaelementplayer({
      alwaysShowControls: true,
      videoVolume: 'horizontal',
      features: ['playpause','progress','current','duration','tracks','volume','fullscreen'],
    });

  });



})(jQuery);
