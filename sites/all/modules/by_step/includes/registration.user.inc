<?php

define('DEFAULT_PASS', 1);


/**
 *  callback function for /user-reg-teacher page
 *  see hook_menu in by_step.module
 */
function _teacher_reg() {
  global $user;

  if(!empty($_POST['c_n']) && !empty($_POST['c_m']) && !empty($_POST['c_p']) && !empty($_POST['c_rm'])){

    if(!valid_email_address($_POST['c_m'])){
      $out['er_m'] = 1;
    }else{
      $sql_mail = db_select('users', 'u');
      $sql_mail->fields('u', array('uid'));
      $sql_mail->condition('u.mail', db_like($_POST['c_m']), 'LIKE');
      $mail_res = $sql_mail->execute()->fetchAll();
    }

    $sql_name = db_select('users', 'u');
    $sql_name->fields('u', array('uid'));
    $sql_name->condition('u.name', db_like($_POST['c_n']), 'LIKE');
    $name_res = $sql_name->execute()->fetchAll();

    if(empty($name_res)){ $data['name'] = $_POST['c_n']; }
    if(empty($mail_res)){ $data['mail'] = $_POST['c_m']; }

    if(!empty($data['mail']) && !empty($data['name'])){
      $fields = array(
          'name' => $data['name'],
          'mail' => $data['mail'],
          'pass' => DEFAULT_PASS,
          'status' => 1,
          'init' => $data['mail'],
          'roles' => array(4 => TRUE),
          'access' => time(),
      );
      $account = user_save('', $fields);

      send_mail_to_user($data['name'], $data['mail']);
      send_mail_to_admin_teacher($data['name'], $data['mail'], $_POST['c_p'], $_POST['c_rm']);

      if ($uid = user_authenticate($data['name'], DEFAULT_PASS)) {
        $user = user_load($uid);
        $user->field_phone_number['und'][0]['value'] = $_POST['c_p'];
        user_save($user);
      }
    }

    if(empty($data['name']) && empty($data['mail'])) {
      $out['ex_n_m'] = 1;
    }else{

      if(empty($data['name'])) {
        $out['ex_n'] = 1;
      }
      elseif(empty($data['mail'])){
        $out['ex_m'] = 1;
      }
      else{
        $out['good'] = 1;
      }

    }

    return drupal_json_output($out);
  }
}


/**
 *  callback function for /user-reg-pupil page
 *  @see hook_menu in by_step.module
 */
function _pupil_reg() {
  global $user;

  $out = array();
  if(!empty($_POST['s_m']) && !empty($_POST['s_p']) && !empty($_POST['s_n'])){

    if(!valid_email_address($_POST['s_m'])){
      $out['er_m'] = 1;
    }else{
      $sql_mail = db_select('users', 'u');
      $sql_mail->fields('u', array('uid'));
      $sql_mail->condition('u.mail', db_like($_POST['s_m']), 'LIKE');
      $mail_res = $sql_mail->execute()->fetchAll();
    }

    $sql_name = db_select('users', 'u');
    $sql_name->fields('u', array('uid'));
    $sql_name->condition('u.name', db_like($_POST['s_n']), 'LIKE');
    $name_res = $sql_name->execute()->fetchAll();

    if(empty($name_res)){ $data['name'] = $_POST['s_n']; }
    if(empty($mail_res)){ $data['mail'] = $_POST['s_m']; }

    if(!empty($data['mail']) && !empty($data['name'])){
      $fields = array(
          'name' => $data['name'],
          'mail' => $data['mail'],
          'pass' => DEFAULT_PASS,
          'status' => 1,
          'init' => $data['mail'],
          'roles' => array(6 => TRUE),
          'access' => time(),
      );
      $account = user_save('', $fields);

      send_mail_to_user($data['name'], $data['mail']);
      send_mail_to_admin_pupil($data['name'], $data['mail'], $_POST['s_p']);

      if ($uid = user_authenticate($data['name'], DEFAULT_PASS)) {
        $user = user_load($uid);
        $user->field_phone_number['und'][0]['value'] = $_POST['s_p'];
        user_save($user);
      }
    }

    if(empty($data['name']) && empty($data['mail'])) {
      $out['ex_n_m'] = 1;
    }else{

      if(empty($data['name'])) {
        $out['ex_n'] = 1;
      }
      elseif(empty($data['mail'])){
        $out['ex_m'] = 1;
      }
      else{
        $out['good'] = 1;
      }

    }

    return drupal_json_output($out);
  }
}


/**
 *  function for sending mail to user after registration
 */
function send_mail_to_user($u_name, $u_mail) {

  global $user;

  $from = variable_get('site_mail', '');
  $str_body = _user_mail_text('register_no_approval_required_body', NULL, array(), FALSE);
  $symbol_body = $u_mail;
  $mailbody = str_replace('[user:mail]', "$symbol_body", $str_body);

  $str = _user_mail_text('register_pending_approval_subject', NULL, array(), FALSE);
  $symbol = $u_name;
  $mailsubject = str_replace('[user:name]', "$symbol", $str);

  drupal_mail('system', 'mail', $u_mail, language_default(),  array(
    'context' => array(
      'subject' => $mailsubject,
      'message' => $mailbody,
    )
  ), $from);
}


function send_mail_to_admin_pupil($u_name, $u_mail, $u_phone) {


  $sql_name = db_select('users', 'u');
  $sql_name->fields('u', array('uid'));
  $sql_name  ->condition('u.mail', db_like($u_mail), 'LIKE');
  $name_res = $sql_name->execute()->fetchAll();
  $user_uid = $name_res[0]->uid;

  $mail[0] = 'evgenytop@gmail.com';
  $mail[1] = 'addminoz@gmail.com';


  $from = variable_get('site_mail', '');
  $params['field_pupil_reg_name'] = $u_name;
  $params['field_pupil_reg_mail'] = $u_mail;
  $params['field_pupil_reg_phone'] = $u_phone;
  $params['field_pupil_reg_id'] = $user_uid;

  foreach ($mail as $value) {

    drupal_mail('system', 'mail', $value, language_default(),  array(
      'context' => array(
        'subject' => t('новий ученик на сайте ' . variable_get('site_name', '')),
        'message' => t('
  ************************************************************************

   ID ученика: !pupil_id
   Имя пользователя: !pupil_name
   E-mail пользователя: !pupil_mail
   Номер телефона пользователя: !pupil_phone

  ************************************************************************
  ',
        //  массив значений которіе мы берем из полей формы
        array(
          '!pupil_id' => $params['field_pupil_reg_id'],
          '!pupil_name' => $params['field_pupil_reg_name'],
          '!pupil_mail' => $params['field_pupil_reg_mail'],
          '!pupil_phone' => $params['field_pupil_reg_phone']
        )
      ),
      )
    ), $from);

  }


}


function send_mail_to_admin_teacher($u_name, $u_mail, $u_phone, $u_quest, $u_find) {

  $sql_name = db_select('users', 'u');
  $sql_name->fields('u', array('uid'));
  $sql_name  ->condition('u.mail', db_like($u_mail), 'LIKE');
  $name_res = $sql_name->execute()->fetchAll();
  $user_uid = $name_res[0]->uid;

  $mail[0] = 'evgenytop@gmail.com';
  $mail[1] = 'addminoz@gmail.com';

  $from = variable_get('site_mail', '');
  $params['field_teacher_reg_name'] = $u_name;
  $params['field_teacher_reg_mail'] = $u_mail;
  $params['field_teacher_reg_phone'] = $u_phone;
  $params['field_teacher_reg_question'] = $u_quest;
  $params['field_teacher_reg_id'] = $user_uid;
  $params['field_teacher_reg_site_info'] = $u_find;


  foreach ($mail as $value) {

    drupal_mail('system', 'mail', $value, language_default(),  array(
      'context' => array(
        'subject' => t('новий преподаватель на сайте ' . variable_get('site_name', '')),
        'message' => t('
  ************************************************************************

   ID преподавателя: !teacher_id
   Имя пользователя: !teacher_name
   E-mail пользователя: !teacher_mail
   Номер телефона пользователя: !teacher_phone
   Цель регистрации на платформе?: !teacher_question
   Как Вы узнали о Up-lvl.ru?: !teacher_find_site

  ************************************************************************
  ',
        //  массив значений которіе мы берем из полей формы
        array(
          '!teacher_id' => $params['field_teacher_reg_id'],
          '!teacher_name' => $params['field_teacher_reg_name'],
          '!teacher_mail' => $params['field_teacher_reg_mail'],
          '!teacher_phone' => $params['field_teacher_reg_phone'],
          '!teacher_question' => $params['field_teacher_reg_question'],
          '!teacher_find_site' => $params['field_teacher_reg_site_info']
        )
      ),
      )
    ), $from);
  }

}
