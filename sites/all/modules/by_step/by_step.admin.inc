<?php

function by_step_admin_page_request() {

  $items = array(
    '0' => array( 'data' => l('Настройки страницы куратора', "admin/settings/request/curator") .
              '<div class="description">Данная страница позволяет настроить текст на странице кураторов.</div>'),
    '1' => array( 'data' => l('Отзывы на одобрение', "admin/settings/request/reviews-aprove") .
              '<div class="description">Данная страница позволяет управлять отзывами.</div>'),
    '2' => array( 'data' => l('Настройка текста на странице регистрации', "admin/settings/request/register-settings") .
              '<div class="description">Данная страница позволяет настраивать текст на странице регистрации.</div>'),
    '3' => array( 'data' => l('Настройка ссылок на странице приветствия', "admin/settings/request/video-links") .
          '<div class="description">Данная страница позволяет настраивать ссылки видео на странице приветствия.</div>'),
    '4' => array( 'data' => l('Ссылки видео инструкции', "admin/settings/request/video-instructions")),
    '5' => array( 'data' => l('Системное сообщение', "admin/settings/request/sys-msg")),
    '6' => array( 'data' => l('Настрйоки главной страницы', "admin/settings/request/front")),
  );

  return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ul', 'attributes' => array('class' => array('admin-list'))));
}


function front_page_settings_form($form, &$form_state) {
  $form = array();

  $form['field_set1'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  for ($i = 1; $i <= 7; $i++) {

    $form['field_set1']['menu' . $i] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('menu' . $i, ''),
    );

  }

  $form['field_set2'] = array(
    '#type' => 'fieldset',
    '#title' => 'Шаги',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  for ($i = 0; $i < 4; $i++) {

    $form['field_set2']['step' . $i] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('step' . $i, ''),
    );

  }


  $form['field_set3'] = array(
    '#type' => 'fieldset',
    '#title' => 'Отзывы',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  for ($i = 1; $i <= 4; $i++) {
    $form['field_set3']['review' . $i] = array(
      '#type' => 'fieldset',
      '#title' => 'Отзыв ' . $i,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
    );

    $form['field_set3']['review' . $i]['review_name_' . $i] = array(
      '#type' => 'textfield',
      '#default_value' => variable_get('review_name_' . $i, ''),
    );

    $form['field_set3']['review' . $i]['review_body_' . $i] = array(
      '#type' => 'textarea',
      '#default_value' => variable_get('review_body_' . $i, ''),
    );

  }


  return system_settings_form($form);
}


function _add_super_teacher() {
  $form = drupal_get_form('add_super_teacher_form');
  return drupal_render($form);
}

/**
 * Add super teacher to site
 */
function add_super_teacher_form($form, &$form_state){
  $form = array();

  $ajax_settings['callback'] = 'super_teacher_add_callback';
  $ajax_settings['wrapper'] = 'add-super-teacher-form-wrapper';
  $ajax_settings['method'] = 'html';
  $ajax_settings['progress']['type'] = 'throbber';
  $ajax_settings['progress']['message'] = t('Пожалуйста подождите...');
  $ajax_settings['effect'] = 'fade';

  $form['names'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="names-wrapper">',
    '#suffix' => '</div>',
  );

  // See example_add_more_form_add().
  if (empty($form_state['name_count'])) {
    $form_state['name_count'] = 1;
  }

  for ($i = 0; $i < $form_state['name_count']; $i++) {
    $class = $i%2 == 1 ? 'odd' : 'even';
    $title = $i==0 ? t('E-mail') : '';
    $description = $i==0 ? '<em>'.t('В случае, если преподавателя с указной почтой нет на платформе, наша система его зарегистрирует и вышлет ему уведомление на указанный e-mail.').'</em>' : '';
    $form['names'][$i]['name'] = array(
      // '#prefix' => "<div class='fields fields-{$i} $class'>",
      '#title' => $title,
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      // '#description' => $description
    );
  }

  $form['names_description'] = array(
    '#prefix' => '<div class="names-description">' . $description,
    '#suffix' => '</div>',
  );

  $form['add_more'] = array(
    '#type' => 'submit',
    '#value' => t('+ Добавить поле'),
    '#submit' => array('add_more_super_teachers_form_add'),
    '#ajax' => array(
      'wrapper' => 'names-wrapper',
      'callback' => 'add_more_super_teachers_form_update',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Загрузить данные'),
    '#ajax' => $ajax_settings,
  );

  $form['field_result'] = array(
    '#prefix' => '<div id="add-super-teacher-form-wrapper">'.super_teachers_admin_table(),
    '#suffix' => '</div>',
  );

  return $form;
}


function super_teacher_add_callback($form, &$form_state) {
  foreach ($form_state['values']['names'] as $key => $value) {
    if(!empty($value['name'])){
      if(valid_email_address($value['name'])){
        $user = user_load_by_mail($value['name']);
        if(!empty($user)){
          $user->roles[10] = true;
          user_save($user);
        }else{
          // reg user
          $fields = array(
            'name' => $value['name'],
            'mail' => $value['name'],
            'pass' => DEFAULT_PASS_USER,
            'status' => 1,
            'init' => $value['name'],
            'roles' => array(10 => TRUE),
            'access' => time(),
          );
          $account = user_save('', $fields);

          //load user. We need to get his ID
          $user = user_load_by_mail($value['name']);

          add_super_teacher_mail_send($user->uid);

        }

        $mark = '<div class="messages status"><h2 class="element-invisible">Статус</h2>'.t('Супер преподаватели успешно добавлены на сайт').'</div>'.super_teachers_admin_table();
      }else{
        $mark = '<div class="er-mess">'.t('Неверный формат в поле E-mail: '). $value['name'] .'</div>'.super_teachers_admin_table();
      }
    }
  }

  return $mark;
}


/**
 * "Add more" button submit callback.
 */
function add_more_super_teachers_form_add($form, &$form_state) {
  $form_state['name_count']++;
  $form_state['rebuild'] = TRUE;
}


/**
 * "Add more" button ajax callback.
 */
function add_more_super_teachers_form_update($form, $form_state) {
  return $form['names'];
}


function super_teachers_admin_table() {
  $header = array(
    'Имя',
    'E-mail',
    'Дата активности',
    'Действия'
  );
  $rows = array();

  $query = db_select('users_roles', 'ur');
  $query->fields('ur', array('uid'));
  $query->condition('ur.rid', 10);
  $results = $query->execute()->fetchAll();

  foreach ($results as $value) {
    $user = user_load($value->uid);
    $name = ($user->field_fio)?$user->field_fio['und'][0]['value']:$user->name;

    $rows[] = array(
      'name' => l($name, "super-teacher/courses/$user->uid"),
      'mail' => $user->mail,
      'date' => ($user->access)?date('d M Y', $user->access):'',
      'actions' => l('Редактировать профиль преподавателя', "user/$user->uid/edit"),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('cellpadding' => 0, 'cellspacing' => 0)));
}


function _add_courses_to_super_teacher($user) {
  $name = ($user->field_fio)?$user->field_fio['und'][0]['value']:$user->name;
  drupal_set_title('Добавление курсов для супер преподавателя ' . $name);
  $form = drupal_get_form('super_teacher_courses_form', $user->uid);

  return drupal_render($form);
}


function super_teacher_courses_form($form, &$form_state, $uid) {
  $query = db_select('node', 'n');
  $query->fields('n', array('nid', 'title'));
  $query->condition('n.type', 'course');
  $query->orderBy('n.title');
  $results = $query->execute()->fetchAll();

  $options = array();
  foreach ($results as $value) {
    $options[$value->nid] = $value->title . ' (ID ' . $value->nid . ')';
  }

  $query = db_select('ul_steachers_courses', 'stc');
  $query->fields('stc', array('cid'));
  $query->condition('tid', $uid);
  $results = $query->execute()->fetchAll();

  $default_value = array();
  foreach ($results as $value) {
    $default_value[] = $value->cid;
  }

  $ajax_settings['callback'] = 'super_teacher_courses_add_callback';
  $ajax_settings['wrapper'] = 'add-super-teacher-courses-form-wrapper';
  $ajax_settings['method'] = 'html';
  $ajax_settings['progress']['type'] = 'throbber';
  $ajax_settings['progress']['message'] = t('Пожалуйста подождите...');
  $ajax_settings['effect'] = 'fade';

  $form['courses'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $default_value,
    '#title' => 'Прикрепленные курсы:',
    '#description' => '<div id="add-super-teacher-courses-form-wrapper"></div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Сохранить данные'),
    '#ajax' => $ajax_settings,
  );

  return $form;
}



function super_teacher_courses_add_callback($form, &$form_state) {
  $uid = $form_state['build_info']['args'][0];

  db_delete('ul_steachers_courses')
    ->condition('tid', $uid)
    ->execute();

  foreach ($form_state['values']['courses'] as $key => $value) {
    if ($value > 0) {
      $id = db_insert('ul_steachers_courses')
        ->fields(array('tid' => $uid, 'cid' => $value, 'created' => time()))
        ->execute();

      $mark = '<div class="messages status"><h2 class="element-invisible">Статус</h2>'.t('Данные о курсах для супер преподавателя успешно сохранены').'</div>';
    }
  }

  return $mark;
}





function search_autocomplete_hometask($arg = NULL, $string = '') {

  $matches = array();
  if($arg == 'user'){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'user')
      ->propertyCondition('mail', '%' . db_like($string) . '%', 'LIKE')
      ->range(0, 10);
    $result = $query->execute();

    $news_items_nids = array_keys($result['user']);
    $nodes = entity_load('user', $news_items_nids);
    foreach ($nodes as $row) {
      $matches[check_plain($row->mail)] = check_plain($row->mail);
    }
  }else{
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'lesson')
      ->propertyCondition('title', '%' . db_like($string) . '%', 'LIKE');
    $result = $query->execute();
    $news_items_nids = array_keys($result['node']);

    $nodes = entity_load('node', $news_items_nids);
    foreach ($nodes as $key => $value) {
      $node_wrapper = entity_metadata_wrapper('node', $value);
      $matches[check_plain($value->title)] = check_plain($node_wrapper->field_lesson_reference->value()->title) . ' | ' . check_plain($value->title);
    }
  }

  drupal_json_output($matches);
}


function search_user_hometask_form($form, &$form_state) {
  $form = array();

  $form['search_email'] = array(
    '#type' => 'textfield',
    '#title' => 'E-mail пользователя',
    '#autocomplete_path' => 'search/autocomplete/user',
  );

  $form['search_lesson'] = array(
    '#type' => 'textfield',
    '#title' => 'Название урока',
    '#autocomplete_path' => 'search/autocomplete/lesson',
    '#description' => 'Начните вводить название курса, вы получите перечень уроков этого курса, пример: Курс | Урок'
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('search'),
    '#submit' => array('search_user_hometask_submit'),
    '#ajax' => array(
      'callback' => 'search_user_hometask_update',
      'wrapper' => 'search_results',
      'method' => 'html',
      'effect' => 'fade',
    ),
  );

  $form['results'] = array(
    '#tree' => true,
    '#prefix' => '<div id="search_results">',
    '#suffix' => '</div>'
  );

  return $form;
}

function search_user_hometask_submit($form, &$form_state) {
  // echo "<b>Debug data:</b><pre>"; print_r($form_state['values']); echo "</pre>";
}


function search_user_hometask_update($form, &$form_state) {
  return $form['results'];
}


function system_msg_create_form($form, &$form_state) {
  $form = array();

  $form['message'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('sys_msg', ''),
    '#title' => t('Текст сообщение'),
  );

  $roles = user_roles($membersonly = FALSE, $permission = NULL);
  unset($roles[7]);

  $form['roles'] = array(
    '#type' => 'select',
    '#options' => $roles,
    '#default_value' => variable_get('sys_msg_role', 1)
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Сохранить настройки')
  );

  return $form;
}

function system_msg_create_form_submit($form, &$form_state) {
  variable_set('sys_msg', $form_state['values']['message']);
  variable_set('sys_msg_role', $form_state['values']['roles']);
  drupal_set_message(t('Настройки сохранены'), 'status', FALSE);
}


/**
 * autocomplete helper
 * @$string = string for search
 */
function _courses_autocomplete($string = '') {

  // для примера, возвратим заголовки нод
  $result = db_select('node', 'n')
    ->fields('n', array('title', 'nid'))
    ->condition('title', '%' . db_like($string) . '%', 'LIKE')
    ->range(0, 10)
    ->execute();

  $matches = array();
  foreach ($result as $row) {
    $matches[$row->nid] = $row->nid . ' | ' . check_plain($row->title);
  }

  drupal_json_output($matches);
}


function up_lvl_block_users_content() {
  $form = drupal_get_form('up_lvl_block_users_form');
  $content = drupal_render($form);

  if(!empty($_GET['cid'])){
    $table = drupal_get_form('course_admin_table_form', $_GET['cid']);
    $content .= drupal_render($table);
  }

  return $content;
}


function up_lvl_block_users_form($form, &$form_state) {
  $form = array();

  $form['course'] = array(
    '#title' => t('Название курса'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#autocomplete_path' => 'courses/autocomplete',
    '#default_value' => (!empty($_GET['cid']))? $_GET['cid'] : ''
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

function up_lvl_block_users_form_submit($form, &$form_state) {
  $query = array('cid' => $form_state['values']['course']);
  drupal_goto(drupal_get_destination(), array('query' => $query));
}


function course_admin_table_form($form, &$form_state) {
  $form = array();

  $options = array();
  if(is_numeric($form_state['build_info']['args'][0])){

    $query = db_select('field_data_field_course_curator', 'n');
    $query->fields('n', array('field_course_curator_target_id'));
    $query->condition('n.entity_id', $form_state['build_info']['args'][0]);
    $courators = $query->execute()->fetchAll();

    $course = node_load($form_state['build_info']['args'][0]);
    $c_admin = user_load($course->uid);

    $header = array(
      'username' => array('data' => t('Username')),
      'status' => array('data' => t('Status')),
      'roles' => array('data' => t('Roles')),
    );

    $status = array(0 => t('active'), 1 => t('blocked'));

    $select = db_select('ul_blocked_user', 'ubu');
    $select->addExpression('COUNT(*)');
    $select->condition('ubu.uid', $c_admin->uid);
    $st = $select->execute()->fetchField();
    $options[$c_admin->uid] = array(
      'username' => theme('username', array('account' => $c_admin)),
      'status' => $status[$st],
      'roles' => t('Создатель/администратор курса'),
    );

    foreach ($courators as $account) {
      $account = user_load($account->field_course_curator_target_id);

      if($account){
        $select = db_select('ul_blocked_user', 'ubu');
        $select->addExpression('COUNT(*)');
        $select->condition('ubu.uid', $account->uid);
        $st = $select->execute()->fetchField();

        $options[$account->uid] = array(
          'username' => theme('username', array('account' => $account)),
          'status' => $status[$st],
          'roles' => t('Куратор курса'),
        );
      }
    }

    $form['table_op'] = array(
      '#type' => 'fieldset',
      '#title' => t('ПАРАМЕТРЫ ОБНОВЛЕНИЯ'),
      '#collapsible' => FALSE,
      '#collapsed' => TRUE,
    );

    $operations = array(
      1 => t('Заблокировать аккаунт'),
      2 => t('Активировать аккаунт')
    );
    $form['table_op']['operation'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#title_display' => 'invisible',
      '#options' => $operations,
      '#default_value' => 'unblock',
    );

    $form['table_op']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => array('courators_account_submit'),
    );
  }

  $form['accounts'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No people available.'),
  );

  return $form;
}

function courators_account_submit($form, &$form_state) {

  if($form_state['values']['operation'] == 1){
    foreach ($form_state['values']['accounts'] as $key => $value) {
      if($value > 0){
        $select = db_select('ul_blocked_user', 'ubu');
        $select->addExpression('COUNT(*)');
        $select->condition('ubu.uid', $value);
        $st = $select->execute()->fetchField();
        if($st == 0){
          db_insert('ul_blocked_user')
            ->fields(array('uid' => $value))
            ->execute();
        }
      }
    }
    drupal_set_message(t('Все указанные пользователи были заблокированы'), 'status', FALSE);
  }else{
    foreach ($form_state['values']['accounts'] as $key => $value) {
      if($value > 0){
        db_delete('ul_blocked_user')
          ->condition('uid', $value)
          ->execute();
      }
    }
    drupal_set_message(t('Все указанные пользователи были активированы'), 'status', FALSE);
  }

}


function by_step_admin_page_request_video_instructions_form($form, &$form_state) {
  $form = array();

  // drupal_add_js(drupal_get_path('module', 'colorbox') . '/js/colorbox_inline.js');
  // drupal_add_js(drupal_get_path('module', 'colorbox') . '/js/colorbox_load.js');
  // drupal_add_js(drupal_get_path('module', 'colorbox') . '/js/colorbox.js');
  // drupal_add_css(libraries_get_path('colorbox') . '/example5/colorbox.css');

  $s_video_l = variable_get('student_video_link_instructions', '');
  $c_video_l = variable_get('courator_video_link_instructions', '');

  $form['student_link_instructions_content'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки ссылок видео для учеников',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['student_link_instructions_content']['student_video_preview'] = array(
    '#type' => 'item',
    '#markup' => '<a class="colorbox-load" href="'.$s_video_l.'">'. t('Видео инструкция учеников') .'</a>',
  );

  $form['student_link_instructions_content']['student_video_link_instructions'] = array(
    '#type' => 'textfield',
    '#title' => t('Ссылка для ученика'),
    '#default_value' => variable_get('student_video_link_instructions', 'http://www.youtube.com/v/NM_jDz-Mcg8?width=560&height=315&iframe=true'),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => 'пример ссылки: http://www.youtube.com/v/NM_jDz-Mcg8?width=560&height=315&iframe=true'
  );

  $form['courator_link_instructions_content'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки ссылок видео для преподавателей',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['courator_link_instructions_content']['courator_video_preview'] = array(
    '#type' => 'item',
    '#markup' => '<a class="colorbox-load" href="'.$c_video_l.'">'. t('Видео инструкция преподавателя') .'</a>',
  );

  $form['courator_link_instructions_content']['courator_video_link_instructions'] = array(
    '#type' => 'textfield',
    '#title' => t('Ссылка для преподавателя'),
    '#default_value' => variable_get('courator_video_link_instructions', 'http://www.youtube.com/v/NM_jDz-Mcg8?width=560&height=315&iframe=true'),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => 'пример ссылки: http://www.youtube.com/v/NM_jDz-Mcg8?width=560&height=315&iframe=true'
  );

  return system_settings_form($form);
}


function by_step_admin_page_request_video_links() {
  $form = drupal_get_form('video_link_sittengs_form');
  return $form;
}


function video_link_sittengs_form($form, &$form_state) {

  $video_id = parse_youtube_link(variable_get('student_video_link', array('value' => '', 'format' => NULL)));

  $form['student_link_content'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки ссылок видео для учеников',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['student_link_content']['student_video_preview'] = array(
    '#type' => 'item',
    '#markup' => '<iframe width="425" height="350" src="//www.youtube.com/embed/'.$video_id.'" frameborder="0" allowfullscreen></iframe>',
  );

  $form['student_link_content']['student_video_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Ссылка для ученика'),
    '#default_value' => variable_get('student_video_link', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => 'пример ссылки: http://youtu.be/p6gnJnd14Pc'
  );

  $video_id1 = parse_youtube_link(variable_get('courator_video_link', array('value' => '', 'format' => NULL)));

  $form['courator_link_content'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки ссылки видео для преподавателя',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['courator_link_content']['courator_video_preview'] = array(
    '#type' => 'item',
    '#markup' => '<iframe width="425" height="350" src="//www.youtube.com/embed/'.$video_id1.'" frameborder="0" allowfullscreen></iframe>',
  );

  $form['courator_link_content']['courator_video_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Ссылка для преподавателя'),
    '#default_value' => variable_get('courator_video_link', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => 'пример ссылки: http://youtu.be/p6gnJnd14Pc'
  );

  return system_settings_form($form);
}


function by_step_admin_page_request_task_text() {
  $form = drupal_get_form('by_step_task_text_form');
  return $form;
}


function by_step_task_text_form($form, &$form_state) {
  $form = array();

  $confirm_task_text = variable_get('confirm_task_text', array('value' => '', 'format' => NULL));
  $form['confirm_task_text'] = array(
    '#title' => t('Настройки текста подтверждения для прохождения задания на время'),
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $confirm_task_text['value'],
  );

  $complete_task_text = variable_get('complete_task_text', array('value' => '', 'format' => NULL));
  $form['complete_task_text'] = array(
    '#title' => t('Настройки текста для завершенного задания'),
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $complete_task_text['value'],
  );

  $blocked_task_text = variable_get('blocked_task_text', array('value' => '', 'format' => NULL));
  $form['blocked_task_text'] = array(
    '#title' => t('Настройки текста замороженого задания'),
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $blocked_task_text['value'],
  );

  return system_settings_form($form);
}


function by_step_admin_page_request_register_page_settings() {
  $form = drupal_get_form('by_step_register_page_setting_form');
  return $form;
}


function by_step_register_page_setting_form($form, &$form_state){

  $form = array();

  $main_register_body_value = 'Уважаемые посетители!

Мы рады тому, что у Вас есть интерес к современным образовательным технологиям.
Вы цените собственное время и хотите максимально эффективно участвовать в процессе обучения.
На данный момент Вам неободимо определиться с ролью на платформе.

Если у Вас есть желание пройти обучение по одному из направлений, представленных на Up-lvl.ru, регистрируйтесь в качестве ученика.

Если Вы готовы создавать собственные программы дистанционного обучения, регистрируйте аккаунт преподавателя!
Наш менеджер в ближайшее время свяжествся с Вами и окажет всестороннюю поддержку!
йте аккаунт преподавателя! Наш менеджер в ближайшее время свяжествся с Вами и окажет всестороннюю поддержку!';

  $main_register_body = variable_get('main_register_body', array('value' => $main_register_body_value, 'format' => NULL));
  $form['main_register_body'] = array(
    '#type' => 'text_format',
    '#title' => 'Настройки текста на основной странице регистрации',
    '#format' => 'full_html',
    '#default_value' => $main_register_body['value'],
  );


  $teacher_register_body_value = 'Уважаемые преподаватели!
Мы действительно понимаем все то, что Вам необходимо для полноценной работы с обучающейся аудиторией. Более того, мы самостоятельно разрабатываем свои курсы, которые, естественно, находятся также на этой платформе.

Мы готовы предоставлять нашу платформу компаниям, тренинговым центрам, лицам, которые ведут образовательную деятельность (инфобизнесменам), чтобы и Вы и Ваши ученики почувствовали все преимущества обучения своей аудитории через автоматизированный метод.

Мы предлагаем Вам:
1. Месяц бесплатного использования платформы Up-lvl.ru
2. Определение стоимости после тестового периода - индивидуально с каждым клиентом
3. Учёт всех Ваших замечаний и пожеланий, постоянную оптимизацию системы';


  $teacher_register_body = variable_get('teacher_register_body', array('value' => $teacher_register_body_value, 'format' => NULL));
  $form['teacher_register_body'] = array(
    '#title' => 'Настройки текста для преподавателей',
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $teacher_register_body['value'],
  );

  $form['pupil_register_text'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки текста для учеников',
  );

  $pupil_register_body_1_value = '
ВАЖНО!!!
Спешим Вас уведомить о том, что регистрация учеников на какой-либо курс производить только преподаватель в его личном кабинете. Если Вы желаете пройти обучение по одному из направлений, Вам необходимо связаться с преподавателем по выбранному курсу с просьбой о регистрации!';

  $pupil_register_body_1 = variable_get('pupil_register_body_1', array('value' => $pupil_register_body_1_value, 'format' => NULL));
  $form['pupil_register_text']['pupil_register_body_1'] = array(
    '#title' => 'Настройки текста для учеников. Над формой регистрации',
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $pupil_register_body_1['value'],
  );

  $pupil_register_body_2_value = 'После внесения Вас в список обучающих, Вам на e-mail будет выслано письмо с указанием логина и пароля для входа в систему up-lvl.ru. Письмо может так же находиться в папке «Спам», обязательно проверьте ее!';

  $pupil_register_body_2 = variable_get('pupil_register_body_2', array('value' => '', 'format' => NULL));
  $form['pupil_register_text']['pupil_register_body_2'] = array(
    '#title' => 'Настройки текста для учеников. Под формой регистрации',
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $pupil_register_body_2['value'],
  );

  return system_settings_form($form);
}



function by_step_admin_page_request_review_page_aprove(){

  $form_publish = drupal_get_form('review_publish_form');

  if(isset($_GET['nodeid']) && isset($_GET['status']) && @$_GET['status'] == 'publish') {
    $html_render = '';
    $html_render .= '<div>Вы действительно хотите опубликовать данный материал?</div>';
    $html_render .= drupal_render($form_publish);
    return $html_render;
  }


  $query = db_select('node', 'n');
  $query->fields('n', array('nid'));
  $query->condition('n.type', 'testimonials ');
  $query->condition('n.status', 0);
  $query->orderBy('n.created', 'DESC');
  $list = $query->execute()->fetchAll();

  $header = array(
   array('data' => 'Имя пользователя'),
   array('data' => 'Содержимое'),
   array('data' => 'Дата создания'),
   array('data' => 'редактирование'),
   array('data' => 'Опубликовать'),
  );

  if (!$list) return '<div class="epmty-text">' . 'Нет статей на одобрение.' . '</div>';
  $rows = array();

  foreach ($list as $value){
    $node = node_load($value->nid);
    $u = user_load($node->uid);
    $row = array(
     0 => $u->name,
     1 => @$node->body['ru'][0]['value'] ? $node->body['ru'][0]['value'] : 'Содержимое отзыва отсутствует',
     2 => date('Y-m-d H:i', $node->created),
     3 => l($node->title, 'node/'.$node->nid.'/edit'),
     4 => l('Опубликовать', 'admin/reviews-aprove' , array('query' => array('nodeid' => $node->nid, 'status' => 'publish',  drupal_get_destination()))),
    );
    $rows[] = $row;
  }


  $pager = theme('pager', array('quantity' => 4));
  return theme('table', array( 'header' => $header, 'rows' => $rows, 'attributes' => array('width' => '100%'))) . $pager;
}


function review_publish_form(){

  $form = array();

  $form['submit'] = array('#type' => 'submit', '#value' => t('Опубликовать'), '#attributes' => array('class' => array('second-button-submit')));

  return $form;
}


function review_publish_form_submit($form, $form_state) {
  $node = node_load($_GET['nodeid']);
  $node->status = 1;
  node_save($node);
}

function by_step_admin_page_request_curator_list_settings() {
  $form = drupal_get_form('by_step_curator_form');

  return $form;
}

function by_step_curator_form($form, &$form_state) {
  $path = drupal_get_path('theme', 'base') . '/js/bystep_script.js';
  drupal_add_js($path, 'file');

  $form['curator_list'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки страницы Список кураторов',
  );
  $curator_list_body = variable_get('curator_list_body', array('value' => '', 'format' => NULL));
  $form['curator_list']['curator_list_body'] = array(
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $curator_list_body['value'],
    '#title' => 'Видео инструкция: <strong>Кураторы и закрепление учеников</strong>',
  );

  $form['curator_list']['curator_list_video'] = array(
    '#prefix' => '<p class="curator-list-video-preview"></p>',
    '#type' => 'textfield',
    '#title' => 'Видео инструкция',
    '#default_value' => variable_get('curator_list_video', ''),
    '#suffix' => '<a class="upload button">Загрузить</a>',
  );

  $form['curator_add'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки страницы Добавить куратора',
  );
  $curator_add_body = variable_get('curator_add_body', array('value' => '', 'format' => NULL));
  $form['curator_add']['curator_add_body'] = array(
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $curator_add_body['value'],
    '#title' => 'Видео инструкция: <strong>Как добавить или активировать кураторов</strong>',
  );

  $form['curator_add']['curator_add_video'] = array(
    '#prefix' => '<p class="curator-add-video-preview"></p>',
    '#type' => 'textfield',
    '#title' => 'Видео инструкция',
    '#default_value' => variable_get('curator_add_video', ''),
    '#suffix' => '<a class="upload button">Загрузить</a>',
  );

  $form['students_add'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки страницы Добавить учеников',
  );
  $students_add_body = variable_get('students_add_body', array('value' => '', 'format' => NULL));
  $form['students_add']['students_add_body'] = array(
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#default_value' => $students_add_body['value'],
    '#title' => 'Видео инструкция: <strong>Как добавить или активировать учеников</strong>',
  );

  $form['students_add']['students_add_video'] = array(
    '#prefix' => '<p class="students-add-video-preview"></p>',
    '#type' => 'textfield',
    '#title' => 'Видео инструкция',
    '#default_value' => variable_get('students_add_video', ''),
    '#suffix' => '<a class="upload button">Загрузить</a>',
  );

  return system_settings_form($form);
}


function table_pupil_test_form($form, &$form_state) {
  $form = array();

  $header = array(
    'number_of_user' => t('№'),
    'user_name' => t('Имя'),
    'user_mail' => t('E-mail'),
    'data_access' => t('Дата активности'),
  );

  $query_date = db_select('non_approved_students', 'n');
  // $query_date = db_select('non_approved_students', 'n')->extend('PagerDefault');
  $query_date->innerJoin('users', 'u', 'u.uid = n.uid');
  $query_date->fields('n', array('uid', 'current_lesson_id'));
  $query_date->condition('n.cid', 2315);
  // $query_date->limit(30);
  // $query_date->extend('TableSort')->orderByHeader($header);
  $user_list = $query_date->execute()->fetchAll();

  foreach ($user_list as $list) {
    $user = user_load($list->uid);

    $user_name = !empty($user->field_fio) ? $user->field_fio['und'][0]['value'] : '';

    $users[] = array(
      'uid' => $user->uid,
      'user_name' => $user_name,
      'user_mail' => $user->mail,
      'data_access' => date('d M Y', $user->access),
    );
  }

  $table_num = 1;
  $table_users = array();
  foreach ($users as $u_key => $u_value) {

    $table_users[] = array(
      'number_of_user' => $table_num,
      'user_name' => $u_value['user_name'],
      'user_mail' => $u_value['user_mail'],
      'data_access' => $u_value['data_access'],
    );

    $table_num++;
  }


  $query_date = db_select('non_approved_students', 'n');
  $query_date->fields('n', array('nasid', 'uid', 'current_lesson_id', 'lesson_id', 'status'));
  $query_date->condition('n.cid', 2315);
  $query_date->condition('n.uid', 4784);
  $user_list = $query_date->execute()->fetchAll();

  drupal_set_message('<pre><b>Debuging data:</b> ' . check_plain(print_r($user_list, 1)) . '</pre>');

  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $table_users,
    '#empty' => t('No users found'),
  );


  return $form;
}
