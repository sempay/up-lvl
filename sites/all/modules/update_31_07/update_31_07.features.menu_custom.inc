<?php
/**
 * @file
 * update_31_07.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function update_31_07_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-admin-left-menu.
  $menus['menu-admin-left-menu'] = array(
    'menu_name' => 'menu-admin-left-menu',
    'title' => 'admin_left_menu',
    'description' => '',
  );
  // Exported menu: menu-curator-left-menu.
  $menus['menu-curator-left-menu'] = array(
    'menu_name' => 'menu-curator-left-menu',
    'title' => 'curator_left_menu',
    'description' => '',
  );
  // Exported menu: menu-pupil-left-menu.
  $menus['menu-pupil-left-menu'] = array(
    'menu_name' => 'menu-pupil-left-menu',
    'title' => 'pupil_left_menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('admin_left_menu');
  t('curator_left_menu');
  t('pupil_left_menu');


  return $menus;
}
