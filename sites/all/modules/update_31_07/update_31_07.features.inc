<?php
/**
 * @file
 * update_31_07.features.inc
 */

/**
 * Implements hook_node_info().
 */
function update_31_07_node_info() {
  $items = array(
    'materials' => array(
      'name' => t('Материал к уроку'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
