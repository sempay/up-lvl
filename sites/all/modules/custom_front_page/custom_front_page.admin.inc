<?php

/**
 * @file
 * Admin page callback file for the main module.
 * Setting block 1 in the custom front page.
 */

function cfp_admin_form_block1($form, &$form_state) {

  $form['cfp_fb_video'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Url from YouTube'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fb_video', ''),
  );
  $form['cfp_fb_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text after video'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_fb_text', ''),
  );
  $form['cfp_fb_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('button after video'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fb_button', ''),
  );
  $form['cfp_fb_text_second'] = array(
    '#type'          => 'textarea',
    '#title'         => t('second text in first block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_fb_text_second', ''),
  );
   return system_settings_form($form);
}

/**
 * Setting block 2 in the custom front page.
 */

function cfp_admin_form_block2($form, &$form_state) {

  $form['cfp_sb_first_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Text in the second block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_first_text', ''),
  );

  $form['cfp_sb_first_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'First miniblock:' . '</h3></p>'),
  );

  $form['cfp_sb_fr_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_sb_fr_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_sb_fr_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_fr_title', ''),
  );

  $form['cfp_sb_fr_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Text'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_sb_fr_text', ''),
  );

  $form['cfp_sb_second_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Second miniblock:' . '</h3></p>'),
  );

  $form['cfp_sb_sr_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#default_value'     => variable_get('cfp_sb_sr_image', ''),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );

  $form['cfp_sb_sr_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_sr_title', ''),
  );

  $form['cfp_sb_sr_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Text'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_sb_sr_text', ''),
  );

  $form['cfp_sb_third_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Third miniblock:' . '</h3></p>'),
  );

  $form['cfp_sb_tr_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_sb_tr_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_sb_tr_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_tr_title', ''),
  );

  $form['cfp_sb_tr_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Text'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_sb_tr_text', ''),
  );

  $form['cfp_sb_fourth_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Fourth miniblock:' . '</h3></p>'),
  );

  $form['cfp_sb_fourth_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_sb_forth_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_sb_forth_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_forth_r_title', ''),
  );

  $form['cfp_sb_forth_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Text'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_sb_forth_r_text', ''),
  );

  $form['cfp_sb_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Button in the second block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_button', ''),
  );

   return system_settings_form($form);
}

/**
 * Setting block 3 in the custom front page.
 */

function cfp_admin_form_block3($form, &$form_state) {

  $form['cfp_tb_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Text'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_tb_text', ''),
  );

   return system_settings_form($form);
}

/**
 * Setting block 4 in the custom front page.
 */

function cfp_admin_form_block4($form, &$form_state) {

  $form['cfp_fourth_b_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title in the Fourth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fourth_b_title', ''),
    );

  $form['cfp_fourth_b_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text in the Fourth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_fourth_b_text', ''),
    );


   return system_settings_form($form);
}

/**
 * Setting block 5 in the custom front page.
 */

function cfp_admin_form_block5($form, &$form_state) {

  $form['cfp_fifth_b_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title in the Fisth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_title', ''),
    );

  $form['cfp_fifth_b_first_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'First miniblock:' . '</h3></p>'),
  );

  $form['cfp_fifth_b_fr_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_fifth_b_fr_image', ''),
    '#attributes'        => array('class' => array('')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_fifth_b_fr_title'] = array(
    '#type'             => 'textfield',
    '#title'            => t('the amount in the Firth block'),
    '#required'         => TRUE,
    '#size'             => 60,
    '#default_value'    => variable_get('cfp_fifth_b_fr_title', ''),
    );

  $form['cfp_fifth_b_fr_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text in the Fifth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_fr_text', ''),
    );

  $form['cfp_fifth_b_second_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Second miniblock:' . '</h3></p>'),
  );

  $form['cfp_fifth_b_sr_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_fifth_b_sr_image', ''),
    '#attributes'        => array('class' => array('')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_fifth_b_sr_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('the amount in the Second block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_sr_title', ''),
    );

  $form['cfp_fifth_b_sr_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text in the Second block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_sr_text', ''),
     );

  $form['cfp_fifth_b_third_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Third miniblock:' . '</h3></p>'),
  );

  $form['cfp_fifth_b_tr_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_fifth_b_tr_image', ''),
    '#attributes'        => array('class' => array('')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );

  $form['cfp_fifth_b_tr_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('the amount in the Third block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_tr_title', ''),
    );

  $form['cfp_fifth_b_tr_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text in the Third block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_tr_text', ''),
    );

  $form['cfp_fifth_b_fourth_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Fourth miniblock:' . '</h3></p>'),
  );

  $form['cfp_fifth_b_fourth_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_fifth_b_fourth_r_image', ''),
    '#attributes'        => array('class' => array('')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );

  $form['cfp_fifth_b_fourth_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('the amount in the Fourth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_fourth_r_title', ''),
    );

  $form['cfp_fifth_b_fourth_r_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text in the Fourth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_fourth_r_text', ''),
    );

  $form['cfp_fifth_b_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('button in the Fourth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_button', ''),
    );

   return system_settings_form($form);
}

/**
 * Setting block 6 in the custom front page.
 */

function cfp_admin_form_block6($form, &$form_state) {

  $form['cfp_sixth_b_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sixth_b_title', ''),
  );

  $form['cfp_sixth_b_first_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'First miniblock:' . '</h3></p>'),
  );

  $form['cfp_b_first_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_b_first_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_first_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title first region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_first_r_title', ''),
    );

  $form['cfp_six_b_first_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text first region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_six_b_first_r_text', ''),
    );

  $form['cfp_sixth_b_second_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Second miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_second_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_six_b_second_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_second_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title second region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_second_r_title', ''),
    );

  $form['cfp_six_b_second_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text second region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_six_b_second_r_text', ''),
    );

  $form['cfp_sixth_b_third_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Third miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_third_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_six_b_third_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_third_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title third region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_third_r_title', ''),
    );

  $form['cfp_six_b_third_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text third region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_six_b_third_r_text', ''),
    );

  $form['cfp_sixth_b_fourth_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Fourth miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_fourth_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_six_b_fourth_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_fourth_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title fourth region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_fourth_r_title', ''),
    );

  $form['cfp_six_b_fourth_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text fourth region in the Sixth block'),
    '#required'         => TRUE,
    '#default_value' => variable_get('cfp_six_b_fourth_r_text', ''),
    );

  $form['cfp_sixth_b_fifth_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Fifth miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_fifth_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_six_b_fifth_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_fifth_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title fifth region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_fifth_r_title', ''),
    );
  $form['cfp_six_b_fifth_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text fifth region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_six_b_fifth_r_text', ''),
    );

  $form['cfp_sixth_b_six_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Sixth miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_six_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_six_b_six_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_six_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title sixth region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_six_r_title', ''),
    );

  $form['cfp_six_b_six_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text sixth region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_six_b_six_r_text', ''),
    );

  $form['cfp_sixth_b_seven_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Seven miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_seven_r_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_fifth_b_seven_r_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_seven_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title seventh region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_seven_r_title', ''),
    );

  $form['cfp_six_b_seven_r_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text seventh region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_six_b_seven_r_text', ''),
    );

  $form['cfp_sixth_b_eight_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Eight miniblock:' . '</h3></p>'),
  );

  $form['cfp_six_b_er_image'] = array(
    '#type'              => 'managed_file',
    '#title'             => t('Image'),
    /*'#required'          => TRUE,*/
    '#size'              => 60,
    '#upload_location'   => 'public://front/',
    '#default_value'     => variable_get('cfp_six_b_er_image', ''),
    '#attributes'        => array('class' => array('img-responsive centred')),
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
  );

  $form['cfp_six_b_er_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title eighth region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_six_b_er_title', ''),
    );

  $form['cfp_six_b_er_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('text eight region in the Sixth block'),
    /*'#required'      => TRUE,*/
    '#default_value' => variable_get('cfp_fifth_b_er_text', ''),
    );

   return system_settings_form($form);
}

/**
 * Setting block 7 in the custom front page.
 */

function cfp_admin_form_block7($form, &$form_state) {

  $form['cfp_seventh_b_first_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'First block:' . '</h3></p>'),
  );

  $form['cfp_seven_b_fr_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_fr_title', ''),
  );

  $form['cfp_seven_b_fr_price'] = array(
    '#type'          => 'textfield',
    '#title'         => t('price'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_fr_price', ''),
  );

  $form['cfp_sb_text_markup_first'] = array(
    '#markup' => t ('<p><h4>' . 'First text information:' . '</h4></p>'),
  );

  $form['cfp_sb_fr_fc_first_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 1 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_fr_fc_first_text', ''),
  );

  $form['cfp_sb_fr_fc_second_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 2 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_fr_fc_second_text', ''),
  );

  $form['cfp_sb_fr_fc_third_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 3 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_fr_fc_third_text', ''),
  );

  $form['cfp_sb_fr_fc_fourth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 4 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_fr_fc_fourth_text', ''),
  );

  $form['cfp_sb_fr_fc_fifth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 5 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_fr_fc_fifth_text', ''),
  );

  $form['cfp_seven_b_fr_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('button'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_fr_button', ''),
  );

  $form['cfp_seventh_b_second_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Second block:' . '</h3></p>'),
  );

  $form['cfp_seventh_b_second_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seventh_b_second_r_title', ''),
  );

  $form['cfp_seventh_b_second_r_price'] = array(
    '#type'          => 'textfield',
    '#title'         => t('price'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seventh_b_second_r_price', ''),
  );

  $form['cfp_sb_text_markup_second'] = array(
    '#markup' => t ('<p><h4>' . 'Second text information:' . '</h4></p>'),
  );

  $form['cfp_sb_sr_fc_first_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 1 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_sr_fc_first_text', ''),
  );

  $form['cfp_sb_sr_fc_second_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 2 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_sr_fc_second_text', ''),
  );

  $form['cfp_sb_sr_fc_third_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 3 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_sr_fc_third_text', ''),
  );

  $form['cfp_sb_sr_fc_fourth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 4 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_sr_fc_fourth_text', ''),
  );

  $form['cfp_sb_sr_fc_fifth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 5 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_sr_fc_fifth_text', ''),
  );

  $form['cfp_fifth_b_second_r_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('button'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_fifth_b_second_r_button', ''),
  );

  $form['cfp_seventh_b_third_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Third block:' . '</h3></p>'),
  );

  $form['cfp_seven_b_third_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_third_r_title', ''),
  );

  $form['cfp_seven_b_third_r_price'] = array(
    '#type'          => 'textfield',
    '#title'         => t('price'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_third_r_price', ''),
  );

  $form['cfp_sb_text_markup_third'] = array(
    '#markup' => t ('<p><h4>' . 'Third text information:' . '</h4></p>'),
  );

  $form['cfp_sb_tr_fc_first_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 1 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_tr_fc_first_text', ''),
  );

  $form['cfp_sb_tr_fc_second_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 2 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_tr_fc_second_text', ''),
  );

  $form['cfp_sb_tr_fc_third_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 3 row'),
    '#required'         => TRUE,
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_tr_fc_third_text', ''),
  );

  $form['cfp_sb_tr_fc_fourth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 4 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_tr_fc_fourth_text', ''),
  );

  $form['cfp_sb_tr_fc_fifth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 5 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_tr_fc_fifth_text', ''),
  );

  $form['cfp_seven_b_third_r_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('button'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_third_r_button', ''),
  );

  $form['cfp_seventh_b_fourth_text_markup'] = array(
    '#markup' => t ('<p><h3>' . 'Fourth block:' . '</h3></p>'),
  );

  $form['cfp_seven_b_fourth_r_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_fourth_r_title', ''),
  );

  $form['cfp_seven_b_fourth_r_price'] = array(
    '#type'          => 'textfield',
    '#title'         => t('price'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_fourth_r_price', ''),
  );

  $form['cfp_sb_text_markup_fourth'] = array(
    '#markup' => t ('<p><h4>' . 'Fourth text information:' . '</h4></p>'),
  );

  $form['cfp_sb_four_r_fc_first_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 1 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_four_r_fc_first_text', ''),
  );

  $form['cfp_sb_four_r_fc_second_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 2 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_four_r_fc_second_text', ''),
  );

  $form['cfp_sb_four_r_fc_third_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 3 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_four_r_fc_third_text', ''),
  );

  $form['cfp_sb_four_r_fc_fourth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 4 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_four_r_fc_fourth_text', ''),
  );

  $form['cfp_sb_four_r_fc_fifth_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('text 5 row'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_sb_four_r_fc_fifth_text', ''),
  );

  $form['cfp_seven_b_fourth_r_button'] = array(
    '#type'          => 'textfield',
    '#title'         => t('button'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_seven_b_fourth_r_button', ''),
  );

   return system_settings_form($form);
}

/**
 * Setting block 8 in the custom front page.
 */

function cfp_admin_form_block8($form, &$form_state) {

  $form['cfp_eb_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('title'),
    /*'#required'      => TRUE,*/
    '#size'          => 60,
    '#default_value' => variable_get('cfp_eb_title', ''),
  );

  $form['cfp_eb_nid_form'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Form id'),
    /*'#required'      => TRUE,*/
    '#element_validate' => array('cfp_eb_nid_form_validate'),
    '#size'          => 60,
    '#default_value' => variable_get('cfp_eb_nid_form', ''),
  );

   return system_settings_form($form);
}

/**
 * Setting block 8 in the custom front page.
 */

function cfp_admin_form_block_footer($form, &$form_state) {
  $form['cfp_nb_link_vk'] = array(
    '#type' => 'textfield',
    '#title' => t('link to vkontakte'),
    '#size' => 60,
    '#default_value' => variable_get('cfp_nb_link_vk', ''),
  );

  $form['cfp_nb_link_youtube'] = array(
    '#type' => 'textfield',
    '#title' => t('link to YouTube'),
    '#size' => 60,
    '#default_value' => variable_get('cfp_nb_link_youtube', ''),
  );

  return system_settings_form($form);
}

/**
 * Validate function for cfp_eb_nid_form_validate.
 */
function cfp_eb_nid_form_validate($element, &$form_state, $form) {
 if (!is_numeric($form_state['values']['cfp_eb_nid_form'])) {
    form_error($element, t('Value in field "Form id" must bee numeric.'));
  }
}