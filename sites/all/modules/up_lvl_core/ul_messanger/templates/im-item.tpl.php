<?php
  $alter = array(
    'max_length'    => 85,
    'word_boundary' => FALSE,
    'ellipsis'      => TRUE,
    'html'          => FALSE,
  );
?>
<div class="im-item clearfix" id="private-message-dialog-<?php print $dialog_id; ?>">
  <div class="im-initer">
    <div class="picture">
      <?php
        $avatar = theme('image', array(
          'path' => $picture,
        ));
        print l($avatar, 'user/' . $initer, array(
          'attributes' => array(
            'target' => '_blank',
          ),
          'html' => TRUE,
        ));
      ?>
    </div>
    <div class="im-line">
      <div class="name">
        <?php
          if (!empty($field_fio)) {
            print l($field_fio, 'user/' . $initer, array(
              'attributes' => array(
                'target' => '_blank',
              ),
            ));
          }
          else {
           print l($name, 'user/' . $initer, array(
              'attributes' => array(
                'target' => '_blank',
              ),
            ));
          }
        ?>
      </div>
      <div class="time">
        <?php
          print ul_messanger_time_ago($last_time);
        ?>
      </div>
    </div>
  </div>
  <div class="im-last" onclick="window.location.href = '<?php print url('messages/view/' . $dialog_id); ?>';">
    <?php
      if ($current != $last_message_author):
    ?>

      <div class="last-message">
    <?php
      else:
    ?>
      <div class="last-message no-pic">
    <?php
      endif;
    ?>

      <?php
        print views_trim_text($alter, $last_message);
      ?>
    </div>
  </div>
</div>
