
<div class="user_wrapper">
  <div class="im-users-item clearfix" id="private-message-users-<?php print $uid; ?>">
    <div class="picture-user">
      <?php
        $avatar = theme('image', array(
          'path' => $image_user_list,
        ));
        print l($avatar, 'user/' . $uid, array(
          'attributes' => array(
            'target' => '_blank',
          ),
          'html' => TRUE,
        ));
      ?>
    </div>
    <div class="name-user">
      <?php
        if (!empty($field_fio_user_list)) {
          print l($field_fio_user_list, 'user/' . $uid, array(
            'attributes' => array(
              'target' => '_blank',
            ),
          ));
        }
        else {
          print l($email_user_list, 'user/' . $uid, array(
            'attributes' => array(
              'target' => '_blank',
            ),
          ));
        }
      ?>
    </div>
    <div class="role-user">
      <?php
        print $role;
      ?>
    </div>
    <div class="user-message" >
      <?php
        print l('написать сообщение', 'im/user-load/' . $uid);
      ?>
    </div>
  </div>
</div>