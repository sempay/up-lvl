<?php



function my_messages_page() {
  global $user;

  $ims  = ul_messanger_ims($user->uid, UPLVL_IM_PER_PAGE);
  $html = '';
  $instance        = field_info_instance('user', 'field_user_image', 'user');
  $default_fid     = isset($instance['settings']['default_image']) ? $instance['settings']['default_image'] : NULL;
  $default_picture = file_load($default_fid);
  if (empty($ims)) {
          $html .= '<div class="empty-text">' . 'У вас нет еще сообщений' . '</div>';
  }
  else {
    foreach ($ims as $im) {
      if (empty($im->picture) && !empty($default_picture->uri)) {
        $picture = $default_picture->uri;
      }
      elseif (!empty($im->picture)) {
        $picture = $im->picture;
      }
      else {
        $picture = '';
      }

      $html .= theme('ul_messanger_item', array(
        'picture'             => $picture,
        'initer'              => $im->recipient,
        'field_fio'           => $im->field_fio,
        'name'                => $im->name_user,
        'last_time'           => $im->last_message_time,
        'last_picture'        => $im->last_message_picture ? $im->last_message_picture : $default_picture->uri,
        'last_message'        => $im->last_message_body,
        'last_message_author' => $im->last_message_author,
        'current'             => $user->uid,
        'dialog_id'           => $im->dialog_id,
      ));
    }
  }
  $html .= theme('pager', array(
    'element' => 0,
  ));

  return '<div ng-controller="imsList" class="ims-list">' . $html . '</div>';
}

function pager_array_splice($data, $limit = 9, $element = 0) {
  global $pager_page_array, $pager_total, $pager_total_items;
  $page = isset($_GET['page']) ? $_GET['page'] : '';

  // Convert comma-separated $page to an array, used by other functions.
  $pager_page_array = explode(',', $page);

  // We calculate the total of pages as ceil(items / limit).
  $pager_total_items[$element] = count($data);
  $pager_total[$element] = ceil($pager_total_items[$element] / $limit);
  $pager_page_array[$element] = max(0, min((int)$pager_page_array[$element], ((int)$pager_total[$element]) - 1));
  return array_slice($data, $pager_page_array[$element] * $limit, $limit, TRUE);
}

function list_contacts() {
  global $user;
  $params = array(
      ':entity_type' => 'user',
      ':bundle'      => 'user',
  );
  if (isset($user->roles[UPLVL_RID_PUPIL]) || isset($user->roles[UPLVL_RID_PUPIL_NO_ACTIVE])) {
    $query = db_select('non_approved_students', 'nas');
    $query->leftJoin('node', 'n', 'n.nid = nas.cid');
    $query->leftJoin('users', 'u', 'u.uid = n.uid');
    $query->leftJoin('field_data_field_fio', 'field_fio', 'field_fio.entity_id = u.uid AND field_fio.entity_type = :entity_type AND field_fio.bundle = :bundle', $params);
    $query->leftJoin('field_data_field_user_image', 'field_user_image', 'field_user_image.entity_id = u.uid AND field_user_image.entity_type = :entity_type AND field_user_image.bundle = :bundle', $params);
    $query->leftJoin('file_managed', 'file', 'field_user_image.field_user_image_fid = file.fid');
    $query->fields('u', array('name', 'uid'));
    $query->fields('field_fio', array('field_fio_value'));
    $query->fields('file', array('uri'));
    $query->condition('nas.uid', $user->uid);
    $query->condition('nas.status', 1);
    if (isset($_GET['s'])) {
      $s = '%' . db_like($_GET['s']) . '%';
      $or = db_or();
      $or->condition('field_fio.field_fio_value', $s, 'like');
      $or->condition('u.name', $s, 'like');
      $query->condition($or);
    }
    $results = $query->distinct()->execute()->fetchAll();
    $cource_user = array();
    foreach ($results as $item) {
      $cource_user[] = array(
        'uid' => $item->uid,
        'name' => $item->name,
        'field_fio' => $item->field_fio_value,
        'file' => $item->uri,
        'role' => 'преподаватель',
      );
    }

    $query_curators = db_select('non_approved_students', 'nas');
    $query_curators->leftJoin('node', 'n', 'n.nid = nas.cid');
    $query_curators->leftJoin('field_data_field_course_curator', 'field_course_curator', 'field_course_curator.entity_id = n.nid AND field_course_curator.entity_type = :entity_type AND field_course_curator.bundle = :bundle', array(':entity_type' => 'node', ':bundle' => 'course'));
    $query_curators->innerJoin('users', 'u', 'u.uid = field_course_curator.field_course_curator_target_id');
    $query_curators->leftJoin('field_data_field_fio', 'field_fio', 'field_fio.entity_id = field_course_curator.field_course_curator_target_id');
    $query_curators->leftJoin('field_data_field_user_image', 'field_user_image', 'field_user_image.entity_id = field_course_curator.field_course_curator_target_id AND field_user_image.entity_type = :entity_type AND field_user_image.bundle = :bundle', $params);
    $query_curators->leftJoin('file_managed', 'file', 'field_user_image.field_user_image_fid = file.fid');
    $query_curators->fields('u', array('name', 'uid'));
    $query_curators->fields('field_fio', array('field_fio_value'));
    $query_curators->fields('file', array('uri'));
    $query_curators->condition('nas.uid', $user->uid);
    $query_curators->condition('nas.status', 1);
    if (isset($_GET['s'])) {
      $s = '%' . db_like($_GET['s']) . '%';
      $or = db_or();
      $or->condition('field_fio.field_fio_value', $s, 'like');
      $or->condition('u.name', $s, 'like');
      $query_curators->condition($or);
    }
    $results_curators = $query_curators->extend('PagerDefault')->limit(UPLVL_IM_PER_PAGE)->distinct()->execute()->fetchAll();
    foreach ($results_curators as $item) {
      $cource_user[] = array(
        'uid' => $item->uid,
        'name' => $item->name,
        'field_fio' => $item->field_fio_value,
        'file' => $item->uri,
        'role' => 'куратор'
      );
    }
  }
  elseif (isset($user->roles[UPLVL_RID_TEACHER]) || isset($user->roles[UPLVL_RID_ADMIN])) {
    $params = array(
      ':entity_type' => 'user',
      ':bundle'      => 'user',
    );
    $query = db_select('node', 'n');
    $query->innerJoin('field_data_field_course_curator', 'field_course_curator', 'field_course_curator.entity_id = n.nid AND field_course_curator.entity_type = :entity_type AND field_course_curator.bundle = :bundle', array(':entity_type' => 'node', ':bundle' => 'course'));
    $query->leftJoin('users', 'u', 'u.uid = field_course_curator.field_course_curator_target_id');
    $query->leftJoin('field_data_field_fio', 'field_fio', 'field_fio.entity_id = field_course_curator.field_course_curator_target_id AND field_fio.entity_type = :entity_type AND field_fio.bundle = :bundle', $params);
    $query->leftJoin('field_data_field_user_image', 'field_user_image', 'field_user_image.entity_id = field_course_curator.field_course_curator_target_id AND field_user_image.entity_type = :entity_type AND field_user_image.bundle = :bundle', $params);
    $query->leftJoin('file_managed', 'file', 'field_user_image.field_user_image_fid = file.fid');
    $query->fields('u', array('name', 'uid'));
    $query->fields('field_fio', array('field_fio_value'));
    $query->fields('file', array('uri'));
    $query->condition('n.type', 'course');
    $query->condition('n.uid', $user->uid);
    if (isset($_GET['s'])) {
      $s = '%' . db_like($_GET['s']) . '%';
      $or = db_or();
      $or->condition('field_fio.field_fio_value', $s, 'like');
      $or->condition('u.name', $s, 'like');
      $query->condition($or);
    }
    $results = $query->distinct()->execute()->fetchAll();
    $cource_user = array();
    foreach ($results as $item) {
      $cource_user[] = array(
        'uid' => $item->uid,
        'name' => $item->name,
        'field_fio' => $item->field_fio_value,
        'file' => $item->uri,
        'role' => 'куратор',
      );
    }

    $query_pupils = db_select('node', 'n');
    $query_pupils->leftJoin('non_approved_students', 'nas', 'nas.cid = n.nid ');
    $query_pupils->innerJoin('users', 'u', 'u.uid = nas.uid');
    $query_pupils->leftJoin('field_data_field_fio', 'field_fio', 'field_fio.entity_id = nas.uid AND field_fio.entity_type = :entity_type AND field_fio.bundle = :bundle', $params);
    $query_pupils->leftJoin('field_data_field_user_image', 'field_user_image', 'field_user_image.entity_id = nas.uid AND field_user_image.entity_type = :entity_type AND field_user_image.bundle = :bundle', $params);
    $query_pupils->leftJoin('file_managed', 'file', 'field_user_image.field_user_image_fid = file.fid');
    $query_pupils->fields('u', array('name', 'uid'));
    $query_pupils->fields('field_fio', array('field_fio_value'));
    $query_pupils->fields('file', array('uri'));
    $query_pupils->condition('n.type', 'course');
    $query_pupils->condition('n.uid', $user->uid);
    $query_pupils->condition('nas.status', 1);
    if (isset($_GET['s'])) {
      $s = '%' . db_like($_GET['s']) . '%';
      $or = db_or();
      $or->condition('field_fio.field_fio_value', $s, 'like');
      $or->condition('u.name', $s, 'like');
      $query_pupils->condition($or);
    }
    $results_pupils = $query_pupils->extend('PagerDefault')->limit(UPLVL_IM_PER_PAGE)->distinct()->execute()->fetchAll();
    foreach ($results_pupils as $item) {
      $cource_user[] = array(
        'uid' => $item->uid,
        'name' => $item->name,
        'field_fio' => $item->field_fio_value,
        'file' => $item->uri,
        'role' => 'ученик',
      );
    }
  }
  elseif (isset($user->roles[UPLVL_RID_CURATOR]) || isset($user->roles[UPLVL_RID_ADMIN])) {
    $params = array(
      ':entity_type' => 'user',
      ':bundle'      => 'user',
    );
    $query = db_select('field_data_field_course_curator', 'field_course_curator');
    $query->innerJoin('node', 'n', 'field_course_curator.entity_id = n.nid');
    $query->leftJoin('users', 'u', 'u.uid = n.uid');
    $query->leftJoin('field_data_field_fio', 'field_fio', 'field_fio.entity_id = u.uid AND field_fio.entity_type = :entity_type AND field_fio.bundle = :bundle', $params);
    $query->leftJoin('field_data_field_user_image', 'field_user_image', 'field_user_image.entity_id = u.uid AND field_user_image.entity_type = :entity_type AND field_user_image.bundle = :bundle', $params);
    $query->leftJoin('file_managed', 'file', 'field_user_image.field_user_image_fid = file.fid');
    $query->fields('u', array('name', 'uid'));
    $query->fields('field_fio', array('field_fio_value'));
    $query->fields('file', array('uri'));
    $query->condition('field_course_curator.field_course_curator_target_id', $user->uid);
    if (isset($_GET['s'])) {
      $s = '%' . db_like($_GET['s']) . '%';
      $or = db_or();
      $or->condition('field_fio.field_fio_value', $s, 'like');
      $or->condition('u.name', $s, 'like');
      $query->condition($or);
    }
    $results = $query->distinct()->execute()->fetchAll();
    $cource_user = array();
    foreach ($results as $item) {
      $cource_user[] = array(
        'uid' => $item->uid,
        'name' => $item->name,
        'field_fio' => $item->field_fio_value,
        'file' => $item->uri,
        'role' => 'преподаватель',
      );
    }

    $query_pupils = db_select('field_data_field_course_curator', 'field_course_curator');
    $query_pupils->innerJoin('node', 'n', 'field_course_curator.entity_id = n.nid');
    $query_pupils->leftJoin('non_approved_students', 'nas', 'nas.cid = n.nid ');
    $query_pupils->innerJoin('users', 'u', 'u.uid = nas.uid');
    $query_pupils->leftJoin('field_data_field_fio', 'field_fio', 'field_fio.entity_id = nas.uid AND field_fio.entity_type = :entity_type AND field_fio.bundle = :bundle', $params);
    $query_pupils->leftJoin('field_data_field_user_image', 'field_user_image', 'field_user_image.entity_id = nas.uid AND field_user_image.entity_type = :entity_type AND field_user_image.bundle = :bundle', $params);
    $query_pupils->leftJoin('file_managed', 'file', 'field_user_image.field_user_image_fid = file.fid');
    $query_pupils->fields('u', array('name', 'uid'));
    $query_pupils->fields('field_fio', array('field_fio_value'));
    $query_pupils->fields('file', array('uri'));
    $query_pupils->condition('field_course_curator.field_course_curator_target_id', $user->uid);
    $query_pupils->condition('nas.status', 1);
    if (isset($_GET['s'])) {
      $s = '%' . db_like($_GET['s']) . '%';
      $or = db_or();
      $or->condition('field_fio.field_fio_value', $s, 'like');
      $or->condition('u.name', $s, 'like');
      $query_pupils->condition($or);
    }
    $results_pupils = $query_pupils->extend('PagerDefault')->limit(UPLVL_IM_PER_PAGE)->distinct()->execute()->fetchAll();
    foreach ($results_pupils as $item) {
      $cource_user[] = array(
        'uid' => $item->uid,
        'name' => $item->name,
        'field_fio' => $item->field_fio_value,
        'file' => $item->uri,
        'role' => 'ученик',
      );
    }
  }
  $html = '';
  $instance        = field_info_instance('user', 'field_user_image', 'user');
  $default_fid     = isset($instance['settings']['default_image']) ? $instance['settings']['default_image'] : NULL;
  $default_image   = file_load($default_fid);

  if (empty($cource_user)) {
        $html .= '<div class="empty-text">' . 'У вас нет контактов' . '</div>';
  }
  else {
    foreach ($cource_user as $res) {
      if (empty($res['file']) && !empty($default_image->uri)) {
        $image_user_list = $default_image->uri;
      }
      elseif (!empty($res['file'])) {
        $image_user_list = $res['file'];
      }
      else {
        $image_user_list = '';
      }
      $html .= theme('ul_messanger_users_item', array(
        'image_user_list'     => $image_user_list,
        'email_user_list'     => $res['name'],
        'field_fio_user_list' => $res['field_fio'],
        'uid'                 => $res['uid'],
        'role'                => $res['role'],
      ));
    }
  }

  $html .= theme('pager', array(
    'element' => 0,
    ));

  return '<div class="im_users">' . $html . '</div>';
}

function list_contacts_load($users) {
  global $user;

  $sub_query = db_select('pm_message', 'm');
  $sub_query->join('pm_index', 'i', 'i.mid = m.mid');
  $sub_query
    ->fields('m', array('mid', 'body', 'timestamp', 'author'))
    ->condition('i.recipient', $user->uid);

  $query = db_select('pm_index', 'i');
  $query->join('pm_index', 'i2', 'i2.thread_id = i.thread_id AND i2.recipient <> :uid', array(
    ':uid' => $user->uid,
  ));
  $query->addJoin('INNER', $sub_query, 'm', 'i.mid = m.mid');
  $query->fields('i', array('thread_id'));
  $query->condition('i2.recipient', $users);
  $results = $query->execute()->fetchObject();
  if (!empty($results)) {
    drupal_goto('messages/view/' . $results->thread_id);
  }
  else {
    drupal_goto('messages/new/' . $users);
  }
}
