<?php


function bonuses_all_content() {
  global $user;

  if(array_key_exists(4, $user->roles)){

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'bonus')
      ->propertyCondition('uid', $user->uid)
      ->propertyOrderBy('created', 'DESC')
      ->range(0, 1);
    $result = $query->execute();
  } elseif(array_key_exists(5, $user->roles)) {

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'course')
      ->fieldCondition('field_course_curator', 'target_id', $user->uid, '=')
      ->propertyOrderBy('created', 'DESC');
    $result = $query->execute();
    $courses = array_keys($result['node']);

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'bonus')
      ->fieldCondition('field_bonuse_reference', 'nid', $courses, 'IN')
      ->range(0, 1);
    $result = $query->execute();
  }

  if(isset($result['node'])){
    $nid = current($result['node'])->nid;
    //drupal_goto("bonus/{$nid}");
  }else{
    drupal_goto("bonus/add");
  }

  return '';
}


function bonus_view_page_content($arg) {
  global $user;

  $bonus = node_load($arg);
  if(!$bonus || $bonus->type != 'bonus'){
    drupal_not_found();
  }

  $bonus_wrapper = entity_metadata_wrapper('node', $bonus);

  $row['title'] = $bonus->title;
  $row['body'] = $bonus_wrapper->body->raw();
  $image_path = $bonus_wrapper->field_bonus_image->value();

  if($image_path){
    $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'bonus_image'), 'getsize' => FALSE);
  }else{
    $instance = field_info_instance('node', 'field_bonus_image', 'bonus');
    $default_image = file_load($instance['settings']['default_image']);
    $image_settings = array('style_name' => 'list_images', 'path' => $default_image->uri, 'attributes' => array('class' => 'bonus_image'), 'getsize' => FALSE);
  }
  $img =  theme('image_style', $image_settings);

  $row['img'] = l($img, "bonus/{$bonus->nid}", array('html' => TRUE));

  if($bonus_wrapper->field_bonuse_reference->value()){
    foreach ($bonus_wrapper->field_bonuse_reference->value() as $key => $value) {
      if($key == 0){
        $row['course_ref'] = $value->title;
      }
      else{
        $row['course_ref'] .= ', ' . $value->title;
      }
    }
  }else{
    $row['course_ref'] = '';
  }

  $row['price'] = format_plural($bonus_wrapper->field_bonus_price->raw(), '1 point', '@count points');

  if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
    $row['actions']['edit'] = l("Редактировать", "bonus/{$bonus->nid}/edit");
    $row['actions']['delete'] = l("Удалить", "bonus/{$bonus->nid}/delete");
  }
  elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    $row['actions']['buy'] = drupal_get_form('buy_bonus_form', $bonus->nid);
  }

  $content = theme('bonus_view_page', array('item' => $row));

  return $content;
}


function buy_bonus_form($form, &$form_state) {
  global $user;
  $form = array();

  $query = db_select('ul_user_points', 'up');
  $query->fields('up', array('points'));
  $query->condition('up.uid', $user->uid);
  $c_point = $query->execute()->fetchObject();

  $bonus = node_load($form_state['build_info']['args'][0]);
  $bonus_wrapper = entity_metadata_wrapper('node', $bonus);

  $query = db_select('ul_bonus_order', 'bo');
  $query->fields('bo', array('boid'));
  $query->condition('bo.uid', $user->uid);
  $query->condition('bo.nid', $bonus->nid);
  $o_status = $query->execute()->fetchObject();

  if(!empty($o_status)){
    $form['already'] = array(
      '#type' => 'item',
      '#markup' => t('У Вас уже есть этот бонус'),
    );
  }elseif($bonus_wrapper->field_bonus_price->raw() > $c_point->points) {
    $form['no_money'] = array(
      '#type' => 'item',
      '#markup' => t('У Вас недостаточно баллов для покупки этого бонуса'),
    );
  }else{
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Buy')
    );
  }

  return $form;
}


function buy_bonus_form_submit($form, &$form_state) {
  global $user;
  $bonus = node_load($form_state['build_info']['args'][0]);

  $buy = db_insert('ul_bonus_order')
    ->fields(array('uid' => $user->uid, 'nid' => $bonus->nid))
    ->execute();

  $query = db_select('ul_user_points', 'up');
  $query->fields('up', array('points'));
  $query->condition('up.uid', $user->uid);
  $c_point = $query->execute()->fetchObject();

  $bonus = node_load($form_state['build_info']['args'][0]);
  $bonus_wrapper = entity_metadata_wrapper('node', $bonus);

  $new_points = $c_point->points - $bonus_wrapper->field_bonus_price->raw();

  $point = db_update('ul_user_points')
    ->fields(array('points' => $new_points))
    ->condition('uid', $user->uid)
    ->execute();

  if($buy && $point){
    drupal_set_message(t('Бонус куплен'), 'status', FALSE);
  }else{
    drupal_set_message(t('Бонус не куплен'), 'warning', FALSE);
  }
}


function bonus_add_page_content() {
  global $user;
  module_load_include('inc', 'node', 'node.pages');

  $node = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => 'bonus',
    'language' => LANGUAGE_NONE
  );
  $form = drupal_get_form('bonus_node_form', $node);

  return $form;
}


function bonus_edit_page_content($arg) {
  global $user;
  module_load_include('inc', 'node', 'node.pages');

  $node = node_load($arg);
  if($node->uid != $user->uid){
    drupal_access_denied();
  }

  $form = drupal_get_form('bonus_node_form', $node);

  return $form;
}


function bonus_delete_page_content($arg) {
  $node = node_load($arg);
  $content = "<div>Вы действительно хотите удалить бонус {$node->title}?</div>";

  $form = drupal_get_form('delete_bonus_form');
  $content .= render($form);

  return $content;
}


function delete_bonus_form($form, &$form_state) {

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Удалить')
  );

  return $form;
}


function delete_bonus_form_submit($form, &$form_state) {
  node_delete(arg(1));
  $form_state['redirect'] = 'bonus';
}


function bonuses_slider_subject() {
  global $user;

  if(array_key_exists(4, $user->roles)){
    $nodes = db_select('node', 'n');
    $nodes->addExpression('COUNT(*)');
    $nodes->condition('n.status', 0);
    $nodes->condition('n.type', 'bonus');
    $nodes->condition('n.uid', $user->uid);
    $c_nodes = $nodes->execute()->fetchObject();

    $bonuses = $c_nodes->expression;
  }
  elseif(array_key_exists(5, $user->roles)){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'course')
      ->fieldCondition('field_course_curator', 'target_id', $user->uid, '=')
      ->propertyOrderBy('created', 'DESC');
    $result = $query->execute();
    $courses = array_keys($result['node']);

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'bonus')
      ->fieldCondition('field_bonuse_reference', 'nid', $courses, 'IN');
    $bonuses = $query->count()->execute();

  }
  elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    $nodes = db_select('non_approved_students', 'nas');
    $nodes->fields('nas', array('cid', 'status'));
    $nodes->condition('nas.uid', $user->uid);
    $nodes->innerJoin('field_data_field_bonuse_reference', 'fdfbr', 'nas.cid = fdfbr.field_bonuse_reference_nid');
    $nodes->fields('fdfbr', array('entity_id'));
    $nodes->addExpression('COUNT(*)');
    $c_nodes = $nodes->execute()->fetchObject();

    $bonuses = $c_nodes->expression;
  }
  $subject = '<h4 class="title">' . t('Мои бонусы') . '</h4>';
  $subject .= '<div class="b_count">' . t('Доступно ') . $bonuses . '</div>';

  return $subject;
}


function bonuses_slider_content() {
  global $user;

  $nodes = array();

  if(array_key_exists(4, $user->roles)){

    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));
    $query->condition('n.status', 0);
    $query->condition('n.type', 'bonus');
    $query->condition('n.uid', $user->uid);
    $nodes = $query->execute()->fetchAll();

    $path_img = 'save-img.png';
    $am_params = array('style_name' => 'list_images', 'path' => $path_img, 'attributes' => array('class' => 'image '), 'getsize' => FALSE);
    $am_image =   theme('image_style', $am_params);

    $items[] = l($am_image, "bonus/add", array('html' => TRUE)) . l(t('Добавить бонус'), "bonus/add", array('html' => TRUE));
  }
  elseif(array_key_exists(5, $user->roles)){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'course')
      ->fieldCondition('field_course_curator', 'target_id', $user->uid, '=')
      ->propertyOrderBy('created', 'DESC');
    $result = $query->execute();
    $courses = array_keys($result['node']);

    if(!empty($courses)){

      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'bonus')
        ->fieldCondition('field_bonuse_reference', 'nid', $courses, 'IN');
      $bonuses = $query->execute();

      if(!empty($bonuses)){
        $bonus_arr = array_keys($bonuses['node']);

        foreach ($bonus_arr as $value) {
          $node = new stdClass();
          $node->nid = $value;
          $nodes[] = $node;
        }
      }
    }

    $path_img = 'save-img.png';
    $am_params = array('style_name' => 'list_images', 'path' => $path_img, 'attributes' => array('class' => 'image '), 'getsize' => FALSE);
    $am_image =   theme('image_style', $am_params);

    $items[] = l($am_image, "bonus/add", array('html' => TRUE)) . l(t('Добавить бонус'), "bonus/add", array('html' => TRUE));
  }
  elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    global $user;

    $query = db_select('non_approved_students', 'nas');
    $query ->fields('nas', array('cid', 'status'));
    $query ->condition('nas.uid', $user->uid);
    $query->innerJoin('field_data_field_bonuse_reference', 'fdfbr', 'nas.cid = fdfbr.field_bonuse_reference_nid');
    $query ->fields('fdfbr', array('entity_id'));
    $bonuses = $query->execute()->fetchAll();
    $nodes = '';
    foreach ($bonuses as $value) {
      $node = new stdClass();
      $node->nid = $value->entity_id;
      $nodes[] = $node;
    }
  }

  $i = 1;
  if($nodes){
    foreach ($nodes as $node) {
      $links = array();

      $bonus = node_load($node->nid);
      $bonus_wrapper = entity_metadata_wrapper('node', $bonus);

      $image_path = $bonus_wrapper->field_bonus_image->value();
      if($image_path){
        $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'bonus_image'), 'getsize' => FALSE);
      }else{
        $instance = field_info_instance('node', 'field_bonus_image', 'bonus');
        $default_image = file_load($instance['settings']['default_image']);
        $image_settings = array('style_name' => 'list_images', 'path' => $default_image->uri, 'attributes' => array('class' => 'bonus_image'), 'getsize' => FALSE);
      }
      $img =  theme('image_style', $image_settings);

      $title = truncate_utf8($bonus_wrapper->title->value(), 30, FALSE, TRUE);

      $items[] = l($img, "bonus/{$bonus->nid}", array('html' => TRUE)) . l($title, "bonus/{$bonus->nid}", array('html' => TRUE));

      $i++;
    }
  }

  $options = array(
    'visible' => 6,
    'scroll' => ($i < 6)? 0 : 1,
    'size' => $i++,
    'wrap' => 'circular',
  );

  $content = theme('jcarousel', array('items' => $items, 'options' => $options));

  return $content;
}

function bonus_product_drop_list() {
 global $user;
  ctools_include('ajax');
  drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/drop-lists.css');

$nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'bonus')
    ->execute()
    ->fetchAllKeyed(0, 0);
  $node = node_load_multiple($nids);
  $html = '<div class="drop_list_content"><button class="title-drop-list btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-align-justify"></span>Все бонусы</button>';
  $html .= '<ul class="all_item_list_drop dropdown-menu">';
  foreach ($node as $key => $value) {
    if (isset($value->title)) {
      $html .= '<li>' . $value->title . '</li>';
    }
  //echo $key->title .'<br>';
  }
  $html .= '</ul><div>';
    //print('<pre>' . print_r($value->title,1) . '</pre>');

  return $html;
}