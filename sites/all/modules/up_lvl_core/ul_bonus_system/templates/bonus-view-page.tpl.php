<div class="b_view_conten">
  <div class="uplvl-bonus-content">
    <h1 class="bonus_title"><?php print $item['title']; ?></h1>
    <div class="bonus_body"><?php print $item['body']['value'] ?></div>
  </div>
  <div class="uplvl-bonus-inform">
    <div class="bonus_img"><?php print $item['img']; ?></div>
    <div class="bonus_course">Бонус для курса: <div class="bonus-kurses"><?php print $item['course_ref']; ?></div></div>
    <div class="bonus_cost_points">Цена бонуса: <div class="bonus-price"><?php print $item['price']; ?></div></div>
  </div>

</div>

<?php if(!empty($item['actions']['edit']) || !empty($item['actions']['delete'])): ?>
  <div class="edit_links">
    <span class="edit_link edit_bonus"><?php print $item['actions']['edit']; ?></span>
    <span class="edit_link delete_bonus"><?php print $item['actions']['delete']; ?></span>
  </div>
<?php endif; ?>

<?php if(!empty($item['actions']['buy'])): ?>
  <div class="edit_links"><?php print drupal_render($item['actions']['buy']); ?></div>
<?php endif; ?>
