(function ($) {

  $(document).ready(function() {

    $('.s_add_text').live('click', function(){
      $('#section-node-form').fadeIn('slow');

      $('html,body').animate({
        scrollTop: $("#section-node-form").offset().top
      });
    });

    $('.s_add_button').live('click', function(){
      $('#section-node-form').fadeIn('slow');

      $('html,body').animate({
        scrollTop: $("#section-node-form").offset().top
      });
    });

    $('.s_number').live('click', function(){
      $('.section_all_content').find('span').removeClass('highlighted');
      $('.section_all_content').find('.s_number').removeClass('highlight');

      $(this).parent('div').find('span.s_line').addClass('highlighted');
      $(this).parent('div').find('.s_number').addClass('highlight');
    });

    $('.s_name').live('click', function(){
      $('.section_all_content').find('span').removeClass('highlighted');
      $('.section_all_content').find('.s_number').removeClass('highlight');

      $(this).parent('div').find('span.s_line').addClass('highlighted');
      $(this).parent('div').find('.s_number').addClass('highlight');
    });

    $('.section_all_content .s_number').live('click', function(){
      var sectionID = $(this).attr('id');
      $('.section_edit_links .edit_section').live('click', function(){
        window.location.href = window.location.origin + '/section/' + sectionID + '/edit';
      });
    });

    $('.section_all_content .s_name').live('click', function(){
      var sectionID = $(this).attr('id');
      $('.section_edit_links .edit_section').live('click', function(){
        window.location.href = window.location.origin + '/section/' + sectionID + '/edit';
      });
    });

  });

  // $(window).load(function() {
  //   $('#block-ul-library-system-libraries-slider .jcarousel-container').fadeIn("fast");
  // });

})(jQuery);
