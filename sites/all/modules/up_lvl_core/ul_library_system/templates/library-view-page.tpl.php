<?php
/*
 *  $item - contains all data about library node.
 **/
?>
<div class="library_content">
  <div class="l_view_conten">
    <div class="library_content_block">
      <h1 class="library_title"><?php print $item['title']; ?></h1>
      <div class="library_body"><?php print $item['body']['value'] ?></div>
    </div>

    <div class="library-information">
    <?php if (isset($item['img'])): ?>
      <div class="library_img"><?php print $item['img']; ?></div>
    <?php endif; ?>
      <div class="library_course">Библиотека для курса: <div class="cource"><?php print $item['course_ref']; ?></div></div>
      <div class="library_price">Цена библиотеки: <div class="price-count"><?php print $item['price']; ?></div></div>
    </div>
  </div>
  <?php if(!empty($item['actions']['admin'])): ?>
    <div class="edit_links">
      <?php foreach ($item['actions']['admin'] as $key => $value): ?>
        <div class="edit_link"><?php print $value; ?></div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>

  <?php if(!empty($item['actions']['buy'])): ?>
    <div class="edit_links"><?php print drupal_render($item['actions']['buy']); ?></div>
  <?php endif; ?>
</div>

