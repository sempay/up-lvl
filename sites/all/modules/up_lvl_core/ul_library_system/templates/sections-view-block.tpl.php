<div class="block-sdw"></div>
<div class="sections_content">

    <div class="section_edit_links">
      <?php if($role && $role == 'admin'): ?>
      <div class="empty"></div>
      <?php endif; ?>
      <?php if (!empty($items)) : ?>
      <div class="section_header_title"><?php print t('Разделы библиотеки'); ?></div>
      <?php endif; ?>
    </div>

  <div class="section_all_content">
    <?php if($role && $role == 'admin' && count($items) < 20): ?>
      <div class="s_add section">
        <div class="s_add_button"></div>

        <?php if($items): ?>
          <span class="s_line highlighted"></span>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if($items): ?>
      <?php $i = 1; ?>
        <div class="section">
        <?php foreach ($items as $key => $value): ?>
          <div class="uplvl-section">
          <div id="<?php print $key; ?>" class="s_number"><?php print $i; ?></div>
          <div class="section-short-inform">
            <div id="<?php print $key; ?>" class="s_name"><?php print $value['title']; ?></div>
              <?php if($role && $role == 'admin'): ?>
              <div class="section-edit"><a href="/section/<?php print $key; ?>/edit" class="link-edit-section"></a></div>
              <?php endif; ?>
            <div class="s_files_number"><?php print $value['files_number']; ?></div>
          </div>
          <?php if($i < count($items)): ?>
            <span class="s_line"></span>
          <?php endif; ?>
            <?php $i++; ?>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
      <?php if($role && $role == 'admin'): ?>
        <div>
          <span class="s_line"></span>
          <div class="s_add_text"><?php print l('', 'add-section/'.arg(1)); ?></div>
          <div class="uplvl_s_add_text"><?php print l('Создать раздел библиотеки', 'add-section/'.arg(1)); ?></div>
        </div>
      <?php endif; ?>
      </div>
    <?php if(!$items && $role != 'admin'): ?>
      <div class="empty"><?php print t('Извините, Здесь пока нету разделов'); ?></div>
    <?php endif; ?>
  </div>
<div class="block-sdw2"></div>
