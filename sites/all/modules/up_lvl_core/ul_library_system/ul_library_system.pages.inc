<?php


function library_all_content() {
  global $user;

  if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'library')
      ->propertyCondition('uid', $user->uid)
      ->propertyOrderBy('created', 'DESC')
      ->range(0, 1);
    $result = $query->execute();
    if(isset($result['node'])){
      $nid = current($result['node'])->nid;
      //drupal_goto("library/{$nid}");
    }else{
      drupal_goto("library/add");
    }
  }

  return '';
}


function library_users_view_page_content($node) {
  global $user;
  $t_rows = array();

  if($node->type != 'library'){
    drupal_not_found();
  }

  if($node->type == 'library'){
    $library_wrapper = entity_metadata_wrapper('node', $node);

    $row['title'] = $library_wrapper->label();

    $row['body'] = $library_wrapper->body->raw();
    $image_path = $library_wrapper->field_library_image->value();

    if($image_path){
      $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
    }else{
      $instance = field_info_instance('node', 'field_library_image', 'library');
      $default_image = file_load($instance['settings']['default_image']);
      $image_settings = array('style_name' => 'list_images', 'path' => $default_image->uri, 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
    }
    $img =  theme('image_style', $image_settings);

    $row['img'] = l($img, "library/{$library_wrapper->getIdentifier()}", array('html' => TRUE));

    if($library_wrapper->field_library_course->value()){
      foreach ($library_wrapper->field_library_course->value() as $key => $value) {
        if($key == 0){
          $row['course_ref'] =  $value->title;
        }
        else{
          $row['course_ref'] .= ', ' . $value->title;
        }
      }
    }else{
      $row['course_ref'] = '';
    }

    if($library_wrapper->field_library_price->raw() > 0){
      $row['price'] = format_plural($library_wrapper->field_library_price->raw(), '1 point', '@count points');
    }else{
      $row['price'] = '<p class="green">' . t('Бесплатно') . '</p>';
    }

    if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
      $row['actions'][] = l("Редактировать библиотеку", "library/{$library_wrapper->getIdentifier()}/edit");
      $row['actions'][] = l("Удалить библиотеку", "library/{$library_wrapper->getIdentifier()}/delete");
      $row['actions'][] = l("Пользователи библиотеки", "library/users/{$library_wrapper->getIdentifier()}");
    }

    $content = theme('library_view_page', array('item' => $row));


    $query = db_select('ul_library_order', 'lo');
    $query->fields('lo', array('oid', 'uid', 'price', 'created', 'nid'));
    $query->condition('lo.nid', $library_wrapper->getIdentifier());
    $o_status = $query->execute()->fetchAll();

    $header = array('Имя пользователя', 'получил доступ', '');
    foreach ($o_status as $value) {

      $l_user = user_load($value->uid);

      $form = drupal_get_form('delete_library_user_' . $value->uid, $value->oid);
      $t_rows[$value->oid] = array(
        array('data' => l($l_user->name, "user/$l_user->uid", array('attributes' => array('target' => '_blank')))),
        array('data' => format_date($value->created, 'message')),
        array('data' => drupal_render($form), 'class' => 'del_lib_button')
      );
    }

    $table = theme('table', array('header' => $header, 'rows' => $t_rows, 'attributes' => array('cellpadding' => 0, 'cellspacing' => 0)));

    $form = drupal_get_form('add_user_library_form', $library_wrapper->getIdentifier());
    $form = drupal_render($form);

    return $content . $table . $form;
  }
}


function delete_library_user_form($form, &$form_state, $args) {

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('delete')
  );

  return $form;
}


function delete_library_user_form_submit($form, &$form_state) {

  $delete = db_delete('ul_library_order')
    ->condition('oid', $form_state['build_info']['args'][0])
    ->execute();

}


function user_mail_search_autocomplete($string = '') {

  $result = db_select('users', 'u')
    ->fields('u', array('mail', 'uid'))
    ->condition('mail', '%' . db_like($string) . '%', 'LIKE')
    ->range(0, 10)
    ->execute();

  $matches = array();
  foreach ($result as $row) {
    $matches[check_plain($row->mail)] = check_plain($row->mail);
  }

  drupal_json_output($matches);
}


function add_user_library_form($form, &$form_state) {

  $form['e_mail'] = array(
    '#title' => t('E-mail ученика'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#autocomplete_path' => 'user/mail/autocomplete',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}


function add_user_library_form_submit($form, &$form_state) {

  $e_mail = $form_state['values']['e_mail'];

  if($pupil = user_load_by_mail($e_mail)){

    $query = db_select('ul_library_order', 'lo');
    $query->fields('lo', array('oid'));
    $query->condition('lo.nid', $form_state['build_info']['args'][0]);
    $query->condition('lo.uid', $pupil->uid);
    $o_status = $query->execute()->fetchAll();

    if(!$o_status){
      db_insert('ul_library_order')
        ->fields(array('uid' => $pupil->uid, 'nid' => arg(2), 'price' => 0, 'created' => time()))
        ->execute();
    }
    else{
      drupal_set_message(t('У ученика уже есть доступ к этой библиотеке'), 'warning', FALSE);
    }
  }

}


function section_content_callback($js = NULL, $node) {
  ctools_include('ajax');

  $node_wrapper = entity_metadata_wrapper('node', $node);
  $library_id = $node_wrapper->field_section_reference->raw();

  $commands = array();
  if($node_wrapper->field_section_files->value()){
    foreach ($node_wrapper->field_section_files->value() as $value) {
      $link = l($value['filename'], file_create_url($value['uri']), array('attributes' => array('target' => '_black')));

      $t_rows[$value['fid']] = array(
        array('data' => $link)
      );
    }

    $header = array('Файлы библиотеки');

    $table = theme('table', array('header' => $header, 'rows' => $t_rows, 'attributes' => array('cellpadding' => 0, 'cellspacing' => 0)));
    $commands[] = ajax_command_html('#section_content', $table);
  }

  if($js) {
    return array('#type' => 'ajax', '#commands' => $commands);
  }else{
    drupal_goto("library/$library_id");
  }
}


function libraries_slider_subject() {
  global $user;
  $count = 0;
  if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
    $nodes = db_select('node', 'n');
    $nodes->addExpression('COUNT(*)');
    $nodes->condition('n.status', 0);
    $nodes->condition('n.type', 'library');
    $nodes->condition('n.uid', $user->uid);
    $c_nodes = $nodes->execute()->fetchObject();

    $count = $c_nodes->expression;
  }
  elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    $query = db_select('ul_library_order', 'lo');
    $query->fields('lo', array('nid'));
    $query->condition('lo.uid', $user->uid);
    $results = $query->execute()->fetchAll();

    if(!empty($results)){
        foreach ($results as $value) {
          $result = new stdClass();
          $result->nid = $value->nid;
          $nodes[] = $result;
        }
        $count = count($nodes);
    }
  }
  $subject = '<h4 class="title">' . t('Мои библиотеки') . '</h4>';
  $subject .= '<div class="b_count">' . t('Доступно ') . $count . '</div>';

  return $subject;
}


function libraries_slider_content() {
  global $user;

  $nodes = array();
  $items = array();
  if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){

    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));
    $query->condition('n.status', 0);
    $query->condition('n.type', 'library');
    $query->condition('n.uid', $user->uid);
    $nodes = $query->execute()->fetchAll();

    $path_img = 'save-img.png';
    $am_params = array('style_name' => 'list_images', 'path' => $path_img, 'attributes' => array('class' => 'image '), 'getsize' => FALSE);
    $am_image =   theme('image_style', $am_params);

    $items[] = l($am_image, "library/add", array('html' => TRUE)) . l(t('Добавить библиотеку'), "library/add", array('html' => TRUE));
  }
  elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    $query = db_select('ul_library_order', 'lo');
    $query->fields('lo', array('nid'));
    $query->condition('lo.uid', $user->uid);
    $results = $query->execute()->fetchAll();

    if(!empty($results)){
        foreach ($results as $value) {
          $result = new stdClass();
          $result->nid = $value->nid;
          $nodes[] = $result;
        }

        $count = count($nodes);
    }
  }

  $i = 1;
  if($nodes){
    foreach ($nodes as $node) {
      $links = array();

      $library = node_load($node->nid);
      $library_wrapper = entity_metadata_wrapper('node', $library);

      $image_path = $library_wrapper->field_library_image->value();
      if($image_path){
        $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
      }else{
        $instance = field_info_instance('node', 'field_library_image', 'library');
        $default_image = file_load($instance['settings']['default_image']);
        $image_settings = array('style_name' => 'list_images', 'path' => $default_image->uri, 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
      }
      $img =  theme('image_style', $image_settings);

      $title = truncate_utf8($library_wrapper->title->value(), 30, FALSE, TRUE);

      $items[] = l($img, "library/{$library->nid}", array('html' => TRUE)) . l($title, "library/{$library->nid}", array('html' => TRUE));


      $i++;
    }
  }

  $options = array(
    'visible' => 6,
    'scroll' => ($i < 6)? 0 : 1,
    'size' => $i++,
    'wrap' => 'circular',
  );

  if($items){
    $content = theme('jcarousel', array('items' => $items, 'options' => $options));
  }else{
    $content = '<div class="empty-lib">Нет доступных библиотек</div>';
  }

  return $content;
}


function library_view_page_content($node) {
  global $user;

  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $library = $node;
  // $library = node_load($arg);
  if(!$library || $library->type != 'library'){
    drupal_not_found();
  }

  $library_wrapper = entity_metadata_wrapper('node', $library);

  $query = db_select('ul_library_order', 'lo');
  $query->fields('lo', array('oid'));
  $query->condition('lo.uid', $user->uid);
  $query->condition('lo.nid', $library->nid);
  $o_status = $query->execute()->fetchObject();


  $row['title'] = $library->title;

  $row['body'] = $library_wrapper->body->raw();
  $image_path = $library_wrapper->field_library_image->value();

  if($image_path){
    $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
  }else{
    $instance = field_info_instance('node', 'field_library_image', 'library');
    $default_image = file_load($instance['settings']['default_image']);
    $image_settings = array('style_name' => 'list_images', 'path' => $default_image->uri, 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
  }
  $img =  theme('image_style', $image_settings);

  $row['img'] = l($img, "library/{$library->nid}", array('html' => TRUE));

  if($library_wrapper->field_library_course->value()){
    foreach ($library_wrapper->field_library_course->value() as $key => $value) {
      if($key == 0){
        $row['course_ref'] = @$value->title;
      }
      else{
        $row['course_ref'] .= ', ' . $value->title;
      }
    }
  }else{
    $row['course_ref'] = '';
  }

  if($library_wrapper->field_library_price->raw() > 0){
    $row['price'] = format_plural($library_wrapper->field_library_price->raw(), '1 point', '@count points');
  }else{
    $row['price'] = '<p class="green">' . t('бесплатно') . '</p>';
  }


  $row['actions'] = array();
  if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
    $row['actions']['admin'][] = '<div class="edit-library-button">' . l("Редактировать", "library/{$library->nid}/edit") . '</div>';
    $row['actions']['admin'][] = '<div class="delete-library-button">' . l("Удалить", "library/{$library->nid}/delete") . '</div>';
    $row['actions']['admin'][] = '<div class="library-user">' . l("Пользователи библиотеки", "library/users/{$library->nid}") . '</div>';

    $role = 'admin';
  }elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    if(empty($o_status) && ($library_wrapper->field_library_price->raw() > 0 || $library_wrapper->field_library_price->raw())){
      // $row['actions']['buy'] = drupal_get_form('buy_bonus_form', $library->nid);
    }
    $role = 'student';
  }

  $content = theme('library_view_page', array('item' => $row));

  // block with library parts.
  $s_item = array();
  $sections = library_get_section($node->nid);
  foreach ($sections as $key => $value) {
    $section = node_load($value->entity_id);
    $section_wrapper = entity_metadata_wrapper('node', $section);

    if(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
      $s_item[$section->nid]['title'] =  l($section->title, 'section/nojs/view/' . $section->nid, array('html' => TRUE, 'attributes' => array('class' => array('use-ajax'))));
    }else{
      $s_item[$section->nid]['title'] = $section->title;
    }

    $s_item[$section->nid]['files_number'] = format_plural(count($section_wrapper->field_section_files->value()), '1 file', '@count files');
    $s_item[$section->nid]['files'] = $section_wrapper->field_section_files->value();
  }

  $content_section = '';
  if($role == 'student'){
    if($library_wrapper->field_library_price->raw() == 0 || !empty($o_status)){
      $content_section = theme('block_sections', array('items' => $s_item, 'role' => $role));
    }
  }elseif($role == 'admin'){
    $content_section = theme('block_sections', array('items' => $s_item, 'role' => $role));
  }

  // add section form to current library
  // module_load_include('inc', 'node', 'node.pages');

  // $add_section = (object) array(
  //   'uid' => $user->uid,
  //   'name' => (isset($user->name) ? $user->name : ''),
  //   'type' => 'section',
  //   'language' => LANGUAGE_NONE
  // );
  $section_content = '';
  if(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    $section_content = '<div id="section_content"></div>';
  }
  return $content . $content_section . $section_content;
}


// function buy_bonus_form($form, &$form_state) {
//   global $user;
//   $form = array();

//   $query = db_select('ul_user_points', 'up');
//   $query->fields('up', array('points'));
//   $query->condition('up.uid', $user->uid);
//   $c_point = $query->execute()->fetchObject();

//   $library = node_load($form_state['build_info']['args'][0]);
//   $library_wrapper = entity_metadata_wrapper('node', $library);

//   if(empty($c_point->points) || $library_wrapper->field_library_price->raw() > $c_point->points && $library_wrapper->field_library_price->raw() > 0) {
//     $form['no_money'] = array(
//       '#type' => 'item',
//       '#markup' => t('У Вас недостаточно баллов для покупки этой библиотеки'),
//     );
//   }else{
//     $form['submit'] = array(
//       '#type' => 'submit',
//       '#value' => t('Buy')
//     );
//   }

//   return $form;
// }


// function buy_bonus_form_submit($form, &$form_state) {
//   global $user;
//   $library = node_load($form_state['build_info']['args'][0]);

//   $query = db_select('ul_user_points', 'up');
//   $query->fields('up', array('points'));
//   $query->condition('up.uid', $user->uid);
//   $c_point = $query->execute()->fetchObject();

//   $library_wrapper = entity_metadata_wrapper('node', $library);

//   $new_points = $c_point->points - $library_wrapper->field_library_price->raw();

//   $point = db_update('ul_user_points')
//     ->fields(array('points' => $new_points))
//     ->condition('uid', $user->uid)
//     ->execute();

//   $price = 0;
//   if($library_wrapper->field_library_price->raw()){
//     $price = $library_wrapper->field_library_price->raw();
//   }

//   $buy = db_insert('ul_library_order')
//     ->fields(array('uid' => $user->uid, 'nid' => $library->nid, 'created' => time(), 'price' => $price))
//     ->execute();

//   if($buy && $point){
//     drupal_set_message(t('Библиотека куплена'), 'status', FALSE);
//   }else{
//     drupal_set_message(t('Библиотека не куплена'), 'warning', FALSE);
//   }
// }


function section_edit_page_content($arg) {
  global $user;
  module_load_include('inc', 'node', 'node.pages');

  $node = node_load($arg);
  if($node->uid != $user->uid){
    drupal_access_denied();
  }

  $form = drupal_get_form('section_node_form', $node);
  return drupal_render($form);
}


function section_delete_page_content($arg) {
  $node = node_load($arg);
  $content = "<div>Вы действительно хотите удалить раздел '{$node->title}'?</div>";

  $form = drupal_get_form('delete_section_form');
  $content .= render($form);

  return $content;
}


function delete_section_form($form, &$form_state) {

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Удалить')
  );

  return $form;
}


function delete_section_form_submit($form, &$form_state) {
  node_delete(arg(1));
  $form_state['redirect'] = 'library';
}


function library_add_page_content() {
  global $user;
  module_load_include('inc', 'node', 'node.pages');

  $node = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => 'library',
    'language' => LANGUAGE_NONE
  );
  $form = drupal_get_form('library_node_form', $node);

  return $form;
}


function library_edit_page_content($arg) {
  global $user;
  module_load_include('inc', 'node', 'node.pages');

  $node = node_load($arg);
  if($node->uid != $user->uid){
    drupal_access_denied();
  }

  $form = drupal_get_form('library_node_form', $node);

  return $form;
}


function library_delete_page_content($arg) {
  $node = node_load($arg);
  $content = "<div>Вы действительно хотите удалить библиотеку '{$node->title}' и все её разделы?</div>";

  $form = drupal_get_form('delete_library_form');
  $content .= render($form);

  return $content;
}


function delete_library_form($form, &$form_state) {

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Удалить')
  );

  return $form;
}


function delete_library_form_submit($form, &$form_state) {

  $sections = library_get_section(arg(1));

  foreach ($sections as $section) {
    $rows[] = $section->entity_id;
  }
  $rows[] = arg(1);

  node_delete_multiple($rows);
  $form_state['redirect'] = 'library';
}



function library_product_drop_list() {
  global $user;
  $max_title_length  = 40;
  ctools_include('ajax');
  drupal_add_css(drupal_get_path('theme', 'uplvl') . '/css/drop-lists.css');


  $html = '<div class="drop_list_content"><button class="title-drop-list btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-align-justify"></span>Все библиотеки</button>';
  $html .= '<ul class="all_item_list_drop dropdown-menu">';
  $nodes = get_list_of_library();
  foreach ($nodes as $value) {
    $node = node_load($value->nid);
    if ($node) {
      $link = l(mb_substr(strip_tags($node->title), 0, $max_title_length) . '...', "library/$node->nid", array('html' => TRUE, 'attributes' => array('class' => array('library-link'))));
      $html .= '<li>' . $link . '</li>';
    }
  }
  $html .= '</ul><div>';

  return $html;
}

function get_list_of_library() {
  global $user;

  $nodes = array();
  if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){

    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));
    $query->condition('n.status', 0);
    $query->condition('n.type', 'library');
    $query->condition('n.uid', $user->uid);
    $nodes = $query->execute()->fetchAll();

  }elseif(array_key_exists(6, $user->roles) || array_key_exists(9, $user->roles)){
    $query_lo = db_select('ul_library_order', 'lo');
    $query_lo->fields('lo', array('nid'));
    $query_lo->condition('lo.uid', $user->uid);

    $query_co = db_select('non_approved_students', 'nas');
    $query_co ->fields('nas', array('cid'));
    $query_co ->condition('nas.uid', $user->uid);
    $results = $query_co->execute()->fetchAll();
    if(!empty($results)){
      foreach ($results as $tid) {
        $courses[] = $tid->cid;
      }
      $query_ln = db_select('node', 'ln');
      $query_ln->fields('ln', array('nid'));
      $query_ln->innerJoin('field_data_field_library_course', 'lc', 'lc.entity_id = ln.nid');
      $query_ln->condition('ln.status', NODE_NOT_PUBLISHED);
      $query_ln->condition('ln.type', 'library');
      $query_ln->condition('lc.field_library_course_nid', $courses, 'IN');

      $query_lo->union($query_ln, 'UNION');
    }
    $nodes = $query_lo->distinct()->execute()->fetchAll();
  }
  
  return $nodes;
}