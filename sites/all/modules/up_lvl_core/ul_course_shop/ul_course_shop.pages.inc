<?php


function courses_shop_content_callback() {
  $form = drupal_get_form('course_shop_search_form');
  return $form;
}


function course_shop_search_form($form, $form_state) {
  $form = array();

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Выбор курса'),
    '#collapsible' => FALSE, // Added
    '#collapsed' => FALSE,  // Added
  );

  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Поиск'),
    // '#submit' => array('course_shop_search_form_submit'),
    '#ajax' => array(
      'wrapper' => 'courses-wrapper',
      'callback' => 'course_shop_search_form_update',
    ),
  );

  $discipline_terms = taxonomy_get_tree(2);
  $disciplines = array('_none' => t('любой'));
  foreach ($discipline_terms as $key) {
    $disciplines[$key->tid] = $key->name;
  }

  $form['search']['first'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="first-wrapper">',
    '#suffix' => '</div>',
  );

  $form['search']['first']['discipline'] = array(
    '#type' => 'select',
    '#title' => t('по направлению:'),
    '#options' => $disciplines,
    '#default_value' => '_none',
  );

  $form['search']['first']['pupil'] = array(
    '#type' => 'select',
    '#title' => t('по учебному уч-нию:'),
    '#options' => array('_none' => 'любой'),
    '#default_value' => '_none',
  );

  $query = db_select('users', 'u');
  $query->innerJoin('users_roles', 'ur', 'ur.uid = u.uid');
  $query->fields('u', array('uid'));
  $query->condition('u.status', 1);
  $query->condition('ur.rid', 4);
  $c_admin = $query->execute()->fetchAll();

  $courators = array('_none' => t('любой'));
  foreach ($c_admin as $key) {
    $courator = user_load($key->uid);
    $user_wrapper = entity_metadata_wrapper('user', $courator);
    $courators[$courator->uid] = ($user_wrapper->field_fio->raw())? $user_wrapper->field_fio->raw() :$courator->name;
  }

  $form['search']['second'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="second-wrapper">',
    '#suffix' => '</div>',
  );

  $form['search']['second']['courator'] = array(
    '#type' => 'select',
    '#title' => t('по преподавателю:'),
    '#options' => $courators,
    '#default_value' => '_none',
  );

  $form['search']['second']['price'] = array(
    '#type' => 'select',
    '#title' => t('платно/бесплатно:'),
    '#options' => array(
      '_none' => t('любой'),
      1 => 'платно',
      2 => 'бесплатно',
      3 => 'платно за баллы',
    ),
    '#default_value' => '_none',
  );

  // $form['courses'] = array(
  //   '#tree' => TRUE,
  //   '#prefix' => '<div id="courses-wrapper">',
  //   '#suffix' => '</div>',
  // );

  return $form;
}


function course_shop_search_form_update($form, &$form_state) {
  ctools_include('ajax');
  global $user;

  $query = db_select('node', 'n');
  $query->fields('n', array('nid'));
  $query->condition('n.type', 'course_product');
  if($form_state['values']['first']['discipline'] != '_none'){
    $query->innerJoin('field_revision_field_cp_study_direction', 'cpsd', 'cpsd.entity_id = n.nid');
    $query->condition('cpsd.field_cp_study_direction_tid', $form_state['values']['first']['discipline']);
  }

  if($form_state['values']['second']['price'] != '_none'){
    $query->innerJoin('field_revision_field_cp_price', 'cpp', 'cpp.entity_id = n.nid');
    $query->condition('cpp.field_cp_price_value', $form_state['values']['second']['price']);
  }

  if($form_state['values']['second']['courator'] != '_none'){
    $query->condition('n.uid', $form_state['values']['second']['courator']);
  }else{
    $query->condition('n.uid', $user->uid);
  }
  $c_products = $query->execute()->fetchAll();

  $content = '<div class="product_courses_view"><div id="empty_c_p">Нет материалов</div></div>';
  // if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
  //   $content .= '<div class="add_button">' . l(t('Add'), 'p_course/nojs/add', array('html' => TRUE, 'attributes' => array('class' => array('use-ajax')))) . '</div>';
  //   $content .= '<div class="add-p-course-wrapper"></div>';
  // }

  $commands = array();
  if($c_products){
    foreach ($c_products as $key => $value) {
      $node = node_load($value->nid);
      $node_wrapper = entity_metadata_wrapper('node', $node);

      $image_path = $node_wrapper->field_cp_image->value();
      $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
      $img =  theme('image_style', $image_settings);

      $row[$node->nid]['image'] = $img;
      $row[$node->nid]['study_direction'] = $node_wrapper->field_cp_study_direction->value()->name;
      $row[$node->nid]['educational_institution'] = $node_wrapper->field_cp_educational_institution->raw();
      $row[$node->nid]['author'] = $node_wrapper->field_cp_author->raw();
      $row[$node->nid]['title'] = $node_wrapper->label();
      $row[$node->nid]['body'] = $node_wrapper->body->raw();
      $row[$node->nid]['price'] = $node_wrapper->field_cp_price->raw();
      $settings = array(
        'content_type' => 'node',
        'content_id' => $node->nid,
        'entity' => $node,
        'stars' => 5,
        'field_name' => 'field_cp_rating',
        'autosubmit' => TRUE,
        'allow_clear' => FALSE,
        'langcode' => $node->language,
        'text' => 'none', // options are none, average, smart, user, and dual
        'tag' => 'vote',
        'style' => 'average', // options are average, dual, smart, user
        'widget' => array( 'name' => 'blue_stars', 'css' => drupal_get_path('module', 'fivestar') . '/widgets/blue_stars/blue_stars.css' )
      );

      $fivestar_values = fivestar_get_votes('node', $node->nid, 'vote', $user->uid);
      $fi = drupal_get_form('fivestar_custom_widget', $fivestar_values, $settings);
      $fivestar_form = render($fi);
      $row[$node->nid]['vote'] = $fivestar_form;

      if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
        $row[$node->nid]['edit'] =  l(t('Edit'), 'p_course/nojs/edit/' . $node->nid, array('html' => TRUE, 'attributes' => array('class' => array('use-ajax'))));
      }
    }

    $add = null;
    // if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
    //   $add =  l(t('Add'), 'p_course/nojs/add', array('html' => TRUE, 'attributes' => array('class' => array('use-ajax'))));
    // }

    $content = theme('product_courses_view', array('items' => $row, 'add' => $add));
  }

  $commands[] = ajax_command_html('.course_product_content', $content);

  return array('#type' => 'ajax', '#commands' => $commands);
}

function course_product_list_content() {
  global $user;
  ctools_include('ajax');

  $query = db_select('node', 'n');
  $query->fields('n', array('nid'));
  $query->condition('n.uid', $user->uid);
  $query->condition('n.type', 'course_product');
  $c_products = $query->execute()->fetchAll();

  $content = '<div class="product_courses_view"><div id="empty_c_p">Нет материалов</div></div>';
  // if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
  //   $content .= '<div class="add_button">' . l(t('Add'), 'p_course/nojs/add', array('html' => TRUE, 'attributes' => array('class' => array('use-ajax')))) . '</div>';
  //   $content .= '<div class="add-p-course-wrapper"></div>';
  // }

  if($c_products){
    foreach ($c_products as $key => $value) {
      $node = node_load($value->nid);
      $node_wrapper = entity_metadata_wrapper('node', $node);

      $image_path = $node_wrapper->field_cp_image->value();
      $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
      $img =  theme('image_style', $image_settings);

      $row[$node->nid]['image'] = $img;
      $row[$node->nid]['study_direction'] = @$node_wrapper->field_cp_study_direction->value()->name;
      $row[$node->nid]['educational_institution'] = $node_wrapper->field_cp_educational_institution->raw();
      $row[$node->nid]['author'] = $node_wrapper->field_cp_author->raw();
      $row[$node->nid]['title'] = $node_wrapper->label();
      $row[$node->nid]['body'] = $node_wrapper->body->raw();
      $row[$node->nid]['price'] = $node_wrapper->field_cp_price->raw();

      $settings = array(
        'content_type' => 'node',
        'content_id' => $node->nid,
        'entity' => $node,
        'stars' => 5,
        'field_name' => 'field_cp_rating',
        'autosubmit' => TRUE,
        'allow_clear' => FALSE,
        'langcode' => $node->language,
        'text' => 'none', // options are none, average, smart, user, and dual
        'tag' => 'vote',
        'style' => 'average', // options are average, dual, smart, user
        'widget' => array( 'name' => 'blue_stars', 'css' => drupal_get_path('module', 'fivestar') . '/widgets/blue_stars/blue_stars.css' )
      );

      $fivestar_values = fivestar_get_votes('node', $node->nid, 'vote', $user->uid);
      $fi = drupal_get_form('fivestar_custom_widget', $fivestar_values, $settings);
      $fivestar_form = render($fi);
      $row[$node->nid]['vote'] = $fivestar_form;

      if((array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)) && $node->uid == $user->uid){
        $row[$node->nid]['edit'] =  l(t('Edit'), 'p_course/nojs/edit/' . $node->nid, array('html' => TRUE, 'attributes' => array('class' => array('use-ajax'))));
      }
    }

    $add = null;
    // if(array_key_exists(4, $user->roles) || array_key_exists(8, $user->roles) || array_key_exists(5, $user->roles)){
    //   $add =  l(t('Add'), 'p_course/nojs/add', array('html' => TRUE, 'attributes' => array('class' => array('use-ajax'))));
    // }

    $content = theme('product_courses_view', array('items' => $row, 'add' => $add));
  }

  return $content;
}


function p_course_add_callback($js = NULL) {
  ctools_include('ajax');
  global $user;

  module_load_include('inc', 'node', 'node.pages');

  $add_course = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => 'course_product',
    'language' => LANGUAGE_NONE
  );

  $form = drupal_get_form('course_product_node_form', $add_course);

  $commands[] = ajax_command_html('.add-p-course-wrapper', drupal_render($form));
  if($js){
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}


function p_course_edit_callback($js = NULL, $node) {
  ctools_include('ajax');

  $form = drupal_get_form('edit_product_course_' . $node->nid, $node);

  $commands[] = ajax_command_html('#product_course_' . $node->nid, drupal_render($form));
  if($js){
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}


function edit_product_course_form($form, &$form_state) {
  $form = array();

  $node = $form_state['build_info']['args'][0];
  $node_wrapper = entity_metadata_wrapper('node', $node);

  $form['#attributes'] = array('class' => 'edit_form');

  $form['left_form_wrapper'] = array(
    '#tree' => true,
    '#prefix' => '<div class="left_form_wrapper">',
    '#suffix' => '</div>'
  );

  $form['right_form_wrapper'] = array(
    '#tree' => true,
    '#prefix' => '<div class="right_form_wrapper">',
    '#suffix' => '</div>'
  );

  $form['left_form_wrapper']['file_fid'] = array(
    '#type' => 'managed_file',
    '#size' => 5000,
    '#upload_location' => 'public://',
    '#default_value' => @$node->field_cp_image['und'][0]['fid'],
    // '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('png gif jpg jpeg'),
      'file_validate_size' => array(103809024),
    ),
    '#theme' => 'image_upload_preview',
  );

  $form['left_form_wrapper']['node_id'] = array(
    '#type' => 'item',
    '#markup' => 'ID:' . $node->nid
  );

  $discipline_terms = taxonomy_get_tree(2);
  foreach ($discipline_terms as $key) {
    $disciplines[$key->tid] = $key->name;
  }

  $form['right_form_wrapper']['study_direction'] = array(
    '#type' => 'select',
    '#title' => t('[<span>Направление:</span>'),
    '#options' => $disciplines,
    '#default_value' => $node_wrapper->field_cp_study_direction->value()->tid,
  );

  $form['right_form_wrapper']['educational_institution'] = array(
    '#type' => 'textfield',
    '#title' => t('[<span>Учебное учреждение:</span>'),
    '#default_value' => $node_wrapper->field_cp_educational_institution->raw()
  );

  $form['right_form_wrapper']['author'] = array(
    '#type' => 'textfield',
    '#title' => t('[<span>Автор:</span>'),
    '#default_value' => $node_wrapper->field_cp_author->raw()
  );

  $form['right_form_wrapper']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Название:'),
    '#default_value' => $node_wrapper->label()
  );

  $body = $node_wrapper->body->raw();

  $form['right_form_wrapper']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Описание:'),
    '#default_value' => $body['value']
  );

  $form['right_form_wrapper']['search']['price'] = array(
    '#type' => 'select',
    '#title' => t('платно/бесплатно:'),
    '#options' => array(
      1 => 'платно',
      2 => 'бесплатно',
      3 => 'платно за баллы',
    ),
    '#default_value' => $node_wrapper->field_cp_price->raw(),
  );

  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Сохранить'),
    '#ajax' => array(
      'wrapper' => 'courses-wrapper',
      'callback' => 'edit_product_course_form_update',
    ),
  );

  return $form;
}


function edit_product_course_form_update($form, &$form_state) {
  ctools_include('ajax');
  global $user;

  $node = $form_state['build_info']['args'][0];
  $node = node_load($node->nid);
  $node_wrapper = entity_metadata_wrapper('node', $node);

  $new_node = $form_state['values'];

  $file = file_load($form_state['values']['left_form_wrapper']['file_fid']);
  $file_arr = array();
  foreach ($file as $key => $value) {
    $file_arr[$key] = $value;
  }
  $node_wrapper->field_cp_image->set($file_arr);


  $node_wrapper->field_cp_study_direction->set($form_state['values']['right_form_wrapper']['study_direction']);
  $node_wrapper->field_cp_educational_institution->set($form_state['values']['right_form_wrapper']['educational_institution']);
  $node_wrapper->field_cp_author->set($form_state['values']['right_form_wrapper']['author']);
  $node_wrapper->title->set($form_state['values']['right_form_wrapper']['title']);
  $text = array(
    'value' => $form_state['values']['right_form_wrapper']['body'],
    'format' => 'text'
  );
  $node_wrapper->body->set($text);
  $node_wrapper->field_cp_price->set($form_state['values']['right_form_wrapper']['search']['price']);
  $node_wrapper->save();

  $node = node_load($node->nid);
  $node_wrapper = entity_metadata_wrapper('node', $node);

  $image_path = $node_wrapper->field_cp_image->value();
  $image_settings = array('style_name' => 'list_images', 'path' => $image_path['uri'], 'attributes' => array('class' => 'library_image'), 'getsize' => FALSE);
  $img =  theme('image_style', $image_settings);

  $body = $node_wrapper->body->raw();

  // $vote = field_view_field('node', $node, 'field_cp_rating');

  $settings = array(
    'content_type' => 'node',
    'content_id' => $node->nid,
    'entity' => $node,
    'stars' => 5,
    'field_name' => 'field_cp_rating',
    'autosubmit' => TRUE,
    'allow_clear' => FALSE,
    'langcode' => $node->language,
    'text' => 'none', // options are none, average, smart, user, and dual
    'tag' => 'vote',
    'style' => 'average', // options are average, dual, smart, user
    'widget' => array( 'name' => 'blue_stars', 'css' => drupal_get_path('module', 'fivestar') . '/widgets/blue_stars/blue_stars.css' )
  );

  $fivestar_values = fivestar_get_votes('node', $node->nid, 'vote', $user->uid);
  $form_fs = drupal_get_form('fivestar_custom_widget', $fivestar_values, $settings);
  $fivestar_form = render($form_fs);

  $content = '<div class="left_block">';
    $content .= '<div class="left_block_vote">' . $fivestar_form . '</div>';
    $content .= '<div class="left_block_img">' . $img . '</div>';
    $content .= '<div class="left_block_id">' . 'ID: ' . $node->nid . '</div>';
  $content .= '</div>';
  $content .= '<div class="right_block"><div>';
    $content .= '<div class="study_direction">' . t('[<span>Направление: ') . $node_wrapper->field_cp_study_direction->value()->name . '</span>]</div>';
    $content .= '<div class="ed_inst">' . t('[<span>Учебное учреждение: ') . $node_wrapper->field_cp_educational_institution->raw() . '</span>]</div>';
    $content .= '<div class="author">' . t('[<span>Автор: ') . $node_wrapper->field_cp_author->raw() . '</span>]</div>';
    $content .= '<div class="edit">' . l(t('Edit'), 'p_course/nojs/edit/' . $node->nid, array('html' => TRUE, 'attributes' => array('class' => array('use-ajax')))) . '</div>';
    $content .= '</div>';

    $content .= '<div class="wrapper">';
      $content .= '<div class="kurs_name">' . t('<span>Название: </span>') . $node_wrapper->label() . '</div>';
      $content .= '<div class="description">' . t('<span>Описание:</span></br> ') . $body['value'] . '</div>';
      $content .= '<div class="big_description">' . l(t('полное описание >>'), "courses-shop") . '</div>';
    $content .= '</div>';
  $content .= '</div>';

  $commands[] = ajax_command_html('#product_course_' . $node->nid, $content);

  return array('#type' => 'ajax', '#commands' => $commands);
}


