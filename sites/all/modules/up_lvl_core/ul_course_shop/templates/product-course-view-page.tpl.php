<div class="course_product_content">
  <?php foreach ($items as $key => $value): ?>
    <div id="product_course_<?php print $key; ?>" class="product_course">
      <div class="left_block">
        <div class="left_block_vote"><?php print render($value['vote']); ?></div>
        <div class="left_block_img"><?php print $value['image']; ?></div>
        <div class="left_block_id"><?php print 'ID: ' . $key; ?></div>
      </div>
      <div class="right_block">
        <div>
          <div class="study_direction">[<span><?php print t('Направление: ') . $value['study_direction']; ?></span>]</div>
          <div class="ed_inst">[<span><?php print t('Учебное учреждение: ') . $value['educational_institution']; ?></span>]</div>
          <div class="author">[<span><?php print t('Автор: ') . $value['author']; ?></span>]</div>
          <div class="edit"><?php print $value['edit']; ?></div>
        </div>
        <div class="wrapper">
          <div class="kurs_name"><?php print t('<span>Название:</span> ') . $value['title']; ?></div>
          <div class="description"><?php print t('<span>Описание: </span></br>') . $value['body']['value']; ?></div>
          <div class="big_description"><?php print l(t('полное описание >>'), "courses-shop"); ?></div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>

  <?php if($add): ?>
    <div class="add_button"><?php print $add; ?></div>
    <div class="add-p-course-wrapper"></div>
  <?php endif; ?>
</div>
