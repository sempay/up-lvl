<?php
/**
 * @file
 * add_dev.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function add_dev_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 3,
  );

  // Exported role: Отображать в слайдере (только для учителей).
  $roles['Отображать в слайдере (только для учителей)'] = array(
    'name' => 'Отображать в слайдере (только для учителей)',
    'weight' => 2,
  );

  // Exported role: администратор курса.
  $roles['администратор курса'] = array(
    'name' => 'администратор курса',
    'weight' => 4,
  );

  // Exported role: куратор курса.
  $roles['куратор курса'] = array(
    'name' => 'куратор курса',
    'weight' => 5,
  );

  // Exported role: ученик.
  $roles['ученик'] = array(
    'name' => 'ученик',
    'weight' => 6,
  );

  return $roles;
}
