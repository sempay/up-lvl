<?php
/**
 * @file
 * add_dev.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function add_dev_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|user|user|default';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Контактная информация',
    'weight' => '1',
    'children' => array(
      0 => 'field_phone_number',
      1 => 'field_skype',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Контактная информация',
      'instance_settings' => array(
        'classes' => ' group-contact field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_contact|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_more|user|user|default';
  $field_group->group_name = 'group_more';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Дополнительная информация',
    'weight' => '2',
    'children' => array(
      0 => 'field_about_me',
      1 => 'field_company',
      2 => 'field_position',
      3 => 'field_exp',
      4 => 'field_foreign_languages',
      5 => 'field_pay_information',
      6 => 'field_cel_reg',
      7 => 'field_cat_ob_progr',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Дополнительная информация',
      'instance_settings' => array(
        'classes' => ' group-more field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_more|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_private|user|user|default';
  $field_group->group_name = 'group_private';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Информация о пользователе',
    'weight' => '0',
    'children' => array(
      0 => 'field_fio',
      1 => 'field_country',
      2 => 'field_city',
      3 => 'field_old',
      4 => 'field_education',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Информация о пользователе',
      'instance_settings' => array(
        'classes' => ' group-private field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_private|user|user|default'] = $field_group;

  return $export;
}
