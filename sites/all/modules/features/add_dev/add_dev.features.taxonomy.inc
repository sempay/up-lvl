<?php
/**
 * @file
 * add_dev.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function add_dev_taxonomy_default_vocabularies() {
  return array(
    'discipline' => array(
      'name' => 'Дисциплина',
      'machine_name' => 'discipline',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'evaluation' => array(
      'name' => 'Оценка платформы: от 1 до 5',
      'machine_name' => 'evaluation',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'ticher' => array(
      'name' => 'Учитель',
      'machine_name' => 'ticher',
      'description' => 'Список учителей',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
