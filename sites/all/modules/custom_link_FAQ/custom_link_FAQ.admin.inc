  <?php

/**
 * @file
 * Admin page callback file for the main module.
 * Setting block 1 in the custom front page.
 */

function uplvl_training_video_form($form, &$form_state) {
  $form['video_on_profile'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about profile'),
    '#default_value' => variable_get('video_on_profile'),
  );

  $form['video_on_add_curator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about add curator'),
    '#default_value' => variable_get('video_on_add_curator'),
  );

  $form['video_on_message'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about private messages'),
    '#default_value' => variable_get('video_on_message'),
  );

  $form['video_on_add_pupils'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about add pupils'),
    '#default_value' => variable_get('video_on_add_pupils'),
  );

  $form['video_on_list_pupils'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about list pupils'),
    '#default_value' => variable_get('video_on_list_pupils'),
  );

  $form['video_on_statistic_pupils'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about statistic pupils'),
    '#default_value' => variable_get('video_on_statistic_pupils'),
  );

  $form['video_on_alert_system'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about alert system'),
    '#default_value' => variable_get('video_on_alert_system'),
  );

  $form['video_on_edit_course'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about edit course'),
    '#default_value' => variable_get('video_on_edit_course'),
  );

  $form['video_on_check_course'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about check course'),
    '#default_value' => variable_get('video_on_check_course'),
  );

  $form['video_on_add_lesson'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about add lesson'),
    '#default_value' => variable_get('video_on_add_lesson'),
  );

  $form['video_on_edit_lesson'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about edit lesson'),
    '#default_value' => variable_get('video_on_edit_lesson'),
  );

  $form['video_on_edit_task'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about edit task'),
    '#default_value' => variable_get('video_on_edit_task'),
  );

  $form['video_on_library'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Video about library'),
    '#default_value' => variable_get('video_on_library'),
  );

  return system_settings_form($form);
}
