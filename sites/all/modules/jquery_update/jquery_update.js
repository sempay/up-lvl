(function (D) {
  var beforeSerialize = D.ajax.prototype.beforeSerialize;
  D.ajax.prototype.beforeSerialize = function (element, options) {
    beforeSerialize.call(this, element, options);
    options.data['ajax_page_state[jQuery]'] = D.settings.ajaxPageState.jQuery;
    options.data['ajax_page_state[jQueryUI]'] = D.settings.ajaxPageState.jQueryUI;
  }
})(Drupal);