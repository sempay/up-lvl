<div id="teacher-reg-form">
  <div class="registration-description">
    <p>ВАЖНО!!!</p>
    <p>Спешим Вас уведомить о том, что регистрация учеников на какой-либо курс производить только преподаватель в его личном кабинете. Если Вы желаете пройти обучение по одному из направлений,
    Вам необходимо связаться с преподавателем по выбранному курсу с просьбой о регистрации!</p>
   <p> Отправить заявку на обучение:</p>
   <br>
  </div>
  <div id="reg-form-content">
    <div class="mail-phone">
      <?php
        print render($form['mail']);
        print render($form['phone']);
      ?>
    </div>
    <div calss="username">
      <?php
        print render($form['name']);
      ?>
    </div>
    <?php
        print drupal_render_children($form);
    ?>
  </div>
</div>