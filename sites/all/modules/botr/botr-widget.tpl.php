<?php /* $Id: botr-widget.tpl.php 13312 2012-02-21 12:21:39Z tom $ */ ?>
<div id='botr-video-box' class='fieldset-wrapper'>
  <div id='botr-list-wrapper'>
    <input type='text' class='form-text' value='<?php echo t('Search videos') ?>' id='botr-search-box' />
    <ul id='botr-video-list'></ul>
  </div>
  <span>
    <fieldset id='botr-upload-video'>
      <legend>
        <?php echo t('Upload a new video') ?>
      </legend>
      <div class='botr-row'>
        <label for='botr-upload-title'><?php echo t('Title') ?></label>
        <span><input type='text' class='form-text' id='botr-upload-title' name='botr-upload-title' /></span>
      </div>
      <div class='botr-row'>
        <label for='botr-upload-file'><?php echo t('File') ?></label>
        <span>
          <button id='botr-upload-browse'><?php echo t('Choose file') ?></button>
          <input type='text' class='form-text' id='botr-upload-file' name='botr-upload-file' value='<?php echo t('no file selected') ?>' disabled='disabled' />
          <input type='text' id='botr-progress-bar' value='0%' readonly='readonly' />
        </span>
      </div>
      <div class='botr-row'>
        <p id='botr-upload-message'></p>
        <button type='submit' id='botr-upload-button'><?php echo t('Upload video') ?></button>
      </div>
    </div>
  </span>
</div>
