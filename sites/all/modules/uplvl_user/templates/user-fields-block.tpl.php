<div class="fields-container">
  <div class="user-name"><?php print $user_fio; ?></div>
  <div class="user-main-info clearfix">
    <div class="years-city">
      <?php
        if ($years):
      ?>
        <div class="user-age">
          <?php
            print $years;
          ?>
        </div>
      <?php
        endif;
        if ($city):
      ?>
        <div class="user-city">
          <?php
            print $city;
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
    <div class="phone-skype">
      <?php
        if ($skype):
      ?>
        <div class="user-skype">
          <?php
            print t('Skype: !skype', array(
                '!skype' => '<span>' . $skype . '</span>',
              ));
          ?>
        </div>
      <?php
        endif;
        if ($phone):
      ?>
        <div class="user-phone">
          <?php
            print t('Телефон: !phone', array(
                '!phone' => '<span>' . $phone . '</span>',
              ));
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
  </div>
  <div class="user-info clearfix">
    <div class="left">
      <?php
        if ($company):
      ?>
        <div class="user-company">
          <?php
            print t('Компания: !company', array(
              '!company' => '<span>' . $company . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($company):
      ?>
        <div class="user-job-title">
          <?php
            print t('Должность: !job_title', array(
              '!job_title' => '<span>' . $job_title . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($company):
      ?>
        <div class="user-education">
          <?php
            print t('Образование: !education', array(
              '!education' => '<span>' . $education . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($company):
      ?>
        <div class="user-languages">
          <?php
            print t('Иностарнные языки: !languages', array(
              '!languages' => '<span>' . $languages . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
    <div class="right">
      <?php
        if ($vk):
      ?>
        <div class="user-vk">
          <?php
            print t('Vkontakte: !vk', array(
              '!vk' => '<span>' . $vk . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($ok):
      ?>
        <div class="user-ok">
          <?php
            print t('Odnoklassniki: !ok', array(
              '!ok' => '<span>' . $ok . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($fb):
      ?>
        <div class="user-facebook">
          <?php
            print t('Facebook: !fb', array(
              '!fb' => '<span>' . $fb . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>

      <?php
        if ($twitter):
      ?>
        <div class="user-twitter">
          <?php
            print t('Twitter: !twitter', array(
              '!twitter' => '<span>' . $twitter . '</span>',
            ));
          ?>
        </div>
      <?php
        endif;
      ?>
    </div>
  </div>
    <?php
      if ($experience):
    ?>
  <div class="bottom">

      <div class="user-experience">
        <?php
          print t('Опыт работы: !experience', array(
            '!experience' => '<span>' . $experience . '</span>',
          ));
        ?>
      </div>
    <?php
      endif;
    ?>

    <?php
      if ($about):
    ?>
      <div class="user-about">
        <?php
          print t('О себе: !about', array(
            '!about' => '<span>' . $about . '</span>',
          ));
        ?>
      </div>
  </div>
    <?php
      endif;
    ?>
</div>