<div class="mini-profile">
  <?php
    if (!empty($picture->uri)) {
      print '<div class="picture">';
      print theme('image_style', array(
        'path'       => $picture->uri,
        'attributes' => array(
          'class' => 'img-responsive',
        ),
        'style_name' => 'mini_prof_cs40x40',
      ));
      print '</div>';
    }
      print $dropdown;
  ?>
</div>