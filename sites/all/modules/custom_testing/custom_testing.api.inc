<?php
  function alphavit_array() {
    return explode(' ', 'a b c d e f g h i j k l m n o p q r s t u v w x y z');
  }
  function ajax_add_question_for_test($form, &$form_state){
    $form_state['values']['testings']['operation']='add_question';
    $form_state['input']['testings']['operation']='add_question';
    $form['testings']['operation']['#value']='add_question';
    return $form;
  }
  function ajax_add_question($form, &$form_state){
    $form_state['values']['testings']['operation']='add_variant';
    $form_state['input']['testings']['operation']='add_variant';
    $form['testings']['operation']['#value']='add_variant';
    return $form;
  }
  function task_testing_form($form, &$form_state){
    $alph = alphavit_array();
    if(isset($form_state['values']['testings']['question_num'])&&($form_state['clicked_button']['#value']=='Добавить вопрос')) $question_num = $form_state['values']['testings']['question_num'] + 1;
    else  $question_num = 3;
    $form['testings']['#prefix'] = '<div id="testing_form">';
    $form['testings']['#suffix'] = '</div>';
    $form['testings']['#tree'] = TRUE;
    $form['testings']['question_num'] = array(
      '#type'=>'hidden',
      '#default_value' => $question_num,
    );
    $form['testings']['operation']=array(
      '#type' => 'hidden',
      '#default_value'=> '',
    );
    for($j=1; $j<$question_num; $j++){
      $form['testings']['quest'.$j]= array(
        '#type' => 'fieldset',
        '#title' => $j.' Вопрос',
      );
      $form['testings']['quest'.$j]['#prefix'] = '<div id=quest'.$j.'>';
      $form['testings']['quest'.$j]['#suffix'] = '</div>';
      if((isset($form_state['values']['testings']['quest'.$j]['question_ansv']))&&($j==$form_state['clicked_button']['#value']))
      {$question_answer = $form_state['values']['testings']['quest'.$j]['question_ansv'] + 1; }
      elseif(isset($form_state['values']['testings']['quest'.$j]['question_ansv']))
      { $question_answer = $form_state['values']['testings']['quest'.$j]['question_ansv'];       }
      else{ $question_answer=3; }
      $form['testings']['quest'.$j]['question_ansv'] = array(
        '#type' => 'hidden',
        '#value' => $question_answer,
      );
      $form['testings']['quest'.$j]['number'.$j] = array(
        '#type' => 'textfield',
        '#title' => 'Номер вопроса Под каким номером будет воводится на экран',
        '#size' => 60,
      );
      $form['testings']['quest'.$j]['title'] = array(
        '#type' => 'textfield',
        '#title' => 'Впишите вопрос для студента',
        '#size' => 60,
      );
      for($i=1; $i<$question_answer; $i++){
        $str='question_'.$i;
        $form['testings']['quest'.$j]['question'][$str]['question'] = array(
          '#type' => 'textfield',
          '#title' => $alph[$i-1].'. ',
          '#size' => 60,
          '#maxlength' => 256,
        );
        $form['testings']['quest'.$j]['question'][$str]['add_q'] = array(
          '#type' => 'button',
          '#value' => $j,
          '#ajax' => array(
            'callback' => 'ajax_add_question',
            'wrapper' => 'testing_form',
            //'wrapper' => 'quest'.$j,
            'method' => 'replace',
            'effect' => 'fade',
          ),
        );
        $form['testings']['quest'.$j]['question'][$str]['is_right'] = array(
          '#type' => 'checkbox',
        );
      }
    }
    $form['testings']['question_num'] = array(
      '#type' => 'hidden',
      '#value' => $question_num,
    );
    $form['testings']['add_question'] = array(
      '#type' => 'button',
      '#value' => 'Добавить вопрос',
      '#ajax' => array(
        'callback' => 'ajax_add_question_for_test',
        'wrapper' => 'testing_form',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
    $form['testings']['save'] = array(
      '#type' => 'submit',
      '#value' => 'Сохранить тест',
    );
    return $form;
  }

function custom_test_create(){
  return drupal_get_form('task_testing_form');
}
