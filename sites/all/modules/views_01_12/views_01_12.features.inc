<?php
/**
 * @file
 * views_01_12.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_01_12_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
