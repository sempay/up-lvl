<?php
/**
 * @file
 * views_01_12.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_01_12_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'shops';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'shops';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Магазин курсов';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access user profiles';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Пользователь: Content authored */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'users';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Поле: Содержимое: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'uid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Поле: Содержимое: Рейтинг */
  $handler->display->display_options['fields']['field_rating']['id'] = 'field_rating';
  $handler->display->display_options['fields']['field_rating']['table'] = 'field_data_field_rating';
  $handler->display->display_options['fields']['field_rating']['field'] = 'field_rating';
  $handler->display->display_options['fields']['field_rating']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_rating']['label'] = '';
  $handler->display->display_options['fields']['field_rating']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_rating']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_rating']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'default',
    ),
    'expose' => 0,
    'style' => 'average',
    'text' => 'none',
  );
  /* Поле: Содержимое: Направление */
  $handler->display->display_options['fields']['field_vector']['id'] = 'field_vector';
  $handler->display->display_options['fields']['field_vector']['table'] = 'field_data_field_vector';
  $handler->display->display_options['fields']['field_vector']['field'] = 'field_vector';
  $handler->display->display_options['fields']['field_vector']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_vector']['type'] = 'taxonomy_term_reference_plain';
  /* Поле: Содержимое: Специализация */
  $handler->display->display_options['fields']['field_special']['id'] = 'field_special';
  $handler->display->display_options['fields']['field_special']['table'] = 'field_data_field_special';
  $handler->display->display_options['fields']['field_special']['field'] = 'field_special';
  $handler->display->display_options['fields']['field_special']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_special']['type'] = 'taxonomy_term_reference_plain';
  /* Поле: Пользователь: Название */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Автор';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  /* Поле: Содержимое: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['relationship'] = 'uid';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  /* Поле: Содержимое: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_image']['alter']['text'] = '[field_image] 
<div class=\'id-course\'>ID: [nid] </div>';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => '115x115_round',
    'image_link' => 'content',
  );
  /* Поле: Содержимое: Заголовок */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'uid';
  $handler->display->display_options['fields']['title']['label'] = 'Название';
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Поле: Содержимое: Краткое описание */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'field_data_field_description';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_description']['label'] = 'Описание';
  /* Поле: Содержимое: Ссылка */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['relationship'] = 'uid';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'полное описание »»»';
  /* Поле: Содержимое: Доступность */
  $handler->display->display_options['fields']['field_accessed']['id'] = 'field_accessed';
  $handler->display->display_options['fields']['field_accessed']['table'] = 'field_data_field_accessed';
  $handler->display->display_options['fields']['field_accessed']['field'] = 'field_accessed';
  $handler->display->display_options['fields']['field_accessed']['relationship'] = 'uid';
  $handler->display->display_options['fields']['field_accessed']['label'] = '';
  $handler->display->display_options['fields']['field_accessed']['element_label_colon'] = FALSE;
  /* Sort criterion: Пользователь: Created date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'users';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Пользователь: Название */
  $handler->display->display_options['arguments']['name']['id'] = 'name';
  $handler->display->display_options['arguments']['name']['table'] = 'users';
  $handler->display->display_options['arguments']['name']['field'] = 'name';
  $handler->display->display_options['arguments']['name']['default_action'] = 'default';
  $handler->display->display_options['arguments']['name']['exception']['title'] = 'Все';
  $handler->display->display_options['arguments']['name']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['name']['default_argument_options']['index'] = '1';
  $handler->display->display_options['arguments']['name']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['name']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['name']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['name']['limit'] = '0';
  /* Filter criterion: Пользователь: Активен */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Содержимое: Тип */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'uid';
  $handler->display->display_options['filters']['type']['value'] = array(
    'course' => 'course',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Содержимое: Направление (field_vector) */
  $handler->display->display_options['filters']['field_vector_tid']['id'] = 'field_vector_tid';
  $handler->display->display_options['filters']['field_vector_tid']['table'] = 'field_data_field_vector';
  $handler->display->display_options['filters']['field_vector_tid']['field'] = 'field_vector_tid';
  $handler->display->display_options['filters']['field_vector_tid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['field_vector_tid']['value'] = '';
  $handler->display->display_options['filters']['field_vector_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_vector_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_vector_tid']['expose']['operator_id'] = 'field_vector_tid_op';
  $handler->display->display_options['filters']['field_vector_tid']['expose']['label'] = 'Направление:';
  $handler->display->display_options['filters']['field_vector_tid']['expose']['operator'] = 'field_vector_tid_op';
  $handler->display->display_options['filters']['field_vector_tid']['expose']['identifier'] = 'field_vector_tid';
  $handler->display->display_options['filters']['field_vector_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_vector_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_vector_tid']['vocabulary'] = 'vector';
  /* Filter criterion: Пользователь: Authentication name */
  $handler->display->display_options['filters']['authname']['id'] = 'authname';
  $handler->display->display_options['filters']['authname']['table'] = 'authmap';
  $handler->display->display_options['filters']['authname']['field'] = 'authname';
  $handler->display->display_options['filters']['authname']['group'] = 1;
  $handler->display->display_options['filters']['authname']['exposed'] = TRUE;
  $handler->display->display_options['filters']['authname']['expose']['operator_id'] = 'authname_op';
  $handler->display->display_options['filters']['authname']['expose']['label'] = 'по преподавателю';
  $handler->display->display_options['filters']['authname']['expose']['operator'] = 'authname_op';
  $handler->display->display_options['filters']['authname']['expose']['identifier'] = 'authname';
  $handler->display->display_options['filters']['authname']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Содержимое: Специализация (field_special) */
  $handler->display->display_options['filters']['field_special_tid']['id'] = 'field_special_tid';
  $handler->display->display_options['filters']['field_special_tid']['table'] = 'field_data_field_special';
  $handler->display->display_options['filters']['field_special_tid']['field'] = 'field_special_tid';
  $handler->display->display_options['filters']['field_special_tid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['field_special_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_special_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_special_tid']['expose']['operator_id'] = 'field_special_tid_op';
  $handler->display->display_options['filters']['field_special_tid']['expose']['label'] = 'Специализация:';
  $handler->display->display_options['filters']['field_special_tid']['expose']['operator'] = 'field_special_tid_op';
  $handler->display->display_options['filters']['field_special_tid']['expose']['identifier'] = 'field_special_tid';
  $handler->display->display_options['filters']['field_special_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_special_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_special_tid']['vocabulary'] = 'specialization';
  /* Filter criterion: Содержимое: Доступность (field_accessed) */
  $handler->display->display_options['filters']['field_accessed_value']['id'] = 'field_accessed_value';
  $handler->display->display_options['filters']['field_accessed_value']['table'] = 'field_data_field_accessed';
  $handler->display->display_options['filters']['field_accessed_value']['field'] = 'field_accessed_value';
  $handler->display->display_options['filters']['field_accessed_value']['relationship'] = 'uid';
  $handler->display->display_options['filters']['field_accessed_value']['group'] = 1;
  $handler->display->display_options['filters']['field_accessed_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_accessed_value']['expose']['operator_id'] = 'field_accessed_value_op';
  $handler->display->display_options['filters']['field_accessed_value']['expose']['label'] = 'платно/бесплатно:';
  $handler->display->display_options['filters']['field_accessed_value']['expose']['operator'] = 'field_accessed_value_op';
  $handler->display->display_options['filters']['field_accessed_value']['expose']['identifier'] = 'field_accessed_value';
  $handler->display->display_options['filters']['field_accessed_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['path'] = 'shops/%';
  $translatables['shops'] = array(
    t('Master'),
    t('Магазин курсов'),
    t('more'),
    t('Apply'),
    t('Сбросить'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('nodes'),
    t('Nid'),
    t('Направление'),
    t('Специализация'),
    t('Автор'),
    t('[field_image] 
<div class=\'id-course\'>ID: [nid] </div>'),
    t('Название'),
    t('Описание'),
    t('полное описание »»»'),
    t('Все'),
    t('Направление:'),
    t('по преподавателю'),
    t('Специализация:'),
    t('платно/бесплатно:'),
    t('Page'),
  );
  $export['shops'] = $view;

  return $export;
}
