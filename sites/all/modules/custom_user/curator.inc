<?php




  function activity_task_count($lesson_id){
    return db_select('custom_user_pupil_submission')
      ->condition('nid',$lesson_id,'=')
      ->condition('status', 0, '=')
      ->countQuery()
      ->execute()
      ->fetchField();
  }

function curator_add_comment_form(){
  $form=array();
  return $form;
}

 function get_active_users_task($nid){
    return db_select('custom_user_pupil_submission', 'd')
      ->fields('d')
      ->condition('nid', $nid,'=')
      ->condition('status', 0, '=')
      ->execute()
      ->fetchAllAssoc('cups');
  }
function get_user_name_by_id($uid){
   $account = entity_metadata_wrapper('user', user_load($uid));
   if($account->field_fio->value()){
     return  '<td><a href="/user/'.$uid.'">'.$account->field_fio->value().'</a></td><td>'.$account->mail->value().'</td>';
   }
  else return  '<td><a href="/user/'.$uid.'">'.$account->mail->value().'</a></td><td>'.$account->mail->value().'</td>';
}

function custom_user_accepr_all_task_form_submit($form, &$form_state){
  $lid=arg(1);
  $list_task = get_active_users_task($lid);
  foreach($list_task as $item){
    $account=user_load($item->uid);
    if($account){
       $sid= get_sid_by_user($account->uid,$lid);
       curator_accept_pupil_task($sid);
    }
    else deleting_user($item->uid);
  }
}
function get_uid_by_sid($sid){
  return db_select('webform_submissions', 'd')
    ->fields('d')
    ->condition('sid',$sid, '=')
    ->execute()
    ->fetchCol(2);
}
function custom_user_accepr_all_task_form($form, &$form_submit){
  $form['lid'] = array(
    '#type'=>'hidden',
    '#value'=>arg(1),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Одобрить все текущие задания',
  );
  return $form;
}

function pupils_home_task(){
  $lesson_id=arg(1);
  $node=entity_metadata_wrapper('node', node_load($lesson_id));
  $lesson_name = custom_user_get_title($lesson_id);
  $form=drupal_get_form('custom_user_accepr_all_task_form');
  $title = drupal_render($form).'<div class="titl_les"><span class=number_lesson>Урок №'.$node->field_weight->value().'</span><span class=h4>'.$lesson_name['title'].'</span></div>';
  $main='<table>';
  $list_task = get_active_users_task($lesson_id);
  foreach($list_task as $item){
  	if(user_load($item->uid)) $main.='<tr>'.get_user_name_by_id($item->uid).'<td><a href="/verification/'.$item->sid.'/'.$lesson_id.'">Проверить</a></td></tr>';
  	else deleting_user($item->uid);
  }
  $main.='</table>';
  $output = $title.$main;
  return $output;
}
function load_webform_component($lesson_id){
  return db_select('webform_component', 'd')
    ->fields('d')
    ->condition('nid',$lesson_id, '=')
    ->orderBy('weight','ASC')
    ->execute()
    ->fetchAllAssoc('weight');
}
function ajax_add_curator_comment($form, &$form_state){
  return $form;
}
function curator_accept_pupil_task($sid){
  db_update('custom_user_pupil_submission')
    ->fields(array('status'=>1,))
    ->condition('sid', $sid, '=')
    ->execute();
  $curent_task = db_select('custom_user_pupil_submission','d')
    ->fields('d')
    ->condition('sid', $sid, '=')
    ->execute()
    ->fetchAssoc();
 $course = get_course_by_lesson($curent_task['nid']);
 $lesson_acc=access_to_lesson($course['cid'], $curent_task['uid']);
 $uid=get_uid_by_sid($sid);
 $title=custom_user_get_title($course['cid']);
 $account=user_load($uid[0]);
 if(isset($lesson_acc['lid'])){
   $num_les=$lesson_acc['lid']+1;
 }
 else
   $num_les = 2;
 db_merge('custom_user_user')
    ->key(array('uid' => $curent_task['uid'], 'cid' => $course['cid']))
    ->fields(array(
    'uid' => $curent_task['uid'],
    'cid' => $course['cid'],
    'lid' => $num_les,
  ))
    ->execute();
  drupal_mail('system', 'accept_task', $account->mail, language_default(), array(
    'context' => array(
      'subject' => 'Прохождение курсов на сайте by-step.ru',
      'message' => '<div style="font-weight: bold;">Вы успешно прошли урок и можете приступать к следующему.</div> <br><br> Куратор курса '.$title['title'],
    )
  ));
}
function curator_decline_pupil_task($sid){
  db_update('custom_user_pupil_submission')
    ->fields(array('status'=>3,))
    ->condition('sid', $sid, '=')
    ->execute();
}

function custom_user_add_comment($sid, $nid, $key, $message){
  db_merge('custom_user_comment')
    ->key(array('sid' => $sid, 'key_subm' => $key))
    ->fields(array(
    'sid' => $sid,
    'nid' => $nid,
    'key_subm' => $key,
    'message' => $message,
  ))->execute();
}

function curator_user_save_comment($array){
  foreach($array['items'] as $key=>$item){
    custom_user_add_comment($array['sid'], $array['nid'], $key, $item);
  }
}

function custom_user_verification_form_submit($form, &$form_state){
  $uid=get_uid_by_sid($form_state['values']['submittion']);
  $course=get_course_by_lesson($form_state['input']['node']);
  $title=custom_user_get_title($course['cid']);
  $account=user_load($uid[0]);
  if($form_state['clicked_button']['#value']=='accept'){
     curator_accept_pupil_task($form_state['values']['submittion']);
  }
  else{
    $array_to_comment_record = array();
    $array_to_comment_record['sid']=$form_state['input']['submittion'];
    $array_to_comment_record['nid']=$form_state['input']['node'];
    foreach($form['fields'] as $key=>$item){
      if(isset($item['#type']))
       if(in_array($item['#type'], array('fieldset'))) {
           $array_to_comment_record['items'][$key]=$form_state['input']['curator_comment_'.$key];
       }
    }
    curator_user_save_comment($array_to_comment_record);
    curator_decline_pupil_task($form_state['values']['submittion']);
    drupal_mail('system', 'decline_task', $account->mail, language_default(), array(
      'context' => array(
        'subject' => 'Прохождение курсов на сайте by-step.ru',
        'message' => '<div style="font-weight: bold;">Ваше задание проверено, необходимо внести корректировки.</div> <br><br> Куратор курса '.$title['title'],
      )
    ));
  }
  drupal_goto('/curator/courses');
}
function custom_user_verification_form($form, &$form_state){
  $submission_id=arg(1);
  $lesson_id=arg(2);
  $submission_data=webform_menu_submission_load($submission_id, $lesson_id);
  $form_component=node_load($lesson_id);
  $form['node']=array(
    '#type'=>'hidden',
    '#value'=>arg(2),
  );
  $form['submittion']=array(
    '#type'=>'hidden',
    '#value'=>arg(1),
  );
  if(isset($submission_data->data))
  foreach($submission_data->data as $key=>$value){
    $coment=get_user_comment_by_index($submission_data->sid,$key);
    if($coment){
      $def_val=$coment['message'];
    }
    else $def_val='';
    if(isset($form_component->webform['components'][$key]['extra']['description']['value'])){
      $descr=$form_component->webform['components'][$key]['extra']['description']['value'];
    }
    else $descr=$form_component->webform['components'][$key]['extra']['description'];
    $markup='<div class="tasking-item"><div class="task"><h6>'.$form_component->webform['components'][$key]['name'].'</h6>
      <div class="title">Вопрос</div>'.$descr.'</div>
      <div class="ansver"><div class="title">Ответ ученика</div>'.$value['value'][0].'</div></div>';
    $form['fields'][$key]['#type'] = 'fieldset';
    $form['fields'][$key]['index_task_'.$key]= array(
      '#type'=>'hidden',
      '#value' => $key,
      '#name'=>'index_task',
    );
    $form['fields'][$key]['pretitle_'.$key] = array(
      '#type' => 'markup',
      '#markup'=> $markup
    );
    $form['fields'][$key]['curator_comment_'.$key]= array(
      '#title'=>'Комментарий к "'.$form_component->webform['components'][$key]['name'].'"',
      '#type'=>'textarea',
      '#default_value'=>$def_val,
    );
  }
  $form['accept']=array(
    '#type'=>'submit',
    '#value'=>'accept',
  );
  $form['decline']=array(
    '#type'=>'submit',
    '#value'=>'decline',
  );
  return $form;
}

function custom_user_verification_task(){
  $form=drupal_get_form('custom_user_verification_form');
  return drupal_render($form);
}

function custom_user_curator_courses(){
  global $user;
  $courses = custom_user_get_curuser_courses($user->uid, '5');
  if(isset($courses)){
    $j=0;
    $output='<ul class="courses">';
    foreach($courses as $key=>$course){
      $course_lesson = custom_user_course_lessons($course->nid);
      $name=custom_user_get_title($course->nid);
      $lesson_cours = '<ul class="lessons">';
      $index_l=0;
      $course_lesson_count = 0;
      foreach($course_lesson as  $lesson){
        $index_l++;
        $lesson_active = activity_task_count($lesson->lid);
        $course_lesson_count += $lesson_active;
        if($lesson_active>0)$link_to_course = '<span class="go_to"><a href="/pupils_home_task/'.$lesson->lid.'">Проверить задание'.$lesson_active.'</a></span>';
        else $link_to_course='';
        $lessonName=custom_user_get_title($lesson->lid);
        $lesson_cours.= '<li> Урок №'.$lesson->weight.' '.$lessonName["title"].' '.$link_to_course.'</li>';
      }
      $j++;
      $lesson_cours .= '</ul>';
      $output.='<li><span class="num">'.$j.'</span><span class="course_title">'.$name['title'].'</span>
      '.$lesson_cours.'</li>';
    }
    $output.='</ul>';
  }
  else $output = 'Отсутствуют курсы для обучения';
  return $output;
}
function cu_all_answ($user, $course){
    $j=0;
    $output='<ul class="courses">';
    $course_node = node_load($course);
    $cur_course_lesson = curent_lesson_of_course($course, $user->uid);
    if(!isset($cur_course_lesson['lid'])){$lesson_index = 1; }
    else{$lesson_index = $cur_course_lesson['lid'];}
    $course_lesson = custom_user_course_lessons($course);
    $name=custom_user_get_title($course);
    $lesson_cours = '<ul class="lessons">';
    $index_l=0;
      foreach($course_lesson as  $lesson){
        $index_l++;
        if($index_l==$lesson_index)
        {
          $prev_sumbission = user_send_this($user->uid, $lesson->lid);
          if(isset($prev_sumbission['sid'])){
            if($prev_sumbission['status']==0)  $link_to_course = '<span class="check_for_teacer stat_lesson">задание на проверке</span>';
            if($prev_sumbission['status']==3)  $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_task/'.$user->uid.'/'.$lesson->lid.'">Доработать</a></span>';
          }
          else
            $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_lessons/'.$lesson->lid.'"> Пройти урок</a></span>';
        }
        elseif($index_l<$lesson_index)
        {
            $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_task/'.$user->uid.'/'.$lesson->lid.'/'.$course.'"> Просмотреть урок</a></span>';
        }
        else   $link_to_course='';
        $lessonName=custom_user_get_title($lesson->lid);
        if($index_l<=$lesson_index) $lesson_cours.= '<li> Урок №'.$lesson->weight.' '.$lessonName["title"].' '.$link_to_course.'</li>';
      }
      $j++;
      $lesson_cours .= '</ul>';
      $output.='<li><span class="num">'.$j.'</span><span class="course_title">'.$name['title'].'</span> <span class="look_course"><a href="/full_course_view/'.$course.'">Просмотреть курс</a></span>
      '.$lesson_cours.'</li>';
    $output.='</ul>';
  return $output;
}

function cu_lesson_task(){
  $lesson_id=arg(2);
  $node=node_load($lesson_id);
  $node=node_view($node, 'full');
  return drupal_render($node);
}
function custom_user_ps_by_course(){
  $lesson = node_load(arg(3));
  $pupil= user_load(arg(2));
  if(isset($pupil->field_fio['und'][0]['value'])){
    $name=$pupil->field_fio['und'][0]['value'];
  }
  else {
    $name=$pupil->init;
  }
  drupal_set_title('Статистика курса - '.$name);
  $output = '<h6>'.$lesson->title.'</h6>';
  $next =cu_all_answ($pupil, arg(3));
  $output.=$next;
  return $output;
}
function cu_can_see_cur($cid){
  global $user;
  return db_select('custom_user_nodes', 'd')
    ->fields('d')
    ->condition('uid',$user->uid,'=')
    ->condition('nid',$cid,'=')
    ->condition('roles','4','=')
    ->execute()
    ->fetchAssoc();
}
function curators_curs_list(){
  global $user;
  $courses = custom_user_get_curuser_courses(arg(1), '5');
  if(isset($courses)){
    $j=0;
    $output='<ul class="courses">';
    foreach($courses as $key=>$course){
      $course_lesson = custom_user_course_lessons($course->nid);
      $name=custom_user_get_title($course->nid);
      $course_count=0;
      if(cu_can_see_cur($course->nid)){
        $j++;
        $output.='<li><span class="num">'.$j.'</span><span class="course_title" style="font-size: 18px;"><a href="/curator/pupil_list/'.arg(1).'/'.$course->nid.'">'.$name['title'].'</a></span>
   </li>';
      }
     }
    $output.='</ul>';
  }
  else $output = 'У вас нет администрируемых вами курсов';
  return $output;
}

function lesson_cc_enable(){
  return  cu_can_see_cur(arg(3));
}

function cc_pupils(){
  $cid=arg(2);
  $course = arg(3);
  return db_select('cu_cp','d')
    ->fields('d')
    ->condition('cid',$cid,'=')
    ->condition('course',$course,'=')
    ->execute()
    ->fetchAllAssoc('id');

}
function if_is_curator($user, $course){
  return db_select('cu_cp', 'd')
    ->fields('d')
    ->condition('pid',$user,'=')
    ->condition('course',$course,'=')
    ->execute()
    ->fetchAssoc();
}
function get_c_p_without_cur(){
  $result = db_select('custom_user_nodes', 'd')
    ->fields('d')
    ->condition('nid',arg(3),'=')
    ->condition('roles',6,'=')
    ->execute()
    ->fetchAllAssoc('uid');
  $option = array();
  foreach($result as $pupil){
    $account = user_load($pupil->uid);
    $is_cur=if_is_curator($pupil->uid, $pupil->nid);
    if((!$is_cur)&&($account))
      $option[$pupil->uid] = $account->name;
  }
  return $option;
}
function add_to_cur_p_form_submit($form, $form_state){
  db_insert('cu_cp')
    ->fields(array(
      'pid' => $form_state['values']['pupil'],
      'cid' => 1*arg(2),
      'course' => 1*arg(3),
    ))
    ->execute();
}
function add_to_cur_p_form($form, $form_state){
  $option=get_c_p_without_cur();
  $form['pupil'] = array(
    '#type' => 'select',
    '#title' => 'Выберите ученика',
    '#options' => $option,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Добавить',
  );
  return $form;
}

function delete_cur_p_form_submit($form, $form_state){
  db_delete('cu_cp')
    ->condition('pid', $form_state['values']['pid'])
    ->condition('course', $form_state['values']['course'])
    ->execute();
}
function delete_cur_p_form($form, $form_state){
  $form['course'] = array(
    '#type' => 'hidden',
  );
  $form['pid'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Удалить',
  );
  return $form;
}

function cu_ps_by_course(){
  $out='<ul>';
  $pupil = cc_pupils();
  $curator = user_load(arg(2));
  drupal_set_title('Список учеников у куратора - '.$curator->name);
  $i=1;
  foreach($pupil as $item){
    $i++;
    $form=drupal_get_form('delete_cur_p_form');
    $form['pid']['#value'] = $item->pid;
    $form['course']['#value'] = arg(3);
    $accaunt = user_load($item->pid);
    $out.='<li class="li-'.($i/2).'">'.$accaunt->name.''.drupal_render($form).'</li>';
    if($i>1) $i=0;
  }
  $out.='</ul>';
  $form=drupal_get_form('add_to_cur_p_form');
  $out.=drupal_render($form);
  return $out;
}
function cu_curators_pupil(){
  $out= curators_curs_list();
  return $out;
}

