<?php

   function access_to_lesson($course, $uid){
     return db_select('custom_user_user','d')
       ->fields('d')
       ->condition('cid',$course, '=')
       ->condition('uid',$uid, '=')
       ->execute()
       ->fetchAssoc();
   }
   function pupil_lesson_enable(){
     global $user;
     $lesson_id=arg(1);
     $course = get_course_by_lesson($lesson_id);
     $enable=false;
     if(isset($course))
     {
       $course_node=node_load($course['cid']);
       $enable= access_to_course($course['cid'], $user->uid);
       if(isset($enable['roles'])){
         $enable_lesson = access_to_lesson($course['cid'], $user->uid);
         $lesson=entity_metadata_wrapper('node', node_load($course['lid']));
         if($enable_lesson['lid']==$lesson->field_weight->value()){
           $enable=true;
         }
         elseif(!isset($enable_lesson['lid'])&&($lesson->field_weight->value()<2)){
           $enable=true;
         }
         elseif(isset($course_node->field_if_lesson_open['und'][0])&&($course_node->field_if_lesson_open['und'][0]['value']==1)){
           $enable=true;
         }
         elseif(($enable['roles']==4)||($enable['roles']==5)){
           $enable=true;
         }
         else
         {
           $enable=false;
           drupal_set_message('Нет доступа к данному уроку', 'error', false);
         }
       }
     }
     else
     {
       $enable=false;
       drupal_set_message('Не найдено курса связанного с уроком', 'error', TRUE);
     }
     return $enable;
   }
   function curent_lesson_of_course($course, $uid){
     return db_select('custom_user_user', 'd')
       ->fields('d')
       ->condition('uid',$uid,'=')
       ->condition('cid', $course,'=')
       ->execute()
       ->fetchAssoc('ccu');
   }
   function custom_user_pupil_courses(){
     global $user;
     $courses = custom_user_get_curuser_courses($user->uid, '6');
     if(isset($courses)){
       $j=0;
       $output='<ul class="courses">';
       foreach($courses as $key=>$course){
         $course_node = node_load($course->nid);
         $cur_course_lesson = curent_lesson_of_course($course->nid, $user->uid);
         if(!isset($cur_course_lesson['lid'])){$lesson_index = 1; }
          else{$lesson_index = $cur_course_lesson['lid'];}
         $course_lesson = custom_user_course_lessons($course->nid);
         $name=custom_user_get_title($course->nid);
         $lesson_cours = '<ul class="lessons">';
         $index_l=0;
         foreach($course_lesson as  $lesson){
           $index_l++;
           if($index_l==$lesson_index)
           {
             $current_lesson = node_load($lesson->lid);
             $prev_sumbission = user_send_this($user->uid, $lesson->lid);
             if(isset($current_lesson->field_finish_page['und'][0])&&($current_lesson->field_finish_page['und'][0]['value']==1))
             {
               $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_lessons/'.$lesson->lid.'">Итоги курса</a></span>';
             }
             elseif(isset($prev_sumbission['sid'])){
               if($prev_sumbission['status']==0)  $link_to_course = '<span class="check_for_teacer stat_lesson">задание на проверке</span>';
               if($prev_sumbission['status']==3)  $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_task/'.$lesson->lid.'">Доработать</a></span>';
             }
             else
               $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_lessons/'.$lesson->lid.'"> Пройти урок</a></span>';
           }
           elseif($index_l<$lesson_index)
           {
             if(isset($course_node->field_if_lesson_open['und'][0])&&($course_node->field_if_lesson_open['und'][0]['value']==1))
               $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_lessons/'.$lesson->lid.'"> Просмотреть урок</a></span>';
             else
               $link_to_course = '<span class="complate stat_lesson">задание выполнено</span>';
           }
           else   $link_to_course='';
           $lessonName=custom_user_get_title($lesson->lid);
           if($index_l<=$lesson_index) $lesson_cours.= '<li> Урок №'.$lesson->weight.' '.$lessonName["title"].' '.$link_to_course.'</li>';
         }
         $j++;
         $lesson_cours .= '</ul>';
         $en_course='no_finished';
         if(isset($current_lesson->field_finish_page['und'][0])&&($current_lesson->field_finish_page['und'][0]['value']==1))
           $en_course='finished';
         else $en_course='no_finished';
         $output.='<li class="'.$en_course.'"><span class="num">'.$j.'</span><span class="course_title">'.$name['title'].'</span> <span class="look_course"><a href="/full_course_view/'.$course->nid.'">Просмотреть курс</a></span>
      '.$lesson_cours.'</li>';
       }
       $output.='</ul>';
     }
     else $output = 'Отсутствуют курсы для обучения';
     return $output;
   }


function custom_user_pupil_courses_block(){
  global $user;
  $courses = custom_user_get_curuser_courses(arg(1), '6');
  if(isset($courses)){
    $j=0;
    $output='<a href="#" class="close" >закрыть х</a><ul class="courses">';
    foreach($courses as $key=>$course){
      $cur_course_lesson = curent_lesson_of_course($course->nid, arg(1));
      if(!isset($cur_course_lesson['lid'])){$lesson_index = 1; }
      else{$lesson_index = $cur_course_lesson['lid'];}
      $course_lesson = custom_user_course_lessons($course->nid);
      $name=custom_user_get_title($course->nid);
      $lesson_cours = '<ul class="lessons">';
      $index_l=0;
      foreach($course_lesson as  $lesson){
        $index_l++;
        $lessonName=custom_user_get_title($lesson->lid);
        if($index_l<=$lesson_index) $lesson_cours.= '<li> Урок №'.$lesson->weight.' '.$lessonName["title"].'</li>';
      }
      $j++;
      $lesson_cours .= '</ul>';
      $output.='<li><span class="num">'.$j.'</span><span class="course_title">'.$name['title'].'</span>
      '.$lesson_cours.'</li>';
    }
    $output.='</ul>';
  }
  else $output = 'Отсутствуют курсы для обучения';
  return $output;
}


function custom_user_course_lesson_name(){
  $lesson_id=arg(1);
  $course = get_course_by_lesson($lesson_id);
  $lesson_name = custom_user_get_title($lesson_id);
  $course_name = custom_user_get_title($course['cid']);
  $output='<div class="lesson_num">'.$course['weight'].'</div><h3>Курс: <span>'. $course_name['title'].'</span></h3><h3>Урок№ '.$course['weight'].': <span>'. $lesson_name['title'].'</span></h3>';
  return $output;
}
function custom_user_lesson_video(){
  $lesson_id=arg(1);
  $enabled=pupil_lesson_enable();
  if($enabled){
    $node=entity_metadata_wrapper('node', node_load($lesson_id));
    if($node->field_video->value())
    {
      $lesson_name = custom_user_get_title($lesson_id);
      $url="<h4 class='title'>".$lesson_name['title']."</h4>";
      foreach($node->field_video->value() as $video){
        $video_data = array($node->field_video->value());
        $f_name=str_replace('public://','',$video['uri']);
        $url.= "</h4> <div class='video hidden'>/sites/default/files/".$f_name."</div>";
      }
     return $url."<div class='wrap_video'><div id=videoplayer></div></div>";
    }
    else{
      return '';
    }
  }
  else return '';
 }

function custom_user_lesson(){
  $lesson_id=arg(1);
  $node=entity_metadata_wrapper('node', node_load($lesson_id));
  $lesson_name = custom_user_get_title($lesson_id);
  $title = '<div class="titl_les"><span class=number_lesson>Урок №'.$node->field_weight->value().'</span><span class=h4>'.$lesson_name['title'].'</span></div>';
  if(!$node->body->value())
     $main='';
  else
  {
    $main=$node->body->value->value();
  }
  $finish = $node->field_finish_page->value(array(0 => TRUE));
  if(isset($finish[0])&&($finish[0]==1))$output=$title.$main;
  else   $output = $title.$main.'<spav class="look_course"><a href="/pupils_task/'.$lesson_id.'"> Выполнить задание</a></span>';
  return $output;
}

function custom_user_lesson_task(){
  global $user;
  drupal_flush_all_caches();
  $lesson_id=arg(1);
  $node=node_load($lesson_id);
  $node=node_view($node, 'full');
  return drupal_render($node);
}

function custom_user_course_plan(){
    global $user;
    $course_id=arg(1);
    $cur_course_lesson = curent_lesson_of_course($course_id, $user->uid);
    if(!isset($cur_course_lesson['lid'])){$lesson_index = 1; }
    else{$lesson_index = $cur_course_lesson['lid'];}
    $course_lesson = custom_user_course_lessons($course_id);
    $name=custom_user_get_title($course_id);
    $lesson_cours = '<ul class="lessons">';
    $index_l=0;
    foreach($course_lesson as  $lesson){
      $index_l++;
      if(($index_l+0)<=(0+$lesson_index)){
        if(($index_l+0)==(0+$lesson_index))
        {
          $prev_sumbission = user_send_this($user->uid, $lesson->lid);
          if(isset($prev_sumbission['sid'])){
            if($prev_sumbission['status']==0)  $link_to_course = '<span class="check_for_teacer stat_lesson">задание на проверке</span>';
            if($prev_sumbission['status']==3)  $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_task/'.$lesson->lid.'">Доработать</a></span>';
          }
          else
            $link_to_course = '<span class="go_to  stat_lesson"><a href="/pupils_lessons/'.$lesson->lid.'"> Пройти урок</a></span>';
        }
        elseif(($index_l+0)<(0+$lesson_index)) {

          $link_to_course = '<span class="complate stat_lesson">задание выполнено</span>';
        }
        $lesson_node=node_load($lesson->lid);
        if(isset($lesson_node->field_short_descr['und'][0]['value'])) $descr=$lesson_node->field_short_descr['und'][0]['value'];
        else $descr='';
        $lesson_cours.= '<li style="overflow: hidden;"> <span  class="lesson_title">Урок №'.$index_l.' '.$lesson_node->title.'</span> '.$link_to_course.'<div class="description">'.$descr.'</div></li>';
      }
    }
    $lesson_cours .= '</ul>';
  return '<ul>'.$lesson_cours.'</ul>';
}