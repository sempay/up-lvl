<?php
function custom_user_create_pupil(){
  $form=drupal_get_form('custom_user_add_pupil_form');
  return drupal_render($form);
}
function custom_user_create_curator(){
  $form=drupal_get_form('custom_user_add_curator_form');
  return drupal_render($form);
}

function custom_user_access_to_add($uid){
  $form=drupal_get_form('custom_user_add_user_form');
  return drupal_render($form);
}

// function custom_user_node_validate($node, $form, &$form_state) {
//   if(($node->type=='lesson')&&(!isset($_GET['course_id']))){
//     $temp_node=entity_metadata_wrapper('node', $node);
//     if(!$temp_node->field_course_id->value()) form_set_error('name', "Не прикреплен курс, попробуйте вернуться к списку ваших курсов");
//   }
// }

function lesson_statistic($cid, $lid){
return $count = db_select('custom_user_user')
  ->condition('lid', $lid, '=')
  ->condition('cid', $cid, '=')
  ->countQuery()
  ->execute()
  ->fetchField();
}

function deleting_user($uid){
        db_delete('custom_user_user')
          ->condition('uid',$uid,'=')
          ->execute();
		  
        db_delete('custom_user_nodes')
          ->condition('uid',$uid,'=')
          ->execute();
		  
        db_delete('custom_user_pupil_submission')
          ->condition('uid',$uid,'=')
          ->execute();

        db_delete('custom_user_dependence')
          ->condition('custom_uid',$uid,'=')
          ->execute();
}

function get_course_by_lesson($l_id){
  return db_select('custom_user_lessons', 'd')
    ->fields('d')
    ->condition('lid',$l_id,'=')
    ->execute()
    ->fetchAssoc();
}

function access_to_course($course, $uid){
  return db_select('custom_user_nodes','d')
    ->fields('d')
    ->condition('nid',$course, '=')
    ->condition('uid',$uid, '=')
    ->execute()
    ->fetchAssoc();
}

function access_statistic_view($course,$uid){
  return db_select('custom_user_nodes', 'd')
  ->fields('d')
  ->condition(
    db_or()
      ->condition('roles',4, '=')
      ->condition('roles',5, '=')
  )
  ->condition('uid',$uid, '=')
  ->condition('nid',$course,'=')
  ->execute()
  ->fetchAssoc();
}

function lesson_statistic_enable(){
  global $user;
  if(arg(0)=='statistic_lesson') $course=get_course_by_lesson(arg(1));
  else $course=array('cid'=>arg(1));
  $access=access_statistic_view($course['cid'], $user->uid);
  return $access;
}


function list_lesson_pupil(){
  $course=get_course_by_lesson(arg(1));
  return db_select('custom_user_user', 'd')
  ->fields('d')
    ->condition('cid', $course['cid'], '=')
    ->condition('lid', $course['weight'], '=')
    ->execute()
    ->fetchAllAssoc('ccu');
}


function list_course_pupil(){
  $course_id= arg(1);
  return db_select('custom_user_user', 'd')
    ->fields('d')
    ->condition('cid',$course_id , '=')
    ->execute()
    ->fetchAllAssoc('ccu');
}


function custom_user_block_form($form, &$form_state){
  return $form;
}


function ajax_block_user(){
}


function custom_user_block($uid, $cid){
  $form=drupal_get_form('custom_user_block_form');
  $form['testings']['#prefix'] = '<div id="'.$uid.'_form">';
  $form['testings']['#suffix'] = '</div>';
  $form['testings']['#tree'] = TRUE;
  $form['cid']=array(
    '#type'=>'hidden',
    '#value' => $cid,
  );
  $form['uid']=array(
    '#type'=>'hidden',
    '#value' => $uid,
  );
  $form['submit'] = array(
    '#type'=>'button',
    '#value' => $uid,
    '#ajax' => array(
      'callback' => 'ajax_block_user',
    ),
  );
  return drupal_render($form);
}


function filter_statistic_form_submit($form, &$form_state){
  if($form_state['input']['by_mail']!='')
  {
    $redirect ='/'.arg(0).'/'.arg(1).'?by_mail='.$form_state['input']['by_mail'];
    $redirect = str_replace(" ","",$redirect);
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".$redirect."");
    exit();
  }
  else {
    $redirect ='/'.arg(0).'/'.arg(1);
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".$redirect."");
    exit();
  }

}


function filter_statistic_form($form, &$form_state){
  $form['by_mail'] = array(
    '#type' => 'textfield',
    '#description'=> 'Используйте эту форму для фильтрации ученика по e-mail',
    '#attributes'=> array(
      '#placeholder'=>'Имя для поиска',
    )
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Применить',
  );
  return $form;
}


function change_user_lesson_form_submit($form, &$form_state){
  $item = $form_state['values'];
  db_merge('custom_user_user')
    ->key(array('uid'=>$item['accaunt'], 'cid' => $item['course']))
    ->fields(array(
    'lid' => $item['lesson'],
    'uid' => $item['accaunt'],
    'cid' => $item['course'],
  ))
    ->execute();
  $lessons = custom_user_course_lessons($item['course']);
  foreach($lessons as $lesson){
     db_delete('custom_user_pupil_submission')
    ->condition('uid',$item['accaunt'], '=')
    ->condition('nid', $lesson->lid, '=')
    ->execute();
    db_delete('custom_user_pupil_submission')
      ->condition('uid',$item['accaunt'], '=')
      ->condition('nid', $lesson->lid, '=')
      ->execute();
    $webform = db_select('webform_submissions','d')
      ->fields('d')
      ->condition('uid',$item['accaunt'], '=')
      ->condition('nid', $lesson->lid, '=')
      ->execute()
      ->fetchAssoc();
    db_delete('webform_submissions')
      ->condition('sid',$webform['sid'], '=')
      ->execute();
    db_delete('webform_submitted_data')
      ->condition('sid',$webform['sid'], '=')
      ->execute();
  }
}


function change_user_lesson_form($form, &$form_state){
   $form['course'] = array(
     '#type'=>'hidden',
     '#default_value'=>arg(1)
   );
   $form['#attributes'] = array('class'=>array('change-lesson'));
   $form['lesson']=array(
     '#type'=>'textfield',
   );
    $form['accaunt'] = array(
      '#type'=>'hidden',
    );
   $form['submit'] = array(
     '#type'=>'submit',
     '#value'=>'изменить',
   );
   return $form;
}


function get_lesson_by_c_w($cid,$lid){
  return db_select('custom_user_lessons', 'd')
    ->fields('d')
    ->condition('weight',$lid,'=')
    ->condition('cid',$cid,'=')
    ->execute()
    ->fetchAssoc();
}

function get_sid_by_u_l($uid,$lid){
  return db_select('custom_user_pupil_submission', 'd')
    ->fields('d')
    ->condition('uid',$uid,'=')
    ->condition('nid',$lid,'=')
    ->execute()
    ->fetchAssoc();
}


function custom_user_lesson_statistic(){
  if(arg(0)=='statistic_lesson') $list=list_lesson_pupil();
  else $list=list_course_pupil();
  global $user;
  foreach($list as $key=>$item){
    $accaunt=user_load($item->uid);
    if($accaunt){
      if(arg(0)!='statistic_lesson') $cid1=arg(1);
      else{
        $cours=get_course_by_lesson(arg(1));
        $cid1=$cours['cid'];
      }
      $is_cur = if_is_curator($accaunt->uid, $cid1);
      if($is_cur){
          if($user->uid!=$is_cur['cid']){
            unset($list[$key]);
          }
      }
    }
    else{
      deleting_user($item->uid);
    }
  }
  $form=drupal_get_form('filter_statistic_form');
  $output= drupal_render($form).'<table>';
  $output .= '<thead><th>№</th><th>Имя</th><th>почта</th><th>skype</th><th>Текущий урок</th><th>Проверка</th></thead>';
  $i=0;
  foreach($list as $item){
    $accaunt=user_load($item->uid);
    $link_ver='';
    if($accaunt)
    {
      $i++;
      $accaunt = entity_metadata_wrapper('user', $accaunt);
      if($accaunt->field_fio->value()) $name=$accaunt->field_fio->value();
      else $name = $accaunt->name->value();
      if($accaunt->field_skype->value()) $field_skype=$accaunt->field_skype->value();
      else $field_skype = '';
      $cur_course_lesson = curent_lesson_of_course(arg(1), $accaunt->uid->value());
      if(arg(0)!='statistic_lesson')
      {
        $cid1=arg(1);
        $form = drupal_get_form('change_user_lesson_form');
        $form['accaunt']['#value']=$accaunt->uid->value();
        if(!$cur_course_lesson){
          $form['lesson']['#value'] = 1;
          $l_num=1;
        }
        else{
          $form['lesson']['#value'] = $cur_course_lesson['lid'];
          $l_num=$cur_course_lesson['lid'];
        }
        $f = drupal_render($form);
        $cur_l_node=get_lesson_by_c_w($item->cid, $l_num);
        $cur_subm=get_sid_by_u_l($accaunt->uid->value(), $cur_l_node['lid']);
        if($cur_subm){
          if($cur_subm['status']==0){
            $link_ver='<a style="background: red;" href="/verification/'.$cur_subm['sid'].'/'.$cur_subm['nid'].'"><img src="/'.drupal_get_path('module', 'custom_user').'/images/arrow.png"></a>';
          }
        }
      }
      else {
        $cours=get_course_by_lesson(arg(1));
        $cid1=$cours['cid'];
        if(!$cur_course_lesson){
          $f= '';
          $l_num=1;
        }
        else{
          $f = '';
          $l_num= $cur_course_lesson['lid'];
        }
        //$cur_l_node=get_lesson_by_c_w(arg(1), $l_num);
        $cur_subm=get_sid_by_u_l($accaunt->uid->value(), arg(1));
        if($cur_subm){
          if($cur_subm['status']==0){
            $link_ver='<a href="/verification/'.$cur_subm['sid'].'/'.$cur_subm['nid'].'"><img src="/'.drupal_get_path('module', 'custom_user').'/images/arrow.png"></a>';
          }
        }
      };
      if(isset($_GET['by_mail'])){
        if($_GET['by_mail']==$accaunt->mail->value()) $output.='<tr><td>'.$i.'</td><td>'.$name.'</td><td>'.$accaunt->mail->value().'</td><td>'.$field_skype.'</td><td>'.$f.'</td><td>'.$link_ver.'</td></tr>';
      }
      else {
        $output.='<tr><td>'.$i.'</td><td><a href="/stat/pupil/'.$accaunt->uid->value().'/'.$cid1.'">'.$name.'</a></td><td>'.$accaunt->mail->value().'</td><td>'.$field_skype.'</td><td>'.$f.'</td><td>'.$link_ver.'</td></tr>';

      }
    }
    else{
      deleting_user($item->uid);
    }
 }
  $output.='</table>';

  return $output;
}

function custom_user_curators_course(){
  global $user;
  $courses = custom_user_get_curuser_courses($user->uid, '4', '5');
  $j=0;
  if(isset($courses)){
    $j++;
    $output='<ul>';
    foreach($courses as $key=>$course){
      $name=custom_user_get_title($course->nid);
      $output.='<li><span class="num">'.$j.'</span><span class="course_title" style="font-size: 18px;"><a href="/curators_list/'.$course->nid.'">'.$name['title'].'</a></span>
     </li><div class="hor"></div>';
    }
    $output.='</ul>';
  }
  else $output = 'У вас нет администрируемых вами курсов';
  return $output;
}

function get_curators_list($cid){
  return db_select('custom_user_nodes', 'd')
    ->fields('d')
    ->condition('roles','5','=')
    ->condition('nid',$cid,'=')
    ->execute()
    ->fetchAllAssoc('cid');
}


function delete_curator_form_submit($form, $form_state){
  global $user;
  $curator = $form_state['values']['uid'];
  $curs = $form_state['values']['cid'];
  db_delete('custom_user_nodes')
    ->condition('roles','5','=')
    ->condition('nid',$curs,'=')
    ->condition('uid',$curator,'=')
    ->execute();
}
function delete_curator_form($form, $form_state){
  $form['uid'] = array(
    '#type' => 'hidden',
  );
  $form['cid'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'удалить',
  );
   return $form;
  }
function curators_statistic_enable(){
  global $user;
  $cid = arg(1);
  $access = db_select('custom_user_nodes', 'd')
    ->fields('d')
    ->condition('roles','4','=')
    ->condition('nid',$cid,'=')
    ->condition('uid',$user->uid,'=')
    ->execute()
    ->fetchAssoc();
  if(isset($access['cid'])) return true;
  else return false;
}
function curators_course(){
  $curators=get_curators_list(arg(1));
  $output='<ul>';
  foreach($curators as $curator){
    $cur_curator = user_load($curator->uid);
    $form = drupal_get_form('delete_curator_form');
    $form['uid']['#value'] = $curator->uid;
    $form['cid']['#value'] = arg(1);
    $output.='<li style="overflow: hidden; padding: 10px;"><a href="/pupils_curator/'.$curator->uid.'">'.$cur_curator->init.'</a><div style="float: right;">'.drupal_render($form).'</div></li>';
  }
  $output.='</ul>';
  return $output;
}
function custom_user_statistic(){
  global $user;
  $courses = custom_user_get_curuser_courses($user->uid, '4', '5');
  if(isset($courses)){
    $j=0;
    $output='<ul class="courses">';
    foreach($courses as $key=>$course){
      $course_lesson = custom_user_course_lessons($course->nid);
      $name=custom_user_get_title($course->nid);
      $lesson_cours = '<ul class="lessons">';
      $course_count=0;
      foreach($course_lesson as $key=>$lesson){
        $pupil_count = lesson_statistic($course->nid, $lesson->weight);
        $course_count+=$pupil_count;
        $lessonName=custom_user_get_title($lesson->lid);
        $lesson_cours.= '<li style="text-decoration: none;"> Урок №'.$lesson->weight.' <a href="/pupils_lessons/'.$lesson->lid.'">'.$lessonName["title"].'</a> <span style="width: 80px; text-align: center;"><a style="display: block;" href="/statistic_lesson/'.$lesson->lid.'">'.$pupil_count.'</a></span></li>';
      }
      $j++;
      $lesson_cours .= '</ul>';
      $output.='<li><span class="num">'.$j.'</span><span class="course_title" style="font-size: 18px;"><a >'.$name['title'].'</a></span>
      <span style="width: 80px; float: right; text-align: center;"><label>Учеников</label><a style="color: #333; font-weight: bold; display: block;" href="/statistic_course/'.$course->nid.'">'.$course_count.'</a></span>'.$lesson_cours.'</li><div class="hor"></div>';
    }
    $output.='</ul>';
  }
  else $output = 'У вас нет администрируемых вами курсов';
  return $output;
}


function get_sid_by_user($uid,$nid){
  return db_select('custom_user_pupil_submission','d')
    ->fields('d', array('sid'))
    ->condition('uid', $uid,'=')
    ->condition('nid',$nid, '=')
    ->execute()
    ->fetchField();
}

function custom_user_node_submit($node, $form, &$form_state){
  $temp_node=entity_metadata_wrapper('node', $node);
  /**
   * Связать курс и автора
   */
  if(($node->type=='course')&&(!$node->nid)){
    node_save($node);
    $rec = array(
      'uid' => $node->uid,
      'roles' => 4,
      'nid' => $node->nid,
    );
    drupal_write_record('custom_user_nodes', $rec);
    drupal_goto('my-courses');
  }
  /**
  * Связать урок и курс
  */
  if($node->type=='lesson'){
    $i=0;
    if((isset($_GET['course_id']))&&(!$node->nid)){
      $temp_node->field_course_id = $_GET['course_id'];
      $temp_node->save();
      $rec = array(
        'cid' => $_GET['course_id'],
        'lid' => $node->nid,
        // 'weight' => $temp_node->field_weight->value(),
      );
      drupal_write_record('custom_user_lessons', $rec);
    }
    elseif(($temp_node->field_course_id->value())&&(!$node->nid)){
      $temp_node->save();
      $rec = array(
        'cid' => $temp_node->field_course_id->value(),
        'lid' => $node->nid,
        // 'weight' => $temp_node->field_weight->value(),
      );
      drupal_write_record('custom_user_lessons', $rec);
    }
    // elseif($node->nid){
    //   $temp_node->save();
    //   db_update('custom_user_lessons')
    //     ->fields(array('weight' => $temp_node->field_weight->value()))
    //     ->condition('lid', $node->nid)
    //     ->execute();
    // }
  }
  /**
   * отправка задания поместить в таблицу
   */
}


function custom_user_add($course_id, $user_role){
  return drupal_render(drupal_get_form('custom_user_add_user_form'));
}


function custom_user_get_curuser_courses($uid, $roles, $roles1=-1){
  return db_select('custom_user_nodes','d')
    ->fields('d')
    ->condition('uid', $uid, '=')
    ->condition(
    db_or()
        ->condition('roles',$roles, '=')
        ->condition('roles',$roles1, '=')
    )
    ->execute()
    ->fetchAllAssoc('cid');
}


function  custom_user_course_lessons($cid){
  $result= db_select('custom_user_lessons','d')
    ->fields('d')
    ->condition('cid', $cid, '=')
    ->orderBy('weight','ASC')
    ->execute()
    ->fetchAllAssoc('weight');
   ksort($result);
  return $result;
}


function  curs_clone_form_submit($form, &$form_state){
  $course_nid=$form_state['values']['cid'];
  $node=node_load($course_nid);
  unset($node->nid);
  unset($node->vid);
  node_save($node);
  $rec = array(
    'uid' => $node->uid,
    'roles' => 4,
    'nid' => $node->nid,
  );
  drupal_write_record('custom_user_nodes', $rec);
  $course_lesson = custom_user_course_lessons($course_nid);
  foreach($course_lesson as $lesson){
    $l_n=node_load($lesson->lid);
    unset($l_n->nid);
    unset($l_n->vid);
    node_save($l_n);
    $temp_node=entity_metadata_wrapper('node', $l_n);
    $l_n->field_course_id = $node->nid;
    $temp_node->save();
    $rec = array(
      'cid' => $node->nid,
      'lid' => $l_n->nid,
      // 'weight' => $temp_node->field_weight->value(),
    );
    drupal_write_record('custom_user_lessons', $rec);
  }
  drupal_goto('my-courses');
}
function  curs_clone_form($form, &$form_state){
  $form['cid'] = array(
    '#type'=>'hidden',
  );
  $form['submit'] = array(
    '#type'=>'submit',
    '#value'=>'Clone',
  );
  return $form;
}

function custom_user_get_title($nid){
  return db_select('node', 'n')
    ->fields('n', array('title'))
    ->condition('n.nid', $nid)
    ->execute()
    ->fetchAssoc();
}


function custom_user_admin_courses(){
  global $user;
  $courses = custom_user_get_curuser_courses($user->uid, '4');
  if(isset($courses)){
    $j=0;
    $output='<ul class="courses">';
    foreach($courses as $key=>$course){
      $course_lesson = custom_user_course_lessons($course->nid);
      $name=custom_user_get_title($course->nid);
      $lesson_cours = '<ul class="lessons">';
      foreach($course_lesson as $lesson){
        $lessonName=custom_user_get_title($lesson->lid);
        $lesson_cours.= '<li> Урок №'.$lesson->weight.' <a href="/pupils_lessons/'.$lesson->lid.'">'.$lessonName["title"].'</a> <span><a href="/node/'.$lesson->lid.'/edit?destination=course/admin_courses">Редактировать</a></span></li>';
      }
      $j++;
      $lesson_cours .= '</ul>';
      $t_form=drupal_get_form('curs_clone_form');
      $t_form['cid']['#value'] = $course->nid;
      $output.='<li><span class="num">'.$j.'</span><span class="course_title">'.$name['title'].'</span>
      <span class="add_lesson"><a href="/node/add/lesson?destination=course/admin_courses&course_id='.$course->nid.'">Создать урок</a></span><span class="add_lesson"><a href="/node/'.$course->nid.'/edit">Редактировать курс</a></span><span class="add_lesson" style="padding: 0; background: 0; border: none;">'.drupal_render($t_form).'</span>'.$lesson_cours.'</li>';
    }
    $output.='</ul>';
  }
  else $output = 'У вас нет созданных вами курсов';
  return $output;
}


function custom_user_add_to_courses1_form_submit($form, &$form_state){
  $i=0;

}


function custom_user_add_to_courses1_form(){
  drupal_add_js(drupal_get_path('module', 'custom_user') . "/js/init.js");
  $form=array();
  global $user;
  $courses_array=array();
  $courses = custom_user_get_curuser_courses($user->uid, '4');
  foreach($courses as $item){
    $courseName=custom_user_get_title($item->nid);
    $courses_array[$item->nid] = $courseName['title'];
  }
  $form['courses']['names'] = array(
    '#type' => 'checkboxes',
    '#options' => $courses_array,
  );
  return $form;
}
function custom_user_add_to_course(){
  global $user;
  $courses = custom_user_get_curuser_courses($user->uid, '4');
  if(isset($courses)){
    $form=drupal_get_form('custom_user_add_to_courses1_form');
    return drupal_render($form);
  }
  else return 'У вас нет созданных вами курсов';
}
function deleting_node($nid){
  db_delete('custom_user_lessons')
    ->condition('lid', $nid)
    ->execute();
  db_delete('custom_user_lessons')
    ->condition('cid', $nid)
    ->execute();
  db_delete('custom_user_nodes')
    ->condition('nid', $nid)
    ->execute();
}

function get_user_comment_by_index($sid,$key){
  return db_select('custom_user_comment','d')
    ->fields('d')
    ->condition('sid',$sid, '=')
    ->condition('key_subm',$key, '=')
    ->execute()
    ->fetchAssoc();
}

function lesson_ps_enable(){
  global $user;
  $lesson=arg(3);
  $node=node_load($lesson);
  $access=access_statistic_view($lesson, $user->uid);
  return $access;
}

function custom_user_posting(){
  drupal_add_js(drupal_get_path('module', 'custom_user'). '/js/posting.js');
}