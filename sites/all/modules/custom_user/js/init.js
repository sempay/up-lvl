(function ($) {
   $(function () {
     $('#custom-user-add-to-courses1-form .form-checkboxes input').bind('change', function(){
       $('#block-custom-user-custom-user-create-curator input[value="'+$(this).val()+'"], #block-custom-user-custom-user-create-pupil input[value="'+$(this).val()+'"]').click();
     });
   });
})(jQuery);