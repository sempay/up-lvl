


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>jquery-ui/ui/jquery.ui.core.js at master · jquery/jquery-ui · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <link rel="logo" type="image/svg" href="https://github-media-downloads.s3.amazonaws.com/github-logo.svg" />
    <meta property="og:image" content="https://github.global.ssl.fastly.net/images/modules/logos_page/Octocat.png">
    <meta name="hostname" content="github-fe128-cp1-prd.iad.github.net">
    <meta name="ruby" content="ruby 1.9.3p194-tcs-github-tcmalloc (e233cae611) [x86_64-linux]">
    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035/">
    <link rel="xhr-socket" href="/_sockets" />
    


    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="C1EE629D:3B8D:D2FB635:5268BFB8" name="octolytics-dimension-request_id" />
    

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="xXeQQv/pvOfveS2zcXDHimLIjNc/FNsi1lQHdU8zzO0=" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-37b6e181a972ce6706fa61ad91a3f4994af90049.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-8159a6c9a2c0054ae5779d7c98fa8d0b5dd25fca.css" media="all" rel="stylesheet" type="text/css" />
    

    

      <script src="https://github.global.ssl.fastly.net/assets/frameworks-9cf67afca6308356615fd17ad1d5bb84b5a22d05.js" type="text/javascript"></script>
      <script src="https://github.global.ssl.fastly.net/assets/github-521d89f061984571415cfab6b3aaa0c52142ce5a.js" type="text/javascript"></script>
      
      <meta http-equiv="x-pjax-version" content="0a17041247215a7938a5515656278983">

        <link data-pjax-transient rel='permalink' href='/jquery/jquery-ui/blob/dbbf3a9611e1c78b147e72e64c8ae1c1578c0f40/ui/jquery.ui.core.js'>
  <meta property="og:title" content="jquery-ui"/>
  <meta property="og:type" content="githubog:gitrepository"/>
  <meta property="og:url" content="https://github.com/jquery/jquery-ui"/>
  <meta property="og:image" content="https://github.global.ssl.fastly.net/images/gravatars/gravatar-user-420.png"/>
  <meta property="og:site_name" content="GitHub"/>
  <meta property="og:description" content="jquery-ui - The official jQuery user interface library."/>

  <meta name="description" content="jquery-ui - The official jQuery user interface library." />

  <meta content="70142" name="octolytics-dimension-user_id" /><meta content="jquery" name="octolytics-dimension-user_login" /><meta content="478996" name="octolytics-dimension-repository_id" /><meta content="jquery/jquery-ui" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="478996" name="octolytics-dimension-repository_network_root_id" /><meta content="jquery/jquery-ui" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/jquery/jquery-ui/commits/master.atom" rel="alternate" title="Recent Commits to jquery-ui:master" type="application/atom+xml" />

  </head>


  <body class="logged_out  env-production windows vis-public  page-blob">
    <div class="wrapper">
      
      
      


      
      <div class="header header-logged-out">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions">
        <a class="button primary" href="/join">Sign up</a>
      <a class="button signin" href="/login?return_to=%2Fjquery%2Fjquery-ui%2Fblob%2Fmaster%2Fui%2Fjquery.ui.core.js">Sign in</a>
    </div>

    <div class="command-bar js-command-bar  in-repository">

      <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
        <li class="features"><a href="/features">Features</a></li>
          <li class="enterprise"><a href="https://enterprise.github.com/">Enterprise</a></li>
          <li class="blog"><a href="/blog">Blog</a></li>
      </ul>
        <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<input type="text" data-hotkey="/ s" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    
      data-repo="jquery/jquery-ui"
      data-branch="master"
      data-sha="5f3a381b740a6526db93b51f5549815bda8a5b51"
  >

    <input type="hidden" name="nwo" value="jquery/jquery-ui" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="octicon help tooltipped downwards" title="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
    </div>

  </div>
</div>


      


          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">


  <li>
  <a href="/login?return_to=%2Fjquery%2Fjquery-ui"
  class="minibutton with-count js-toggler-target star-button entice tooltipped upwards"
  title="You must be signed in to use this feature" rel="nofollow">
  <span class="octicon octicon-star"></span>Star
</a>
<a class="social-count js-social-count" href="/jquery/jquery-ui/stargazers">
  7,872
</a>

  </li>

    <li>
      <a href="/login?return_to=%2Fjquery%2Fjquery-ui"
        class="minibutton with-count js-toggler-target fork-button entice tooltipped upwards"
        title="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-git-branch"></span>Fork
      </a>
      <a href="/jquery/jquery-ui/network" class="social-count">
        2,659
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author">
            <a href="/jquery" class="url fn" itemprop="url" rel="author"><span itemprop="title">jquery</span></a>
          </span>
          <span class="repohead-name-divider">/</span>
          <strong><a href="/jquery/jquery-ui" class="js-current-repository js-repo-home-link">jquery-ui</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">

      <div class="repository-with-sidebar repo-container ">

        <div class="repository-sidebar">
            

<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped leftwards" title="Code">
        <a href="/jquery/jquery-ui" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-gotokey="c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /jquery/jquery-ui">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


      <li class="tooltipped leftwards" title="Pull Requests"><a href="/jquery/jquery-ui/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="p" data-selected-links="repo_pulls /jquery/jquery-ui/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>48</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped leftwards" title="Pulse">
        <a href="/jquery/jquery-ui/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /jquery/jquery-ui/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped leftwards" title="Graphs">
        <a href="/jquery/jquery-ui/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /jquery/jquery-ui/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped leftwards" title="Network">
        <a href="/jquery/jquery-ui/network" aria-label="Network" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-selected-links="repo_network /jquery/jquery-ui/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

            <div class="only-with-full-nav">
              

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/jquery/jquery-ui.git" readonly="readonly">

    <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/jquery/jquery-ui.git" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/jquery/jquery-ui" readonly="readonly">

    <span class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/jquery/jquery-ui" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>,
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <span class="octicon help tooltipped upwards" title="Get help on which URL is right for you.">
    <a href="https://help.github.com/articles/which-remote-url-should-i-use">
    <span class="octicon octicon-question"></span>
    </a>
  </span>
</p>


  <a href="http://windows.github.com" class="minibutton sidebar-button">
    <span class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

              <a href="/jquery/jquery-ui/archive/master.zip"
                 class="minibutton sidebar-button"
                 title="Download this repository as a zip file"
                 rel="nofollow">
                <span class="octicon octicon-cloud-download"></span>
                Download ZIP
              </a>
            </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:4874072f7ddd361f2a2eb1f29f7c122a -->

<p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

<a href="/jquery/jquery-ui/find/master" data-pjax data-hotkey="t" class="js-show-file-finder" style="display:none">Show File Finder</a>

<div class="file-navigation">
  
  

<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    role="button" aria-label="Switch branches or tags" tabindex="0">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax>

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/1-8-stable/ui/jquery.ui.core.js"
                 data-name="1-8-stable"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1-8-stable">1-8-stable</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/1-9-stable/ui/jquery.ui.core.js"
                 data-name="1-9-stable"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1-9-stable">1-9-stable</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/1-10-stable/ui/jquery.ui.core.js"
                 data-name="1-10-stable"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1-10-stable">1-10-stable</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/UMD/ui/jquery.ui.core.js"
                 data-name="UMD"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="UMD">UMD</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/datepicker/ui/jquery.ui.core.js"
                 data-name="datepicker"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="datepicker">datepicker</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/datepicker-dead/ui/jquery.ui.core.js"
                 data-name="datepicker-dead"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="datepicker-dead">datepicker-dead</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/dialog-track-focus/ui/jquery.ui.core.js"
                 data-name="dialog-track-focus"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="dialog-track-focus">dialog-track-focus</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/editable/ui/jquery.ui.core.js"
                 data-name="editable"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="editable">editable</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/fix-5816/ui/jquery.ui.core.js"
                 data-name="fix-5816"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="fix-5816">fix-5816</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/fix-event-keycode/ui/jquery.ui.core.js"
                 data-name="fix-event-keycode"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="fix-event-keycode">fix-event-keycode</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/formcontrols/ui/jquery.ui.core.js"
                 data-name="formcontrols"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="formcontrols">formcontrols</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/grid/ui/jquery.ui.core.js"
                 data-name="grid"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="grid">grid</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/interactions/ui/jquery.ui.core.js"
                 data-name="interactions"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="interactions">interactions</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/interactions-polymer/ui/jquery.ui.core.js"
                 data-name="interactions-polymer"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="interactions-polymer">interactions-polymer</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/jscs/ui/jquery.ui.core.js"
                 data-name="jscs"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="jscs">jscs</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/manifest/ui/jquery.ui.core.js"
                 data-name="manifest"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="manifest">manifest</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/mask/ui/jquery.ui.core.js"
                 data-name="mask"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="mask">mask</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/master/ui/jquery.ui.core.js"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/menubar/ui/jquery.ui.core.js"
                 data-name="menubar"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="menubar">menubar</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/pr/982/ui/jquery.ui.core.js"
                 data-name="pr/982"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="pr/982">pr/982</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/progressbar/ui/jquery.ui.core.js"
                 data-name="progressbar"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="progressbar">progressbar</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/selectmenu/ui/jquery.ui.core.js"
                 data-name="selectmenu"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="selectmenu">selectmenu</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/blob/timepicker-clearempty/ui/jquery.ui.core.js"
                 data-name="timepicker-clearempty"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="timepicker-clearempty">timepicker-clearempty</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.10.3/ui/jquery.ui.core.js"
                 data-name="1.10.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.10.3">1.10.3</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.10.2/ui/jquery.ui.core.js"
                 data-name="1.10.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.10.2">1.10.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.10.1/ui/jquery.ui.core.js"
                 data-name="1.10.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.10.1">1.10.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.10.0-rc.1/ui/jquery.ui.core.js"
                 data-name="1.10.0-rc.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.10.0-rc.1">1.10.0-rc.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.10.0-beta.1/ui/jquery.ui.core.js"
                 data-name="1.10.0-beta.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.10.0-beta.1">1.10.0-beta.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.10.0/ui/jquery.ui.core.js"
                 data-name="1.10.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.10.0">1.10.0</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m7/ui/jquery.ui.core.js"
                 data-name="1.9m7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m7">1.9m7</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m6/ui/jquery.ui.core.js"
                 data-name="1.9m6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m6">1.9m6</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m5/ui/jquery.ui.core.js"
                 data-name="1.9m5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m5">1.9m5</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m4/ui/jquery.ui.core.js"
                 data-name="1.9m4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m4">1.9m4</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m3/ui/jquery.ui.core.js"
                 data-name="1.9m3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m3">1.9m3</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m2/ui/jquery.ui.core.js"
                 data-name="1.9m2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m2">1.9m2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9m1/ui/jquery.ui.core.js"
                 data-name="1.9m1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9m1">1.9m1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9.2/ui/jquery.ui.core.js"
                 data-name="1.9.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9.2">1.9.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9.1/ui/jquery.ui.core.js"
                 data-name="1.9.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9.1">1.9.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9.0-rc.1/ui/jquery.ui.core.js"
                 data-name="1.9.0-rc.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9.0-rc.1">1.9.0-rc.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9.0m8/ui/jquery.ui.core.js"
                 data-name="1.9.0m8"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9.0m8">1.9.0m8</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9.0-beta.1/ui/jquery.ui.core.js"
                 data-name="1.9.0-beta.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9.0-beta.1">1.9.0-beta.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.9.0/ui/jquery.ui.core.js"
                 data-name="1.9.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.9.0">1.9.0</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8rc3/ui/jquery.ui.core.js"
                 data-name="1.8rc3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8rc3">1.8rc3</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8rc2/ui/jquery.ui.core.js"
                 data-name="1.8rc2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8rc2">1.8rc2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8rc1/ui/jquery.ui.core.js"
                 data-name="1.8rc1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8rc1">1.8rc1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8b1/ui/jquery.ui.core.js"
                 data-name="1.8b1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8b1">1.8b1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8a2/ui/jquery.ui.core.js"
                 data-name="1.8a2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8a2">1.8a2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8a1/ui/jquery.ui.core.js"
                 data-name="1.8a1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8a1">1.8a1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.24/ui/jquery.ui.core.js"
                 data-name="1.8.24"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.24">1.8.24</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.23/ui/jquery.ui.core.js"
                 data-name="1.8.23"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.23">1.8.23</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.22/ui/jquery.ui.core.js"
                 data-name="1.8.22"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.22">1.8.22</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.21/ui/jquery.ui.core.js"
                 data-name="1.8.21"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.21">1.8.21</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.20/ui/jquery.ui.core.js"
                 data-name="1.8.20"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.20">1.8.20</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.19/ui/jquery.ui.core.js"
                 data-name="1.8.19"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.19">1.8.19</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.18/ui/jquery.ui.core.js"
                 data-name="1.8.18"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.18">1.8.18</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.17/ui/jquery.ui.core.js"
                 data-name="1.8.17"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.17">1.8.17</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.16/ui/jquery.ui.core.js"
                 data-name="1.8.16"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.16">1.8.16</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.15/ui/jquery.ui.core.js"
                 data-name="1.8.15"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.15">1.8.15</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.14/ui/jquery.ui.core.js"
                 data-name="1.8.14"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.14">1.8.14</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.13/ui/jquery.ui.core.js"
                 data-name="1.8.13"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.13">1.8.13</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.12/ui/jquery.ui.core.js"
                 data-name="1.8.12"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.12">1.8.12</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.11/ui/jquery.ui.core.js"
                 data-name="1.8.11"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.11">1.8.11</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.10/ui/jquery.ui.core.js"
                 data-name="1.8.10"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.10">1.8.10</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.9/ui/jquery.ui.core.js"
                 data-name="1.8.9"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.9">1.8.9</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.8/ui/jquery.ui.core.js"
                 data-name="1.8.8"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.8">1.8.8</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.7/ui/jquery.ui.core.js"
                 data-name="1.8.7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.7">1.8.7</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.6/ui/jquery.ui.core.js"
                 data-name="1.8.6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.6">1.8.6</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.5/ui/jquery.ui.core.js"
                 data-name="1.8.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.5">1.8.5</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.4/ui/jquery.ui.core.js"
                 data-name="1.8.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.4">1.8.4</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.3/ui/jquery.ui.core.js"
                 data-name="1.8.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.3">1.8.3</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.2/ui/jquery.ui.core.js"
                 data-name="1.8.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.2">1.8.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8.1/ui/jquery.ui.core.js"
                 data-name="1.8.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8.1">1.8.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.8/ui/jquery.ui.core.js"
                 data-name="1.8"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.8">1.8</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.7/ui/jquery.ui.core.js"
                 data-name="1.7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.7">1.7</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.6rc6/ui/jquery.ui.core.js"
                 data-name="1.6rc6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.6rc6">1.6rc6</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.6rc5/ui/jquery.ui.core.js"
                 data-name="1.6rc5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.6rc5">1.6rc5</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.6rc3/ui/jquery.ui.core.js"
                 data-name="1.6rc3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.6rc3">1.6rc3</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.6rc2/ui/jquery.ui.core.js"
                 data-name="1.6rc2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.6rc2">1.6rc2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.6/ui/jquery.ui.core.js"
                 data-name="1.6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.6">1.6</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.5.2/ui/jquery.ui.core.js"
                 data-name="1.5.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.5.2">1.5.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/jquery/jquery-ui/tree/1.5.1/ui/jquery.ui.core.js"
                 data-name="1.5.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="1.5.1">1.5.1</a>
            </div> <!-- /.select-menu-item -->
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/jquery/jquery-ui" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">jquery-ui</span></a></span></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/jquery/jquery-ui/tree/master/ui" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">ui</span></a></span><span class="separator"> / </span><strong class="final-path">jquery.ui.core.js</strong> <span class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="ui/jquery.ui.core.js" data-copied-hint="copied!" title="copy to clipboard"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>



  <div class="commit file-history-tease">
    <img class="main-avatar" height="24" src="https://0.gravatar.com/avatar/5572003a2d29e47e0d8084ac35ba3a42?d=https%3A%2F%2Fidenticons.github.com%2Fb587da25f0be5b669a8bc5d582f4b165.png&amp;s=140" width="24" />
    <span class="author"><a href="/bgrins" rel="author">bgrins</a></span>
    <time class="js-relative-date" datetime="2013-05-20T19:10:04-07:00" title="2013-05-20 19:10:04">May 20, 2013</time>
    <div class="commit-title">
        <a href="/jquery/jquery-ui/commit/24756a978a977d7abbef5e5bce403837a01d964f" class="message" data-pjax="true" title="Draggable: enabled draggable from within iframe. Fixed #5727 - draggable...

...: cannot drag element inside iframe">Draggable: enabled draggable from within iframe. Fixed #5727 - dragga…</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>9</strong> contributors</a></p>
          <a class="avatar tooltipped downwards" title="scottgonzalez" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=scottgonzalez"><img height="20" src="https://1.gravatar.com/avatar/35da631954825179143c86fa42a10954?d=https%3A%2F%2Fidenticons.github.com%2Fb5022b8fa3fa419ab039a0f8d70866a2.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="jzaefferer" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=jzaefferer"><img height="20" src="https://0.gravatar.com/avatar/a9d4d2558b560b0ef168ced0f6c5198c?d=https%3A%2F%2Fidenticons.github.com%2F3fdc35e4182c8401034d9710c5e5f8c1.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="rdworth" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=rdworth"><img height="20" src="https://2.gravatar.com/avatar/d92ea7772f465256ad836de1ce642b37?d=https%3A%2F%2Fidenticons.github.com%2Fb68e660d30f1d99f22bb6d1b10692420.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="mikesherov" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=mikesherov"><img height="20" src="https://1.gravatar.com/avatar/a123410cfb9ea6510294a295bb548530?d=https%3A%2F%2Fidenticons.github.com%2Fae34019b0e5765f1f0bca5a596d2bf4a.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="maljub01" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=maljub01"><img height="20" src="https://0.gravatar.com/avatar/258b9e6c34849e1286236cc2c3ba1d0e?d=https%3A%2F%2Fidenticons.github.com%2Fe0629ccae90628306e0f9a0e85765791.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="kborchers" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=kborchers"><img height="20" src="https://2.gravatar.com/avatar/911518c9eb1079cb417b06b78215414b?d=https%3A%2F%2Fidenticons.github.com%2Fe4821a21090c3e844bccedd5f0f9176b.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="timmywil" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=timmywil"><img height="20" src="https://2.gravatar.com/avatar/6b57220ae48e1ac5d02227bdada8a4d6?d=https%3A%2F%2Fidenticons.github.com%2F56e67f4a89bd82c06a9f07ad35c07262.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="btburnett3" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=btburnett3"><img height="20" src="https://1.gravatar.com/avatar/0cf4b746ee192c2a9123482becb9ffa9?d=https%3A%2F%2Fidenticons.github.com%2F47233cada37e3377ce5d1a88a09a20ee.png&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped downwards" title="bgrins" href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js?author=bgrins"><img height="20" src="https://0.gravatar.com/avatar/5572003a2d29e47e0d8084ac35ba3a42?d=https%3A%2F%2Fidenticons.github.com%2Fb587da25f0be5b669a8bc5d582f4b165.png&amp;s=140" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
        <li class="facebox-user-list-item">
          <img height="24" src="https://1.gravatar.com/avatar/35da631954825179143c86fa42a10954?d=https%3A%2F%2Fidenticons.github.com%2Fb5022b8fa3fa419ab039a0f8d70866a2.png&amp;s=140" width="24" />
          <a href="/scottgonzalez">scottgonzalez</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://0.gravatar.com/avatar/a9d4d2558b560b0ef168ced0f6c5198c?d=https%3A%2F%2Fidenticons.github.com%2F3fdc35e4182c8401034d9710c5e5f8c1.png&amp;s=140" width="24" />
          <a href="/jzaefferer">jzaefferer</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://2.gravatar.com/avatar/d92ea7772f465256ad836de1ce642b37?d=https%3A%2F%2Fidenticons.github.com%2Fb68e660d30f1d99f22bb6d1b10692420.png&amp;s=140" width="24" />
          <a href="/rdworth">rdworth</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://1.gravatar.com/avatar/a123410cfb9ea6510294a295bb548530?d=https%3A%2F%2Fidenticons.github.com%2Fae34019b0e5765f1f0bca5a596d2bf4a.png&amp;s=140" width="24" />
          <a href="/mikesherov">mikesherov</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://0.gravatar.com/avatar/258b9e6c34849e1286236cc2c3ba1d0e?d=https%3A%2F%2Fidenticons.github.com%2Fe0629ccae90628306e0f9a0e85765791.png&amp;s=140" width="24" />
          <a href="/maljub01">maljub01</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://2.gravatar.com/avatar/911518c9eb1079cb417b06b78215414b?d=https%3A%2F%2Fidenticons.github.com%2Fe4821a21090c3e844bccedd5f0f9176b.png&amp;s=140" width="24" />
          <a href="/kborchers">kborchers</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://2.gravatar.com/avatar/6b57220ae48e1ac5d02227bdada8a4d6?d=https%3A%2F%2Fidenticons.github.com%2F56e67f4a89bd82c06a9f07ad35c07262.png&amp;s=140" width="24" />
          <a href="/timmywil">timmywil</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://1.gravatar.com/avatar/0cf4b746ee192c2a9123482becb9ffa9?d=https%3A%2F%2Fidenticons.github.com%2F47233cada37e3377ce5d1a88a09a20ee.png&amp;s=140" width="24" />
          <a href="/btburnett3">btburnett3</a>
        </li>
        <li class="facebox-user-list-item">
          <img height="24" src="https://0.gravatar.com/avatar/5572003a2d29e47e0d8084ac35ba3a42?d=https%3A%2F%2Fidenticons.github.com%2Fb587da25f0be5b669a8bc5d582f4b165.png&amp;s=140" width="24" />
          <a href="/bgrins">bgrins</a>
        </li>
      </ul>
    </div>
  </div>

<div id="files" class="bubble">
  <div class="file">
    <div class="meta">
      <div class="info">
        <span class="icon"><b class="octicon octicon-file-text"></b></span>
        <span class="mode" title="File Mode">file</span>
          <span>294 lines (256 sloc)</span>
        <span>7.531 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
            <a class="minibutton tooltipped leftwards"
               href="http://windows.github.com" title="Open this file in GitHub for Windows">
                <span class="octicon octicon-device-desktop"></span> Open
            </a>
              <a class="minibutton disabled js-entice" href=""
                 data-entice="You must be signed in to make or propose changes">Edit</a>
          <a href="/jquery/jquery-ui/raw/master/ui/jquery.ui.core.js" class="button minibutton " id="raw-url">Raw</a>
            <a href="/jquery/jquery-ui/blame/master/ui/jquery.ui.core.js" class="button minibutton ">Blame</a>
          <a href="/jquery/jquery-ui/commits/master/ui/jquery.ui.core.js" class="button minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->
          <a class="minibutton danger empty-icon js-entice" href=""
             data-entice="You must be signed in and on a branch to make or propose changes">
          Delete
        </a>
      </div><!-- /.actions -->

    </div>
        <div class="blob-wrapper data type-javascript js-blob-data">
        <table class="file-code file-diff">
          <tr class="file-code-line">
            <td class="blob-line-nums">
              <span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>
<span id="L86" rel="#L86">86</span>
<span id="L87" rel="#L87">87</span>
<span id="L88" rel="#L88">88</span>
<span id="L89" rel="#L89">89</span>
<span id="L90" rel="#L90">90</span>
<span id="L91" rel="#L91">91</span>
<span id="L92" rel="#L92">92</span>
<span id="L93" rel="#L93">93</span>
<span id="L94" rel="#L94">94</span>
<span id="L95" rel="#L95">95</span>
<span id="L96" rel="#L96">96</span>
<span id="L97" rel="#L97">97</span>
<span id="L98" rel="#L98">98</span>
<span id="L99" rel="#L99">99</span>
<span id="L100" rel="#L100">100</span>
<span id="L101" rel="#L101">101</span>
<span id="L102" rel="#L102">102</span>
<span id="L103" rel="#L103">103</span>
<span id="L104" rel="#L104">104</span>
<span id="L105" rel="#L105">105</span>
<span id="L106" rel="#L106">106</span>
<span id="L107" rel="#L107">107</span>
<span id="L108" rel="#L108">108</span>
<span id="L109" rel="#L109">109</span>
<span id="L110" rel="#L110">110</span>
<span id="L111" rel="#L111">111</span>
<span id="L112" rel="#L112">112</span>
<span id="L113" rel="#L113">113</span>
<span id="L114" rel="#L114">114</span>
<span id="L115" rel="#L115">115</span>
<span id="L116" rel="#L116">116</span>
<span id="L117" rel="#L117">117</span>
<span id="L118" rel="#L118">118</span>
<span id="L119" rel="#L119">119</span>
<span id="L120" rel="#L120">120</span>
<span id="L121" rel="#L121">121</span>
<span id="L122" rel="#L122">122</span>
<span id="L123" rel="#L123">123</span>
<span id="L124" rel="#L124">124</span>
<span id="L125" rel="#L125">125</span>
<span id="L126" rel="#L126">126</span>
<span id="L127" rel="#L127">127</span>
<span id="L128" rel="#L128">128</span>
<span id="L129" rel="#L129">129</span>
<span id="L130" rel="#L130">130</span>
<span id="L131" rel="#L131">131</span>
<span id="L132" rel="#L132">132</span>
<span id="L133" rel="#L133">133</span>
<span id="L134" rel="#L134">134</span>
<span id="L135" rel="#L135">135</span>
<span id="L136" rel="#L136">136</span>
<span id="L137" rel="#L137">137</span>
<span id="L138" rel="#L138">138</span>
<span id="L139" rel="#L139">139</span>
<span id="L140" rel="#L140">140</span>
<span id="L141" rel="#L141">141</span>
<span id="L142" rel="#L142">142</span>
<span id="L143" rel="#L143">143</span>
<span id="L144" rel="#L144">144</span>
<span id="L145" rel="#L145">145</span>
<span id="L146" rel="#L146">146</span>
<span id="L147" rel="#L147">147</span>
<span id="L148" rel="#L148">148</span>
<span id="L149" rel="#L149">149</span>
<span id="L150" rel="#L150">150</span>
<span id="L151" rel="#L151">151</span>
<span id="L152" rel="#L152">152</span>
<span id="L153" rel="#L153">153</span>
<span id="L154" rel="#L154">154</span>
<span id="L155" rel="#L155">155</span>
<span id="L156" rel="#L156">156</span>
<span id="L157" rel="#L157">157</span>
<span id="L158" rel="#L158">158</span>
<span id="L159" rel="#L159">159</span>
<span id="L160" rel="#L160">160</span>
<span id="L161" rel="#L161">161</span>
<span id="L162" rel="#L162">162</span>
<span id="L163" rel="#L163">163</span>
<span id="L164" rel="#L164">164</span>
<span id="L165" rel="#L165">165</span>
<span id="L166" rel="#L166">166</span>
<span id="L167" rel="#L167">167</span>
<span id="L168" rel="#L168">168</span>
<span id="L169" rel="#L169">169</span>
<span id="L170" rel="#L170">170</span>
<span id="L171" rel="#L171">171</span>
<span id="L172" rel="#L172">172</span>
<span id="L173" rel="#L173">173</span>
<span id="L174" rel="#L174">174</span>
<span id="L175" rel="#L175">175</span>
<span id="L176" rel="#L176">176</span>
<span id="L177" rel="#L177">177</span>
<span id="L178" rel="#L178">178</span>
<span id="L179" rel="#L179">179</span>
<span id="L180" rel="#L180">180</span>
<span id="L181" rel="#L181">181</span>
<span id="L182" rel="#L182">182</span>
<span id="L183" rel="#L183">183</span>
<span id="L184" rel="#L184">184</span>
<span id="L185" rel="#L185">185</span>
<span id="L186" rel="#L186">186</span>
<span id="L187" rel="#L187">187</span>
<span id="L188" rel="#L188">188</span>
<span id="L189" rel="#L189">189</span>
<span id="L190" rel="#L190">190</span>
<span id="L191" rel="#L191">191</span>
<span id="L192" rel="#L192">192</span>
<span id="L193" rel="#L193">193</span>
<span id="L194" rel="#L194">194</span>
<span id="L195" rel="#L195">195</span>
<span id="L196" rel="#L196">196</span>
<span id="L197" rel="#L197">197</span>
<span id="L198" rel="#L198">198</span>
<span id="L199" rel="#L199">199</span>
<span id="L200" rel="#L200">200</span>
<span id="L201" rel="#L201">201</span>
<span id="L202" rel="#L202">202</span>
<span id="L203" rel="#L203">203</span>
<span id="L204" rel="#L204">204</span>
<span id="L205" rel="#L205">205</span>
<span id="L206" rel="#L206">206</span>
<span id="L207" rel="#L207">207</span>
<span id="L208" rel="#L208">208</span>
<span id="L209" rel="#L209">209</span>
<span id="L210" rel="#L210">210</span>
<span id="L211" rel="#L211">211</span>
<span id="L212" rel="#L212">212</span>
<span id="L213" rel="#L213">213</span>
<span id="L214" rel="#L214">214</span>
<span id="L215" rel="#L215">215</span>
<span id="L216" rel="#L216">216</span>
<span id="L217" rel="#L217">217</span>
<span id="L218" rel="#L218">218</span>
<span id="L219" rel="#L219">219</span>
<span id="L220" rel="#L220">220</span>
<span id="L221" rel="#L221">221</span>
<span id="L222" rel="#L222">222</span>
<span id="L223" rel="#L223">223</span>
<span id="L224" rel="#L224">224</span>
<span id="L225" rel="#L225">225</span>
<span id="L226" rel="#L226">226</span>
<span id="L227" rel="#L227">227</span>
<span id="L228" rel="#L228">228</span>
<span id="L229" rel="#L229">229</span>
<span id="L230" rel="#L230">230</span>
<span id="L231" rel="#L231">231</span>
<span id="L232" rel="#L232">232</span>
<span id="L233" rel="#L233">233</span>
<span id="L234" rel="#L234">234</span>
<span id="L235" rel="#L235">235</span>
<span id="L236" rel="#L236">236</span>
<span id="L237" rel="#L237">237</span>
<span id="L238" rel="#L238">238</span>
<span id="L239" rel="#L239">239</span>
<span id="L240" rel="#L240">240</span>
<span id="L241" rel="#L241">241</span>
<span id="L242" rel="#L242">242</span>
<span id="L243" rel="#L243">243</span>
<span id="L244" rel="#L244">244</span>
<span id="L245" rel="#L245">245</span>
<span id="L246" rel="#L246">246</span>
<span id="L247" rel="#L247">247</span>
<span id="L248" rel="#L248">248</span>
<span id="L249" rel="#L249">249</span>
<span id="L250" rel="#L250">250</span>
<span id="L251" rel="#L251">251</span>
<span id="L252" rel="#L252">252</span>
<span id="L253" rel="#L253">253</span>
<span id="L254" rel="#L254">254</span>
<span id="L255" rel="#L255">255</span>
<span id="L256" rel="#L256">256</span>
<span id="L257" rel="#L257">257</span>
<span id="L258" rel="#L258">258</span>
<span id="L259" rel="#L259">259</span>
<span id="L260" rel="#L260">260</span>
<span id="L261" rel="#L261">261</span>
<span id="L262" rel="#L262">262</span>
<span id="L263" rel="#L263">263</span>
<span id="L264" rel="#L264">264</span>
<span id="L265" rel="#L265">265</span>
<span id="L266" rel="#L266">266</span>
<span id="L267" rel="#L267">267</span>
<span id="L268" rel="#L268">268</span>
<span id="L269" rel="#L269">269</span>
<span id="L270" rel="#L270">270</span>
<span id="L271" rel="#L271">271</span>
<span id="L272" rel="#L272">272</span>
<span id="L273" rel="#L273">273</span>
<span id="L274" rel="#L274">274</span>
<span id="L275" rel="#L275">275</span>
<span id="L276" rel="#L276">276</span>
<span id="L277" rel="#L277">277</span>
<span id="L278" rel="#L278">278</span>
<span id="L279" rel="#L279">279</span>
<span id="L280" rel="#L280">280</span>
<span id="L281" rel="#L281">281</span>
<span id="L282" rel="#L282">282</span>
<span id="L283" rel="#L283">283</span>
<span id="L284" rel="#L284">284</span>
<span id="L285" rel="#L285">285</span>
<span id="L286" rel="#L286">286</span>
<span id="L287" rel="#L287">287</span>
<span id="L288" rel="#L288">288</span>
<span id="L289" rel="#L289">289</span>
<span id="L290" rel="#L290">290</span>
<span id="L291" rel="#L291">291</span>
<span id="L292" rel="#L292">292</span>
<span id="L293" rel="#L293">293</span>

            </td>
            <td class="blob-line-code">
                    <div class="highlight"><pre><div class='line' id='LC1'><span class="cm">/*!</span></div><div class='line' id='LC2'><span class="cm"> * jQuery UI Core @VERSION</span></div><div class='line' id='LC3'><span class="cm"> * http://jqueryui.com</span></div><div class='line' id='LC4'><span class="cm"> *</span></div><div class='line' id='LC5'><span class="cm"> * Copyright 2013 jQuery Foundation and other contributors</span></div><div class='line' id='LC6'><span class="cm"> * Released under the MIT license.</span></div><div class='line' id='LC7'><span class="cm"> * http://jquery.org/license</span></div><div class='line' id='LC8'><span class="cm"> *</span></div><div class='line' id='LC9'><span class="cm"> * http://api.jqueryui.com/category/ui-core/</span></div><div class='line' id='LC10'><span class="cm"> */</span></div><div class='line' id='LC11'><span class="p">(</span><span class="kd">function</span><span class="p">(</span> <span class="nx">$</span><span class="p">,</span> <span class="kc">undefined</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC12'><br/></div><div class='line' id='LC13'><span class="kd">var</span> <span class="nx">uuid</span> <span class="o">=</span> <span class="mi">0</span><span class="p">,</span></div><div class='line' id='LC14'>	<span class="nx">runiqueId</span> <span class="o">=</span> <span class="sr">/^ui-id-\d+$/</span><span class="p">;</span></div><div class='line' id='LC15'><br/></div><div class='line' id='LC16'><span class="c1">// $.ui might exist from components with no dependencies, e.g., $.ui.position</span></div><div class='line' id='LC17'><span class="nx">$</span><span class="p">.</span><span class="nx">ui</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">ui</span> <span class="o">||</span> <span class="p">{};</span></div><div class='line' id='LC18'><br/></div><div class='line' id='LC19'><span class="nx">$</span><span class="p">.</span><span class="nx">extend</span><span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">ui</span><span class="p">,</span> <span class="p">{</span></div><div class='line' id='LC20'>	<span class="nx">version</span><span class="o">:</span> <span class="s2">&quot;@VERSION&quot;</span><span class="p">,</span></div><div class='line' id='LC21'><br/></div><div class='line' id='LC22'>	<span class="nx">keyCode</span><span class="o">:</span> <span class="p">{</span></div><div class='line' id='LC23'>		<span class="nx">BACKSPACE</span><span class="o">:</span> <span class="mi">8</span><span class="p">,</span></div><div class='line' id='LC24'>		<span class="nx">COMMA</span><span class="o">:</span> <span class="mi">188</span><span class="p">,</span></div><div class='line' id='LC25'>		<span class="nx">DELETE</span><span class="o">:</span> <span class="mi">46</span><span class="p">,</span></div><div class='line' id='LC26'>		<span class="nx">DOWN</span><span class="o">:</span> <span class="mi">40</span><span class="p">,</span></div><div class='line' id='LC27'>		<span class="nx">END</span><span class="o">:</span> <span class="mi">35</span><span class="p">,</span></div><div class='line' id='LC28'>		<span class="nx">ENTER</span><span class="o">:</span> <span class="mi">13</span><span class="p">,</span></div><div class='line' id='LC29'>		<span class="nx">ESCAPE</span><span class="o">:</span> <span class="mi">27</span><span class="p">,</span></div><div class='line' id='LC30'>		<span class="nx">HOME</span><span class="o">:</span> <span class="mi">36</span><span class="p">,</span></div><div class='line' id='LC31'>		<span class="nx">LEFT</span><span class="o">:</span> <span class="mi">37</span><span class="p">,</span></div><div class='line' id='LC32'>		<span class="nx">PAGE_DOWN</span><span class="o">:</span> <span class="mi">34</span><span class="p">,</span></div><div class='line' id='LC33'>		<span class="nx">PAGE_UP</span><span class="o">:</span> <span class="mi">33</span><span class="p">,</span></div><div class='line' id='LC34'>		<span class="nx">PERIOD</span><span class="o">:</span> <span class="mi">190</span><span class="p">,</span></div><div class='line' id='LC35'>		<span class="nx">RIGHT</span><span class="o">:</span> <span class="mi">39</span><span class="p">,</span></div><div class='line' id='LC36'>		<span class="nx">SPACE</span><span class="o">:</span> <span class="mi">32</span><span class="p">,</span></div><div class='line' id='LC37'>		<span class="nx">TAB</span><span class="o">:</span> <span class="mi">9</span><span class="p">,</span></div><div class='line' id='LC38'>		<span class="nx">UP</span><span class="o">:</span> <span class="mi">38</span></div><div class='line' id='LC39'>	<span class="p">}</span></div><div class='line' id='LC40'><span class="p">});</span></div><div class='line' id='LC41'><br/></div><div class='line' id='LC42'><span class="c1">// plugins</span></div><div class='line' id='LC43'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">extend</span><span class="p">({</span></div><div class='line' id='LC44'>	<span class="nx">focus</span><span class="o">:</span> <span class="p">(</span><span class="kd">function</span><span class="p">(</span> <span class="nx">orig</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC45'>		<span class="k">return</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">delay</span><span class="p">,</span> <span class="nx">fn</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC46'>			<span class="k">return</span> <span class="k">typeof</span> <span class="nx">delay</span> <span class="o">===</span> <span class="s2">&quot;number&quot;</span> <span class="o">?</span></div><div class='line' id='LC47'>				<span class="k">this</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC48'>					<span class="kd">var</span> <span class="nx">elem</span> <span class="o">=</span> <span class="k">this</span><span class="p">;</span></div><div class='line' id='LC49'>					<span class="nx">setTimeout</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC50'>						<span class="nx">$</span><span class="p">(</span> <span class="nx">elem</span> <span class="p">).</span><span class="nx">focus</span><span class="p">();</span></div><div class='line' id='LC51'>						<span class="k">if</span> <span class="p">(</span> <span class="nx">fn</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC52'>							<span class="nx">fn</span><span class="p">.</span><span class="nx">call</span><span class="p">(</span> <span class="nx">elem</span> <span class="p">);</span></div><div class='line' id='LC53'>						<span class="p">}</span></div><div class='line' id='LC54'>					<span class="p">},</span> <span class="nx">delay</span> <span class="p">);</span></div><div class='line' id='LC55'>				<span class="p">})</span> <span class="o">:</span></div><div class='line' id='LC56'>				<span class="nx">orig</span><span class="p">.</span><span class="nx">apply</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nx">arguments</span> <span class="p">);</span></div><div class='line' id='LC57'>		<span class="p">};</span></div><div class='line' id='LC58'>	<span class="p">})(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">focus</span> <span class="p">),</span></div><div class='line' id='LC59'><br/></div><div class='line' id='LC60'>	<span class="nx">scrollParent</span><span class="o">:</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC61'>		<span class="kd">var</span> <span class="nx">scrollParent</span><span class="p">;</span></div><div class='line' id='LC62'>		<span class="k">if</span> <span class="p">((</span><span class="nx">$</span><span class="p">.</span><span class="nx">ui</span><span class="p">.</span><span class="nx">ie</span> <span class="o">&amp;&amp;</span> <span class="p">(</span><span class="sr">/(static|relative)/</span><span class="p">).</span><span class="nx">test</span><span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;position&quot;</span><span class="p">)))</span> <span class="o">||</span> <span class="p">(</span><span class="sr">/absolute/</span><span class="p">).</span><span class="nx">test</span><span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="s2">&quot;position&quot;</span><span class="p">)))</span> <span class="p">{</span></div><div class='line' id='LC63'>			<span class="nx">scrollParent</span> <span class="o">=</span> <span class="k">this</span><span class="p">.</span><span class="nx">parents</span><span class="p">().</span><span class="nx">filter</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC64'>				<span class="k">return</span> <span class="p">(</span><span class="sr">/(relative|absolute|fixed)/</span><span class="p">).</span><span class="nx">test</span><span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;position&quot;</span><span class="p">))</span> <span class="o">&amp;&amp;</span> <span class="p">(</span><span class="sr">/(auto|scroll)/</span><span class="p">).</span><span class="nx">test</span><span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;overflow&quot;</span><span class="p">)</span><span class="o">+</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;overflow-y&quot;</span><span class="p">)</span><span class="o">+</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;overflow-x&quot;</span><span class="p">));</span></div><div class='line' id='LC65'>			<span class="p">}).</span><span class="nx">eq</span><span class="p">(</span><span class="mi">0</span><span class="p">);</span></div><div class='line' id='LC66'>		<span class="p">}</span> <span class="k">else</span> <span class="p">{</span></div><div class='line' id='LC67'>			<span class="nx">scrollParent</span> <span class="o">=</span> <span class="k">this</span><span class="p">.</span><span class="nx">parents</span><span class="p">().</span><span class="nx">filter</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC68'>				<span class="k">return</span> <span class="p">(</span><span class="sr">/(auto|scroll)/</span><span class="p">).</span><span class="nx">test</span><span class="p">(</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;overflow&quot;</span><span class="p">)</span><span class="o">+</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;overflow-y&quot;</span><span class="p">)</span><span class="o">+</span><span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span><span class="k">this</span><span class="p">,</span><span class="s2">&quot;overflow-x&quot;</span><span class="p">));</span></div><div class='line' id='LC69'>			<span class="p">}).</span><span class="nx">eq</span><span class="p">(</span><span class="mi">0</span><span class="p">);</span></div><div class='line' id='LC70'>		<span class="p">}</span></div><div class='line' id='LC71'><br/></div><div class='line' id='LC72'>		<span class="k">return</span> <span class="p">(</span> <span class="sr">/fixed/</span> <span class="p">).</span><span class="nx">test</span><span class="p">(</span> <span class="k">this</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="s2">&quot;position&quot;</span><span class="p">)</span> <span class="p">)</span> <span class="o">||</span> <span class="o">!</span><span class="nx">scrollParent</span><span class="p">.</span><span class="nx">length</span> <span class="o">?</span> <span class="nx">$</span><span class="p">(</span> <span class="k">this</span><span class="p">[</span> <span class="mi">0</span> <span class="p">].</span><span class="nx">ownerDocument</span> <span class="o">||</span> <span class="nb">document</span> <span class="p">)</span> <span class="o">:</span> <span class="nx">scrollParent</span><span class="p">;</span></div><div class='line' id='LC73'>	<span class="p">},</span></div><div class='line' id='LC74'><br/></div><div class='line' id='LC75'>	<span class="nx">uniqueId</span><span class="o">:</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC76'>		<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC77'>			<span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="k">this</span><span class="p">.</span><span class="nx">id</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC78'>				<span class="k">this</span><span class="p">.</span><span class="nx">id</span> <span class="o">=</span> <span class="s2">&quot;ui-id-&quot;</span> <span class="o">+</span> <span class="p">(</span><span class="o">++</span><span class="nx">uuid</span><span class="p">);</span></div><div class='line' id='LC79'>			<span class="p">}</span></div><div class='line' id='LC80'>		<span class="p">});</span></div><div class='line' id='LC81'>	<span class="p">},</span></div><div class='line' id='LC82'><br/></div><div class='line' id='LC83'>	<span class="nx">removeUniqueId</span><span class="o">:</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC84'>		<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC85'>			<span class="k">if</span> <span class="p">(</span> <span class="nx">runiqueId</span><span class="p">.</span><span class="nx">test</span><span class="p">(</span> <span class="k">this</span><span class="p">.</span><span class="nx">id</span> <span class="p">)</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC86'>				<span class="nx">$</span><span class="p">(</span> <span class="k">this</span> <span class="p">).</span><span class="nx">removeAttr</span><span class="p">(</span> <span class="s2">&quot;id&quot;</span> <span class="p">);</span></div><div class='line' id='LC87'>			<span class="p">}</span></div><div class='line' id='LC88'>		<span class="p">});</span></div><div class='line' id='LC89'>	<span class="p">}</span></div><div class='line' id='LC90'><span class="p">});</span></div><div class='line' id='LC91'><br/></div><div class='line' id='LC92'><span class="c1">// selectors</span></div><div class='line' id='LC93'><span class="kd">function</span> <span class="nx">focusable</span><span class="p">(</span> <span class="nx">element</span><span class="p">,</span> <span class="nx">isTabIndexNotNaN</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC94'>	<span class="kd">var</span> <span class="nx">map</span><span class="p">,</span> <span class="nx">mapName</span><span class="p">,</span> <span class="nx">img</span><span class="p">,</span></div><div class='line' id='LC95'>		<span class="nx">nodeName</span> <span class="o">=</span> <span class="nx">element</span><span class="p">.</span><span class="nx">nodeName</span><span class="p">.</span><span class="nx">toLowerCase</span><span class="p">();</span></div><div class='line' id='LC96'>	<span class="k">if</span> <span class="p">(</span> <span class="s2">&quot;area&quot;</span> <span class="o">===</span> <span class="nx">nodeName</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC97'>		<span class="nx">map</span> <span class="o">=</span> <span class="nx">element</span><span class="p">.</span><span class="nx">parentNode</span><span class="p">;</span></div><div class='line' id='LC98'>		<span class="nx">mapName</span> <span class="o">=</span> <span class="nx">map</span><span class="p">.</span><span class="nx">name</span><span class="p">;</span></div><div class='line' id='LC99'>		<span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="nx">element</span><span class="p">.</span><span class="nx">href</span> <span class="o">||</span> <span class="o">!</span><span class="nx">mapName</span> <span class="o">||</span> <span class="nx">map</span><span class="p">.</span><span class="nx">nodeName</span><span class="p">.</span><span class="nx">toLowerCase</span><span class="p">()</span> <span class="o">!==</span> <span class="s2">&quot;map&quot;</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC100'>			<span class="k">return</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC101'>		<span class="p">}</span></div><div class='line' id='LC102'>		<span class="nx">img</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span> <span class="s2">&quot;img[usemap=#&quot;</span> <span class="o">+</span> <span class="nx">mapName</span> <span class="o">+</span> <span class="s2">&quot;]&quot;</span> <span class="p">)[</span><span class="mi">0</span><span class="p">];</span></div><div class='line' id='LC103'>		<span class="k">return</span> <span class="o">!!</span><span class="nx">img</span> <span class="o">&amp;&amp;</span> <span class="nx">visible</span><span class="p">(</span> <span class="nx">img</span> <span class="p">);</span></div><div class='line' id='LC104'>	<span class="p">}</span></div><div class='line' id='LC105'>	<span class="k">return</span> <span class="p">(</span> <span class="sr">/input|select|textarea|button|object/</span><span class="p">.</span><span class="nx">test</span><span class="p">(</span> <span class="nx">nodeName</span> <span class="p">)</span> <span class="o">?</span></div><div class='line' id='LC106'>		<span class="o">!</span><span class="nx">element</span><span class="p">.</span><span class="nx">disabled</span> <span class="o">:</span></div><div class='line' id='LC107'>		<span class="s2">&quot;a&quot;</span> <span class="o">===</span> <span class="nx">nodeName</span> <span class="o">?</span></div><div class='line' id='LC108'>			<span class="nx">element</span><span class="p">.</span><span class="nx">href</span> <span class="o">||</span> <span class="nx">isTabIndexNotNaN</span> <span class="o">:</span></div><div class='line' id='LC109'>			<span class="nx">isTabIndexNotNaN</span><span class="p">)</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC110'>		<span class="c1">// the element and all of its ancestors must be visible</span></div><div class='line' id='LC111'>		<span class="nx">visible</span><span class="p">(</span> <span class="nx">element</span> <span class="p">);</span></div><div class='line' id='LC112'><span class="p">}</span></div><div class='line' id='LC113'><br/></div><div class='line' id='LC114'><span class="kd">function</span> <span class="nx">visible</span><span class="p">(</span> <span class="nx">element</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC115'>	<span class="k">return</span> <span class="nx">$</span><span class="p">.</span><span class="nx">expr</span><span class="p">.</span><span class="nx">filters</span><span class="p">.</span><span class="nx">visible</span><span class="p">(</span> <span class="nx">element</span> <span class="p">)</span> <span class="o">&amp;&amp;</span></div><div class='line' id='LC116'>		<span class="o">!</span><span class="nx">$</span><span class="p">(</span> <span class="nx">element</span> <span class="p">).</span><span class="nx">parents</span><span class="p">().</span><span class="nx">addBack</span><span class="p">().</span><span class="nx">filter</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC117'>			<span class="k">return</span> <span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="s2">&quot;visibility&quot;</span> <span class="p">)</span> <span class="o">===</span> <span class="s2">&quot;hidden&quot;</span><span class="p">;</span></div><div class='line' id='LC118'>		<span class="p">}).</span><span class="nx">length</span><span class="p">;</span></div><div class='line' id='LC119'><span class="p">}</span></div><div class='line' id='LC120'><br/></div><div class='line' id='LC121'><span class="nx">$</span><span class="p">.</span><span class="nx">extend</span><span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">expr</span><span class="p">[</span> <span class="s2">&quot;:&quot;</span> <span class="p">],</span> <span class="p">{</span></div><div class='line' id='LC122'>	<span class="nx">data</span><span class="o">:</span> <span class="nx">$</span><span class="p">.</span><span class="nx">expr</span><span class="p">.</span><span class="nx">createPseudo</span> <span class="o">?</span></div><div class='line' id='LC123'>		<span class="nx">$</span><span class="p">.</span><span class="nx">expr</span><span class="p">.</span><span class="nx">createPseudo</span><span class="p">(</span><span class="kd">function</span><span class="p">(</span> <span class="nx">dataName</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC124'>			<span class="k">return</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">elem</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC125'>				<span class="k">return</span> <span class="o">!!</span><span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="nx">dataName</span> <span class="p">);</span></div><div class='line' id='LC126'>			<span class="p">};</span></div><div class='line' id='LC127'>		<span class="p">})</span> <span class="o">:</span></div><div class='line' id='LC128'>		<span class="c1">// support: jQuery &lt;1.8</span></div><div class='line' id='LC129'>		<span class="kd">function</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="nx">i</span><span class="p">,</span> <span class="nx">match</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC130'>			<span class="k">return</span> <span class="o">!!</span><span class="nx">$</span><span class="p">.</span><span class="nx">data</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="nx">match</span><span class="p">[</span> <span class="mi">3</span> <span class="p">]</span> <span class="p">);</span></div><div class='line' id='LC131'>		<span class="p">},</span></div><div class='line' id='LC132'><br/></div><div class='line' id='LC133'>	<span class="nx">focusable</span><span class="o">:</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">element</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC134'>		<span class="k">return</span> <span class="nx">focusable</span><span class="p">(</span> <span class="nx">element</span><span class="p">,</span> <span class="o">!</span><span class="nb">isNaN</span><span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">attr</span><span class="p">(</span> <span class="nx">element</span><span class="p">,</span> <span class="s2">&quot;tabindex&quot;</span> <span class="p">)</span> <span class="p">)</span> <span class="p">);</span></div><div class='line' id='LC135'>	<span class="p">},</span></div><div class='line' id='LC136'><br/></div><div class='line' id='LC137'>	<span class="nx">tabbable</span><span class="o">:</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">element</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC138'>		<span class="kd">var</span> <span class="nx">tabIndex</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">attr</span><span class="p">(</span> <span class="nx">element</span><span class="p">,</span> <span class="s2">&quot;tabindex&quot;</span> <span class="p">),</span></div><div class='line' id='LC139'>			<span class="nx">isTabIndexNaN</span> <span class="o">=</span> <span class="nb">isNaN</span><span class="p">(</span> <span class="nx">tabIndex</span> <span class="p">);</span></div><div class='line' id='LC140'>		<span class="k">return</span> <span class="p">(</span> <span class="nx">isTabIndexNaN</span> <span class="o">||</span> <span class="nx">tabIndex</span> <span class="o">&gt;=</span> <span class="mi">0</span> <span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="nx">focusable</span><span class="p">(</span> <span class="nx">element</span><span class="p">,</span> <span class="o">!</span><span class="nx">isTabIndexNaN</span> <span class="p">);</span></div><div class='line' id='LC141'>	<span class="p">}</span></div><div class='line' id='LC142'><span class="p">});</span></div><div class='line' id='LC143'><br/></div><div class='line' id='LC144'><span class="c1">// support: jQuery &lt;1.8</span></div><div class='line' id='LC145'><span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="nx">$</span><span class="p">(</span> <span class="s2">&quot;&lt;a&gt;&quot;</span> <span class="p">).</span><span class="nx">outerWidth</span><span class="p">(</span> <span class="mi">1</span> <span class="p">).</span><span class="nx">jquery</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC146'>	<span class="nx">$</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span> <span class="p">[</span> <span class="s2">&quot;Width&quot;</span><span class="p">,</span> <span class="s2">&quot;Height&quot;</span> <span class="p">],</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">i</span><span class="p">,</span> <span class="nx">name</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC147'>		<span class="kd">var</span> <span class="nx">side</span> <span class="o">=</span> <span class="nx">name</span> <span class="o">===</span> <span class="s2">&quot;Width&quot;</span> <span class="o">?</span> <span class="p">[</span> <span class="s2">&quot;Left&quot;</span><span class="p">,</span> <span class="s2">&quot;Right&quot;</span> <span class="p">]</span> <span class="o">:</span> <span class="p">[</span> <span class="s2">&quot;Top&quot;</span><span class="p">,</span> <span class="s2">&quot;Bottom&quot;</span> <span class="p">],</span></div><div class='line' id='LC148'>			<span class="nx">type</span> <span class="o">=</span> <span class="nx">name</span><span class="p">.</span><span class="nx">toLowerCase</span><span class="p">(),</span></div><div class='line' id='LC149'>			<span class="nx">orig</span> <span class="o">=</span> <span class="p">{</span></div><div class='line' id='LC150'>				<span class="nx">innerWidth</span><span class="o">:</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">innerWidth</span><span class="p">,</span></div><div class='line' id='LC151'>				<span class="nx">innerHeight</span><span class="o">:</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">innerHeight</span><span class="p">,</span></div><div class='line' id='LC152'>				<span class="nx">outerWidth</span><span class="o">:</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">outerWidth</span><span class="p">,</span></div><div class='line' id='LC153'>				<span class="nx">outerHeight</span><span class="o">:</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">outerHeight</span></div><div class='line' id='LC154'>			<span class="p">};</span></div><div class='line' id='LC155'><br/></div><div class='line' id='LC156'>		<span class="kd">function</span> <span class="nx">reduce</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="nx">size</span><span class="p">,</span> <span class="nx">border</span><span class="p">,</span> <span class="nx">margin</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC157'>			<span class="nx">$</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span> <span class="nx">side</span><span class="p">,</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC158'>				<span class="nx">size</span> <span class="o">-=</span> <span class="nb">parseFloat</span><span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="s2">&quot;padding&quot;</span> <span class="o">+</span> <span class="k">this</span> <span class="p">)</span> <span class="p">)</span> <span class="o">||</span> <span class="mi">0</span><span class="p">;</span></div><div class='line' id='LC159'>				<span class="k">if</span> <span class="p">(</span> <span class="nx">border</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC160'>					<span class="nx">size</span> <span class="o">-=</span> <span class="nb">parseFloat</span><span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="s2">&quot;border&quot;</span> <span class="o">+</span> <span class="k">this</span> <span class="o">+</span> <span class="s2">&quot;Width&quot;</span> <span class="p">)</span> <span class="p">)</span> <span class="o">||</span> <span class="mi">0</span><span class="p">;</span></div><div class='line' id='LC161'>				<span class="p">}</span></div><div class='line' id='LC162'>				<span class="k">if</span> <span class="p">(</span> <span class="nx">margin</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC163'>					<span class="nx">size</span> <span class="o">-=</span> <span class="nb">parseFloat</span><span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="nx">elem</span><span class="p">,</span> <span class="s2">&quot;margin&quot;</span> <span class="o">+</span> <span class="k">this</span> <span class="p">)</span> <span class="p">)</span> <span class="o">||</span> <span class="mi">0</span><span class="p">;</span></div><div class='line' id='LC164'>				<span class="p">}</span></div><div class='line' id='LC165'>			<span class="p">});</span></div><div class='line' id='LC166'>			<span class="k">return</span> <span class="nx">size</span><span class="p">;</span></div><div class='line' id='LC167'>		<span class="p">}</span></div><div class='line' id='LC168'><br/></div><div class='line' id='LC169'>		<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">[</span> <span class="s2">&quot;inner&quot;</span> <span class="o">+</span> <span class="nx">name</span> <span class="p">]</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">size</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC170'>			<span class="k">if</span> <span class="p">(</span> <span class="nx">size</span> <span class="o">===</span> <span class="kc">undefined</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC171'>				<span class="k">return</span> <span class="nx">orig</span><span class="p">[</span> <span class="s2">&quot;inner&quot;</span> <span class="o">+</span> <span class="nx">name</span> <span class="p">].</span><span class="nx">call</span><span class="p">(</span> <span class="k">this</span> <span class="p">);</span></div><div class='line' id='LC172'>			<span class="p">}</span></div><div class='line' id='LC173'><br/></div><div class='line' id='LC174'>			<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC175'>				<span class="nx">$</span><span class="p">(</span> <span class="k">this</span> <span class="p">).</span><span class="nx">css</span><span class="p">(</span> <span class="nx">type</span><span class="p">,</span> <span class="nx">reduce</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nx">size</span> <span class="p">)</span> <span class="o">+</span> <span class="s2">&quot;px&quot;</span> <span class="p">);</span></div><div class='line' id='LC176'>			<span class="p">});</span></div><div class='line' id='LC177'>		<span class="p">};</span></div><div class='line' id='LC178'><br/></div><div class='line' id='LC179'>		<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">[</span> <span class="s2">&quot;outer&quot;</span> <span class="o">+</span> <span class="nx">name</span><span class="p">]</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">size</span><span class="p">,</span> <span class="nx">margin</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC180'>			<span class="k">if</span> <span class="p">(</span> <span class="k">typeof</span> <span class="nx">size</span> <span class="o">!==</span> <span class="s2">&quot;number&quot;</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC181'>				<span class="k">return</span> <span class="nx">orig</span><span class="p">[</span> <span class="s2">&quot;outer&quot;</span> <span class="o">+</span> <span class="nx">name</span> <span class="p">].</span><span class="nx">call</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nx">size</span> <span class="p">);</span></div><div class='line' id='LC182'>			<span class="p">}</span></div><div class='line' id='LC183'><br/></div><div class='line' id='LC184'>			<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC185'>				<span class="nx">$</span><span class="p">(</span> <span class="k">this</span><span class="p">).</span><span class="nx">css</span><span class="p">(</span> <span class="nx">type</span><span class="p">,</span> <span class="nx">reduce</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nx">size</span><span class="p">,</span> <span class="kc">true</span><span class="p">,</span> <span class="nx">margin</span> <span class="p">)</span> <span class="o">+</span> <span class="s2">&quot;px&quot;</span> <span class="p">);</span></div><div class='line' id='LC186'>			<span class="p">});</span></div><div class='line' id='LC187'>		<span class="p">};</span></div><div class='line' id='LC188'>	<span class="p">});</span></div><div class='line' id='LC189'><span class="p">}</span></div><div class='line' id='LC190'><br/></div><div class='line' id='LC191'><span class="c1">// support: jQuery &lt;1.8</span></div><div class='line' id='LC192'><span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">addBack</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC193'>	<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">addBack</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">selector</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC194'>		<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">add</span><span class="p">(</span> <span class="nx">selector</span> <span class="o">==</span> <span class="kc">null</span> <span class="o">?</span></div><div class='line' id='LC195'>			<span class="k">this</span><span class="p">.</span><span class="nx">prevObject</span> <span class="o">:</span> <span class="k">this</span><span class="p">.</span><span class="nx">prevObject</span><span class="p">.</span><span class="nx">filter</span><span class="p">(</span> <span class="nx">selector</span> <span class="p">)</span></div><div class='line' id='LC196'>		<span class="p">);</span></div><div class='line' id='LC197'>	<span class="p">};</span></div><div class='line' id='LC198'><span class="p">}</span></div><div class='line' id='LC199'><br/></div><div class='line' id='LC200'><span class="c1">// support: jQuery 1.6.1, 1.6.2 (http://bugs.jquery.com/ticket/9413)</span></div><div class='line' id='LC201'><span class="k">if</span> <span class="p">(</span> <span class="nx">$</span><span class="p">(</span> <span class="s2">&quot;&lt;a&gt;&quot;</span> <span class="p">).</span><span class="nx">data</span><span class="p">(</span> <span class="s2">&quot;a-b&quot;</span><span class="p">,</span> <span class="s2">&quot;a&quot;</span> <span class="p">).</span><span class="nx">removeData</span><span class="p">(</span> <span class="s2">&quot;a-b&quot;</span> <span class="p">).</span><span class="nx">data</span><span class="p">(</span> <span class="s2">&quot;a-b&quot;</span> <span class="p">)</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC202'>	<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">removeData</span> <span class="o">=</span> <span class="p">(</span><span class="kd">function</span><span class="p">(</span> <span class="nx">removeData</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC203'>		<span class="k">return</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">key</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC204'>			<span class="k">if</span> <span class="p">(</span> <span class="nx">arguments</span><span class="p">.</span><span class="nx">length</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC205'>				<span class="k">return</span> <span class="nx">removeData</span><span class="p">.</span><span class="nx">call</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nx">$</span><span class="p">.</span><span class="nx">camelCase</span><span class="p">(</span> <span class="nx">key</span> <span class="p">)</span> <span class="p">);</span></div><div class='line' id='LC206'>			<span class="p">}</span> <span class="k">else</span> <span class="p">{</span></div><div class='line' id='LC207'>				<span class="k">return</span> <span class="nx">removeData</span><span class="p">.</span><span class="nx">call</span><span class="p">(</span> <span class="k">this</span> <span class="p">);</span></div><div class='line' id='LC208'>			<span class="p">}</span></div><div class='line' id='LC209'>		<span class="p">};</span></div><div class='line' id='LC210'>	<span class="p">})(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">removeData</span> <span class="p">);</span></div><div class='line' id='LC211'><span class="p">}</span></div><div class='line' id='LC212'><br/></div><div class='line' id='LC213'><br/></div><div class='line' id='LC214'><br/></div><div class='line' id='LC215'><br/></div><div class='line' id='LC216'><br/></div><div class='line' id='LC217'><span class="c1">// deprecated</span></div><div class='line' id='LC218'><span class="nx">$</span><span class="p">.</span><span class="nx">ui</span><span class="p">.</span><span class="nx">ie</span> <span class="o">=</span> <span class="o">!!</span><span class="sr">/msie [\w.]+/</span><span class="p">.</span><span class="nx">exec</span><span class="p">(</span> <span class="nx">navigator</span><span class="p">.</span><span class="nx">userAgent</span><span class="p">.</span><span class="nx">toLowerCase</span><span class="p">()</span> <span class="p">);</span></div><div class='line' id='LC219'><br/></div><div class='line' id='LC220'><span class="nx">$</span><span class="p">.</span><span class="nx">support</span><span class="p">.</span><span class="nx">selectstart</span> <span class="o">=</span> <span class="s2">&quot;onselectstart&quot;</span> <span class="k">in</span> <span class="nb">document</span><span class="p">.</span><span class="nx">createElement</span><span class="p">(</span> <span class="s2">&quot;div&quot;</span> <span class="p">);</span></div><div class='line' id='LC221'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">extend</span><span class="p">({</span></div><div class='line' id='LC222'>	<span class="nx">disableSelection</span><span class="o">:</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC223'>		<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">bind</span><span class="p">(</span> <span class="p">(</span> <span class="nx">$</span><span class="p">.</span><span class="nx">support</span><span class="p">.</span><span class="nx">selectstart</span> <span class="o">?</span> <span class="s2">&quot;selectstart&quot;</span> <span class="o">:</span> <span class="s2">&quot;mousedown&quot;</span> <span class="p">)</span> <span class="o">+</span></div><div class='line' id='LC224'>			<span class="s2">&quot;.ui-disableSelection&quot;</span><span class="p">,</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">event</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC225'>				<span class="nx">event</span><span class="p">.</span><span class="nx">preventDefault</span><span class="p">();</span></div><div class='line' id='LC226'>			<span class="p">});</span></div><div class='line' id='LC227'>	<span class="p">},</span></div><div class='line' id='LC228'><br/></div><div class='line' id='LC229'>	<span class="nx">enableSelection</span><span class="o">:</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC230'>		<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">unbind</span><span class="p">(</span> <span class="s2">&quot;.ui-disableSelection&quot;</span> <span class="p">);</span></div><div class='line' id='LC231'>	<span class="p">},</span></div><div class='line' id='LC232'><br/></div><div class='line' id='LC233'>	<span class="nx">zIndex</span><span class="o">:</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">zIndex</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC234'>		<span class="k">if</span> <span class="p">(</span> <span class="nx">zIndex</span> <span class="o">!==</span> <span class="kc">undefined</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC235'>			<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="s2">&quot;zIndex&quot;</span><span class="p">,</span> <span class="nx">zIndex</span> <span class="p">);</span></div><div class='line' id='LC236'>		<span class="p">}</span></div><div class='line' id='LC237'><br/></div><div class='line' id='LC238'>		<span class="k">if</span> <span class="p">(</span> <span class="k">this</span><span class="p">.</span><span class="nx">length</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC239'>			<span class="kd">var</span> <span class="nx">elem</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span> <span class="k">this</span><span class="p">[</span> <span class="mi">0</span> <span class="p">]</span> <span class="p">),</span> <span class="nx">position</span><span class="p">,</span> <span class="nx">value</span><span class="p">;</span></div><div class='line' id='LC240'>			<span class="k">while</span> <span class="p">(</span> <span class="nx">elem</span><span class="p">.</span><span class="nx">length</span> <span class="o">&amp;&amp;</span> <span class="nx">elem</span><span class="p">[</span> <span class="mi">0</span> <span class="p">]</span> <span class="o">!==</span> <span class="nb">document</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC241'>				<span class="c1">// Ignore z-index if position is set to a value where z-index is ignored by the browser</span></div><div class='line' id='LC242'>				<span class="c1">// This makes behavior of this function consistent across browsers</span></div><div class='line' id='LC243'>				<span class="c1">// WebKit always returns auto if the element is positioned</span></div><div class='line' id='LC244'>				<span class="nx">position</span> <span class="o">=</span> <span class="nx">elem</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="s2">&quot;position&quot;</span> <span class="p">);</span></div><div class='line' id='LC245'>				<span class="k">if</span> <span class="p">(</span> <span class="nx">position</span> <span class="o">===</span> <span class="s2">&quot;absolute&quot;</span> <span class="o">||</span> <span class="nx">position</span> <span class="o">===</span> <span class="s2">&quot;relative&quot;</span> <span class="o">||</span> <span class="nx">position</span> <span class="o">===</span> <span class="s2">&quot;fixed&quot;</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC246'>					<span class="c1">// IE returns 0 when zIndex is not specified</span></div><div class='line' id='LC247'>					<span class="c1">// other browsers return a string</span></div><div class='line' id='LC248'>					<span class="c1">// we ignore the case of nested elements with an explicit value of 0</span></div><div class='line' id='LC249'>					<span class="c1">// &lt;div style=&quot;z-index: -10;&quot;&gt;&lt;div style=&quot;z-index: 0;&quot;&gt;&lt;/div&gt;&lt;/div&gt;</span></div><div class='line' id='LC250'>					<span class="nx">value</span> <span class="o">=</span> <span class="nb">parseInt</span><span class="p">(</span> <span class="nx">elem</span><span class="p">.</span><span class="nx">css</span><span class="p">(</span> <span class="s2">&quot;zIndex&quot;</span> <span class="p">),</span> <span class="mi">10</span> <span class="p">);</span></div><div class='line' id='LC251'>					<span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="nb">isNaN</span><span class="p">(</span> <span class="nx">value</span> <span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="nx">value</span> <span class="o">!==</span> <span class="mi">0</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC252'>						<span class="k">return</span> <span class="nx">value</span><span class="p">;</span></div><div class='line' id='LC253'>					<span class="p">}</span></div><div class='line' id='LC254'>				<span class="p">}</span></div><div class='line' id='LC255'>				<span class="nx">elem</span> <span class="o">=</span> <span class="nx">elem</span><span class="p">.</span><span class="nx">parent</span><span class="p">();</span></div><div class='line' id='LC256'>			<span class="p">}</span></div><div class='line' id='LC257'>		<span class="p">}</span></div><div class='line' id='LC258'><br/></div><div class='line' id='LC259'>		<span class="k">return</span> <span class="mi">0</span><span class="p">;</span></div><div class='line' id='LC260'>	<span class="p">}</span></div><div class='line' id='LC261'><span class="p">});</span></div><div class='line' id='LC262'><br/></div><div class='line' id='LC263'><span class="c1">// $.ui.plugin is deprecated. Use $.widget() extensions instead.</span></div><div class='line' id='LC264'><span class="nx">$</span><span class="p">.</span><span class="nx">ui</span><span class="p">.</span><span class="nx">plugin</span> <span class="o">=</span> <span class="p">{</span></div><div class='line' id='LC265'>	<span class="nx">add</span><span class="o">:</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">module</span><span class="p">,</span> <span class="nx">option</span><span class="p">,</span> <span class="nx">set</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC266'>		<span class="kd">var</span> <span class="nx">i</span><span class="p">,</span></div><div class='line' id='LC267'>			<span class="nx">proto</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">ui</span><span class="p">[</span> <span class="nx">module</span> <span class="p">].</span><span class="nx">prototype</span><span class="p">;</span></div><div class='line' id='LC268'>		<span class="k">for</span> <span class="p">(</span> <span class="nx">i</span> <span class="k">in</span> <span class="nx">set</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC269'>			<span class="nx">proto</span><span class="p">.</span><span class="nx">plugins</span><span class="p">[</span> <span class="nx">i</span> <span class="p">]</span> <span class="o">=</span> <span class="nx">proto</span><span class="p">.</span><span class="nx">plugins</span><span class="p">[</span> <span class="nx">i</span> <span class="p">]</span> <span class="o">||</span> <span class="p">[];</span></div><div class='line' id='LC270'>			<span class="nx">proto</span><span class="p">.</span><span class="nx">plugins</span><span class="p">[</span> <span class="nx">i</span> <span class="p">].</span><span class="nx">push</span><span class="p">(</span> <span class="p">[</span> <span class="nx">option</span><span class="p">,</span> <span class="nx">set</span><span class="p">[</span> <span class="nx">i</span> <span class="p">]</span> <span class="p">]</span> <span class="p">);</span></div><div class='line' id='LC271'>		<span class="p">}</span></div><div class='line' id='LC272'>	<span class="p">},</span></div><div class='line' id='LC273'>	<span class="nx">call</span><span class="o">:</span> <span class="kd">function</span><span class="p">(</span> <span class="nx">instance</span><span class="p">,</span> <span class="nx">name</span><span class="p">,</span> <span class="nx">args</span><span class="p">,</span> <span class="nx">allowDisconnected</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC274'>		<span class="kd">var</span> <span class="nx">i</span><span class="p">,</span></div><div class='line' id='LC275'>			<span class="nx">set</span> <span class="o">=</span> <span class="nx">instance</span><span class="p">.</span><span class="nx">plugins</span><span class="p">[</span> <span class="nx">name</span> <span class="p">];</span></div><div class='line' id='LC276'><br/></div><div class='line' id='LC277'>		<span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="nx">set</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC278'>			<span class="k">return</span><span class="p">;</span></div><div class='line' id='LC279'>		<span class="p">}</span></div><div class='line' id='LC280'><br/></div><div class='line' id='LC281'>		<span class="k">if</span> <span class="p">(</span> <span class="o">!</span><span class="nx">allowDisconnected</span> <span class="o">&amp;&amp;</span> <span class="p">(</span> <span class="o">!</span><span class="nx">instance</span><span class="p">.</span><span class="nx">element</span><span class="p">[</span> <span class="mi">0</span> <span class="p">].</span><span class="nx">parentNode</span> <span class="o">||</span> <span class="nx">instance</span><span class="p">.</span><span class="nx">element</span><span class="p">[</span> <span class="mi">0</span> <span class="p">].</span><span class="nx">parentNode</span><span class="p">.</span><span class="nx">nodeType</span> <span class="o">===</span> <span class="mi">11</span> <span class="p">)</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC282'>			<span class="k">return</span><span class="p">;</span></div><div class='line' id='LC283'>		<span class="p">}</span></div><div class='line' id='LC284'><br/></div><div class='line' id='LC285'>		<span class="k">for</span> <span class="p">(</span> <span class="nx">i</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nx">i</span> <span class="o">&lt;</span> <span class="nx">set</span><span class="p">.</span><span class="nx">length</span><span class="p">;</span> <span class="nx">i</span><span class="o">++</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC286'>			<span class="k">if</span> <span class="p">(</span> <span class="nx">instance</span><span class="p">.</span><span class="nx">options</span><span class="p">[</span> <span class="nx">set</span><span class="p">[</span> <span class="nx">i</span> <span class="p">][</span> <span class="mi">0</span> <span class="p">]</span> <span class="p">]</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC287'>				<span class="nx">set</span><span class="p">[</span> <span class="nx">i</span> <span class="p">][</span> <span class="mi">1</span> <span class="p">].</span><span class="nx">apply</span><span class="p">(</span> <span class="nx">instance</span><span class="p">.</span><span class="nx">element</span><span class="p">,</span> <span class="nx">args</span> <span class="p">);</span></div><div class='line' id='LC288'>			<span class="p">}</span></div><div class='line' id='LC289'>		<span class="p">}</span></div><div class='line' id='LC290'>	<span class="p">}</span></div><div class='line' id='LC291'><span class="p">};</span></div><div class='line' id='LC292'><br/></div><div class='line' id='LC293'><span class="p">})(</span> <span class="nx">jQuery</span> <span class="p">);</span></div></pre></div>
            </td>
          </tr>
        </table>
  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2013 <span title="0.02272s from github-fe128-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
          <div class="suggester-container">
              <div class="suggester fullscreen-suggester js-navigation-container" id="fullscreen_suggester"
                 data-url="/jquery/jquery-ui/suggestions/commit">
              </div>
          </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

  </body>
</html>

